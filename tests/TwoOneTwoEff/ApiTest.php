<?php

namespace Tests\TwoOneTwoEff;

use Tests\TestCase;
use OnTheMove\Models\Agent;
use OnTheMove\TwoOneTwoEff\RewardsApi;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    public $api;
    public $agent;

    // public function setUp()
    // {
    //     parent::setUp();
    //     $this->agent = Agent::inRandomOrder()->first();
    //     $this->api = new RewardsApi($this->agent->agent_id);
    // }

    /** @test */
    public function it_can_retrive_property_managers_rewards_balance_points()
    {
        $api = new RewardsApi(143290);
        
        //$response = $this->api->getPointsBalance();
        $response = $api->getPointsBalance();
        
        $this->assertTrue($response['status']);
        $this->assertInternalType('integer', $response['property_manager_balance'], 'PM balance was not an integer');
        $this->assertNull($response['real_estate_agency_balance'], 'REA balance was not null for a PM without access to REA balance');
    }

    /** @test */
    public function it_can_retrive_real_estate_agencies_rewards_balance_points()
    {
        $api = new RewardsApi(147006);
        
        //$response = $this->api->getPointsBalance();
        $response = $api->getPointsBalance();
        //  dump($response);
        $this->assertTrue($response['status']);
        $this->assertInternalType('integer', $response['property_manager_balance'], 'PM balance was not an integer');
        $this->assertInternalType('integer', $response['real_estate_agency_balance'], 'REA balance was not an integer');
    }


    /** @test */
    public function it_can_retrive_property_managers_points_history()
    {
        $api = new RewardsApi(143290);
        
        //$response = $this->api->getPointsBalance();
        $response = $api->getPointsHistory(200);
      
        $this->assertTrue($response['status']);
        
        $this->assertInternalType('integer', $response['property_manager']['total_points_accrued'], 'PM balance was not an integer');

        $this->assertArrayHasKey("reference", $response['property_manager']['history'][0], 'History array is missing: reference');
        $this->assertArrayHasKey("connection_date", $response['property_manager']['history'][0], 'History array is missing: connection_date');
        $this->assertArrayHasKey("sale_date", $response['property_manager']['history'][0], 'History array is missing: sale_date');
        $this->assertArrayHasKey("state", $response['property_manager']['history'][0], 'History array is missing: state');
        $this->assertArrayHasKey("category", $response['property_manager']['history'][0], 'History array is missing: category');
        $this->assertArrayHasKey("retailer", $response['property_manager']['history'][0], 'History array is missing: retailer');
        $this->assertArrayHasKey("points", $response['property_manager']['history'][0], 'History array is missing: points');

        $this->assertInternalType('integer', $response['property_manager']['history'][0]['points'], 'Points value was not an integer');

        $this->assertTrue($this->validateDateTime($response['property_manager']['history'][0]['connection_date'], 'd/m/Y'), 'Connection Date format was not d/m/Y');
        $this->assertTrue($this->validateDateTime($response['property_manager']['history'][0]['sale_date'], 'Y-m-d'), 'Sale Date format was not Y-m-d');
    
        $this->assertNull($response['real_estate_agency'], 'REA history was not null for a PM without access to REA history');
    }

    /** @test */
    public function it_can_retrive_real_estate_agency_points_history()
    {
        $api = new RewardsApi(147006);
        
        //$response = $this->api->getPointsBalance();
        $response = $api->getPointsHistory(200);
        //dd($response);
        $this->assertTrue($response['status']);

        $this->assertInternalType('integer', $response['real_estate_agency']['total_points_accrued'], 'PM balance was not an integer');

        $this->assertArrayHasKey("reference", $response['real_estate_agency']['history'][0], 'History array is missing: reference');
        $this->assertArrayHasKey("connection_date", $response['real_estate_agency']['history'][0], 'History array is missing: connection_date');
        $this->assertArrayHasKey("sale_date", $response['real_estate_agency']['history'][0], 'History array is missing: sale_date');
        $this->assertArrayHasKey("state", $response['real_estate_agency']['history'][0], 'History array is missing: state');
        $this->assertArrayHasKey("category", $response['real_estate_agency']['history'][0], 'History array is missing: category');
        $this->assertArrayHasKey("retailer", $response['real_estate_agency']['history'][0], 'History array is missing: retailer');
        $this->assertArrayHasKey("points", $response['real_estate_agency']['history'][0], 'History array is missing: points');

        $this->assertInternalType('integer', $response['real_estate_agency']['history'][0]['points'], 'Points value was not an integer');

        $this->assertTrue($this->validateDateTime($response['real_estate_agency']['history'][0]['connection_date'], 'd/m/Y'), 'Connection Date format was not d/m/Y');
        $this->assertTrue($this->validateDateTime($response['real_estate_agency']['history'][0]['sale_date'], 'Y-m-d'), 'Sale Date format was not Y-m-d');
    
        // $this->assertNull($response['real_estate_agency'], 'REA history was not null for a PM without access to REA history');
    }

    protected function validateDateTime($dateStr, $format)
    {   
        $date = \DateTime::createFromFormat($format, $dateStr);

        return $date && ($date->format($format) === $dateStr);
    }
}
