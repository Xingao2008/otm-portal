<?php

namespace Tests\Flowbiz;

use Auth;
use File;
use Carbon\Carbon;

use Tests\TestCase;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;

use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;

use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;
use Illuminate\Foundation\Testing\WithFaker;
use OnTheMove\Repositories\ApplicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use OnTheMove\Transformers\ApplicationTransformer;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use OnTheMove\Repositories\FlowbizApplicationRepository;

class FlowbizApplicationRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    protected $repository;
    protected $flowbiz;

    public function setUp()
    {
        parent::setUp();
        if (env('FLOWBIZ_ENV') === 'production') {
            $this->markTestSkipped('IN PRODUCTION');
        }
        $this->repository = new FlowbizApplicationRepository();
        $this->flowbiz = new Api(60);
    }

    /** @test */
    public function import_app_from_flowbiz()
    {
        //$this->markTestSkipped('Need manual intervention');

        $now = Carbon::today();
        $searchParams = [
            'ToDate' => $now->format('Y-m-d'),
            'FromDate' => $now->subYear(4)->format('Y-m-d'),
        ];
        
        // Find an random app.
        $search = $this->flowbiz->searchApplications($searchParams);

        for ($i = 0; $i < 25; $i++) {
            $randomResult = rand(0, count($search['Results'])-1);
            $searchApp = $search['Results'][$randomResult];
            
            echo "App: " . $searchApp["Reference"] . " ";

            $app = $this->repository->retreiveFromFlowbiz($searchApp["Reference"]);
            
            $this->assertSame($app->flowbiz_application_id, $searchApp["Reference"], "Details are for a different App");
        }
        //dump($app->toArray());
    }


    /** @test */
    public function update_app_from_flowbiz()
    {
        //$this->markTestSkipped('Need manual intervention');
        
        // Setup


        // Remove know user data
        Customer::whereMobile('0400301235')->delete();
        $customers = Customer::whereMobile('0400301235')->get();
        $this->assertCount(0, $customers);
        
        // Get FE data 
        $data = json_decode(File::get('tests/data/frontend_data.json'), true);
        $appRepo = new ApplicationRepository();
        
        // Submit to Portal
        $app = $appRepo->store($data);
        
        $this->assertCount(1, $app->customers);
        $customers = Customer::whereMobile('0400301235')->get();
        $this->assertCount(1, $customers);
        $this->assertCount(2, $app->customers->first()->identifications);

        // Export to FB
        $app = $appRepo->export($app);
     
        $before = $app->toArray();
        //dd($before);
        // Act
        $updatedData = json_decode(File::get('tests/data/flowbiz_app_update_data.json'), true);
        $updatedData['Results']['Application']['ApplicationReference'] = $before['flowbiz_application_id'];

        $updatedApp = $this->repository->updateFromFlowbiz($app, $updatedData['Results']);

        $updatedApp = Application::find($updatedApp->id);
        $after = $updatedApp->toArray();

        // Assert

       
        // @TODO
        //dd($before['customers'][0]['identifications'], $after['customers'][0]['identifications']);
        $this->assertNotEquals($before['customers'][0]['identifications'][1]['type']['issuer'], $after['customers'][0]['identifications'][1]['type']['issuer']);


        $this->assertNotEquals($before['suburb'], $after['suburb']);
        $this->assertNotEquals($before['street_number'], $after['street_number']);
        $this->assertNotEquals($before['flowbiz_application_status'], $after['flowbiz_application_status']);
        $this->assertNotEquals($before['customers'][0]['title'], $after['customers'][0]['title']);
        $this->assertNotEquals($before['customers'][0]['dob'], $after['customers'][0]['dob']);

        
        
        $this->assertNotEquals($before['real_estate_agent']['agency'], $after['real_estate_agent']['agency']);
    
        $this->assertNotEquals($before['services'], $after['services']);

        $countAfter = Customer::whereMobile('0400301235')->get()->count();
        $this->assertEquals($countAfter, 1);
        //$app = $this->repository->retrieveFromFlowbiz($app->flowbiz_application_id);

       // dd($this->arrayDiffAssocMultidimensional($before, $after));


        // import, find or create an app
        // stub a API response
        // call  FlowbizApplicationRepository->updateFromFlowbiz(Application $app, $details['Results']);

        // assert stubbed changes applied.
        // status changed
        // customer changes?
        // address changes?
        // agent changes
        // products sold
    
        // $now = Carbon::today();
        // $searchParams = [
        //     'ToDate' => $now->format('Y-m-d'),
        //     'FromDate' => $now->subYear(4)->format('Y-m-d'),
        // ];
        
        // // Find an random app.
        // $search = $this->flowbiz->searchApplications($searchParams);
        // $randomResult = rand(0, count($search['Results'])-1);
        // $searchApp = $search['Results'][$randomResult];
        
        // echo "App: " . $searchApp["Reference"] . " ";

        
        
        // $this->assertSame($app->flowbiz_application_id, $searchApp["Reference"], "Details are for a different App");
        
        //$app = $this->repository->retrieveFromFlowbiz("A0011970");
    }

    
private function arrayDiffAssocMultidimensional(array $array1, array $array2): array
{
    $difference = [];
    foreach ($array1 as $key => $value) {
        if (is_array($value)) {
            if (! array_key_exists($key, $array2)) {
                $difference[$key] = $value;
            } elseif (! is_array($array2[$key])) {
                $difference[$key] = $value;
            } else {
                $multidimensionalDiff = $this->arrayDiffAssocMultidimensional($value, $array2[$key]);
                if (count($multidimensionalDiff) > 0) {
                    $difference[$key] = $multidimensionalDiff;
                }
            }
        } else {
            if (! array_key_exists($key, $array2) || $array2[$key] !== $value) {
                $difference[$key] = $value;
            }
        }
    }

    return $difference;
}

}
