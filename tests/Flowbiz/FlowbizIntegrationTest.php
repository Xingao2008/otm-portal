<?php

namespace Tests\Flowbiz;

use Tests\TestCase;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;

use Illuminate\Foundation\Testing\WithFaker;

use OnTheMove\Repositories\ApplicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class FlowbizIntegrationTest extends TestCase
{
    //use DatabaseTransactions;

    protected $flowbiz;
    protected $repository;
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        
        if (env('FLOWBIZ_ENV') === 'production') {
            $this->markTestSkipped('IN PRODUCTION');
        }

        $this->repository = new ApplicationRepository();
        $this->flowbiz = new Api(60);
    }


    /** @test */
    public function an_applications_details_can_be_retrieved_from_flowbiz_for_a_generated_app()
    {   
        // $this->markTestIncomplete("API incomplete");

        $app = $this->scaffoldApp('Normal');
        $services = [
            factory(Service::class, 'power')->create(),
            factory(Service::class, 'gas')->create(),
            factory(Service::class, 'water')->create(),
           // factory(Service::class, 'internet')->create(),
        ];
        $app->services()->saveMany($services);
        $resp = $this->repository->export($app, 120);

        $this->assertEquals($resp->flowbiz_exported, 1, "Application was not exported to flowbiz");
     
        echo "App ID: " . $resp->flowbiz_application_id ." ";

        $details = $this->flowbiz->getApplicationDetails($resp->flowbiz_application_id);
      
        //dd($details, $resp->toArray());
        //dump($details['Results']);

        $this->assertSame($details['Results']['Application']['ApplicationReference'], $resp->flowbiz_application_id, "Different Reference");
        //$this->assertEquals(count($details['Results']['Products']), count($services), "Different number of products");
    }



    protected function scaffoldApp($appType = 'Normal')
    {
        $customer = factory(Customer::class)->create();

        $customer->identifications()->saveMany([
            factory(CustomerId::class, 'concession')->create([
                'customer_id' => $customer->id,
            ]),
            factory(CustomerId::class, 'identification')->create([
                'customer_id' => $customer->id,
            ]),
        ]);

        $app = factory(Application::class)->create([
            "agent_id" => 3289,
            "primary_customer_id" => $customer->id,
            "application_type_id" => function () use ($appType) {
                return ApplicationType::whereType($appType)->first()->id;
            },
            "renter" => function () use ($appType) {
                return ($appType === 'Landlord Temp') ? false : true;
            },
        ]);

        $app->customers()->attach($customer);

        return $app;
    }
}
