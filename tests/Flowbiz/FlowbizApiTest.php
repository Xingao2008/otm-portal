<?php

namespace Tests\Flowbiz;

use Auth;
use File;

use Carbon\Carbon;

use Tests\TestCase;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;

use Illuminate\Foundation\Testing\WithFaker;

use OnTheMove\Repositories\ApplicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

use OnTheMove\Transformers\ApplicationTransformer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FlowbizApiTest extends TestCase
{
    use DatabaseTransactions;

    protected $flowbiz;
    protected $repository;
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        
        $this->repository = new ApplicationRepository();
        $this->transformer = new ApplicationTransformer();
        $this->flowbiz = new Api(60);
    }

    /** @test */
    public function an_full_application_can_be_submitted_to_flowbiz()
    {
        
        //$this->markTestSkipped();
        $exampleAppData = json_decode(File::get('tests/data/flowbiz_api_full_example_generated.json'));
        //$exampleAppData = json_decode(File::get('tests/data/flowbiz_api_full_example.json'));
        $response = $this->flowbiz->submitApplication($exampleAppData);

    
        $this->assertRegExp("/^A\d{7}$/", $response['Results']['Reference'], "Result Reference (Application ID), does not match expected format");
        $this->assertEquals($response['Results']['Status'], "Accepted", "Result Status does not match 'Accepted'");

        echo " Full: ". $response['Results']['Reference'] . " ";
    }

    /** @test */
    public function an_mimimal_application_can_be_submitted_to_flowbiz()
    {
        
        //$this->markTestSkipped();
        $exampleAppData = json_decode(File::get('tests/data/flowbiz_api_minimum_example.json'));
        $response = $this->flowbiz->submitApplication($exampleAppData);
  
        $this->assertRegExp("/^A\d{7}$/", $response['Results']['Reference'], "Result Reference (Application ID), does not match expected format");
        $this->assertEquals($response['Results']['Status'], "Partial Sale", "Result Status does not match 'Partial Sale'");

        echo " Partial: ". $response['Results']['Reference'] . " ";
    }

    /** @test */
    public function flowbiz_can_provide_an_agency_list()
    {
        //$this->markTestSkipped();
        $response = $this->flowbiz->getAgencyList();
       // dump($response['Results'][22]);
        $this->assertEquals($response['Results'][22], [
            "CompanyID" => 141139,
            "CompanyName" => "(UV Student Apartments) Colliers International MAWSON LAKES",
            "CompanyType" => "Student Accomodation",
            "Group" => "Colliers",
            "ABN" => "",
            "GSTRegistered" => true,
            "Ranking" => "A - ACTIVE",
            "BusinessPhone" => "08 8305 8888",
            "Address" => [
                "Address1" => "13 YATES STREET",
                "Address2" => "",
                "Suburb" => "MAWSON LAKES",
                "Postcode" => "5095",
                "State" => "SA",
                "DPID" => "",
            ],
        ]);
    }

    /** @test */
    public function flowbiz_returns_property_managers_for_an_agency()
    {
        //$this->markTestSkipped();
        $response = $this->flowbiz->getPropertyManagers(139505);

        $this->assertEquals($response['Results'][7], [
            "ID" => 145056,
            "UserID" => 163097,
            "Title" => "Mr",
            "Firstname" => "Chris",
            "Lastname" => "Zondanos",
            "Mobile" => "",
            "BusinessPhone" => "",
            "Email" => "czondanos@onthemove.com.au",
            "Active" => true,
            "UserAccount" => "test",
            "DefaultPropertyManager" => false,
            "Agency" => "OTM",
        ]);
    }


    /** @test */
    public function flowbiz_applications_can_be_searched_for()
    {
        //$this->markTestSkipped();

        $now = Carbon::today();
        $ago = Carbon::today()->subYear();

        $searchParams = [
            'ToDate' => $now->format('Y-m-d'),
            'FromDate' => $ago->format('Y-m-d'),
        ];
        
        $results = $this->flowbiz->searchApplications($searchParams);
        $randomResult = rand(0, count($results['Results'])-1);

        // Are all results within the correct date range.
        $firstConDate = Carbon::createFromFormat('Y-m-d', $results['Results'][0]["ConnectionDate"]);
        $lastConDate = Carbon::createFromFormat('Y-m-d', $results['Results'][$randomResult]["ConnectionDate"]);

        // First App is in date range requested
        $this->assertTrue($now->gte($firstConDate), "First App isn't in date range requested");
        $this->assertTrue($ago->lte($firstConDate), "First App isn't in date range requested");

        // Random App is in date range requested
        $this->assertTrue($now->gte($lastConDate), "Random App isn't in date range requested");
        $this->assertTrue($ago->lte($lastConDate), "Random App isn't in date range requested");


        /*
         * Search for apps by an particular Agent
         */
        
        $searchParams["AgentId"] = (string) $results['Results'][$randomResult]["AgentId"];
        $searchParams["View"] = "PM";
        $searchParams["ApplicationType"] = "All";

        $refindedResults = $this->flowbiz->searchApplications($searchParams);
        $randomResult = rand(0, count($refindedResults['Results'])-1);

        $this->assertSame($refindedResults['Results'][0]["AgentId"], (int) $searchParams["AgentId"], "First app isn't from agent requested");
        $this->assertSame($refindedResults['Results'][$randomResult]["AgentId"], (int) $searchParams["AgentId"], "Random app isn't from agent requested");


        /*
         * Search for apps by an all agents in an agency
         */
        $searchParams["View"] = "REA";
        $searchParams["AgentId"] = "145760";
        $refindedResults = $this->flowbiz->searchApplications($searchParams);

        $company = Agent::whereAgentId($searchParams["AgentId"])->firstOrFail();

        foreach($refindedResults['Results'] as $result) {
            
            // skip the same agent
            if (isset($agent)) {
                if ($agent->agent_id === $result["AgentId"]) {
                    continue;
                }
            }
            $agent = Agent::whereAgentId($result["AgentId"])->first();
       
           $this->assertSame($agent->company_id, $company->company_id, "Agent is from a different real estate agency");
        } 


        /*
         * Search for apps by an Customer
         */

        // remove agent requirements
        unset($searchParams["AgentId"], $searchParams["View"], $searchParams["ApplicationType"]);

        list($firstName, $lastName) = array_pad(explode(' ', trim($results['Results'][0]["Customer"])), 2, null);

        $searchParams["Firstname"] = $firstName;
        $searchParams["Lastname"] = $lastName;

        $namedResults = $this->flowbiz->searchApplications($searchParams);
        $randomResult = rand(0, count($namedResults['Results'])-1);
        // dd($namedResults);

        $this->assertSame($namedResults['Results'][0]["Customer"], $results['Results'][0]["Customer"], "First app isn't from customer requested");
        $this->assertSame($namedResults['Results'][$randomResult]["Customer"], $results['Results'][0]["Customer"], "Random app isn't from customer requested");


        /*
         * Search for apps by an Application Reference
         */

        // remove name
        unset($searchParams["Firstname"], $searchParams["Lastname"], $searchParams["ApplicationType"]);

        $searchParams["Reference"] = $results['Results'][0]["Reference"];

        $specificAppResult = $this->flowbiz->searchApplications($searchParams);
        
        $this->assertSame(count($specificAppResult['Results']), 1, "More than 1 app returned");
        $this->assertSame($specificAppResult['Results'][0]["Reference"], $searchParams["Reference"], "App isn't the same reference as requested");

        unset($searchParams["FromDate"], $searchParams["ToDate"]);

        $specificAppResult2 = $this->flowbiz->searchApplications($searchParams);
        
        $this->assertSame(count($specificAppResult2['Results']), 1, "More than 1 app returned");
        $this->assertSame($specificAppResult2['Results'][0]["Reference"], $searchParams["Reference"], "App isn't the same reference as requested");
    }

    /** @test */
    public function an_applications_details_can_be_retrieved_from_flowbiz()
    {
        //$this->markTestSkipped();


        $now = Carbon::today();
        $searchParams = [
            'ToDate' => $now->format('Y-m-d'),
            'FromDate' => $now->subYear()->format('Y-m-d'),
        ];
        
        // Find an random app.
        $search = $this->flowbiz->searchApplications($searchParams);

        $randomResult = rand(0, count($search['Results'])-1);
        $searchApp = $search['Results'][$randomResult];
        list($firstName, $lastName) = array_pad(explode(' ', trim($searchApp["Customer"])), 2, null);

        // Get it's details
        $response = $this->flowbiz->getApplicationDetails($searchApp["Reference"]);

        //dump($response);
        echo $searchApp["Reference"];
        // Are they the same
        $this->assertSame($response['Results']['Application']['ApplicationReference'], $searchApp["Reference"], "Details are for a different App");
        $this->assertSame($response['Results']['Application']['ApplicationStatus'], $searchApp["Status"], "Application status differs");
        
        $this->assertSame($response['Results']['Application']['PropertyManagerID'], $searchApp["AgentId"], "Agent / PM Id differs");

        $this->assertSame($response['Results']['Application']['ConnectionDate'], $searchApp["ConnectionDate"], "Different connection date");

        $this->assertSame(trim($response['Results']['Applicants'][0]['Firstname']), $firstName, "Different first name");
        $this->assertSame(trim($response['Results']['Applicants'][0]['Lastname']), $lastName, "Different last name");

        $this->assertSame($response['Results']['Address']['Postcode'], substr($searchApp["Address"], -4), "Different postcode");
    }
}
