<?php

namespace Tests\Flowbiz;

use Auth;
use File;
use Tests\TestCase;
use OnTheMove\Models\Agent;

use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;

use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;
use Illuminate\Foundation\Testing\WithFaker;
use OnTheMove\Repositories\ApplicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

use OnTheMove\Transformers\ApplicationTransformer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTransformerTest extends TestCase
{
    use DatabaseTransactions;

    protected $repository;
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        if(env('FLOWBIZ_ENV') === 'production') {
            $this->markTestSkipped('IN PRODUCTION');  
        }
        $this->repository = new ApplicationRepository();
        $this->transformer = new ApplicationTransformer();
    }



    /** @test */
    public function an_application_can_be_build_from_many_models()
    {
        $app = $this->scaffoldApp();

        $data = $this->transformer->buildFlowbizDataArray($app);

        // Test a few items
        $this->assertContains($app->connection_date->format('Y-m-d'), $data['Application'], "Connection Date not provided");
        $this->assertContains($app->postcode, $data['Address'], "Postcode not provided");
        $this->assertContains($app->customers()->first()->firstname, $data['Applicants'][0], "1st Customer name");


        // $this->assertJsonStructure([
            //     "Application" => [
            //         "ConnectionDate", "ApplicationType", "Renter", "Notes"
            //     ],
            //     "Address" => [
            //         "PropertyType", "StreetNumber", "StreetName", "UnitNumber", "Suburb", "Postcode", "State", "DPID"
            //     ],
            //     "Applicants" => [
            //         [
            //             "Title", "Firstname", "Lastname", "DOB", "Email", "Mobile", "Phone", "Primary", "Occupation", "Notes",
            //             "Identifications" => [[
            //                 "IdType","Number","Expiry","Issuer",
            //             ]]
            //     ]],
            //     "Products" => [
            //         []
            //     ]
            // ], $jsonData);
    }


    /** @test */
    public function an_application_can_be_built_and_sent_to_flowbiz()
    {
        
        //$this->markTestSkipped();
        $app = $this->scaffoldApp('Normal');
        $app->services()->saveMany([
            factory(Service::class, 'power')->create(),
            factory(Service::class, 'gas')->create(),
            factory(Service::class, 'water')->create(),
           // factory(Service::class, 'internet')->create(),
        ]);
        $resp = $this->repository->export($app);

        $this->assertEquals($resp->flowbiz_exported, 1);
        $this->assertDatabaseHas('applications', [
            'flowbiz_application_id' => $resp->flowbiz_application_id,
        ]);
        echo "Normal: " . $resp->flowbiz_application_id ." ";
    }

    /** @test */
    public function a_temp_app_can_be_built_and_sent_to_flowbiz()
    {
        
        //$this->markTestSkipped();
        $app = $this->scaffoldApp('Agent');
        $app->services()->saveMany([
            factory(Service::class, 'power')->create(),
        ]);
        $resp = $this->repository->export($app);

        $this->assertEquals($resp->flowbiz_exported, 1);
        $this->assertDatabaseHas('applications', [
            'flowbiz_application_id' => $resp->flowbiz_application_id,
        ]);
        echo "Agent: " . $resp->flowbiz_application_id ." ";
    }

    /** @test */
    public function a_landlord_temp_app_can_be_built_and_sent_to_flowbiz()
    {

        ////$this->markTestSkipped();
        $app = $this->scaffoldApp('Landlord Temp');
        $app->services()->saveMany([
            factory(Service::class, 'power')->create(),
        ]);
        $resp = $this->repository->export($app);

        $this->assertEquals($resp->flowbiz_exported, 1);
        $this->assertDatabaseHas('applications', [
            'flowbiz_application_id' => $resp->flowbiz_application_id,
        ]);
        echo "Landlord Temp: " . $resp->flowbiz_application_id ." ";
    }

    protected function scaffoldApp($appType = 'Normal')
    {
        $customer = factory(Customer::class)->create();

        $customer->identifications()->saveMany([
            factory(CustomerId::class, 'concession')->create([
                'customer_id' => $customer->id,
            ]),
            factory(CustomerId::class, 'identification')->create([
                'customer_id' => $customer->id,
            ])
        ]);

        $app = factory(Application::class)->create([
            "primary_customer_id" => $customer->id,
            "application_type_id" => function () use ($appType) {
                return ApplicationType::whereType($appType)->first()->id;
            },
            "renter" => function () use ($appType) {
                return ($appType === 'Landlord Temp') ? false : true;
            },
        ]);

        $app->customers()->attach($customer);

        return $app;
    }
}
