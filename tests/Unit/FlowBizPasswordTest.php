<?php

namespace Tests\Unit;

use Tests\TestCase;

class FlowBizPasswordTest extends TestCase
{
    /** @test */
    public function a_password_can_be_matched()
    {
        $useridPresentational = 147006;
        $useridDatabaseId = 164921;
        $password = "stkilda";

        $expectedHash = "10cY8Mia+YTSUFzpWut0jRQ==";

        $generatedHash = $this->hash($useridDatabaseId, $password, env('FLOWBIZ_SALT'));
        $generatedHash1 = $this->hash($useridPresentational, $password, env('FLOWBIZ_SALT'));

        $this->assertSame($expectedHash, $generatedHash);
        //$this->assertSame($expectedHash, $generatedHash1);
    }
    protected function hash(int $userid, string $password, string $salt)
    {
        $version = "1";
        $key = "$userid-$password-$salt";
        $buff = md5($key, true);

        return $version . base64_encode($buff);
    }
}
