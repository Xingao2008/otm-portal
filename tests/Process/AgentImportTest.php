<?php

namespace Tests\Process;

use Tests\TestCase;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use OnTheMove\Jobs\Tasks\ImportOrUpdateRealEstateAgency;

class AgentImportTest extends TestCase
{
    use DatabaseTransactions;

    protected $flowbiz;


    public function setUp()
    {
        parent::setUp();
        
        $this->flowbiz = new Api(60);
    }

    /** @test */
    public function flowbiz_returns_property_managers_for_an_agency()
    {
        $response = $this->flowbiz->getAgencyList();
        $job = new ImportOrUpdateRealEstateAgency($response['Results'][22]);
        dispatch($job);

        $agent = factory(Agent::class)->create([
            'company_id' => 141139,
        ]);

        dispatch($job);

        $this->assertSoftDeleted('agents', [
            'id' => $agent->id,
            'company_id' => 141139,
        ]);
    }
}
