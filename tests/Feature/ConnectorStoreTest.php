<?php

namespace Tests\Feature;

use Auth;
use File;
use Carbon\Carbon;
use Tests\TestCase;
use OnTheMove\Models\User;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Application;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use OnTheMove\Http\Requests\Connector\StoreRequest;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ConnectorStoreTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        if (env('FLOWBIZ_ENV') === 'production') {
            $this->markTestSkipped('IN PRODUCTION');
        }
        
        $this->rules = (new StoreRequest())->rules();
        $this->validator = $this->app['validator'];
        $this->user = factory(User::class)->states('api-user')->create([
            'allowed_ips' => null,
        ]);
    }

    /** @test */
    public function an_application_cannot_be_stored_twice()
    {
        //$this->markTestSkipped();
        $this->actingAs($this->user, 'user-api');

        $request = [
            "real_estate_agency" => [
                "company_name" => "dgsdfsdf",
            ],
            "property_manager"=> [
                "firstname"=> "tesadgsdg",
                "lastname"=> "tesadgsdg",
            ],
            "connection_date"=> "2018-06-01",
            "application_type_id"=> "1",
            "renter" => true,
            "address"=> [
                "street_number"=> "123",
                "street_name"=> "Somewhere",
                "street_type"=> "St",
                "suburb"=> "Sometown",
                "postcode"=> "1000",
                "state"=> "VIC",
            ],
            "tenants"=> [[
                "lastname"=> "John",
                "firstname"=> "Smith",
                "email"=> "jsmith@test.com",
                "mobile"=> "041234567",
            ]],
        ];
      
        
        $response1 = $this->json('POST', route('api.v1.connector.store'), $request);
        $response1->assertStatus(201);

        // $this->expectException('OnTheMove\Exceptions\ApplicationDuplicateException');
        $response2 = $this->json('POST', route('api.v1.connector.store'), $request);
        $response2->assertStatus(400);
    }
    /** @test */
    public function the_request_has_required_fields()
    {
        //$this->markTestSkipped('IN PRODUCTION');
        $user = factory(User::class)->states('external-api-user')->create([
            'allowed_ips' => null,
        ]);
        $this->actingAs($user, 'user-api');
        
        $response = $this->json('POST', route('api.v1.connector.store'), []);

        $response->assertJsonValidationErrors([
            "renter",
            "real_estate_agency.company_name",
            "property_manager.firstname",
            "property_manager.lastname",
            "address.street_number",
            "address.street_name",
            "address.street_type",
            "address.suburb",
            "address.postcode",
            "address.state",
        ]);
    }

    /** @test */
    public function the_request_can_be_stored()
    {
        //$this->markTestSkipped('IN PRODUCTION');

        $this->actingAs($this->user, 'user-api');
        $pm = Agent::inRandomOrder()->first();
        
        $request = [
            "real_estate_agency" => [
                "company_name" => "", // required
                //"company_name" => $pm->agency, // required
                // "abn"=> "", // preferred
                // "phone"=>"", // preferred
                // "postcode"=> "" // preferred
            ],
            "property_manager"=> [
                "firstname"=> $pm->first_name, // required
                "lastname"=> $pm->last_name, // required
                // "email"=> "", // preferred
                // "mobile_phone"=> "", // preferred
            ],
            "connection_date"=> "2018-06-01", // required (alias to "lease_date"?)
            "application_type_id"=> "1", // required (defauts to "Normal")
            "renter" => true,
            "address"=> [
                "street_number"=> "123", // required
                "street_name"=> "Somewhere", // required
                "street_type"=> "St", // required
                "suburb"=> "Sometown", // required
                "postcode"=> "1000", // required
                "state"=> "VIC", // required
            ],
            "tenants"=> [[
                "lastname"=> "John",  // required
                "firstname"=> "Smith", // required
                "email"=> "jsmith@test.com",// required
                "mobile"=> "041234567", // required
                "identifications" => [[
                    "category" => "Identification",
                    "type" => "Australian Drivers Licence",
                    "number" => "061190179",
                    "expiry" => "",
                    "issuer" => "VIC"
                ]]
            ]],
        ];
        $response = $this->json('POST', route('api.v1.connector.store'), $request);
        $response->assertStatus(201);

        $response->assertJsonStructure([
            "data" => [
                "id", "reference", "status", "connection_date",
                "real_estate_agency" => [
                    "company", "property_manager" => [
                        "firstname",  "lastname",
                    ],
                ],
                "address" => ["unit_number", "street_number", "street_name", "street_type", "suburb", "postcode", "state"],
                "tenants" => [
                    ["title", "firstname", "lastname", "dob", "email", "mobile", "phone"],
                ],
                "recieved_at", "updated_at",
            ],
        ]);

        $payload = $response->decodeResponseJson();
   
        $this->assertDatabaseHas('applications', [
            'id' => $payload['data']['id'],
            'user_id' => $this->user->id,
            'agent_id' => $pm->id,
        ]);

        $app = Application::findOrFail($payload['data']['id']);
        
        $customers = $app->customers;
        $this->assertCount(1, $customers);
        $id = $customers->first()->identifications;
        $this->assertCount(1, $id);

    }

    /** @test */
    public function a_connecter_can_create_an_application_exception()
    {
        // $this->markTestSkipped('IN PRODUCTION');

        $this->actingAs($this->user, 'user-api');
        
        $request = [
            "real_estate_agency" => [
                "company_name" => "dgsdfsdf",
            ],
            "property_manager"=> [
                "firstname"=> "tesadgsdg",
                "lastname"=> "tesadgsdg",
            ],
            "connection_date"=> "2018-06-01",
            "application_type_id"=> "1",
            "renter" => true,
            "address"=> [
                "street_number"=> "123",
                "street_name"=> "Somewhere",
                "street_type"=> "St",
                "suburb"=> "Sometown",
                "postcode"=> "1000",
                "state"=> "VIC",
            ],
            "tenants"=> [[
                "lastname"=> "John",
                "firstname"=> "Smith",
                "email"=> "jsmith@test.com",
                "mobile"=> "041234567",
            ]],
        ];
        $response = $this->json('POST', route('api.v1.connector.store'), $request);
        $response->assertStatus(201);
    }


    /** @test */
    public function a_frontend_request_can_be_stored()
    {
        // $this->markTestSkipped('IN PRODUCTION');
        $pm = Agent::find(4885);
        $this->actingAs($pm, 'agent-api');

        $data = json_decode(File::get('tests/data/frontend_data.json'), true);

        $response = $this->json('POST', route('api.v1.application.store'), $data);
        $response->assertStatus(200);
        $res = $response->getData();
        $this->assertEquals($res->success, true);
        $this->assertEquals($res->sync, true);
        $this->assertEquals($res->id, $res->app->id);
    }


    protected function getFieldValidator($field, $value)
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        );
    }

    protected function validateField($field, $value)
    {
        return $this->getFieldValidator($field, $value)->passes();
    }
}
