<?php

namespace Tests\Feature;

use Auth;
use File;
use Tests\TestCase;
use OnTheMove\Models\Agent;
use OnTheMove\Models\RealEstateAgency;

use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;

use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;
use Illuminate\Foundation\Testing\WithFaker;
use OnTheMove\Repositories\ApplicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;

use OnTheMove\Transformers\ApplicationTransformer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    protected $repository;
    protected $transformer;

    public function setUp()
    {
        parent::setUp();
        
        if (env('FLOWBIZ_ENV') === 'production') {
            $this->markTestSkipped('IN PRODUCTION');
        }

        $this->repository = new ApplicationRepository();
        $this->transformer = new ApplicationTransformer();

        // factory(RealEstateAgency::class)->create();
        // factory(Agent::class, 10)->create();

    }


    /** @test */
    public function an_application_can_be_stored_with_minimal_input()
    {
        //$this->markTestSkipped();
        Auth::guard('agent')->login(Agent::find(4885));
        $data = json_decode(File::get('tests/data/frontend_data.json'), true);
        $app = $this->repository->store($data);
  
        $this->assertDatabaseHas('applications', [
            "id" => $app->id,
            "application_type_id" => $data["details"]["application_type_id"],
            "renter" => $data["details"]["renter"],
            "primary_customer_id" => $app->customers()->first()->id,
        ]);

        $this->assertDatabaseHas('customers', [
            "id" => $app->customers()->first()->id,
            "firstname" => $data["applicants"][0]["firstname"],
            "lastname" => $data["applicants"][0]["lastname"],
            "email" => $data["applicants"][0]["email"],
            "mobile" => $data["applicants"][0]["mobile"],
        ]);
        
        $this->assertSame((int) $app->realEstateAgent->agent_id, $data["details"]["agent_id"], "REA doesn't match");
    }


    /** @test */
    public function an_application_can_be_stored_with_full_input()
    {
        $this->markTestSkipped("Not yet implemented");
        Auth::guard('agent')->login(Agent::find(4885));
        $data = json_decode(File::get('tests/data/frontend_data_full.json'), true);
        $app = $this->repository->store($data);

        $this->assertDatabaseHas('applications', [
            "id" => $app->id,
            "application_type_id" => $data["application_type_id"],
            "renter" => $data["renter"],
            "primary_customer_id" => $app->customers()->first()->id,
        ]);

        $this->assertDatabaseHas('customers', [
            "id" => $app->customers()->first()->id,
            "firstname" => $data["customers"][0]["firstname"],
            "lastname" => $data["customers"][0]["lastname"],
            "email" => $data["customers"][0]["email"],
            "mobile" => $data["customers"][0]["mobile"],
            "occupation" => $data["customers"][0]["occupation"],
        ]);

        $this->assertCount(3, $app->customers()->first()->identifications);
        
        // $this->assertDatabaseHas('customers', [
        //     "id" => $app->customers()->get()->last()->id,
        //     "firstname" => $data["customers"][1]["firstname"],
        //     "lastname" => $data["customers"][1]["lastname"],
        //     "email" => $data["customers"][1]["email"],
        //     "mobile" => $data["customers"][1]["mobile"],
        //     "occupation" => $data["customers"][1]["occupation"],
        // ]);

        $this->assertSame($app->realEstateAgent->id, Auth::guard('agent')->user()->id);
    }

    /** @test */
    public function an_application_can_be_stored_and_sent_to_flowbiz()
    {
        //$this->markTestSkipped();

        Auth::guard('agent')->login(Agent::find(4885));
        $data = json_decode(File::get('tests/data/frontend_data.json'), true);
        //dd($data);
        // as this same data has been used to creata an app, 
        $data['details']['notes'] = 'different';
        $app = $this->repository->store($data);

        // the repo returns an app with it's relations loaded.
        //  $dbApp = Application::findOrFail($app->id);
    
        $resp = $this->repository->export($app, 120);     

        $this->assertEquals($resp->flowbiz_exported, 1);
        $this->assertDatabaseHas('applications', [
            'flowbiz_application_id' => $resp->flowbiz_application_id,
        ]);

        echo "App ID: " . $resp->flowbiz_application_id;
    }


    protected function scaffoldApp($appType = 'Normal')
    {
        $customer = factory(Customer::class)->create();

        $customer->identifications()->saveMany([
            factory(CustomerId::class, 'concession')->create([
                'customer_id' => $customer->id,
            ]),
            factory(CustomerId::class, 'identification')->create([
                'customer_id' => $customer->id,
            ]),
            factory(CustomerId::class, 'identification')->create([
                'customer_id' => $customer->id,
            ]),
        ]);

        $app = factory(Application::class)->create([
            "primary_customer_id" => $customer->id,
            "application_type_id" => function () use ($appType) {
                return ApplicationType::whereType($appType)->first()->id;
            },
            "renter" => function () use ($appType) {
                return ($appType === 'Landlord Temp') ? false : true;
            },
        ]);

        $app->customers()->attach($customer);

        return $app;
    }
}
