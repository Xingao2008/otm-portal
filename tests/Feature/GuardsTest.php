<?php

namespace Tests\Feature;

use Tests\TestCase;
use OnTheMove\Models\User;
use OnTheMove\Models\Agent;
use Laravel\Passport\Passport;
use OnTheMove\Models\Application;

use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GuardsTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function a_api_user_can_access_heartbeat()
    {
        $response = $this->get(route('heartbeat.auth.user-api'));
        $response->assertStatus(302);
        $response->assertRedirect('/login');

        $response = $this->json('GET', route('heartbeat.auth.user-api'));
        $response->assertStatus(401);

        $user = factory(User::class)->create(['api_token' => str_random(60)]);
        $this->actingAs($user, 'user-api');
        
        $response = $this->json('GET', route('heartbeat.auth.user-api'));
        $response->assertStatus(200);
        $response->assertExactJson([
            'health' => true,
        ]);
    }

    /** @test */
    public function a_oauth_agent_can_access_heartbeat()
    {
        $agent = Agent::inRandomOrder()->first();

        Passport::actingAs($agent, [], 'agent-oauth');

        $response = $this->get(route('heartbeat.auth.oauth-api'));
        $response->assertStatus(200);
        $response->assertExactJson([
            'health' => true,
        ]);
    }
    

    /** @test */
    public function a_oauth_authenticated_agent_can_access_api_endpoint()
    {
        $response = $this->get(route('api.v1.agent.get'));
        $response->assertRedirect(route('agent.login'));

        $response = $this->withHeaders([
                'Accept' => 'application/json',
            ])->get(route('api.v1.agent.get'));
        $response->assertStatus(401);

        $agent = Agent::inRandomOrder()->first();

        Passport::actingAs($agent, [], 'agent-oauth');


        $response = $this->get(route('api.v1.agent.get'));
    
        $response->assertStatus(200);
        $response->assertExactJson([
            "data" => [
                "agent_id" => $agent->agent_id,
                "company_id" => $agent->company_id,
                "username" => $agent->username,
                "title" => $agent->title,
                "first_name" => $agent->first_name,
                "last_name" => $agent->last_name,
                "email" => $agent->email,
                "agency" => $agent->agency,
                "is_default_property_manager" => $agent->is_default_agent,
                "active" => $agent->active,
            ],
        ]);
    }

    /** @test */
    public function an_agent_can_only_view_their_own_applications()
    {
        $agent = Agent::whereHas('applications')->inRandomOrder()->firstOrFail();
        $this->actingAs($agent, 'agent');
        
        $app1 = $agent->applications()->inRandomOrder()->firstOrFail();
        $app1->agent_id = $agent->id;
        $app1->save();
        $app2 = Application::inRandomOrder()->firstOrFail();

        $response1 = $this->get(route('agent.application.show', $app1));
        $response2 = $this->get(route('agent.application.show', $app2));
   
        $response1->assertStatus(200);
        $response2->assertStatus(403);
    }

    /** @test */
    public function an_api_user_can_be_restricted_by_ip_address()
    {
        $user = factory(User::class)->states('api-user')->create([
            'allowed_ips' => ['8.8.8.8', '106.133.12.20/30']
        ]);
        $this->actingAs($user, 'user-api');
 
        $response1 = $this->call('GET', route('heartbeat.auth.user-api.whitelist'), [], [], [], ['REMOTE_ADDR' => '10.1.0.1']);
        $response1->assertStatus(403);

        $response2 = $this->call('GET', route('heartbeat.auth.user-api.whitelist'), [], [], [], ['REMOTE_ADDR' => $user->allowed_ips[0]]);
        $response2->assertStatus(200);
        
        $response3 = $this->call('GET', route('heartbeat.auth.user-api.whitelist'), [], [], [], ['REMOTE_ADDR' => '106.133.12.23']);
        $response3->assertStatus(200);

        $response4 = $this->call('GET', route('heartbeat.auth.user-api.whitelist'), [], [], [], ['REMOTE_ADDR' => '106.133.12.22']);
        $response4->assertStatus(200);
    }
}
