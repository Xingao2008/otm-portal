FROM php:7.2-apache

# AS intermediate

RUN apt-get update -y && apt-get install -y \
  apt-utils \
  build-essential \
  mysql-client \
  openssl \
  ca-certificates \
  zlib1g-dev \
  libxml2-dev \
  git \ 
  libc-client-dev \
  libkrb5-dev

#Install extensions
RUN docker-php-ext-install pdo_mysql zip soap bcmath mysqli pcntl

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install imap

#Install NewRelic
RUN cd ~ \
  && export NEWRELIC_VERSION="$(curl -sS https://download.newrelic.com/php_agent/release/ | sed -n 's/.*>\(.*linux\).tar.gz<.*/\1/p')" \
  && curl -sS "https://download.newrelic.com/php_agent/release/${NEWRELIC_VERSION}.tar.gz" | gzip -dc | tar xf - \
  && cd "${NEWRELIC_VERSION}" \
  && NR_INSTALL_SILENT=true ./newrelic-install install \
  && cd ../ \
  && unset NEWRELIC_VERSION


# add credentials on build
RUN mkdir /root/.ssh/
COPY ./id_rsa /root/.ssh/id_rsa

# make sure your domain is accepted
RUN touch /root/.ssh/known_hosts

#Install composer
RUN curl -sS https://getcomposer.org/installer | php \
  && mv composer.phar /usr/local/bin/composer

#Install parallel plugin for composer   
RUN composer global require hirak/prestissimo --no-interaction -v

#change the web_root to laravel /var/www/html/public folder
RUN sed -i -e "s/html/public/g" /etc/apache2/sites-enabled/000-default.conf
#RUN cat /etc/apache2/sites-enabled/000-default.conf

# enable apache module rewrite
RUN a2enmod headers
RUN a2enmod rewrite

WORKDIR /var/www
COPY . /var/www

RUN mkdir -p /var/www/storage/framework/sessions && \
    mkdir -p /var/www/storage/framework/views && \
    mkdir -p /var/www/bootstrap/cache

# Install all PHP dependencies
RUN composer install --no-interaction -v

#private key not needed
#RUN rm -f /root/.ssh/id_rsa

#change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

#change ownership of our applications
RUN chown -R www-data:www-data /var/www

# Inherited from base container
# EXPOSE 80
# CMD ["apache2-foreground"]


# FROM scratch

# # copy the repository form the previous image
# COPY --from=intermediate ./ ./


# #change the web_root to laravel /var/www/html/public folder
# RUN sed -i -e "s/html/public/g" /etc/apache2/sites-enabled/000-default.conf
# #RUN cat /etc/apache2/sites-enabled/000-default.conf

# # enable apache module rewrite
# RUN a2enmod headers
# RUN a2enmod rewrite

# #change uid and gid of apache to docker user uid/gid
# RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

# #change ownership of our applications
# RUN chown -R www-data:www-data /var/www