FROM php:7.2-fpm-alpine

RUN apk update && apk upgrade && \
    apk add --update curl mysql-client openssl libxml2-dev bash

RUN docker-php-ext-install pdo_mysql soap

# RUN cd ~ \
#     && export NEWRELIC_VERSION="$(curl -sS https://download.newrelic.com/php_agent/release/ | sed -n 's/.*>\(.*linux\).tar.gz<.*/\1/p')" \	    && export NEWRELIC_VERSION="$(curl -sS https://download.newrelic.com/php_agent/release/ | sed -n 's/.*>\(.*linux\).tar.gz<.*/\1/p')" \
#     && curl -sS "https://download.newrelic.com/php_agent/release/${NEWRELIC_VERSION}.tar.gz" | gzip -dc | tar xf - \	    && curl -sS "https://download.newrelic.com/php_agent/release/${NEWRELIC_VERSION}.tar.gz" | gzip -dc | tar xf - \
#     && cd "${NEWRELIC_VERSION}" \	    && cd "${NEWRELIC_VERSION}" \
#     && NR_INSTALL_SILENT=true ./newrelic-install install \	    && NR_INSTALL_SILENT=true ./newrelic-install install \
#     && cd ../ \	    && cd ../ \
#     && unset NEWRELIC_VERSION \	    && unset NEWRELIC_VERSION \
#     && sed -i \	    && sed -i \
#         -e "s/;\?newrelic.enabled =.*/newrelic.enabled = true/" \	        -e "s/;\?newrelic.enabled =.*/newrelic.enabled = true/" \
#         -e "s/newrelic.license =.*/newrelic.license = \${NEW_RELIC_LICENSE_KEY}/" \	        -e "s/newrelic.license =.*/newrelic.license = \${NEW_RELIC_LICENSE_KEY}/" \
#         -e "s/newrelic.appname =.*/newrelic.appname = \${NEW_RELIC_APP_NAME}/" \	        -e "s/newrelic.appname =.*/newrelic.appname = \${NEW_RELIC_APP_NAME}/" \
#         -e "s/;\?newrelic.framework =.*/newrelic.framework = 'laravel'/" \	        -e "s/;\?newrelic.framework =.*/newrelic.framework = 'laravel'/" \
#         /usr/local/etc/php/conf.d/newrelic.ini	        /usr/local/etc/php/conf.d/newrelic.ini

# Installing Composer
# RUN curl -sS https://getcomposer.org/installer | php \
#     && mv composer.phar /usr/local/bin/composer

COPY . /var/www
RUN rmdir /var/www/html

RUN mkdir -p /var/www/storage/framework/sessions && \
    mkdir -p /var/www/storage/framework/views && \
    mkdir -p /var/www/bootstrap/cache

CMD chown -R www-data:www-data /var/www && \
    php-fpm

HEALTHCHECK --interval=10s --timeout=10s --retries=10 \
    CMD \
    cgi-fcgi -bind -connect 127.0.0.1:9000 || exit 1
