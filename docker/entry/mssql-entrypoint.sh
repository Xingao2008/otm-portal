/opt/mssql/bin/sqlservr & /usr/src/app/import-data.sh


#wait for the SQL Server to come up
sleep 90s

# #run the setup script to create the DB and the schema in the DB
# /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P P@55w0rd -d master -i setup.sql
#
# #import the data from the csv file
# /opt/mssql-tools/bin/bcp DemoData.dbo.Products in "/usr/src/app/Products.csv" -c -t',' -S localhost -U sa -P P@55w0rd


sqlcmd -S localhost -U SA -P P@55w0rd -Q "RESTORE DATABASE [Flowbiz] FROM DISK = N'/var/opt/mssql/data/FlowBiz_DEV_Nightly_20180430_DB.bak' WITH FILE = 1, NOUNLOAD, REPLACE, STATS = 5"
