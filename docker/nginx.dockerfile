FROM nginx:1.10-alpine

ADD ./docker/nginx.conf /etc/nginx/conf.d/default.conf

COPY public /var/www/public

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
