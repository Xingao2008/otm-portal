###################
# LOCAL DEV       #
###################
up:
	docker-compose up -d --build

start:
	docker-compose up

ps:
	docker-compose ps

down:
	docker-compose down

shell:
	docker-compose run --rm app /bin/bash

unit:
	vendor/bin/phpunit

perf:
	jmeter -n -t ./tests/performance/FlowbizTest.jmx -l tests/performance/results/jmeter.log -e -o tests/performance/results