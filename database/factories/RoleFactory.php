<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['administrator', 'staff', 'manager', 'api', 'api-external']),
    ];
});

$factory->state(OnTheMove\Models\Role::class, 'admin', function ($faker) {
    return [
        'name' => 'administrator',
    ];
});

$factory->state(OnTheMove\Models\Role::class, 'manager', function ($faker) {
    return [
        'name' => 'manager',
    ];
});

$factory->state(OnTheMove\Models\Role::class, 'staff', function ($faker) {
    return [
        'name' => 'staff',
    ];
});

$factory->state(OnTheMove\Models\Role::class, 'api', function ($faker) {
    return [
        'name' => 'api',
    ];
});

$factory->state(OnTheMove\Models\Role::class, 'api-external', function ($faker) {
    return [
        'name' => 'api-external',
    ];
});
