<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(OnTheMove\Models\CustomerId::class, function (Faker $faker) {
    $product = OnTheMove\Models\Customer::inRandomOrder()->first();

    $type = [
        OnTheMove\Models\Identification::class,
        OnTheMove\Models\Concession::class,
    ];

    return [
        "customer_id" => $faker->randomElement(OnTheMove\Models\Customer::pluck('id')->toArray()),
        "type_id" => 1,
        "type_type" => $faker->randomElement($type),
    ];
});


$factory->defineAs(OnTheMove\Models\CustomerId::class, 'identification', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\CustomerId::class);

    $extras = [
        "customer_id" => $faker->randomElement(OnTheMove\Models\Customer::pluck('id')->toArray()),
        "type_id"   => function () {
            return factory(OnTheMove\Models\Identification::class)->create()->id;
        },
        "type_type" => OnTheMove\Models\Identification::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\CustomerId::class, 'concession', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\CustomerId::class);

    $extras = [
        "customer_id" => $faker->randomElement(OnTheMove\Models\Customer::pluck('id')->toArray()),
        "type_id"   => function () {
            return factory(OnTheMove\Models\Concession::class)->create()->id;
        },
        "type_type" => OnTheMove\Models\Concession::class,
    ];

    return array_merge($follow, $extras);
});
