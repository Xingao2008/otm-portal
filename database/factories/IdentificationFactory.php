<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(OnTheMove\Models\Identification::class, function (Faker $faker) {
    $faker = FakerFactory::create('en_AU');


    $type = $faker->randomElement([
        "Australian Drivers Licence",
        "Australian Government Issued Benefits Card",
        "Australian Passport",
        "Bank Statement",
        "Birth Certificate",
        "Blind Citizens Australia Identity Card",
        "Credit Card",
        "Debit Card",
        "International Passport",
        "Medicare Card",
        "Private Health Insurance Card",
        "Tertiary ID Card",
        "Utility Bill or Rates Notice",
        "Valid Police/Defence Force ID",
        "Valid Shooters/Firearms Licence",
        "Working with Children Card",
    ]);

    if ($type === "Australian Drivers Licence") {
        $category = "Drivers License";
    } elseif (in_array($type, ["Australian Passport", "International Passport"])) {
        $category = "Passport";
    } else {
        $category = "Other";
    }

    $issuer = $faker->stateAbbr;

    if ($type === 'International Passport') {
        $issuer = $faker->country;
    } elseif ($type === "Australian Passport") {
        $issuer = "Australia";
    }

    return [
        "category" => $category,
        "type"     => $type,
        "number"   => $faker->regexify("\d{8,12}"),
        "expiry"   => $faker->dateTimeBetween('now', '+5 years'),
        "issuer"   => $issuer,
    ];
});
