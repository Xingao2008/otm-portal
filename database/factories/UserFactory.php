<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(OnTheMove\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'role_id' => function () {
            return factory(OnTheMove\Models\Role::class)->create()->id;
        },
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'api_token' => null,
        'allowed_ips' => null,
    ];
});



$factory->state(OnTheMove\Models\User::class, 'api-user', function ($faker) {
    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));

    return [
        'name' => $faker->company,
        'email' => null,
        'password' => null,
        'remember_token' => null,
        'role_id' => function () {
            return factory(OnTheMove\Models\Role::class)->states('api')->create()->id;
        },
        'api_token' => str_random(60),
        'allowed_ips' => null
    ];
});

$factory->state(OnTheMove\Models\User::class, 'external-api-user', function ($faker) {
    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));

    return [
        'name' => $faker->company,
        'email' => null,
        'password' => null,
        'remember_token' => null,
        'role_id' => function () {
            return factory(OnTheMove\Models\Role::class)->states('api-external')->create()->id;
        },
        'api_token' => str_random(60),
        'allowed_ips' => [$faker->ipv4, $faker->ipv4]
    ];
});
