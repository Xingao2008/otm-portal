<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(OnTheMove\Models\Application::class, function (Faker $faker) {
    $faker = FakerFactory::create('en_AU');

    if (rand(0, 5) === 1) {
        $fbExported = 1;
        $fbId = $faker->regexify("A\d{7}");
        $fbStatus = $faker->randomElement(["Partial Sale", "Accepted"]);
    } else {
        $fbExported = null;
        $fbId = null;
        $fbStatus = null;
    }

    $application_type = OnTheMove\Models\ApplicationType::inRandomOrder()->first();
    if ($application_type->type === 'Landlord Temp') {
        $renter = false;
    } else {
        $renter = $faker->boolean(80);
    }

    return [
        "application_type_id" => $application_type->id,
        "agent_id" => function () {
            return OnTheMove\Models\Agent::inRandomOrder()->first()->id;
            //return OnTheMove\Models\Agent::whereHas('activeRealEstateAgency')->inRandomOrder()->first()->id;
        },
        "user_id" => function () {
            return OnTheMove\Models\User::inRandomOrder()->first()->id;
            //return OnTheMove\Models\Agent::whereHas('activeRealEstateAgency')->inRandomOrder()->first()->id;
        },
        "connection_date" => $faker->dateTimeBetween('now', '+1 month'),
        "renter" => $renter,
        "notes"  => $faker->randomElement(['', $faker->sentence()]),
        "primary_customer_id" => function () {
            return OnTheMove\Models\Customer::inRandomOrder()->first()->id;
        },
        "property_type" => $faker->randomElement(['House', 'House', 'Unit']),
        "unit_number" => $faker->buildingNumber,
        "street_number" => $faker->buildingNumber,
        "street_name" => $faker->streetName,
        "street_type" => $faker->streetSuffix,
        "suburb" => $faker->city,
        "postcode" => $faker->postcode,
        "state" => $faker->stateAbbr,
        "dpid" => null, // leave blank...
        "flowbiz_exported" => $fbExported,
        "flowbiz_application_id" => $fbId,
        "flowbiz_application_status" => $fbStatus,
    ];
});
