<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\Agent::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
    
    $rea = factory(OnTheMove\Models\RealEstateAgency::class)->create();
    
    return [
        "agent_id" => $faker->numberBetween(150000, 280000),
        "user_id" => $faker->numberBetween(150000, 280000),
        "company_id" => $rea->id,
        "first_name" => $faker->firstName,
        "last_name" => $faker->lastName,
        "email" => $faker->safeEmail,
        "agency" => $faker->company,
        "is_default_agent" => $faker->boolean(5),
        "username" => $faker->username,
        "password" => bcrypt($faker->password)
    ];
});
