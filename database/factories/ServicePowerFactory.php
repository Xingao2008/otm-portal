<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\PowerService::class, function (Faker $faker) {
    $lsMachine = $faker->randomElement(["Oxygen Concentrator", "Intermittent Peritonea Dialysis Machine", "Haemodialysis Machine", "Positive Airways Presser (PAP) machine (also known as CPAP)", "Ventilator", "Ventolin Nebuliser"]);


    // Rules from FB Api
    $type = $faker->randomElement(['Standard', 'SDFI', 'VI']);
    $appointment = ($type === 'VI') ? $faker->randomElement(['AM', 'PM']) : '';
    $fee = ($type === 'SDFI') ? $faker->randomElement(['On The Move', 'Customer']) : '';
    $hazards = $faker->boolean(10);
    $access = ($hazards) ? $faker->realText(25) : '';

    return [
        "nmi"                        => $faker->regexify("^(\d{11}|QB\d{9})$"),
        "connection_options"         => $type,
        "appointment_time"           => $appointment,
        "fee_acceptance"             => $fee,
        "additional_access_notes"    => $faker->realText(25),
        "meter_outside"              => $faker->boolean(30),
        "hazards"                    => $hazards,
        "dog_on_premises"            => $faker->boolean(20),
        "access_issues"              => $access,
        "marketing_opt_out"          => $faker->boolean(50),
        "life_support"               => $faker->randomElement(['', '', '', '', $lsMachine]),
        "power_on"                   => $faker->boolean(50),
        "remote_safety_confirmation" => $faker->boolean(5),
    ];
});
