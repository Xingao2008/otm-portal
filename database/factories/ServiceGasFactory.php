<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\GasService::class, function (Faker $faker) {
    return [
        "mirn"                       => $faker->regexify("^5\d{10}$"),
        "additional_access_notes"    => $faker->realText(25),
        "meter_outside"              => $faker->boolean(30),
        "hazards"                    => $faker->boolean(10),
        "dog_on_premises"            => $faker->boolean(20),
        "access_issues"              => $faker->realText(25),
        "marketing_opt_out"          => $faker->boolean(50),
    ];
});
