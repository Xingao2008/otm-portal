<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\Product::class, function (Faker $faker) {
    return [
        "flowbiz_id" => $faker->numberBetween(1000, 99999),
        "type" => $faker->randomElement(['Power', 'Gas', 'Power', 'Gas', 'Power', 'Gas', 'Internet', 'Water', 'Insurance', 'Phone']),
        "provider" => $faker->company(),
        "name" => ucwords($faker->domainWord()),
        "features" => $faker->catchPhrase(),
        "description" => $faker->realText(25),
    ];
});
