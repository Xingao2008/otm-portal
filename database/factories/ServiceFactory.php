<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\Service::class, function (Faker $faker) {
    $product = OnTheMove\Models\Product::inRandomOrder()->first();

    $details = [
        OnTheMove\Models\PowerService::class,
        OnTheMove\Models\GasService::class,
        OnTheMove\Models\InternetService::class,
        OnTheMove\Models\PhoneService::class,
        OnTheMove\Models\PayTvService::class,
        OnTheMove\Models\TelcoBundleService::class,
        OnTheMove\Models\WaterService::class,
    ];

    return [
        "product_id" => $product->id,
        "connection_date" => $faker->dateTimeBetween('now', '+1 month'),
        "details_id" => 1,
        "details_type" => $faker->randomElement($details),

    ];
});


$factory->defineAs(OnTheMove\Models\Service::class, 'power', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::whereType('power')->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\PowerService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\PowerService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'gas', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::whereType('gas')->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\GasService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\GasService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'internet', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::where(['type' => 'internet', 'provider' => 'Telstra'])->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\InternetService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\InternetService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'phone', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::where(['type' => 'phone', 'provider' => 'Telstra'])->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\PhoneService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\PhoneService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'mobile', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::whereType('mobile')->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\MobileService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\MobileService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'paytv', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::where(['type' => 'paytv', 'provider' => 'Telstra'])->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\PayTvService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\PayTvService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'bundle', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::where(['type' => 'bundle', 'provider' => 'Telstra'])->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\TelcoBundleService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\TelcoBundleService::class,
    ];

    return array_merge($follow, $extras);
});

$factory->defineAs(OnTheMove\Models\Service::class, 'water', function (Faker $faker) use ($factory) {
    $follow = $factory->raw(OnTheMove\Models\Service::class);

    $extras = [
        "product_id"   => $faker->randomElement(OnTheMove\Models\Product::whereType('water')->pluck('id')->toArray()),
        "details_id"   => function () {
            return factory(OnTheMove\Models\WaterService::class)->create()->id;
        },
        "details_type" => OnTheMove\Models\WaterService::class,
    ];

    return array_merge($follow, $extras);
});
