<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(OnTheMove\Models\Customer::class, function (Faker $faker) {
    $faker = FakerFactory::create('en_AU');

    return [
        "title" => preg_replace('/\./', '', $faker->title), // faker has a period after the title...
        "firstname" => $faker->firstName,
        "lastname" => $faker->lastName,
        "dob" => $faker->date('Y-m-d', '-18 years'),
        "email" => $faker->safeEmail,
        "mobile" => $faker->regexify('04\d{8}'),
        "phone" => $faker->regexify('0(2|3|7|8)\d{8}'),
        "occupation" => $faker->jobTitle,
        "notes"  => $faker->randomElement([null, $faker->sentence()]),
    ];
});
