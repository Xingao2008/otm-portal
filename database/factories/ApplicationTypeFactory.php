<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\ApplicationType::class, function (Faker $faker) {
    return [
        "type" => $faker->randomElement([
            'Normal', // normal renter app
            'Agent', // business temp in REA's name
            'Landlord Temp', // temp in landlord's name
        ]),
    ];
});
