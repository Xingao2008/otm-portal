<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\MobileService::class, function (Faker $faker) {
    $required = $faker->boolean(80);
    $reason = ($required) ? $faker->randomElement(["Already Arranged", "Customer Requested", "Did not Opt-In", "Not Serviceable", "In Contract", "Arranged prior to receipt of app"]) : "";

    return [
        // "requirements" => $faker->realText(25),
        // "water_required" => $required,
        // "water_only_reason" => $reason,
    ];
});
