<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\RealEstateAgency::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
    $faker->addProvider(new \Faker\Provider\en_AU\Address($faker));

    return [
        "company_id" => $faker->numberBetween(139000, 250000),
        "company_name" => $faker->company,
        "abn" => $faker->regexify("\d{11}"),
        "address1" => $faker->streetAddress,
        "suburb" => $faker->city,
        "postcode" => $faker->postcode,
        "state" => $faker->stateAbbr,
        "active" => $faker->boolean(90),
    ];
});
