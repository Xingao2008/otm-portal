<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\InsuranceService::class, function (Faker $faker) {
    return [
        "insurance_required" => $faker->boolean(5),
    ];
});
