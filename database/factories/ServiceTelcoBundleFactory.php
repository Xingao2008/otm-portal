<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$factory->define(OnTheMove\Models\TelcoBundleService::class, function (Faker $faker) {
    $faker = FakerFactory::create('en_AU');

    return [
        "existing_customer" => $faker->boolean(50),
        "notes" => $faker->realText(25),
        "employer" => $faker->company(),
        "employer_phone" => $faker(),
        "current_employment_duration" => $faker->numberBetween(1, 60),
        "previous_employment_duration" => $faker->numberBetween(1, 60),
        "residential" => $faker->boolean(95),

        "property_type" => $faker->randomElement(['House', 'Unit']),
        "unit_number" => $faker->buildingNumber,
        "street_number" => $faker->buildingNumber,
        "street_name" => $faker->streetName,
        "street_type" => $faker->streetSuffix,
        "suburb" => $faker->city,
        "postcode" => $faker->postcode,
        "state" => $faker->stateAbbr,
        "dpid" => "",
    ];
});
