<?php

use Faker\Generator as Faker;

$factory->define(OnTheMove\Models\Concession::class, function (Faker $faker) {
    return [
        "type" => $faker->randomElement([
            "CDHCC", "DVA_DIS", "DVAG_TPI", "DVAGC", "DVAGC_RHC", "DVAGC_WW", "DVPC", "HCC", "PCC", "QGSC", "SAHCC", "SPHCC"
        ]),
        // "type" => $faker->randomElement([
        //     "CDHCC (Health Care Card Carer (Child Under 16))",
        //     "DVA_DIS (DVA Gold Card (Disability Pension))",
        //     "DVAG_TPI (DVA Gold Card TPI Only)",
        //     "DVAGC (Dept of Veteran Affairs Gold Card)",
        //     "DVAGC (DVA Gold Card Repatriation Health Card)",
        //     "DVAGC_WW (DVA Gold Card War Widow)",
        //     "DVPC (DVA Pension Concession Card)",
        //     "HCC (Health Care Card)",
        //     "PCC (Centrelink Pensioner Concession Card)",
        //     "QGSC (Queensland Government Seniors Card)",
        //     "SAHCC (Health Care Card Sickness Allowance[SA])",
        //     "SPHCC (Health Care Card Special Benefit [SP])",
        // ]),
        "number" => $faker->regexify("\d{8,12}"),
        "expiry" => $faker->dateTimeBetween('now', '+5 years'),
        "issuer" => $faker->randomElement(["CTLK", "DVA", "QDC", "DIBP"]),
    ];
});



