<?php

use Illuminate\Database\Seeder;

class RealEstateAgencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OnTheMove\Models\RealEstateAgency::class, 5)->create();
    }
}
