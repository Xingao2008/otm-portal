<?php

use Illuminate\Database\Seeder;

class ApplicationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OnTheMove\Models\ApplicationType::class)->create([
            'type' => 'Normal', // normal renter app
        ]);
        factory(OnTheMove\Models\ApplicationType::class)->create([
            'type' => 'Agent', // business temp in REA's name
        ]);
        factory(OnTheMove\Models\ApplicationType::class)->create([
            'type' => 'Temp', // temp in landlord's name
        ]);
    }
}
