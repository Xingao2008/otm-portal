<?php

use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ApplicationTypeSeeder::class);
        //$this->call(CustomerSeeder::class);

        // Create 100 apps, for customers
        // with 3x services
        factory(OnTheMove\Models\Application::class, 10)
            ->create()
            ->each(function ($app) {
                $app->customers()->attach($app->primary_customer_id);
                // $app->services()->saveMany([
                //     factory(OnTheMove\Models\Service::class, 'power')->create(),
                //     factory(OnTheMove\Models\Service::class, 'gas')->create(),
                //     factory(OnTheMove\Models\Service::class, 'water')->create(),
                //     factory(OnTheMove\Models\Service::class, 'internet')->create(),
                // ]);
            });
    }
}
