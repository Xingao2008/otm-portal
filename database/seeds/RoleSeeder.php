<?php

use Illuminate\Database\Seeder;
use OnTheMove\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'administrator'
        ]);
        Role::create([
            'name' => 'manager'
        ]);
        Role::create([
            'name' => 'staff'
        ]);
        Role::create([
            'name' => 'api'
        ]);
    }
}
