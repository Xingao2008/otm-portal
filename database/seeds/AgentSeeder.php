<?php

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OnTheMove\Models\Agent::class, 50)->create([
            "company_id" => function () {
                return OnTheMove\Models\RealEstateAgency::inRandomOrder()->first()->id;
            },
        ]);
        factory(OnTheMove\Models\Agent::class)->create([
            "id" => 4885,
            "agent_id" => 147006
        ]);
    }
}
