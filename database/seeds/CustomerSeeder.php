<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OnTheMove\Models\Customer::class, 100)
            ->create()
            ->each(function ($cust) {
                return $cust->identifications()->saveMany([
                    factory(OnTheMove\Models\CustomerId::class, 'concession')->create([
                        'customer_id' => $cust->id,
                    ]),
                    factory(OnTheMove\Models\CustomerId::class, 'identification')->create([
                        'customer_id' => $cust->id,
                    ]),
                ]);
            });
    }
}
