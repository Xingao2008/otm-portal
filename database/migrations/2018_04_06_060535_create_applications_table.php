<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_type_id')->unsigned()->index();
            $table->foreign('application_type_id')->references('id')->on('application_types')->onDelete('cascade');

            $table->integer('agent_id')->unsigned()->index()->nullable()->default(null);
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');

            $table->date("connection_date")->nullable()->default(null); //: "2018-05-18",
            $table->boolean("renter")->nullable()->default(true); //: true,
            $table->text("notes")->nullable(); //: "Test Note"

            $table->integer('primary_customer_id')->unsigned();
            $table->foreign('primary_customer_id')->references('id')->on('customers')->onDelete('cascade');

            $table->string("property_type", 25)->nullable()->default(null); //: "unit",
            $table->string("unit_number", 10)->nullable()->default(null); //: "3",
            $table->string("street_number", 10)->nullable()->default(null); //: "99",
            $table->string("street_name", 80)->nullable()->default(null); //: "fancy street",
            $table->string("street_type", 30)->nullable()->default(null); //: "fancy street",

            $table->string("suburb", 50)->nullable()->default(null); //: "suburbtownville",
            $table->string("postcode", 4)->nullable()->default(null); //: "4000",
            $table->string("state", 3)->nullable()->default(null); //: "qld",
            $table->string("dpid", 10)->nullable()->default(null); //: "123212"


            $table->integer('flowbiz_exported')->unsigned()->nullable()->default(null)->index();
            $table->string('flowbiz_application_id', 8)->nullable()->default(null)->index();
            $table->string('flowbiz_application_status', 20)->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
