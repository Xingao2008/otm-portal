<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("agent_id")->unsigned()->index()->unique()->nullable()->default(null); // => 144384
            $table->integer("company_id")->unsigned()->nullable()->default(null);
            $table->foreign('company_id')->references('company_id')->on('real_estate_agencies')->onDelete('cascade');

            $table->string("first_name", 150)->nullable()->default(null); // => "peter "
            $table->string("last_name", 150)->nullable()->default(null); // => "allen"
            $table->string("email", 150)->unique()->nullable()->default(null); // => "pallen@intellisolutions.com.au"
            $table->string("agency", 150)->nullable()->default(null); // => "Manly Crest"

            $table->string('password');
            $table->string('api_token', 60)->index()->unique()->nullable()->default(null);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
