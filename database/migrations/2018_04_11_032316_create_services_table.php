<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');

            // marketing
            $table->integer("product_id")->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->date("connection_date")->nullable()->default(null);

            $table->string("details_type");
            $table->integer("details_id")->unsigned();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}



//
// Water_Requirements
// Water_Required
// Water_OnlyReason
//
// Insurance_Required
//
//
// Internet_Identifications
//
// "Gas_MIRN"
//
//
// "Gas_AdditionalAccessNotes"
// "Gas_MeterOutside"
// "Gas_Hazards"
// "Gas_DogOnPremises"
// "Gas_AccessIssues"
// "Gas_MarketingOptOut"
//
// "Gas_ConcessionType"
// "Gas_Number"
// "Gas_Expiry"
// "Gas_Confirm"
//
//
// "Power_NMI"
// "Power_ConnectionOptions"
// "Power_AppointmentTime"
// "Power_FeeAcceptance"
//
// "Power_AdditionalAccessNotes"
// "Power_MeterOutside"
// "Power_Hazards"
// "Power_DogOnPremises"
// "Power_AccessIssues"
// "Power_MarketingOptOut"
//
// "Power_ConcessionType"
// "Power_Number"
// "Power_Expiry"
// "Power_Confirm"
//
// "Power_LifeSupport"
// "Power_PowerOn"
// "Power_RemoteSafetyConfirmation"
//

/*
{
  "ID": 2408,
  "ProductType": "Water",
  "Provider": "CWW",
  "ConnectionDate": "2018-05-24",
  "ConnectionDetails": {
    "Requirements": "Notes section Requirements!",
    "WaterRequired": true,
    "WaterOnlyReason": "Already Arranged"
  }
},
{
  "ID": 2409,
  "ProductType": "Insurance",
  "Provider": "Insurance",
  "ConnectionDate": "2018-02-01",
  "ConnectionDetails": {
    "InsuranceRequired": true
  }
},
{
  "ID": 26281,
  "ProductType": "Internet",
  "Provider": "Telstra",
  "ConnectionDate": "2018-02-01",
  "ConnectionDetails": {
    "Identifications": [
      "Tertiary ID Card",
      "Australian Drivers Licence",
      "Credit Card"
    ]
  }
},
{
  "ID": 113331,
  "ProductType": "Gas",
  "Provider": "AGL",
  "ConnectionDate": "2018-02-11",
  "ConnectionDetails": {
    "MIRN": "12341234",
    "AdditionalAccessNotes": "No additional access notes required",
    "MeterOutside": true,
    "Hazards": false,
    "DogOnPremises": false,
    "AccessIssues": "No Issue",
    "MarketingOptOut": true,
    "Concession": {
      "ConcessionType": "HCC (Health Care Card)",
      "Number": "1234",
      "Expiry": "2018-05-15",
      "Confirm": true
    }
  }
},
{
  "ID": 113333,
  "ProductType": "Power",
  "Provider": "AGL",
  "ConnectionDate": "2018-02-11",
  "ConnectionDetails": {
    "NMI": "12345678901",
    "ConnectionOptions": "VI",
    "AppointmentTime": "PM",
    "FeeAcceptance": "Customer",
    "AdditionalAccessNotes": "Additional Access Notes!",
    "MeterOutside": true,
    "Hazards": true,
    "DogOnPremises": true,
    "AccessIssues": "Access issues of note.",
    "MarketingOptOut": true,
    "Concession": {
      "ConcessionType": "HCC (Health Care Card)",
      "Number": "12312312",
      "Expiry": "2018-05-15",
      "Confirm": true
    },
    "LifeSupport": "Ventolin Nebuliser",
    "PowerOn": true,
    "RemoteSafetyConfirmation": true
  }
}
]
*/
