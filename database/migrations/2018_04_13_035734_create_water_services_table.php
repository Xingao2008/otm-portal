<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_water', function (Blueprint $table) {
            $table->increments('id');

            $table->text("requirements")->nullable()->default(null);
            $table->boolean("water_required")->nullable()->default(null);
            $table->string("water_only_reason", 100)->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_water');
    }
}
