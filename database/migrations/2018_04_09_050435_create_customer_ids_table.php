<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id")->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');

            $table->string("type_type");
            $table->integer("type_id")->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_ids');
    }
}



//
// "IdType": "Drivers License",
// "Issuer": "VIC"
//
// "IdType": "Concession",
// "Issuer": "QLD"
//
//
//
// "ConcessionType": "HCC (Health Care Card)",
//
// "Confirm": true
