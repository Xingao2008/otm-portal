<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title", 10)->nullable()->default(null); //: "mrs",
            $table->string("firstname", 50)->nullable()->default(null); //: "shoe",
            $table->string("lastname", 50)->nullable()->default(null); //: "box",
            $table->date("dob")->nullable()->default(null); //: "2018-01-31",
            $table->string("email", 150)->nullable()->default(null); //: "email@address.com",
            $table->string("mobile", 20)->nullable()->default(null); //: "0444111222",
            $table->string("phone", 20)->nullable()->default(null); //: "0999991111",
            $table->string("occupation", 100)->nullable()->default(null); //: "functional storage container",
            $table->text("notes")->nullable()->default(null); //: "No Notes",

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
