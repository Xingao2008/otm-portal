<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->integer("user_id")->unsigned()->nullable()->default(null)->after("agent_id");
            $table->string("title", 150)->nullable()->default(null)->after("company_id");

            $table->string("mobile", 150)->nullable()->default(null)->after("email");
            $table->string("business_phone", 150)->nullable()->default(null)->after("mobile");

            $table->boolean("active")->nullable()->default(null);
            $table->boolean("is_default_agent")->nullable()->default(null);

            $table->string('username', 200)->nullable()->default(null)->after('agency')->index()->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn(["title", "mobile", "active", "is_default_agent", "username"]);
        });
    }
}
