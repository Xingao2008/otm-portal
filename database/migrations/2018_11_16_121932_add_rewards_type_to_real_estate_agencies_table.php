<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRewardsTypeToRealEstateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->string('rewards_type', 50)->nullable()->default(null)->index();

            // enum of PM, Split, REA
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->dropColumn(['rewards_type']);
        });
    }
}
