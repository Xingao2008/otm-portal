<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_power', function (Blueprint $table) {
            $table->increments('id');

            $table->string("nmi", 11)->nullable()->default(null);
            $table->string("connection_options", 20)->nullable()->default(null);
            $table->string("appointment_time", 20)->nullable()->default(null); // AM or PM
            $table->string("fee_acceptance", 30)->nullable()->default(null);
            $table->text("additional_access_notes")->nullable()->default(null);
            $table->boolean("meter_outside")->nullable()->default(null);
            $table->boolean("hazards")->nullable()->default(null);
            $table->boolean("dog_on_premises")->nullable()->default(null);
            $table->text("access_issues")->nullable()->default(null);
            $table->boolean("marketing_opt_out")->nullable()->default(null);

            $table->string("life_support", 100)->nullable()->default(null);
            $table->boolean("power_on")->nullable()->default(null);
            $table->boolean("remote_safety_confirmation")->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_power');
    }
}


// "Power_NMI"
// "Power_ConnectionOptions"
// "Power_AppointmentTime"
// "Power_FeeAcceptance"
//
// "Power_AdditionalAccessNotes"
// "Power_MeterOutside"
// "Power_Hazards"
// "Power_DogOnPremises"
// "Power_AccessIssues"
// "Power_MarketingOptOut"
//
// "Power_ConcessionType"
// "Power_Number"
// "Power_Expiry"
// "Power_Confirm"
//
// "Power_LifeSupport"
// "Power_PowerOn"
// "Power_RemoteSafetyConfirmation"
//
