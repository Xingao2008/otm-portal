<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExceptionMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exception_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_exception_id')->unsigned()->index();
            $table->integer('key')->unsigned();
            $table->text('payload');

            $table->foreign('application_exception_id')->references('id')->on('application_exceptions')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exception_mappings');
    }
}
