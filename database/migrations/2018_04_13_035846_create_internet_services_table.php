<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternetServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_internet', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean("existing_customer")->nullable()->default(null);
            $table->text("notes")->nullable()->default(null);
            // $table->string("identifications", 100)->nullable()->default(null);
            $table->string("employer", 100)->nullable()->default(null);
            $table->string("employer_phone", 100)->nullable()->default(null);
            $table->integer("current_employment_duration")->unsigned()->nullable()->default(null); // in months?
            $table->integer("previous_employment_duration")->unsigned()->nullable()->default(null); // in months?
            $table->boolean("residential")->nullable()->default(null);

            $table->string("property_type", 20)->nullable()->default(null);
            $table->string("street_number", 20)->nullable()->default(null);
            $table->string("street_name", 50)->nullable()->default(null);
            $table->string("street_type", 20)->nullable()->default(null);
            $table->string("unit_number", 20)->nullable()->default(null);
            $table->string("suburb", 50)->nullable()->default(null);
            $table->string("postcode", 4)->nullable()->default(null);
            $table->string("state", 3)->nullable()->default(null);
            $table->string("dpid", 10)->nullable()->default(null);

//
            // "ExistingCustomer": true,
            // "Notes": "",
            // "Identifications": [ "", "" ],
            // "Employer": "",
            // "EmployerPhone": "",
            // "CurrentEmploymentDuration": { "Year" :"", "Month" : "" },
            // "PreviousEmploymentDuration": { "Year" :"", "Month" : "" },
            // "Residential": true,
            // "PreviousAddresses":[{
            // "PropertyType": "House" | "Unit",
            // "StreetNumber": "",
            // "StreetName": "",
            // "UnitNumber": "",
            // "Suburb": "",
            // "Postcode": "",
            // "State": "",
            // "DPID": ""
            // }]
            // The following are mandatory fields if ExistingCustomer is false;
            // o Identifications follows the 100-point validation (points will be calculated)
            // o Employer
            // o EmployerPhone
            // o CurrentEmploymentDuration
            // o PreviousEmploymentDuration
            // o PreviousAddress (maximum of two)



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_internet');
    }
}
