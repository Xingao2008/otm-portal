<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_gas', function (Blueprint $table) {
            $table->increments('id');
            $table->string("mirn", 11)->nullable()->default(null);

            $table->text("additional_access_notes")->nullable()->default(null);
            $table->boolean("meter_outside")->nullable()->default(null);
            $table->boolean("hazards")->nullable()->default(null);
            $table->boolean("dog_on_premises")->nullable()->default(null);
            $table->text("access_issues")->nullable()->default(null);
            $table->boolean("marketing_opt_out")->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_gas');
    }
}


// "Gas_AdditionalAccessNotes"
// "Gas_MeterOutside"
// "Gas_Hazards"
// "Gas_DogOnPremises"
// "Gas_AccessIssues"
// "Gas_MarketingOptOut"
//
//
