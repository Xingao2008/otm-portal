<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIpAddressAndLoginTimestampsToUsersAndAgents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('last_login_at')->after('remember_token')->nullable();
            $table->string('last_login_ip')->after('last_login_at')->nullable();
        });
        Schema::table('agents', function (Blueprint $table) {
            $table->timestamp('last_login_at')->after('remember_token')->nullable();
            $table->string('last_login_ip')->after('last_login_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumns(['last_login_at', 'last_login_ip']);
        });
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumns(['last_login_at', 'last_login_ip']);
        });
    }
}
