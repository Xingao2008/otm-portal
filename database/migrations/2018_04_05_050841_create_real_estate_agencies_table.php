<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealEstateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estate_agencies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("company_id")->unsigned()->index()->unique()->nullable()->default(null); // 139840
            $table->string("company_name", 150)->nullable()->default(null); // "norris first national rosebud"
            $table->string("abn", 11)->nullable()->default(null); // ""
            $table->string("street_name", 100)->nullable()->default(null); // "1015  pt nepean rd"
            $table->string("suburb", 50)->nullable()->default(null); // "rosebud"
            $table->string("postcode", 4)->nullable()->default(null); // "3939"
            $table->string("state", 3)->nullable()->default(null); // "VIC"

            $table->boolean('active')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estate_agencies');
    }
}
