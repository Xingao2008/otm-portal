<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 20)->index();  // EPFS, CIS, EIC, Concession EIC
            $table->string('title', 250)->nullable()->default(null);
            $table->text('content');
            $table->integer('order')->unsigned()->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_terms');
    }
}
