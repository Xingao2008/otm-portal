<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPayloadColumnTypeOnSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // sqlite can't 'ALTER' tables
        if(env('DB_CONNECTION') !== 'sqlite') {
            DB::statement("ALTER TABLE `sessions` MODIFY `payload` `payload` MEDIUMTEXT;");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // sqlite can't 'ALTER' tables
        if (env('DB_CONNECTION') !== 'sqlite') {
            DB::statement("ALTER TABLE `sessions` MODIFY `payload` `payload` TEXT;");
        }
    }
}
