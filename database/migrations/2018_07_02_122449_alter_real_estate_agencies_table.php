<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRealEstateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->string("ranking", 150)->nullable()->default(null)->index()->after("company_name");
            $table->string("company_type", 150)->nullable()->default(null)->index()->after("company_name");
            $table->string("group", 150)->nullable()->default(null)->index()->after("company_name");
            
            $table->string("business_phone", 15)->nullable()->default(null)->after("abn");
            $table->boolean("is_gst_registered", 150)->nullable()->default(0)->index()->after("abn");
            
            
            $table->renameColumn('street_name', 'address1');
            $table->string("address2", 100)->nullable()->default(null)->after("street_name");
            $table->string("dpid", 20)->nullable()->default(null)->after("state");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->renameColumn('address1', 'street_name');

            $table->dropIndex(['group_index', 'company_type_index', 'ranking_index', 'is_gst_registered_index']);
            $table->dropColumn(["group", "company_type", "ranking", "is_gst_registered", "business_phone", "address2", "dpid"]);
        });
    }
}
