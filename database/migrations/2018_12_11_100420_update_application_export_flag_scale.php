<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;
use OnTheMove\Models\Application;

class UpdateApplicationExportFlagScale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $app = new Application;
        DB::table('applications')->whereIn('flowbiz_exported', [3,4])->update(['flowbiz_exported' => $app::BUGGY_EXPORT_IMPORT]);
        DB::table('applications')->where('flowbiz_exported', 2)->update(['flowbiz_exported' => $app::IMPORTED_FROM_FLOWBIZ]);
        DB::table('applications')->where('flowbiz_exported', 1)->update(['flowbiz_exported' => $app::EXPORTED_TO_FLOWBIZ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $app = new Application;
        DB::table('applications')->where('flowbiz_exported', $app::BUGGY_EXPORT_IMPORT)->update(['flowbiz_exported' => 3]);
        DB::table('applications')->where('flowbiz_exported', $app::IMPORTED_FROM_FLOWBIZ)->update(['flowbiz_exported' => 2]);
        DB::table('applications')->where('flowbiz_exported', $app::EXPORTED_TO_FLOWBIZ)->update(['flowbiz_exported' => 1]);
    }
}
