<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRpDataIdToAgentsAndReaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->string('rp_data_id', 50)->nullable()->default(null)->after('company_id')->index();
        });
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->string('rp_data_id', 50)->nullable()->default(null)->after('company_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn('rp_data_id');
        });
        Schema::table('real_estate_agencies', function (Blueprint $table) {
            $table->dropColumn('rp_data_id');
        });
    }
}
