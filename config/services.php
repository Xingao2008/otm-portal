<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => OnTheMove\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'tagmanager' => [
            'production' => 'GTM-ML8788L',
            'nonprod' => 'GTM-NQTBGGW',
        ],
        'gmaps' => [
            'signature' => 'f4WPdwlQwsNDrLBSCxBPwCKX3X4=',
            'key' => 'AIzaSyAyXZJh73nGGhEzHV3aShKuT284WUBDVm0',
        ],
    ],
    
    'nexmo' => [
        'key' => env('NEXMO_KEY'),
        'secret' => env('NEXMO_SECRET'),
        'sms_from' => 'NEXMO',
    ],

    'twilio' => [
        'username' => env('TWILIO_USERNAME'), // optional when using auth token
        'password' => env('TWILIO_PASSWORD'), // optional when using auth token
        'auth_token' => env('TWILIO_AUTH_TOKEN'), // optional when using username and password
        'account_sid' => env('TWILIO_ACCOUNT_SID'),
        'from' => env('TWILIO_FROM'), // optional
    ],

    'flowbiz' => [
        'endpoints' => [
            'base'             => (env('FLOWBIZ_ENV') === 'production') 
                                    ? env('FLOWBIZ_URL_PROD') 
                                    : env('FLOWBIZ_URL_NONPROD'),
            'application'      => '/Application',
            'agencies'         => '/AgencyList',
            'propertymanagers' => '/PropertyManagers',
            'retailer'         => '/Retailer',
            'product'          => '/RetailerProduct',
            'details'          => '/Application/Details',
            'search'           => '/Application/Search',
        ],
        'token' => env('FLOWBIZ_BEARER'),
    ],

    '212f' => [
        'auth' => [env('REWARDS_USERNAME'), env('REWARDS_PASSWORD')],
        'endpoints' => [
            'base'    => (env('REWARDS_ENV') === 'production') 
                            ? env('REWARDS_URL_PROD') 
                            : env('REWARDS_URL_NONPROD'),
            'balance' => '/',
            'history' => '/details',
        ]
    ]

];
