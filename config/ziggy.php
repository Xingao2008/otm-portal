<?php

return [
    'whitelist' => [
        'api.v1.agent.*', 
        'rewardsportal', 
        'api.v1.application.store',
        'agent.application.create',
        'agent.application.show',
    ],
    //'blacklist' => ['debugbar.*', 'admin.*'],
];
