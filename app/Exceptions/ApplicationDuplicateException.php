<?php

namespace OnTheMove\Exceptions;

use Log;
use Bugsnag;
use Exception;

class ApplicationDuplicateException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);

        Log::critical('Duplicate Application Received');
        Bugsnag::notifyException($exception);
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['message' => $exception->getMessage()], 400);
        }

        abort(400);
    }
}
