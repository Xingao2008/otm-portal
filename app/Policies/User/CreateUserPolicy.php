<?php

namespace OnTheMove\Policies\User;

use OnTheMove\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreateUserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function create(User $user)
    {
        return $user->id_admin;
    }
}
