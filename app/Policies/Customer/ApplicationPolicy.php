<?php

namespace OnTheMove\Policies\Customer;

use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the customer can view the application.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function view(Customer $customer, Application $application)
    {
        dd($customer->id, $application->primary_customer_id);

        return $customer->id === $application->primary_customer_id;
    }

    /**
     * Determine whether the customer can create applications.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return mixed
     */
    public function create(Customer $customer)
    {
        //
    }

    /**
     * Determine whether the customer can update the application.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function update(Customer $customer, Application $application)
    {
        return $customer->id === $application->primary_customer_id;
    }

    /**
     * Determine whether the customer can delete the application.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function delete(Customer $customer, Application $application)
    {
        return $customer->id === $application->primary_customer_id;
    }
}
