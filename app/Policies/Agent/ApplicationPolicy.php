<?php

namespace OnTheMove\Policies\Agent;

use OnTheMove\Models\Agent;
use OnTheMove\Models\Application;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the agent can view the application.
     *
     * @param  \OnTheMove\Models\Agent  $agent
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function view(Agent $agent, Application $application)
    {
        return $this->isAllowed($agent, $application);
    }

    /**
     * Determine whether the agent can create applications.
     *
     * @param  \OnTheMove\Models\Agent  $agent
     * @return mixed
     */
    public function create(Agent $agent)
    {
        return true;
    }

    /**
     * Determine whether the agent can update the application.
     *
     * @param  \OnTheMove\Models\Agent  $agent
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function update(Agent $agent, Application $application)
    {
        return $this->isAllowed($agent, $application);
    }

    /**
     * Determine whether the agent can delete the application.
     *
     * @param  \OnTheMove\Models\Agent  $agent
     * @param  \OnTheMove\Models\Application  $application
     * @return mixed
     */
    public function delete(Agent $agent, Application $application)
    {
        return false;
    }

    /**
     * Is the current Agent allowed perform the action on the Application
     *
     * @param Agent $agent
     * @param Application $application
     * @return boolean
     */
    private function isAllowed(Agent $agent, Application $application)
    {
        if (! isset($application->realEstateAgent->id)) {
            return false;
        }
    
        if (! $agent->is_default_agent) {
            return $agent->id === $application->realEstateAgent->id;
        }
        

        // If the logged in Agent is the default PM,
        // they should  be able access apps from
        // any another PM in their agency.
        $propertyManagers = $agent->realEstateAgency()->firstOrFail()->agents()->get();

        if(!$propertyManagers) {
            return false;
        }

        $allowed = false;

        foreach ($propertyManagers as $pm) {
            if ($pm->id === $application->realEstateAgent->id) {
                $allowed = true;
            }
        }

        return $allowed;
    }
}
