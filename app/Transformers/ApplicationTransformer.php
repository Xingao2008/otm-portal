<?php

namespace OnTheMove\Transformers;

use Log;
use Carbon\Carbon;

use OnTheMove\Models\Agent;

use OnTheMove\Models\Product;
use OnTheMove\Models\Service;

use OnTheMove\Models\Customer;
use OnTheMove\Models\Concession;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;

use OnTheMove\Models\Identification;
use OnTheMove\Models\RealEstateAgency;

class ApplicationTransformer
{
    private $HIGH_PRIORITY_THRESHOLD;

    /**
     * Constructor
     *
     * @param integer $days     The number of days to use to determine if an application has a 'high priority'
     */
    public function __construct(int $days = 2)
    {
        $this->HIGH_PRIORITY_THRESHOLD = Carbon::today()->addDays($days);
    }

    /**
     * Build the data structure that the FlowBiz API requires for
     * a given application; transforming the data we store
     * into the mess it requires...
     *
     * @param  Application $app The Application
     * @return Array            The schema array for use with the API
     */
    public function buildFlowbizDataArray(Application $app)
    {
        $schema = [
            "Application"    => $this->getApplicationSchema($app),
            "Applicants"     => $this->getApplicantsForApplication($app),
            "Address"        => $this->getAddressSchema($app),
            "Products"       => $this->getProductsForApplication($app),
        ];

        if ($app->appType->type === "Agent") {
            $schema["BillingAddress"] = $this->getBillingAddressSchema($app);
        }

        //dump($schema);

        return $schema;
    }

    /**
     *  Get the application schema
     *
     * @param  Application $app The Application
     * @return Array            The schema array for use with the API
     */
    private function getApplicationSchema(Application $app)
    {
        $details = [
            "ConnectionDate"    => optional($app->connection_date)->format('Y-m-d') ?? "",
            "ApplicationType"   => $app->appType->type ?? "Normal",
            "Renter"            => $app->renter ?? true,
            "Notes"             => $app->notes ?? "",
            "RealEstateID"      => $app->realEstateAgent->company_id ?? "",
            "HighPriority"      => (optional($app->connection_date)->lt($this->HIGH_PRIORITY_THRESHOLD)) ? true : false,
            "PropertyManagerID" => $app->realEstateAgent->agent_id ?? "",
        ];

        // If the application is for a business temp.
        if ($app->appType->type === "Agent") {
            $details = $this->getBusinessDetails($app, $details);
            // $details['CompanyName'] = $app->realEstateAgent->realEstateAgency->company_name ?? "";
            // $details['CompanyABN'] = $app->realEstateAgent->realEstateAgency->abn ?? "";
        }

        return $details;
    }

    /**
     * Get the business details for a temp, but fall back to REA info
     *
     * @param array $details
     * @return array $details
     */
    private function getBusinessDetails(Application $app, array $details)
    {
        if ($app->business_name) {
            $details['CompanyName'] = $app->business_name ?? "";
        } else {
            $details['CompanyName'] = $app->realEstateAgent->realEstateAgency->company_name ?? "";
        }
        
        if ($app->business_abn) {
            $details['CompanyABN'] = $app->business_abn ?? "";
        } else {
            $details['CompanyABN'] = $app->realEstateAgent->realEstateAgency->abn ?? "";
        }
        
        return $details;
    }
    
    /**
     * Get the connection address schema for the application
     *
     * @param  Application $app The Application
     * @return Array            The schema array for use with the API
     */
    private function getAddressSchema(Application $app)
    {
        $street = ($app->street_name) ? $app->street_name . " " . $app->street_type : "";

        return [
                "PropertyType" => $app->property_type ?? "",
                "StreetNumber" => $app->street_number ?? "",
                "StreetName"   => $street,
                "UnitNumber"   => $app->unit_number ?? "",
                "Suburb"       => $app->suburb ?? "",
                "Postcode"     => $app->postcode ?? "",
                "State"        => $app->state ?? "",
                "DPID"         => $app->dpid ?? "",
        ];
    }

    /**
     * Get the billing address schema for the application
     * Only relevent for 'agent' application types
     *
     * @param  Application $app The Application
     * @return Array            The schema array for use with the API
     */

    private function getBillingAddressSchema(Application $app)
    {
        $rea = $app->realEstateAgent->realEstateAgency ?? null;

        return  [
            "Address1" => $rea->address1 ?? "",
            "Address2" => $rea->address2 ?? "",
            "Suburb"   => $rea->suburb ?? "",
            "State"    => $rea->state ?? "",
            "Postcode" => $rea->postcode ?? "",
            "DPID"     => $rea->dpid ?? "",
        ];
    }

    /**
     * Parse applications products into a format FB requres
     * @param  Application $app the Application to be exported
     * @return Array            array of products for the application
     */
    private function getProductsForApplication(Application $app)
    {
        $appProducts = [];

        $services = $app->services()->with(['product', 'details'])->get();

        foreach ($services as $service) {
            $product = $service->product;

            if (in_array($product->type, ['Power', 'Gas'])) {
                $serviceDetails = $service->details->toArray();
                $serviceDetails = $this->studlyCaseArrayKeys($serviceDetails);

                $concession = $this->getConcessionForProducts($app);

                // if there's no concessions,
                // the value should be set to an empty array
                if (! $concession) {
                    $serviceDetails['Concession'] = [];
                } else {
                    $serviceDetails['Concession'] = $concession;
                }
            }

            if (in_array($product->type, ['Phone', 'Internet', 'Pay TV', 'Bundle'])) {
                $serviceDetails = $this->buildTelcoServiceDetailsArray($app, $service);
            }

            if (in_array($product->type, ['Water', 'Insurance'])) {
                $serviceDetails = $service->details->toArray();
                $serviceDetails = $this->studlyCaseArrayKeys($serviceDetails);
            }


            $appProducts[] = [
                    "ID" => $product->flowbiz_id,
                    "ProductType" => ucwords($product->type),
                    "Provider" => $product->provider,
                    "ConnectionDate" => $service->connection_date->format('Y-m-d'),
                    "ConnectionDetails" => $serviceDetails,
                ];
        }

        return $appProducts;
    }

    /**
     * [buildTelcoServiceDetailsArray description]
     * @param  Application $app     [description]
     * @param  Service     $service [description]
     * @return Array               [description]
     */
    private function buildTelcoServiceDetailsArray(Application $app, Service $service)
    {
        $details = $service->details;

        $idDetails = $this->getCustomerId($app->customers()->first(), 'telco');

        $data = [
                "ExistingCustomer" => $details->existing_customer,
                "Notes" => $details->notes . $idDetails['notes'],
                "Employer" => $details->employer,
                "EmployerPhone" => $details->employer_phone,
                "CurrentEmploymentDuration" => $details->current_employment_duration,
                "PreviousEmploymentDuration" =>$details->previous_employment_duration,
                "Residential" => $details->residential,
                "PreviousAddresses" => [
                    [
                        "PropertyType" => $details->property_type,
                        "StreetNumber" => $details->street_number,
                        "StreetName" => $details->street_name,
                        "UnitNumber" => $details->unit_number,
                        "Suburb" => $details->suburb,
                        "Postcode" => $details->postcode,
                        "State" => $details->state,
                        "DPID" => "",
                    ],
                ],
                "Identifications" => $idDetails['types'],
            ];
        
        return $data;
    }

    /**
     * Flowbiz loves a Studly Case For All It's Keys, but
     * we do not. Let's transform that for us, and
     * keep our database clean-ish
     *
     * @param  Array  $array the array to be transformed
     * @return Array         the transformed array
     */
    private function studlyCaseArrayKeys(array $array)
    {
        $arr = [];
        foreach ($array as $key => $value) {
            if ($key === 'nmi' || $key === 'mirn') {
                $key = strtoupper($key);
            } else {
                $key = studly_case($key);
            }
            $arr[$key] = $value;
        }

        return $arr;
    }

    /**
     * Get the concession for a product (gas or power)
     *
     * @param  Application $app     The application
     * @return Array                Concession array
     */
    private function getConcessionForProducts(Application $app)
    {
        $customer = Customer::find($app->primary_customer_id);

        $ids = $customer->identifications;

        $ids = $ids->filter(function ($id) {
            return $id->type instanceof Concession;
        });

        // if there's not concessions,
        // return false so we can handle that
        if (! $ids || $ids->count() === 0) {
            return false;
        }

        $concession = $ids->first()->type;

        return [
            "ConcessionType" => $concession->type,
            "Number" => $concession->number,
            "Expiry" => $concession->expiry->format('Y-m-d'),
            "Confirm" => true,
        ];
    }


    /**
     * Parse a Application's Customer applicants into the format FB requires
     *
     * @param  Application $app the Application to be exported
     * @return Array            array of Customer applicants
     */
    private function getApplicantsForApplication(Application $app)
    {
        $applicants = [];
        $customers = $app->customers;

        foreach ($customers as $customer) {
            $customerIds = $this->getCustomerId($customer, 'customer');

            $applicants[] = [
                    "Title"           => $customer->title ?? "",
                    "Firstname"       => $customer->firstname ?? "",
                    "Lastname"        => $customer->lastname ?? "",
                    "DOB"             => optional($customer->dob)->format('Y-m-d') ?? "",
                    "Email"           => $customer->email ?? "",
                    "Mobile"          => $customer->mobile ?? "",
                    "Phone"           => $customer->phone ?? "",
                    "Primary"         => ($customer->id === $app->primary_customer_id) ? true : false,
                    "Occupation"      => $customer->occupation ?? "",
                    "Notes"           => $customer->notes ?? "",
                    "Identifications" => $customerIds,
                ];
        }

        return $applicants;
    }


    /**
     * Parse a customer's idenfications into the format FB requires
     * If the ID is required for a telco product, transform it
     * differently, compared to an applicant (customer)
     *
     * @param  Customer $customer the Customer
     * @return Array             array of CustomerId's
     */
    private function getCustomerId(Customer $customer, $format = 'customer')
    {
        if ($format === 'customer') {
            $identifications = [];
        } elseif ($format === 'telco') {
            $identifications = [
                'notes' => "",
                'types' => [],
            ];
        }
        $ids = $customer->identifications;

        if ($format === 'telco') {
            $ids = $ids->filter(function ($id) {
                return $id->type instanceof Identification;
            });
        }
    

        $identifications = [];


        foreach ($ids as $id) {
            $custId = $id->type;

            if ($format === 'customer') {

                // @TODO fix this 💩
                if ($id->type instanceof Identification) {
                    $call = 'getIdentificationDetails';
                } elseif ($id->type instanceof Concession) {
                    $call = 'getConcessionDetails';
                }
                $identifications[] = $this->{$call}($custId);
                // $identifications[] = [
                //         "IdType" => $custId->category,
                //         "Number" => $custId->number,
                //         "Expiry" => $custId->expiry->format('Y-m-d'),
                //         "Issuer" => $custId->issuer,
                //     ];
            }

            // In some cases, Flowbiz can't capture ID in it's 3 categories
            // so add the data to the product notes. This is a hack;
            // there are many like it; but this one is mine.
            
            if ($format === 'telco') {
                $identifications['notes'] = "";
                $identifications['notes'] .=
                        "IdType: " . $custId->type . ", " . "Number: " . $custId->number . ", " . "Expiry: " . $custId->expiry->format('Y-m-d') . ", " . "Issuer: " . $custId->issue . ". ";
                $identifications['types'][] = $custId->type;
            }
        }

        return $identifications;
    }

    private function getIdentificationDetails(Identification $id)
    {
        return [
            "IdType" => $id->category,
            "Number" => $id->number,
            "Expiry" => optional($id->expiry)->format('Y-m-d'),
            "Issuer" => $id->issuer,
        ];
    }

    private function getConcessionDetails(Concession $id)
    {
        return [
            "IdType" => "Concession",
            "Number" => $id->number,
            "Expiry" => optional($id->expiry)->format('Y-m-d'),
            "Issuer" => $id->flowbizIssuer,
        ];
    }
}
