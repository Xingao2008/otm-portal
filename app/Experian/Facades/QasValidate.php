<?php

namespace OnTheMove\Experian\Facades;

use Illuminate\Support\Facades\Facade;

class QasValidate extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'qasvalidate';
    }
}
