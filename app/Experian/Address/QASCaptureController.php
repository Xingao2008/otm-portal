<?php

namespace OnTheMove\Experian\Address;

use OnTheMove\Experian\Address\QASCapture;

/**
 * Rpc Error class
 * @author aikkeongt
 *
 */
class RpcError
{
    public $message;
    public $code;

    /**
     * Constructor
     * @param string $message
     * @param int $code
     */
    public function __construct($message, $code)
    {
        $this->message = $message;
        $this->code = $code;
    }
}

/**
 * Rpc Result class
 * @author aikkeongt
 *
 */
class RpcResult
{
    public $result;
    public $error;

    /**
     *
     * Constructor
     * @param string $result
     */
    public function __construct($result)
    {
        $this->result = $result;
    }
}

/**
 * QASCaptureController
 * @author aikkeongt
 *
 */
class QASCaptureController
{
    const TYPE_POST = "POST";
    const TYPE_GET = "GET";

    const KEY_METHOD = "method";
    const KEY_COUNTRY_ID = "countryId";
    const KEY_SEARCH = "search";
    const KEY_PROMPTSET = "promptset";
    const KEY_LAYOUT = "layout";
    const KEY_REQUEST_TAG = "requestTag";
    const KEY_ENGINE = "engine";
    const KEY_MONIKER = "moniker";
    const KEY_REFINEMENT = "refinement";
    const KEY_LOCALISATION = "localisation";
    const KEY_THRESHOLD = "threshold";
    const KEY_TIMEOUT = "timeout";
    const KEY_FLATTEN = "flatten";
    const KEY_INTENSITY = "intensity";
    const KEY_FORMATTED_ADDRESS_IN_PICKLIST = "formattedAddressInPicklist";

    const METHOD_SEARCH = "Search";
    const METHOD_REFINE = "Refine";
    const METHOD_GET_ADDRESS = "GetAddress";
    const METHOD_GET_DATA = "GetData";
    const METHOD_GET_EXAMPLE_ADDRESSES = "GetExampleAddresses";
    const METHOD_GET_LICENSE_INFO = "GetLicenseInfo";
    const METHOD_GET_SYSTEM_INFO = "GetSystemInfo";
    const METHOD_GET_DATA_MAP_DETAIL = "GetDataMapDetail";
    const METHOD_GET_LAYOUTS = "GetLayouts";
    const METHOD_GET_PROMPTSET = "GetPromptSet";
    const METHOD_CAN_SEARCH = "CanSearch";

    // The method does not exist / is not available.
    const ERROR_INVALID_METHOD = -32601;

    // Invalid method parameter .
    const ERROR_INVALID_PARAM = -32602;

    // Internal JSON-RPC error.
    const ERROR_INTERNAL_ERROR = -32603;
    private $qasCapture = null;
    private $type;
    private $canSearchParam;
    private $searchParam;
    private $refineParam;
    private $getFormattedAddressParam;
    private $getAllDatasetParam;
    private $getDatasetDetailParam;
    private $getAllLayoutParam;
    private $getExampleAddressesParam;
    private $getPromptSetParam;
    private $getSystemInfoParam;

    /**
     * Constructor
     */
    public function __construct($wsdl)
    {
        $this->qasCapture = new QASCapture($wsdl);

        $this->searchParam = [self::KEY_SEARCH, self::KEY_COUNTRY_ID, self::KEY_PROMPTSET, self::KEY_LAYOUT, self::KEY_ENGINE];
        $this->refineParam = [self::KEY_MONIKER, self::KEY_REFINEMENT];
        $this->getFormattedAddressParam = [self::KEY_MONIKER, self::KEY_LAYOUT];
        $this->getAllDatasetParam = [];
        $this->getDatasetDetailParam = [self::KEY_COUNTRY_ID];
        $this->getAllLayoutParam = [self::KEY_COUNTRY_ID];
        $this->getExampleAddressesParam = [self::KEY_COUNTRY_ID, self::KEY_LAYOUT];
        $this->getPromptSetParam = [self::KEY_COUNTRY_ID, self::KEY_PROMPTSET, self::KEY_ENGINE];
        $this->getSystemInfoParam = [];
        $this->canSearchParam = [self::KEY_COUNTRY_ID, self::KEY_LAYOUT, self::KEY_PROMPTSET, self::KEY_ENGINE];

        $this->type = self::TYPE_POST;
    }

    /**
     * Invoke JSON RPC
     */
    public function InvokeJsonRpc($data)
    {
        $jsonResult = "";
        $clientData = $this->getClientData($data);

        // get the method
        $methodName = $clientData[self::KEY_METHOD];
        // Check method is allowed

        try {
            switch ($methodName) {
                case self::METHOD_SEARCH:
                    $retObject = $this->search($data);
                    break;
                case self::METHOD_REFINE:
                    $retObject = $this->refine($data);
                    break;
                case self::METHOD_GET_ADDRESS:
                    $retObject = $this->getAddress($data);
                    break;
                case self::METHOD_GET_DATA:
                    $retObject = $this->getData($data);
                    break;
                // Not working
                case self::METHOD_GET_LICENSE_INFO:
                    $retObject = $this->getLicenseInfo($data);
                    break;
                case self::METHOD_GET_SYSTEM_INFO:
                    $retObject = $this->getSystemInfo($data);
                    break;
                // Not working
                case self::METHOD_GET_DATA_MAP_DETAIL:
                    $retObject = $this->getDataMapDetail($data);
                    break;
                case self::METHOD_GET_EXAMPLE_ADDRESSES:
                    $retObject = $this->getExampleAddresses($data);
                    break;
                case self::METHOD_GET_LAYOUTS:
                    $retObject = $this->getLayouts($data);
                    break;
                case self::METHOD_GET_PROMPTSET:
                    $retObject = $this->getPromptSet($data);
                    break;
                case self::METHOD_CAN_SEARCH:
                    $retObject = $this->canSearch($data);
                    break;
                default:
                    throw new Exception("Invalid method name:" + $methodName, self::ERROR_INVALID_METHOD);
            }

            $rpcResult = new RpcResult($retObject);
            $jsonResult = json_encode($rpcResult);
        } catch (Exception $exception) {
            $rpcResult = new RpcResult(null);
            $message = $this->qasCapture->GetFaultString("");
            if (null != $exception->getCode()) {
                $rpcError = new RpcError($message, $exception->getCode());
            } else {
                $rpcError = new RpcError($message, self::ERROR_INTERNAL_ERROR);
            }
            $rpcResult->error = $rpcError;
            $jsonResult = json_encode($rpcResult);
        }
        //echo $jsonResult;
        return $rpcResult;
    }

    /**
     * Get client data
     * @return string
     */
    private function getClientData($data)
    {
        return $data;

        /*
                if(self::TYPE_POST == $this->type)
                {
                    return $_POST;
                }

                return $_GET;
        */
    }

    /**
     * Search
     * @return object
     */
    private function search($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->searchParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $engine = $clientData[self::KEY_ENGINE];
        $flatten = (bool)$clientData[self::KEY_FLATTEN];
        $intensity = $clientData[self::KEY_INTENSITY];
        $promptset = $clientData[self::KEY_PROMPTSET];
        $threshold = $clientData[self::KEY_THRESHOLD];
        $timeout = $clientData[self::KEY_TIMEOUT];
        $layout = $clientData[self::KEY_LAYOUT];
        $search = $clientData[self::KEY_SEARCH];
        $formattedAddressInPicklist = (bool)$clientData[self::KEY_FORMATTED_ADDRESS_IN_PICKLIST];
        $requestTag = $clientData[self::KEY_REQUEST_TAG];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=Search&countryId=AUS&engine=Singleline&flatten=1&intensity=&promptset=Default&threshold=&timeout=&layout=QADefault&search=Balfour|2033&formattedAddressInPicklist=0&requestTag=&localisation=

        $retObject = $this->qasCapture->Search($search, $countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $layout, $formattedAddressInPicklist, $requestTag, $localisation);

        return $retObject;
    }

    /**
     * Refine
     * @return object
     */
    private function refine($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->refineParam, $clientData);

        $moniker = $clientData[self::KEY_MONIKER];
        $refinement = $clientData[self::KEY_REFINEMENT];
        $layout = $clientData[self::KEY_LAYOUT];
        $formattedAddressInPicklist = (bool)$clientData[self::KEY_FORMATTED_ADDRESS_IN_PICKLIST];
        $threshold = $clientData[self::KEY_THRESHOLD];
        $timeout = $clientData[self::KEY_TIMEOUT];
        $requestTag = $clientData[self::KEY_REQUEST_TAG];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=Search&formattedAddressInPicklist=flase&layout=QADefault&localisation=&method=Refine&moniker=DEU|9080393|07ODEUAQfbBwAAAAABAwEAAAAASgXl0YAhMIISACAAAAAAAAD..2QAAAAA.....wAAAAAAAAAAAHNlZ2ViZXJnZXIgc3RyLHNlZWRvcmYsMjM4MjMA&refinement=1&requestTag=&threshold=25&timeout=15000
        $retObject = $this->qasCapture->Refine($moniker, $refinement, $layout, $formattedAddressInPicklist, $threshold, $timeout, $requestTag, $localisation);

        return $retObject;
    }

    /**
     * Get Address
     * @return object
     */
    private function getAddress($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getFormattedAddressParam, $clientData);

        $moniker = $clientData[self::KEY_MONIKER];
        $layout = $clientData[self::KEY_LAYOUT];
        $requestTag = $clientData[self::KEY_REQUEST_TAG];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetAddress&moniker=AUS|1666284|0_SAUSAQXbBwAAAAABAUJhbGZvdXIsMjAzMwAqAwAA&layout=QADefault&requestTag=&localisation=
        $retObject = $this->qasCapture->GetAddress($moniker, $layout, $requestTag, $localisation);

        return $retObject;
    }

    /**
     * Get Data
     * @return object
     */
    private function getData($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getAllDatasetParam, $clientData);

        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetData&localisation=
        $retObject = $this->qasCapture->GetData($localisation);

        return $retObject;
    }

    /**
     * Get License Info
     * @return object
     */
    private function getLicenseInfo($data)
    {
        $clientData = $this->getClientData($data);

        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetLicenseInfo&localisation=
        $retObject = $this->qasCapture->GetLicenseInfo($localisation);

        return $retObject;
    }

    /**
     * Get System Info
     * @return object
     */
    private function getSystemInfo($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getSystemInfoParam, $clientData);

        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetSystemInfo&localisation=
        $retObject = $this->qasCapture->GetSystemInfo($localisation);

        return $retObject;
    }

    /**
     * Get Datamap Detail
     * @return object
     */
    private function getDataMapDetail($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getDatasetDetailParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetDataMapDetail&countryId=AUS&localisation=
        $retObject = $this->qasCapture->GetDataMapDetail($countryId, $localisation);

        return $retObject;
    }

    /**
     * Get Example Addresses
     * @return object
     */
    private function getExampleAddresses($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getExampleAddressesParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $layout = $clientData[self::KEY_LAYOUT];
        $requestTag = $clientData[self::KEY_REQUEST_TAG];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetExampleAddresses&countryId=AUS&layout=QADefault&requestTag=&localisation=
        $retObject = $this->qasCapture->GetExampleAddresses($countryId, $layout, $requestTag, $localisation);

        return $retObject;
    }

    /**
     * Get Layouts
     * @return object
     */
    private function getLayouts($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getAllLayoutParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetLayouts&countryId=AUS&localisation=
        $retObject = $this->qasCapture->GetLayouts($countryId, $localisation);

        return $retObject;
    }

    /**
     * Get Promptset
     * @return object
     */
    private function getPromptSet($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->getPromptSetParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $engine = $clientData[self::KEY_ENGINE];
        $flatten = (bool)$clientData[self::KEY_FLATTEN];
        $intensity = $clientData[self::KEY_INTENSITY];
        $promptset = $clientData[self::KEY_PROMPTSET];
        $threshold = $clientData[self::KEY_THRESHOLD];
        $timeout = $clientData[self::KEY_TIMEOUT];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=GetPromptSet&countryId=AUS&engine=Singleline&flatten=1&intensity=&promptset=Default&threshold=&timeout=&layout=QADefault&localisation=
        $retObject = $this->qasCapture->GetPromptSet($countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $localisation);

        return $retObject;
    }

    /**
     * Can Search
     * @return object
     */
    private function canSearch($data)
    {
        $clientData = $this->getClientData($data);

        $this->validateParam($this->canSearchParam, $clientData);

        $countryId = $clientData[self::KEY_COUNTRY_ID];
        $engine = $clientData[self::KEY_ENGINE];
        $flatten = (bool)$clientData[self::KEY_FLATTEN];
        $intensity = $clientData[self::KEY_INTENSITY];
        $promptset = $clientData[self::KEY_PROMPTSET];
        $threshold = $clientData[self::KEY_THRESHOLD];
        $timeout = $clientData[self::KEY_TIMEOUT];
        $layout = $clientData[self::KEY_LAYOUT];
        $localisation = $clientData[self::KEY_LOCALISATION];

        // http://localhost/rest/QASCaptureController.php?method=CanSearch&countryId=AUS&engine=Singleline&flatten=1&intensity=&promptset=Default&threshold=&timeout=&layout=QADefault&localisation=
        $retObject = $this->qasCapture->CanSearch($countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $layout, $localisation);

        return $retObject;
    }

    /**
     *
     * Validate Param
     * @param array $paramList
     * @param array $clientData
     * @throws Exception
     */
    private function validateParam($paramList, $clientData)
    {
        foreach ($paramList as $param) {
            if (! array_key_exists($param, $clientData)) {
                throw new Exception("Missing param: " . $param, self::ERROR_INVALID_PARAM);
            }
        }
    }
}
