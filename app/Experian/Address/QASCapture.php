<?php

namespace OnTheMove\Experian\Address;

//REQUIRE_ONCE("CommonClasses.php");
//REQUIRE_ONCE("QASCaptureInterface.php");

// use OnTheMove\Experian\Address\CommonClasses;
use Soap;

use SoapClient;
use SoapHeader;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use OnTheMove\Experian\Address\QASCaptureInterface;

/**
 *
 * Quick Address
 * @author aikkeongt
 *
 */
class QASCapture implements QASCaptureInterface
{
    const PARAM_COUNTRY = "Country";
    const PARAM_ENGINE = "Engine";
    const PARAM_PROMPTSET = "PromptSet";
    const PARAM_REQUEST_TAG = "RequestTag";
    const PARAM_THRESHOLD = "Threshold";
    const PARAM_TIMEOUT = "Timeout";
    const PARAM_LAYOUT = "Layout";
    const PARAM_SEARCH = "Search";
    const PARAM_MONIKER = "Moniker";
    const PARAM_FLATTEN = "Flatten";
    const PARAM_REFINEMENT = "Refinement";
    const SOAP_HEADER_NAMESPACE = "http://www.qas.com/OnDemand-2011-03";
    private $soap = null;

    /**
     * constructor
     * @param string $endPointURL
     */
    public function __construct($endPointURL)
    {
        if (defined('CONTROL_PROXY_NAME')) {
            $this->soap = new SoapClient(
                $endPointURL,
                ['soap_version' => SOAP_1_2,
                        'exceptions' => 0,
                        'classmap' => ['QAAuthentication' => 'QAAuthentication',
                                            'QAQueryHeader' => 'QAQueryHeader', ],
                        'proxy_host' => CONTROL_PROXY_NAME,
                        'proxy_port' => CONTROL_PROXY_PORT,
                        'proxy_login' => CONTROL_PROXY_LOGIN,
                        'proxy_password' => CONTROL_PROXY_PASSWORD,
                    ]
                );
        } else {
            $this->soap = new SoapClient(
                $endPointURL,
                ['soap_version' => SOAP_1_2,
                        'exceptions' => true,
                        'classmap' => ['QAAuthentication' => 'QAAuthentication',
                                            'QAQueryHeader' => 'QAQueryHeader', ],
                    ]
                );
        }

        if (is_soap_fault($this->soap)) {
            $this->soap = null;
        }
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::Search()
     */
    public function Search($search, $countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $layout, $formattedAddressInPicklist, $requestTag, $localisation)
    {
        $engineOptions = ["_" => $engine, self::PARAM_FLATTEN => $flatten];

        if (null != $promptset) {
            $engineOptions[self::PARAM_PROMPTSET] = $promptset;
        }

        if (0 != $threshold) {
            $engineOptions[self::PARAM_THRESHOLD] = $threshold;
        }

        if (-1 != $timeout) {
            $engineOptions[self::PARAM_TIMEOUT] = $timeout;
        }

        $args = [self::PARAM_COUNTRY => $countryId, self::PARAM_SEARCH => $search, self::PARAM_ENGINE => $engineOptions];

        if (null != $layout) {
            $args[self::PARAM_LAYOUT] = $layout;
        }

        if (null != $requestTag) {
            $args[self::PARAM_REQUEST_TAG] = $requestTag;
        }

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoSearch($args));

        if (null != $result) {
            $result = new SearchResult($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::Refine()
     */
    public function Refine($moniker, $refinement, $layout, $formattedAddressInPicklist, $threshold, $timeout, $requestTag, $localisation)
    {
        $args = [self::PARAM_MONIKER => $moniker, self::PARAM_REFINEMENT => $refinement];

        if (0 != $threshold) {
            $args[self::PARAM_THRESHOLD] = $threshold;
        }

        if (-1 != $timeout) {
            $args[self::PARAM_TIMEOUT] = $timeout;
        }

        if (null != $requestTag) {
            $args[self::PARAM_REQUEST_TAG] = $requestTag;
        }

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoRefine($args));

        if (null != $result) {
            $result = new Picklist($result->QAPicklist);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetAddress()
     */
    public function GetAddress($moniker, $layout, $requestTag, $localisation)
    {
        $args = [self::PARAM_LAYOUT => $layout, self::PARAM_MONIKER => $moniker];

        if (null != $requestTag) {
            $args[self::PARAM_REQUEST_TAG] = $requestTag;
        }

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetAddress($args));

        if (null != $result) {
            $result = new FormattedAddress($result->QAAddress);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetData()
     */
    public function GetData($localisation)
    {
        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetData());

        if ($result != null) {
            $result = Dataset::CreateArray($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetLicenseInfo()
     */
    public function GetLicenseInfo($localisation)
    {
        $this->build_auth_header();
        $result = $this->check_soap($this->soap->DoGetLicenseInfo());

        if (null != $result) {
            if (is_array($result->LicensedSet)) {
                return $result->LicensedSet;
            } else {
                return [$result->LicensedSet];
            }
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetSystemInfo()
     */
    public function GetSystemInfo($localisation)
    {
        $this->build_auth_header();
        $result = $this->check_soap($this->soap->DoGetSystemInfo());

        if (null != $result) {
            if (is_array($result->SystemInfo)) {
                return $result->SystemInfo;
            } else {
                return [$result->SystemInfo];
            }
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetDataMapDetail()
     */
    public function GetDataMapDetail($countryId, $localisation)
    {
        $args = [self::PARAM_COUNTRY => $countryId];

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetDataMapDetail($args));

        if (null != $result) {
            $result = LicensedSet::CreateArray($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetExampleAddresses()
     */
    public function GetExampleAddresses($countryId, $layout, $requestTag, $localisation)
    {
        $args = [self::PARAM_COUNTRY => $countryId, self::PARAM_LAYOUT => $layout];

        // Set request tag if supplied
        if (null != $requestTag) {
            $args[self::PARAM_REQUEST_TAG] = $requestTag;
        }

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetExampleAddresses($args));

        if (null != $result) {
            $result = ExampleAddress::CreateArray($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetLayouts()
     */
    public function GetLayouts($countryId, $localisation)
    {
        $args = [self::PARAM_COUNTRY => $countryId];

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetLayouts($args));

        if ($result != null) {
            $result = Layout::CreateArray($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::GetPromptSet()
     */
    public function GetPromptSet($countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $localisation)
    {
        $args = [self::PARAM_COUNTRY => $countryId, self::PARAM_PROMPTSET => $promptset, self::PARAM_ENGINE => $engine];

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoGetPromptSet($args));

        if (null != $result) {
            $result = new PromptSet($result);
        }

        return $result;
    }

    /* (non-PHPdoc)
     * @see QASCaptureInterface::CanSearch()
     */
    public function CanSearch($countryId, $engine, $flatten, $intensity, $promptset, $threshold, $timeout, $layout, $localisation)
    {
        $engineOptions = ["_" => $engine, self::PARAM_FLATTEN => $flatten, self::PARAM_PROMPTSET => $promptset];

        $args = [self::PARAM_COUNTRY => $countryId, self::PARAM_ENGINE => $engineOptions, self::PARAM_FLATTEN => $flatten];

        if (null != $layout) {
            $args[self::PARAM_LAYOUT] = $layout;
        }

        $this->build_auth_header();

        $result = $this->check_soap($this->soap->DoCanSearch($args));

        if (null != $result) {
            $result = new CanSearch($result);
        }

        return $result;
    }

    /**
     *
     * Get fault string
     * @param string $sFault
     * @return string
     */
    public function GetFaultString($sFault)
    {
        if ((! is_string($sFault) || $sFault == "") && ($this->getSoapFault() != null)) {
            return ("[" . $this->getSoapFault() . "]");
        } else {
            return ($sFault);
        }
    }

    /**
     * Build authentication header
     */
    private function build_auth_header()
    {
        $b = new QAQueryHeader(env("QAS_ADDRESS_USERNAME"), env("QAS_ADDRESS_PASSWORD"));

        $authHeader = new SoapHeader(self::SOAP_HEADER_NAMESPACE, 'QAQueryHeader', $b);

        $this->soap->__setSoapHeaders([$authHeader]);
    }

    /**
     * Check soap
     * @param object $soapResult
     * @throws Exception
     * @return object
     */
    private function check_soap($soapResult)
    {
        if (is_soap_fault($soapResult)) {
            $err = "QAS SOAP Fault - " . "Code: {" . $soapResult->faultcode . "}, " . "Description: {"
                . $soapResult->faultstring . "}";

            error_log($err, 0);

            $soapResult = null;
            throw new Exception($err);
        }

        return ($soapResult);
    }

    /**
     * Get SOAP fault
     * @return string
     */
    private function getSoapFault()
    {
        return (isset($this->soap->__soap_fault) ? $this->soap->__soap_fault->faultstring : null);
    }
}



/**
 * Address Line
 * @author aikkeongt
 *
 */
class AddressLine
{
    public $Label;
    public $Line;
    public $DataplusGroup = null;
    public $LineType;
    public $IsTruncated;
    public $IsOverflow;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->Label = $result->Label;
        $this->Line = $result->Line;
        $this->LineType = $result->LineContent;
        $this->IsOverflow = $result->Overflow;
        $this->IsTruncated = $result->Truncated;

        if (isset($result->DataplusGroup) && null != $result->DataplusGroup) {
            $this->DataplusGroup = [];
            if (is_array($result->DataplusGroup)) {
                foreach ($result->DataplusGroup as $dataplusGroup) {
                    array_push($this->DataplusGroup, new DataplusGroup($dataplusGroup));
                }
            } else {
                array_push($this->DataplusGroup, new DataplusGroup($dataplusGroup));
            }
        }
    }
}

/**
 * Can Search
 * @author aikkeongt
 *
 */
class CanSearch
{
    public $IsOk;
    public $Error = 0;
    public $ErrorMessage;

    /**
     * Can Search
     * @param object $result
     */
    public function __construct($result)
    {
        $this->IsOk = $result->IsOk;
        if (isset($result->ErrorCode)) {
            $this->Error = $result->ErrorCode;
        }

        if (isset($result->ErrorMessage) && null != $result->ErrorMessage) {
            $this->ErrorMessage = $result->ErrorMessage;
        }
    }
}

/**
 * Dataplus Group
 * @author aikkeongt
 *
 */
class DataplusGroup
{
    public $Name;
    public $Items;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->Name = $result->GroupName;
        if (isset($result->DataplusGroupItem) && null != $result->DataplusGroupItem) {
            $this->Items =$result->DataplusGroupItem;
        }
    }
}

/**
 * Dataset
 * @author aikkeongt
 *
 */
class Dataset
{
    public $ID;
    public $Name;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->ID = $result->ID;
        $this->Name = $result->Name;
    }

    /**
     * Create Array
     * @param object $results
     * @return array
     */
    public static function CreateArray($results)
    {
        $aResults = [];

        if (null != $results) {
            if (is_array($results->DataSet)) {
                foreach ($results->DataSet as $dataset) {
                    array_push($aResults, new Dataset($dataset));
                }
            } else {
                array_push($aResults, new Layout($results->DataSet));
            }
        }

        return $aResults;
    }
}

/**
 * Example Address
 * @author aikkeongt
 *
 */
class ExampleAddress
{
    public $Address;
    public $Comment;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->Comment = $result->Comment;
        $this->Address = new FormattedAddress($result->Address);
    }

    /**
     * Create Array
     * @param object $results
     * @return array
     */
    public static function CreateArray($results)
    {
        $aResults = [];

        if (null != $results && isset($results->ExampleAddress)) {
            if (is_array($results->ExampleAddress)) {
                foreach ($results->ExampleAddress as $exampleAddress) {
                    array_push($aResults, new ExampleAddress($exampleAddress));
                }
            } else {
                array_push($aResults, new ExampleAddress($results->ExampleAddress));
            }
        }

        return $aResults;
    }
}

/**
 * Formatted Address
 * @author aikkeongt
 *
 */
class FormattedAddress
{
    public $AddressLines = null;
    public $DPVStatus;
    public $IsTruncated;
    public $IsOverFlow;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->IsOverFlow = $result->Overflow;
        $this->IsTruncated = $result->Truncated;
        $this->DPVStatus = $result->DPVStatus;
        if (isset($result->AddressLine) && null != $result->AddressLine) {
            $this->AddressLines = [];
            if (is_array($result->AddressLine)) {
                foreach ($result->AddressLine as $addressLine) {
                    array_push($this->AddressLines, new AddressLine($addressLine));
                }
            } else {
                array_push($this->AddressLines, new AddressLine($addressLine));
            }
        }
    }
}

/**
 * Layout
 * @author aikkeongt
 *
 */
class Layout
{
    public $Name;
    public $Comment;

    /**
     * Layout
     * @param unknown_type $result
     */
    public function __construct($result)
    {
        $this->Name = $result->Name;
        $this->Comment =$result->Comment;
    }

    /**
     * Create Array
     * @param object $results
     * @return array
     */
    public static function CreateArray($results)
    {
        $aResults = [];

        if (null != $results) {
            if (is_array($results->Layout)) {
                foreach ($results->Layout as $layout) {
                    array_push($aResults, new Layout($layout));
                }
            } else {
                array_push($aResults, new Layout($results->Layout));
            }
        }

        return $aResults;
    }
}

/**
 * Licensed Set
 * @author aikkeongt
 *
 */
class LicensedSet
{
    public $ID;
    public $Description;
    public $CopyRight;
    public $Version;
    public $BaseCountry;
    public $Status;
    public $Server;
    public $WarningLevel;
    public $DaysLeft;
    public $DataDaysLeft;
    public $LicenceDaysLeft;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->ID = $result->ID;
        $this->Description = $result->Description;
        $this->CopyRight = $result->Copyright;
        $this->Version = $result->Version;
        $this->BaseCountry = $result->BaseCountry;
        $this->Status = $result->Status;
        $this->Server = $result->Server;
        $this->WarningLevel = $result->WarningLevel;
        $this->DaysLeft = $result->DaysLeft;
        $this->DataDaysLeft = $result->DataDaysLeft;
        $this->LicenceDaysLeft = $result->LicenceDaysLeft;
    }

    /**
     * Create Array
     * @param object $results
     * @return array
     */
    public static function CreateArray($results)
    {
        $aResults = [];

        if (null != $results) {
            if (is_array($results->LicensedSet)) {
                foreach ($results->LicensedSet as $licenseSet) {
                    array_push($aResults, new LicensedSet($licenseSet));
                }
            } else {
                array_push($aResults, new LicensedSet($results->LicensedSet));
            }
        }

        return $aResults;
    }
}

/**
 * Picklist
 * @author aikkeongt
 *
 */
class Picklist
{
    public $Moniker;
    public $Items = null;
    public $Prompt;
    public $Total;
    public $IsAutoStepinSafe;
    public $IsAutoStepinPastClose;
    public $IsAutoformatSafe;
    public $IsAutoformatPastClose;
    public $IsLargePotential;
    public $IsMaxMatches;
    public $AreMoreOtherMatches;
    public $IsOverThreshold;
    public $IsTimeout;

    /**
     * Constructor
     * @param object $results
     */
    public function __construct($results)
    {
        $this->Total = $results->Total;
        $this->Moniker = $results->FullPicklistMoniker;
        $this->Prompt = $results->Prompt;
        $this->IsAutoStepinSafe = $results->AutoStepinSafe;
        $this->IsAutoStepinPastClose = $results->AutoStepinPastClose;
        $this->IsAutoformatSafe = $results->AutoFormatSafe;
        $this->IsAutoformatPastClose = $results->AutoFormatPastClose;
        $this->IsLargePotential = $results->LargePotential;
        $this->IsMaxMatches = $results->MaxMatches;
        $this->AreMoreOtherMatches = $results->MoreOtherMatches;
        $this->IsOverThreshold = $results->OverThreshold;
        $this->IsTimeout = $results->Timeout;

        if (isset($results->PicklistEntry) && null != $results->PicklistEntry) {
            $this->Items = [];
            if (is_array($results->PicklistEntry)) {
                foreach ($results->PicklistEntry as $picklistEntry) {
                    array_push($this->Items, new PicklistItem($picklistEntry));
                }
            } else {
                array_push($this->Items, new PicklistItem($results->PicklistEntry));
            }
        }
    }
}

/**
 * Picklist Item
 * @author aikkeongt
 *
 */
class PicklistItem
{
    public $IsAliasMatch;
    public $IsCanStep;
    public $IsCrossBorderMatch;
    public $IsDummyPOBox;
    public $IsEnhancedData;
    public $IsExtendedData;
    public $IsFullAddress;
    public $IsIncompleteAddress;
    public $IsInformation;
    public $IsMultiples;
    public $IsName;
    public $IsPhantomPrimaryPoint;
    public $IsPostcodeRecode;
    public $IsSubsidiaryData;
    public $IsUnresolvableRange;
    public $IsWarnInformation;
    public $Score;
    public $Moniker;
    public $PartialAddress;
    public $Postcode;
    public $Text;

    /**
     * Constructor
     * @param object $result
     */
    public function __construct($result)
    {
        $this->Text = $result->Picklist;
        $this->Postcode = $result->Postcode;
        $this->Score = $result->Score;
        $this->Moniker = $result->Moniker;
        $this->PartialAddress = $result->PartialAddress;
        $this->IsFullAddress = $result->FullAddress;
        $this->IsMultiples = $result->Multiples;
        $this->IsCanStep = $result->CanStep;
        $this->IsAliasMatch = $result->AliasMatch;
        $this->IsPostcodeRecode = $result->PostcodeRecoded;
        $this->IsCrossBorderMatch = $result->CrossBorderMatch;
        $this->IsDummyPOBox = $result->DummyPOBox;
        $this->IsName = $result->Name;
        $this->IsInformation = $result->Information;
        $this->IsWarnInformation = $result->WarnInformation;
        $this->IsIncompleteAddress = $result->IncompleteAddr;
        $this->IsUnresolvableRange = $result->UnresolvableRange;
        $this->IsPhantomPrimaryPoint = $result->PhantomPrimaryPoint;
        $this->IsSubsidiaryData = $result->SubsidiaryData;
        $this->IsExtendedData = $result->ExtendedData;
        $this->IsEnhancedData = $result->EnhancedData;
    }
}

/**
 * Prompt Line
 * @author aikkeongt
 *
 */
class PromptLine
{
    public $Prompt;
    public $Example;
    public $SuggestedInputLength;

    /**
     * Constructor
     * @param unknown_type $results
     */
    public function __construct($results)
    {
        $this->Prompt = $results->Prompt;
        $this->Example = $results->Example;
        $this->SuggestedInputLength = $results->SuggestedInputLength;
    }
}

/**
 * Prompt Set
 * @author aikkeongt
 *
 */
class PromptSet
{
    public $Lines;
    public $IsDynamic;

    /**
     * Constructor
     * @param object $results
     */
    public function __construct($results)
    {
        $this->IsDynamic = $results->Dynamic;
        if (isset($results->Line) && null != $results->Line) {
            $this->Lines = [];
            if (is_array($results->Line)) {
                foreach ($results->Line as $line) {
                    array_push($this->Lines, new PromptLine($line));
                }
            } else {
                array_push($this->Lines, new PromptLine($results->Line));
            }
        }
    }
}

/**
 * Search Result
 * @author aikkeongt
 *
 */
class SearchResult
{
    public $Address;
    public $Picklist;
    public $VerifyLevel;

    /**
     * Constructor
     * @param object $results
     */
    public function __construct($results)
    {
        if (isset($results->QAAddress) && null != $results->QAAddress) {
            $this->Address = new FormattedAddress($results->QAAddress);
        }

        if (isset($results->QAPicklist) && null != $results->QAPicklist) {
            $this->Picklist = new Picklist($results->QAPicklist);
        }

        $this->VerifyLevel = $results->VerifyLevel;
    }
}

/**
 * Authentication class
 * @author aikkeongt
 *
 */
class QAAuthentication
{
    private $Username;
    private $Password;

    /**
     * Constructor
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password)
    {
        $this->Username = $username;
        $this->Password = $password;
    }
}

/**
 * Query header class
 * @author aikkeongt
 *
 */
class QAQueryHeader
{
    private $QAAuthentication;
    private $Security;

    /**
     * Constructor
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password)
    {
        $this->QAAuthentication = new QAAuthentication($username, $password);
        $this->Security = null;
    }
}
