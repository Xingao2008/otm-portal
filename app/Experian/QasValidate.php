<?php

namespace OnTheMove\Experian;

use OnTheMove\Experian\Address\QASCaptureController;

/**
 * QasValidate class.
 */
class QasValidate
{
    protected $qas;
    private $qasAddressUsername;
    private $qasAddressPassword;
    // private $qasAddressNamespace;
    private $wsdl;
    public function __construct()
    {
        $this->wsdl = env("QAS_ADDRESS_WSDL");
 
        $this->qas = new QASCaptureController($this->wsdl);
        $this->qasAddressUsername = env("QAS_ADDRESS_USERNAME");
        $this->qasAddressPassword = env("QAS_ADDRESS_PASSWORD");
        // $this->qasAddressNamespace = env("QAS_ADDRESS_NAMESPACE");
    }
    
    /**
     * address function.
     *
     * @access public
     * @param mixed $searchParams
     * @return void
     */
    public function address($searchParams)
    {
        return $this->qas->InvokeJsonRpc($searchParams);
    }
}
