<?php

namespace OnTheMove\Http\ViewComposers;

use Auth;

use Illuminate\View\View;
use OnTheMove\Models\User;
use OnTheMove\Models\Agent;

use OnTheMove\Models\Customer;

class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    //protected $user;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $user
     * @return void
     */
    // public function __construct(UserRepository $user)
    // {
    //     // Dependencies automatically resolved by service container...
    //     $this->user = $user;
    // }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = request()->user('agent') ?? null;
        if (! $user) {
            $user = request()->user() ?? null;
        }
        $view->with('user', $user);
    }
}
