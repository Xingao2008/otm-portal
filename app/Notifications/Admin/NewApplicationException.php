<?php

namespace OnTheMove\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use OnTheMove\Models\ApplicationException;

class NewApplicationException extends Notification
{
    use Queueable;

    public $exception;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ApplicationException $exception)
    {
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New Application Exception')
                    ->greeting('Hi ' . str_before($notifiable->name, " ") . ',')
                    ->line("There's a new Application that's been received that need to be mapped before it can be sent to Flowbiz!")
                    ->line("It's been marked as an exception becuase: " . $this->exception->type)
                    ->action('Create Mapping', route('admin.application.match', $this->exception, true));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
