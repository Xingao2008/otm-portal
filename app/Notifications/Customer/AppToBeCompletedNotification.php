<?php

namespace OnTheMove\Notifications\Customer;

use Illuminate\Bus\Queueable;
use OnTheMove\Models\Application;
use Illuminate\Support\Facades\URL;

use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;
use Illuminate\Notifications\Channels\MailChannel;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

class AppToBeCompletedNotification extends Notification
{
    // use Queueable;

    protected $app;
    protected $customer;


    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->customer = $app->customers->first();
    }

    public function via($notifiable)
    {
        //return [TwilioChannel::class];
        return ['mail', 'nexmo'];
    }


    public function routeNotificationForMail($notification)
    {
        return $this->customer->email;
    }

    public function routeNotificationForTwilio($notification)
    {
        return $this->customer->e164_mobile;
    }
    public function routeNotificationForNexmo($notification)
    {
        return $this->customer->e164_mobile;
    }

  

    public function toMail($notifiable)
    {
        $rea = $this->app->realEstateAgent;
        $cust = $this->customer;
        $date = optional($this->app->connection_date)->format('l, jS \o\f F');

        return (new MailMessage)
                    ->subject("$cust->firstname, get your utilities connected for $date")
                    ->greeting("Hi $cust->firstname,")
                    ->line("Your real estate agent $rea->name from $rea->agency has referred you to On The Move on your behalf.")
                    ->line("On The Move is a free utilities connection service, that can help you ensure your power is on by $date - plus much more!")
                    ->action('Get your Utilities Connected', URL::signedRoute('auth.customer.login', ['customer' => $cust->id, 'application' => $cust->applications->first()->id]));
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioSmsMessage())
            ->content("Your account was approved!");
    }

    public function toNexmo($notifiable)
    {
        $rea = $this->app->realEstateAgent;
        $cust = $this->customer;
        $date = optional($this->app->connection_date)->format('l, jS \o\f F');
        $url = url()->signedRoute('auth.customer.login', ['customer' => $this->customer->id, 'application' => $this->app->id]);

        return (new NexmoMessage)
                ->content("$rea->agency: $cust->firstname, get your utilities connected for $date. Get connected: $url");
    }
}
