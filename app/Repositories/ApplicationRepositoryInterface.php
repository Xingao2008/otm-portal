<?php

namespace OnTheMove\Repositories;

use OnTheMove\Models\Application;

interface ApplicationRepositoryInterface
{
    public function get(Application $application);
    public function store(array $data);
    public function search($params);
}
