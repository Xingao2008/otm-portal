<?php

namespace OnTheMove\Repositories;

use Log;
use Auth;
use Bugsnag;
use Exception;
use Notification;
use Carbon\Carbon;

use OnTheMove\Models\User;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;
use OnTheMove\Models\ExceptionMapping;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\Models\ApplicationException;
use OnTheMove\Exceptions\ApplicationDuplicateException;

use OnTheMove\Notifications\Admin\NewApplicationException;

class ConnectorRepository extends BaseRepository
{
    public function store(array $data)
    {
        try {
            $app = $this->findOrCreateApplication($data);
        
            if (! $app instanceof Application) {
                return false;
            }

            if (isset($data['tenants'])) {
                $data['customers'] = $data['tenants'];
            } elseif (isset($data['tenents'])) {
                $data['customers'] = $data['tenents'];
            } elseif (isset($data['applicants'])) {
                $data['customers'] = $data['applicants'];
            }
            
            $app->application_type_id = $this->getApplicationType($data);

            $applicants = $this->createOrUpdateCustomer($app, $data);

            $agent = $this->getAgentForApplication($app, $data);
            
            $user = $this->getUserForApplication();
        
            $app = $this->createOrUpdateApplication($app, $applicants, $agent, $user, $data);
    
            //$app = $this->createOrUpdateProducts($app, $data);
        

            return $app->load(['customers', 'realEstateAgent', 'services']);

        } catch (ApplicationDuplicateException $e) {
            Log::critical('Duplicate Application Received');
            Bugsnag::notifyException($e);

            return $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            Log::error("New External API App Failed");

            return $e->getMessage();
        }
    }

    protected function getUserForApplication()
    {
        return Auth::guard('user-api')->user() ?? null;
    }

    protected function findOrCreateApplication(array $data)
    {
        $checksum = sha1(json_encode($data));
        $app = Application::whereChecksum($checksum)->first();

        if ($app) {
            throw new ApplicationDuplicateException("Duplicate application");
        }

        if (isset($data['id'])) {
            $app = Application::findOrFail($data['id']);
        } else {
            $app = new Application();
            $app->checksum = sha1(json_encode($data));
        }

        return $app;
    }

    protected function getApplicationConnectionDate(Application $app, array $data)
    {
        if (isset($data["connection_date"])) {
            return Carbon::createFromFormat('Y-m-d', $data["connection_date"]);
        } else {
            $this->storeAndSendException($app, "missing connection date", json_encode($data));

            return null;
        }
    }

    protected function setApplicationDetails(Application $app, array $data)
    {
        $app->connection_date = $this->getApplicationConnectionDate($app, $data);

        $app->customer_eic        = $data['contact_consent_given'] ?? 1;
        $app->notes               = $data["notes"] ?? null;
        $app->renter              = $data["renter"] ?? null;

        $app->flowbiz_exported = $app::READY_FOR_EXPORT;
        
        $app->property_type       = $data["address"]["property_type"] ?? "House"; // can't be null
        $app->unit_number         = $data["address"]["unit_number"] ?? null;
        $app->street_number       = $data["address"]["street_number"] ?? null;
        $app->street_name         = $data["address"]["street_name"] ?? null;
        $app->street_type         = $data["address"]["street_type"] ?? null;
        $app->suburb              = $data["address"]["suburb"] ?? null;
        $app->postcode            = $data["address"]["postcode"] ?? null;
        $app->state               = $data["address"]["state"] ?? null;
        $app->dpid                = $data["address"]["dpid"] ?? null;

        return $app;
    }
    // setApplicationDetails

    protected function getAgentForApplication(Application $app, $data)
    {

        // try find pm
        // else try and find rea, then find pm
        // else find existing exception mapping
        // else save app and assoicate application exception

        $pm = $data['property_manager'];
        $rea = $data['real_estate_agency'];

        $agent = Agent::findAgent($pm)->get();
        
        if ($agent->count() === 1) {
            return $agent->first();
        }
        
        if(isset($rea['id'])) {
            $agency = RealEstateAgency::whereCompanyId($rea['otm_id'])->get();
        } else {
            $agency = RealEstateAgency::whereCompanyName($rea['company_name'])->get();
        }

        if ($agency->count() !== 0) {
           
            $agent = Agent::where('first_name', 'LIKE', "%" . $pm['firstname'] . "%")
                            ->whereIn('company_id', $agency->pluck('company_id')->toArray())
                            ->get();
            
            if ($agent->count() === 1) {
                return $agent->first();
            }
            
        }
        $payload = json_encode([$pm, $rea]);

        $mapping = ExceptionMapping::wherePayload($payload)->first();
        
        if ($mapping) {
            return Agent::find($mapping->key);
        }

        
        // gotta save.. so we can associate an exception against it
        $app->save();
       
        $this->storeAndSendException($app, "missing property manager", $payload);

        return null;
    }

    protected function storeOrUpdateCustomers(Application $app, Customer $customer, array $applicant)
    {
        $applicant = array_filter($applicant);

        $customer->title      = $applicant['title'] ?? null;
        $customer->firstname  = $applicant['firstname'] ?? null;
        $customer->lastname   = $applicant['lastname'] ?? null;
        $customer->dob        = (isset($applicant["dob"])) ? Carbon::createFromFormat('Y-m-d', $applicant["dob"]) : null;
        $customer->email      = $applicant['email'] ?? null;
        $customer->mobile     = $applicant['mobile'] ?? null;
        $customer->phone      = $applicant['phone'] ?? null;
        $customer->occupation = $applicant['occupation'] ?? null;
        $customer->notes      = $applicant['notes'] ?? null;

        $customer->save();

        if (! $customer->email) {
            $app->save();
            $this->storeAndSendException($app, "missing tenant email", $customer->toJson());
        }
        if (! $customer->mobile) {
            $app->save();
            $this->storeAndSendException($app, "missing tenant mobile", $customer->toJson());
        }

        return $customer;
    }

    
    private function storeAndSendException(Application $app, string $type, $payload)
    {
        $exception = new ApplicationException();

        $exception->type = $type;
        $exception->payload = $payload;

        $exception->application()->associate($app);
       
        $exception->save();
        
        // A dozen better ways to do this, but this'll do
        $users = User::whereHas('role', function ($query) {
            $query->where('name', 'staff');
        })->get();

        if (env('APP_ENV') === 'production') {
            // only send if in prod
            Notification::send($users, new NewApplicationException($exception));
        }

        return $exception;
    }
}
