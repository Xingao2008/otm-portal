<?php

namespace OnTheMove\Repositories;

use Log;
use Auth;
use Bugsnag;
use Exception;
use Carbon\Carbon;

use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\User;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Product;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Concession;

use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use Illuminate\Support\Collection;
use OnTheMove\Models\Identification;
use Illuminate\Foundation\Bus\DispatchesJobs;
use OnTheMove\Transformers\ApplicationTransformer;

class BaseRepository implements ApplicationRepositoryInterface
{
    use DispatchesJobs;


    public function indexAgent()
    {
        if (! Auth::guard('agent')->check()) {
            return false;
        }

        $user = Auth::guard('agent')->user();

        $apps = new \stdClass;
        $apps->complete = $user->completeApplications()->paginate(10, ['*'], 'complete');
        $apps->incomplete = $user->incompleteApplications()->paginate(10, ['*'], 'incomplete');

        return $apps;
    }

    public function get(Application $app)
    {
        return $app->load(['customers', 'realEstateAgent', 'services']);
    }
    
    public function store(array $data)
    {
        // optionally create customer in Controller,
        // and log them in as the auth'd user

        // Auth::user -> customer()

        //$customer = Auth::user()->customer();
        //var_dump($data);

        $app = $this->findOrCreateApplication($data);

        $app->application_type_id = $this->getApplicationType($data);

        $applicants = $this->createOrUpdateCustomer($app, $data);

        $agent = $this->getAgentForApplication($app, $data);
        
        $app = $this->createOrUpdateApplication($app, $applicants, $agent, null, $data);
    
        $app = $this->createOrUpdateProducts($app, $data);
        

        return $app->load(['customers', 'realEstateAgent', 'services']);
    }

    public function update(Application $app, $data)
    {
        return $app;
    }

    /**
     * Export and application to the Flowbiz API
     *
     * @param  Application $app The app to export
     * @return Application           the app
     */
    public function export(Application $app, $timeout = 60)
    {
        $flowbiz = new Api($timeout);
        $transformer = new ApplicationTransformer();

        $data = $transformer->buildFlowbizDataArray($app);

        try {
            $response = $flowbiz->submitApplication($data);
            Log::debug("Flowbiz Response", ["response" => $response]);
            if (! $response || gettype($response) === 'object') {
                try {
                    throw new Exception($response);
                } catch (Exception $e) {
                    Bugsnag::notifyException($e);
                }

                $app->flowbiz_exported = $app::READY_FOR_EXPORT;
                $app->save();
            } else {
                $app->flowbiz_exported           = $app::EXPORTED_TO_FLOWBIZ;
                $app->flowbiz_application_id     = $response['Results']['Reference'];
                $app->flowbiz_application_status = $response['Results']['Status'];

                $app->save();
            }

            return $app;
        } catch (Exception $e) {
            $app->flowbiz_exported = $app::READY_FOR_EXPORT;
            $app->save();

            Log::error("FlowBiz API error", ['error' => $e->getMessage()]);

            return false;
        }
    }

    /**
     * Search for Applications
     *
     * @param Array $params     Search params
     * @return Collection       A collection of results
     */
    public function search($params)
    {
        $api = new Api(8);

        if (! isset($params['AgentId'])) {
            $params['AgentId'] = Auth::guard('agent')->user()->agent_id ?? null;
            $params['View'] = "PM";
        }

        $response = $api->searchApplications($params);

        $res = $response['Results'];

        $results = collect();
        foreach ($res as $result) {
            $result = (object) $result;

            // Add some flavor
            $result->ConnectionDate = Carbon::createFromFormat('Y-m-d', $result->ConnectionDate);
            $result->agent = Agent::where('agent_id', $result->AgentId)->first() ?? null;

            //dump($result);
            // Add to collection
            $results->push($result);
        }

        return $results->sortByDesc('ConnectionDate');
    }

    protected function findOrCreateApplication(array $data)
    {
        if (isset($data['id'])) {
            $app = Application::findOrFail($data['id']);
        } else {
            $app = new Application();
        }

        return $app;
    }

    protected function createOrUpdateProducts(Application $app, array $data)
    {
        if (isset($data["products"])) {
            foreach ($data["products"] as $service) {

                // create service type
                // attach service to app
                $app = $this->storeServices($app, $service);
            }
        }

        return $app;
    }

    protected function getAgentForApplication(Application $app, $data)
    {
        if (Auth::guard('agent')->check()) {
            return Auth::guard('agent')->user();
        }
        
        if (Auth::guard('agent-api')->check()) {
            return Auth::guard('agent-api')->user();
        }
        
        return Agent::find((int) $data["agent_id"]);
    }

    protected function storeOrUpdateCustomers(Application $app, Customer $customer, array $applicant)
    {
        $applicant = array_filter($applicant);

        $customer->title      = $applicant['title'] ?? null;
        $customer->firstname  = $applicant['firstname'] ?? null;
        $customer->lastname   = $applicant['lastname'] ?? null;
        $customer->dob        = (isset($applicant["dob"])) ? Carbon::createFromFormat('Y-m-d', $applicant["dob"])->format('Y-m-d') : null;
        $customer->email      = $applicant['email'] ?? null;
        $customer->mobile     = $applicant['mobile'] ?? null;
        $customer->phone      = $applicant['phone'] ?? null;
        $customer->occupation = $applicant['occupation'] ?? null;
        $customer->notes      = $applicant['notes'] ?? null;

        $customer->save();

        return $customer;
    }

    protected function createOrUpdateCustomer(Application $app, array $data)
    {
        $applicants = collect();
        foreach ($data['customers'] as $applicant) {

            $customer = new Customer();

            $customer = $this->storeOrUpdateCustomers($app, $customer, $applicant);
            
            $customer = $this->storeIdentifications($customer, $applicant);
            

            $applicants->push($customer);
        }
        $app->primary_customer_id = $applicants->first()->id;

        return $applicants;
    }

    protected function storeIdentifications(Customer $customer, array $applicant)
    {
        // @TODO Fix me
        if (isset($applicant['identifications'])) {
            if (count($applicant['identifications']) !== 0) {
                foreach ($applicant['identifications'] as $id) {
                    if (! isset($id['category'])) {
                        $customer = $this->storeCustomerIds($customer, $id, 'identification');
                    } else {
                        if (in_array(strtolower($id['category']), ['identification', 'concession'])) {
                            $customer = $this->storeCustomerIds($customer, $id, strtolower($id['category']));
                        } else {
                            $customer = $this->storeCustomerIds($customer, $id, 'identification');
                        }
                    }
                }
            }
        }
        
        if (isset($applicant['concession'])) {
            if (count($applicant['concession']) !== 0) {
                $customer = $this->storeCustomerIds($customer, $applicant['concession'], 'concession');
            }
        }

        return $customer;
    }

    protected function getApplicationType(array $data)
    {
        return (int) $data["application_type_id"] ?? 1;
    }
    protected function getApplicationConnectionDate(Application $app, array $data)
    {
        if (isset($data["connection_date"])) {
            return Carbon::createFromFormat('Y-m-d', $data["connection_date"]);
        } else {
            return null;
        }

        // @TODO use the optional helper, rather than the above
        // $app->connection_date = optional($data["connection_date"], function ($data) {
        //     return Carbon::createFromFormat('d/m/Y', $data["connection_date"]);
        // });
    }
    protected function setApplicationDetails(Application $app, array $data)
    {
        $app->connection_date = $this->getApplicationConnectionDate($app, $data);

        $app->notes               = $data["notes"] ?? null;
        $app->renter              = $data["renter"] ?? null;

        $app->property_type       = $data["address"]["split"]["property_type"] ?? "House"; // can't be null
        $app->unit_number         = $data["address"]["split"]["unit_number"] ?? null;
        $app->street_number       = $data["address"]["split"]["street_number"] ?? null;
        $app->street_name         = $data["address"]["split"]["street_name"] ?? null;
        $app->street_type         = $data["address"]["split"]["street_type"] ?? null;
        $app->suburb              = $data["address"]["split"]["suburb"] ?? null;
        $app->postcode            = $data["address"]["split"]["postcode"] ?? null;
        $app->state               = $data["address"]["split"]["state"] ?? null;
        $app->dpid                = $data["address"]["split"]["dpid"] ?? null;

        return $app;
    }

    protected function createOrUpdateApplication(Application $app, Collection $applicants, Agent $agent = null, User $user = null, array $data)
    {
        if ($agent) {
            $app->agent_id = $agent->id;
        }

        if ($user) {
            $app->user_id = $user->id;
        }

        $app = $this->setApplicationDetails($app, $data);
    
        $app->save();
        //dump($app->toArray());
        $ids = $applicants->map(function ($applicant) {
            return $applicant->id;
        });

        $app->customers()->sync($ids->toArray());

        return $app;
    }

    protected function storeCustomerIds(Customer $customer, $data, $type)
    {
        $className = "OnTheMove\Models\\" . ucwords($type);

        $id = new $className;

        $id->type = $data["type"] ?? null;
        $id->number = $data["number"] ?? null;
        try {
            $id->expiry = Carbon::createFromFormat('Y-m-d', $data["expiry"]) ?? null;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $id->expiry =  null;
            //Log::debug("Can't pass service connection date");
        }
        
        $id->issuer = $data["issuer"] ?? null;

        $id->save();

        $cid = new CustomerId();
        $cid->customer_id = $customer->id;
        $cid->type_type = get_class($id);
        $cid->type_id = $id->id;

        $cid->type()->attach($id->id);

        $customer->identifications()->save($cid);

        return $customer;
    }


    protected function storeServices(Application $app, $service)
    {
        $product = Product::whereFlowbizId($service['product_id'])->firstOrFail();
        $className = "OnTheMove\Models\\" . ucwords($product->type) . "Service";

        $serv = new $className;

        // @TODO This is not elegant.
        // @FIXME Be more elegant.
        $keys = $serv->getAllAttributes();

        $service = $this->parseServiceDetails($product, $service);

        foreach ($keys as $key) {
            if (isset($service['details'][$key])) {
                $serv->{$key} = $service['details'][$key];
            }
        }

        $serv->save();

        $s = new Service();
        $s->product_id = $product->id;
        try {
            $s->connection_date = Carbon::createFromFormat('Y-m-d', $service["connection_date"]);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $s->connection_date = null;
            //Log::debug("Can't pass service connection date");
        }
        $s->details_type = get_class($serv);
        $s->details_id = $serv->id;

        //dump($s);
        $s->details()->attach($serv->id);

        $app->services()->save($s);

        return $app;
    }

    // hook to manipulate the service details
    protected function parseServiceDetails(Product $product, array $details)
    {
        return $details;
    }
}
