<?php

namespace OnTheMove\Repositories;

use Log;
use Auth;
use Carbon\Carbon;

use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;
use OnTheMove\Models\Product;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Concession;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use Illuminate\Support\Collection;
use OnTheMove\Models\Identification;
use Illuminate\Foundation\Bus\DispatchesJobs;
use OnTheMove\Transformers\ApplicationTransformer;

class ApplicationRepository extends BaseRepository
{
    use DispatchesJobs;


    public function indexAgent()
    {
        if (! Auth::guard('agent')->check()) {
            return false;
        }

        $user = Auth::guard('agent')->user();

        $apps = new \stdClass;
       
        //dd($user->completeApplications()->paginate(10, ['*'], 'complete'));
        $apps->complete = $user->completeApplications()->paginate(10, ['*'], 'complete');
        $apps->incomplete = $user->incompleteApplications()->paginate(10, ['*'], 'incomplete');

        return $apps;
    }

    public function store(array $data)
    {
        Log::debug("Frontend Data", $data);

        $app = $this->findOrCreateApplication($data);

        $app->application_type_id = $this->getApplicationType($data['details']);
        $data['customers'] = $data['applicants'];

        $applicants = $this->createOrUpdateCustomer($app, $data);

        $agent = $this->getAgentForApplication($app, $data);
        
        $app = $this->createOrUpdateApplication($app, $applicants, $agent, null, $data);
    
        $app = $this->createOrUpdateProducts($app, $data);
        
        return $app->load(['customers', 'realEstateAgent', 'services']);
    }

    protected function getAgentForApplication(Application $app, $data)
    {
        if (isset($data['details']['agent_id'])) {
            $agent = Agent::whereAgentId((int) $data['details']['agent_id'])->first();
            if ($agent) {
                return $agent;
            }
        }
        if (Auth::guard('agent')->check()) {
            return Auth::guard('agent')->user();
        }
        
        if (Auth::guard('agent-api')->check()) {
            return Auth::guard('agent-api')->user();
        }
        
        return Agent::find((int) $data["agent_id"]);
    }

    protected function setApplicationDetails(Application $app, array $data)
    {
        $app->customer_eic = $data['contact_consent_given'] ?? null;

        $details = $data['details'];

        $app->connection_date = $this->getApplicationConnectionDate($app, $details);
        
        $app->notes  = $details["notes"] ?? null;
        $app->renter = $details["renter"] ?? null;
        
        // Api\ApplicationController@store will trigger the export,
        // or default back to READY_TO_EXPORT upon exception/failure
        $app->flowbiz_exported = $app::NOT_READY_FOR_EXPORT;

        if ($app->application_type_id === 2) {
            $app->business_name = $details["business"]["name"] ?? null;
            $app->business_abn  = $details["business"]["abn"] ?? null;
        }
        $addr = $details["address"]["split"];
        $app->property_type = $addr["property_type"] ?? "House"; // can't be null
        $app->unit_number   = $addr["unit_number"] ?? null;
        $app->street_number = $addr["street_number"] ?? null;
        $app->street_name   = $addr["street_name"] ?? null;
        $app->street_type   = $addr["street_type"] ?? null;
        $app->suburb        = $addr["suburb"] ?? null;
        $app->postcode      = $addr["postcode"] ?? null;
        $app->state         = $addr["state"] ?? null;
        $app->dpid          = $addr["dpid"] ?? null;

        return $app;
    }
}
