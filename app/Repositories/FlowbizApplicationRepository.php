<?php

namespace OnTheMove\Repositories;

use Log;
use Bugsnag;
use Exception;
use Carbon\Carbon;

use OnTheMove\Models\Agent;
use OnTheMove\Models\Product;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Concession;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;
use Illuminate\Support\Collection;
use OnTheMove\Models\Identification;

use OnTheMove\Models\ApplicationType;
use Illuminate\Foundation\Bus\DispatchesJobs;

use OnTheMove\Jobs\ImportOrUpdateFlowbizApplication;

class FlowbizApplicationRepository extends BaseRepository
{
    use DispatchesJobs;

    public function updateFromFlowbiz(Application $app, $data)
    {
        $app->flowbiz_application_status = $data['Application']['ApplicationStatus'] ?? $app->flowbiz_application_status;
        $app->save();

        try {
            $app->application_type_id = $this->getApplicationType($data);
            $applicants = $this->createOrUpdateCustomer($app, $data);
            $agent = $this->getAgentForApplication($app, $data);
            $app = $this->createOrUpdateApplication($app, $applicants, $agent, null, $data);
            $app = $this->createOrUpdateProducts($app, $data);
        } catch (Exception $e) {
            Log::error('Update from Flowbiz failed', ['app' => $app->flowbiz_application_id, 'error' => $e->getMessage()]);
            Bugsnag::notifyException($e);
        }

        return $app;
    }

    public function createFromFlowbiz($appId, $details)
    {
        return $this->store($details);
    }

    public function retreiveFromFlowbiz($appId)
    {
        $job = new ImportOrUpdateFlowbizApplication($appId);
        $this->dispatchNow($job);
        
        return $job->getResponse();
    }

   
    protected function storeServices(Application $app, $service)
    {
        $product = Product::whereFlowbizId($service['product_id'])->firstOrFail();

        $existingServices = $app->services()->get();
        $className = "OnTheMove\Models\\" . ucwords($product->type) . "Service";
        $found = false;
        foreach ($existingServices as $existingService) {
            $existing = get_class($existingService->details);
          
            if ($existing === $className) {
                $found = true;
                $serv = $existingService->details;
                $s = $existingService;
                break;
            }
        }
        
        if (! $found) {
            $serv = new $className;
        }
        

        // @TODO This is not elegant.
        // @FIXME Be more elegant.
        $keys = $serv->getAllAttributes();

        $service = $this->parseServiceDetails($product, $service);
        
        foreach ($keys as $key) {
            if (isset($service['details'][$key])) {
                $serv->{$key} = $service['details'][$key];
            }
        }
        

        $serv->save();

        if (! $found) {
            $s = new Service();
        }
        $s->product_id = $product->id;
        try {
            $s->connection_date = Carbon::createFromFormat('Y-m-d', $service["connection_date"]);
        } catch (Exception $e) {
            $s->connection_date = null;
            //Log::debug("Can't pass service connection date");
        }
        $s->details_type = get_class($serv);
        $s->details_id = $serv->id;

        //dump($s);
        if (! $found) {
            $s->details()->attach($serv->id);
        
            $app->services()->save($s);
        } else {
            $s->save();
        }

        return $app;
    }


    protected function getAgentForApplication(Application $app, $data)
    {
        $agent = Agent::where('agent_id', $data['Application']["PropertyManagerID"])->first();
        if (! $agent) {
            Log::info("No agent found for imported flowbiz application", $data);
        }

        return $agent;
        //return Agent::where('agent_id', $data['Application']["PropertyManagerID"])->firstOrFail();
    }

    protected function getApplicationType(array $data)
    {
        return ApplicationType::whereType($data['Application']['ApplicationType'])->first()->id ?? 1;
    }

    protected function createOrUpdateCustomer(Application $app, array $data)
    {
        $applicants = collect();

        foreach ($data['Applicants'] as $applicant) {
            $applicant = $this->snakeCaseArrayKeys($applicant);

            // is all the applicants details blank?
            if (in_array(count(array_filter($applicant)), [0,1])) {
                continue;
            }

            if ($app->customers->count() !== 0) {
                $customer = $app->customers->where('mobile', $applicant['mobile']);

            } else {
                $customer = Customer::where([
                    'email' => $applicant['email'],
                    'mobile' => $applicant['mobile'],
                ])->orWhere([
                    'lastname' => $applicant['lastname'],
                    'mobile' => $applicant['mobile'],
                ])->orWhere([
                    'lastname' => $applicant['lastname'],
                    'phone' => $applicant['phone'],
                ])->get();
            }



            if ($customer->count() > 1) {
                $customer = $customer->where(
                    'firstname',
                    $applicant['firstname']
                )->first();
            } elseif ($customer->count() === 1) {
                $customer = $customer->first();
            }


            if (! $customer || $customer->count() === 0) {
                if ($app->flowbiz_application_id) {
                    Log::debug("Could not find a customer for app: $app->flowbiz_application_id");
                }
                $customer = new Customer();
            }
            
            $customer = $this->storeOrUpdateCustomers($app, $customer, $applicant);
            $customer = $this->storeIdentifications($customer, $applicant);
            //$customer->save();

            $applicants->push($customer);
        }

        if($applicants->count() === 0) {
            throw new Exception("No applicants available to be stored for application");
        }

        $app->primary_customer_id = $applicants->first()->id;

        return $applicants;
    }

    protected function createOrUpdateProducts(Application $app, array $data)
    {
        if (isset($data["Products"])) {
            if (count($data["Products"]) !== 0) {
                foreach ($data["Products"] as $service) {
                    $service = $this->snakeCaseArrayKeys($service);
                  
                    $product = Product::whereFlowbizId($service['id'])->first();
                    
            
                    // safety first, kiddos
                    if ($product) {
                        $service['product_id'] = $product->flowbiz_id;
                    } else {
                        continue;
                    }

                    if (! isset($service['connection_details'])) {
                        continue;
                    }

                    if ($service['connection_details'] === null) {
                        continue;
                    }

                    if (! isset($service['connection_details']['identifications'])) {
                        unset($service['connection_details']['identifications']);
                    }

                    $service['details'] = $this->snakeCaseArrayKeys($service['connection_details']);
                    unset($service['connection_details']);

                    //dump($service);

                    // create service type
                    // attach service to app
                    $app = $this->storeServices($app, $service);
                }
            }
        }

        return $app;
    }
    protected function getApplicationConnectionDate(Application $app, array $data)
    {
        if (isset($data["Application"]["ConnectionDate"])) {
            if ($data["Application"]["ConnectionDate"] !== "") {
                return Carbon::createFromFormat('Y-m-d', $data["Application"]["ConnectionDate"])->format('Y-m-d');
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    protected function setApplicationDetails(Application $app, array $data)
    {
        $app->connection_date = $this->getApplicationConnectionDate($app, $data);

        $app->renter = $data["Application"]["Renter"] ?? null;
        $app->notes  = $data["Application"]["Notes"] ?? null;
        
        // a imported app won't have a app ID to begin with
        if(!$app->flowbiz_application_id) {
            $app->flowbiz_exported = $app::IMPORTED_FROM_FLOWBIZ;
        }

        $app->flowbiz_application_id     = $data["Application"]["ApplicationReference"] ?? null;
        $app->flowbiz_application_status = $data["Application"]["ApplicationStatus"] ?? null;

        $data["Address"]["State"] = $this->parseState($data["Address"]["State"]);
        //    dump($data["Address"]);

        $app->property_type       = $data["Address"]["PropertyType"] ?? "House"; // can't be null
        $app->unit_number         = $data["Address"]["UnitNumber"] ?? null;
        $app->street_number       = $data["Address"]["StreetNumber"] ?? null;
        $app->street_name         = $data["Address"]["StreetName"] ?? null;
        $app->street_type         = $data["Address"]["StreetType"] ?? null;
        $app->suburb              = $data["Address"]["Suburb"] ?? null;
        $app->postcode            = (int) $data["Address"]["Postcode"] ?? null;
        $app->state               = $data["Address"]["State"] ?? null;
        $app->dpid                = $data["Address"]["DPID"] ?? null;

        return $app;
    }
    protected function storeIdentifications(Customer $customer, array $applicant)
    {
    
        // @TODO Fix me
        if (isset($applicant['identifications'])) {
            if (count($applicant['identifications']) !== 0) {
                foreach ($applicant['identifications'] as $id) {
                    $id = $this->snakeCaseArrayKeys($id);
                    if ($id['id_type'] === 'Other') {
                        continue;
                    }
                    $type = ($id['id_type'] === 'Concession') ? 'concession' : 'identification';

                    $customer = $this->storeOrUpdateCustomerIds($customer, $id, $type);
                }
            }
        }

        return $customer;
    }

    protected function storeOrUpdateCustomerIds(Customer $customer, $data, $type)
    {
        
        //dd($data, $type, $customer->identifications()->take(2)->toArray());
        $exisitngId = false;
        foreach ($customer->identifications as $cid) {
            if ($type === 'identification') {
                if (! $cid->type->category) {
                    continue;
                }
                
                if ($cid->type->category === $data['id_type']) {
                    $exisitngId = $cid->type;

                    // dd($cid->toArray(), $exisitngId->toArray());
                    break;
                }
            } elseif ($type === 'concession') {
                if (! $cid->type->type) {
                    continue;
                }

                if ($cid->type->type === $data['id_type']) {
                    $exisitngId = $cid->type;
                    break;
                }
            }
        }

        // dd($data, $type, $customer->identifications->toArray());


        if (! $exisitngId) {
            $className = "OnTheMove\Models\\" . ucwords($type);

            $id = new $className;
        } else {
            $id = $exisitngId;
        }

        // remove empty items, to make them null via null coalese
        $data = array_filter($data);
        // dump($data);
        $id->type = $data["type"] ?? null;
        $id->number = $data["number"] ?? null;
        $id->expiry = (isset($data["expiry"])) ? Carbon::createFromFormat('Y-m-d', $data["expiry"]) : null;
        $id->issuer = $data["issuer"] ?? null;

        $id->save();

        if (! $exisitngId) {
            $cid = new CustomerId();
            $cid->customer_id = $customer->id;
            $cid->type_type = get_class($id);
            $cid->type_id = $id->id;

            $cid->type()->attach($id->id);

            $customer->identifications()->save($cid);
        }

        return $customer;
    }

    
    protected function parseServiceDetails(Product $product, array $details)
    {
        if (strtolower($product->type) === 'internet') {

            // if(isset($details['details'])) {
            //    $details = $details['details'];
            // }
            if (isset($details['details']['notes'])) {
                // a very bad idea...
                $details['details']['notes'] = addslashes($details['details']['notes']);
            }
            if (isset($details['details']['current_employment_duration'])) {
                $duration = $details['details']['current_employment_duration'];
                $details['details']['current_employment_duration'] =  $duration['Year'] * 12;
                $details['details']['current_employment_duration'] +=  $duration['Month'];
            }
            if (isset($details['details']['previous_employment_duration'])) {
                $duration = $details['details']['previous_employment_duration'];
                $details['details']['previous_employment_duration'] =  $duration['Year'] * 12;
                $details['details']['previous_employment_duration'] +=  $duration['Month'];
            }
        }

        return $details;
    }

    private function parseState($state)
    {
        switch (strtoupper($state)) {
                case "VICTORIA":
                    $state = 'VIC';
                break;
                case "NEW SOUTH WALES":
                    $state = 'NSW';
                break;
                case "QUUENSLAND":
                    $state = 'QLD';
                break;
                case "TASMANIA":
                    $state = 'TAS';
                break;
                case "AUSTRALIAN CAPITAL TERRITORY":
                    $state = 'ACT';
                break;
                case "SOUTH AUSTRALIA":
                    $state = 'SA';
                break;
                case "WESTERN AUSTRALIA":
                    $state = 'WA';
                break;
                case "NORTHEN TERRITORY":
                    $state = 'NT';
                break;
                default:
                    $state = ""; // can't fix stupid
                break;
            }

        return $state;
    }

    /**
     * Flowbiz loves a Studly Case For All It's Keys, but
     * we do not. Let's transform that for us, and
     * keep our database clean-ish
     *
     * @param  Array  $array the array to be transformed
     * @return Array         the transformed array
     */
    private function snakeCaseArrayKeys(array $array)
    {
        $arr = [];
        foreach ($array as $key => $value) {
            if (in_array($key, ['NMI', 'MIRN', 'DOB', 'ID', 'DPID'])) {
                $key = strtolower($key);
            } else {
                $key = snake_case($key);
            }
            $arr[$key] = $value;
        }

        return $arr;
    }
}
