<?php

namespace OnTheMove\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use OnTheMove\Http\ViewComposers\UserComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', UserComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
