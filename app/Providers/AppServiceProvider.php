<?php

namespace OnTheMove\Providers;


use Illuminate\Support\Facades\Blade;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // @env / @endenv
        Blade::if('env', function ($environment) {
            return app()->environment($environment);
        });

        // https://josephsilber.com/posts/2018/07/02/eloquent-polymorphic-relations-morph-map
        // Relation::morphMap([
        //     'customers' => 'App\Customer',
        //     'warehouses' => 'App\Warehouse',
        // ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
