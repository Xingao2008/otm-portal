<?php

namespace OnTheMove\Providers;

use OnTheMove\Models\Agent;
use OnTheMove\Models\Product;
use OnTheMove\Models\Service;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Resource;
use OnTheMove\Models\Application;
use Illuminate\Support\Facades\Route;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\Models\ApplicationException;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'OnTheMove\Http\Controllers';
    protected $oAuthNamespace = 'Laravel\Passport\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Route::bind('agent', function ($value) {
        //     return Agent::whereAgentId($value)->first() ?? abort(404);
        // });

        Route::model('agent', Agent::class);
        Route::model('pm', Agent::class);
        Route::model('rea', RealEstateAgency::class);
        Route::model('customer', Customer::class);
        Route::model('application', Application::class);
        Route::model('application_exception', ApplicationException::class);
        Route::model('product', Product::class);
        Route::model('service', Service::class);
        Route::model('resource', Resource::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapOAuthRoutes();

        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapHeartbeatRoutes();

        $this->mapCronRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/agents.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/admins.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            //  ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapOAuthRoutes()
    {
        Route::namespace($this->oAuthNamespace)
             ->group(base_path('routes/oauth.php'));
    }


    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapHeartbeatRoutes()
    {
        Route::prefix('heartbeat')->name('heartbeat.')
             ->group(base_path('routes/heartbeats.php'));
    }

    /**
     * Crons
     */
    protected function mapCronRoutes()
    {
        Route::namespace($this->namespace)
            ->group(base_path('routes/cron.php'));
    }
}
