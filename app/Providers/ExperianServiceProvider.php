<?php

namespace OnTheMove\Providers;

use OnTheMove\Experian\QasValidate;
use Illuminate\Support\ServiceProvider;

class ExperianServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    //public function boot()
    //{
    //
    //}

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('QasValidate', 'OnTheMove\Experian\QasValidate');
    }
}
