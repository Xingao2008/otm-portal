<?php

namespace OnTheMove\Providers;

use OnTheMove\Models\Agent;
use Laravel\Passport\Passport;
use Gate;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;
use Illuminate\Support\Facades\Auth;

use OnTheMove\Policies\Agent\ApplicationPolicy as AgentApplicationPolicy;
use OnTheMove\Policies\User\CreateUserPolicy;

use OnTheMove\Policies\Customer\ApplicationPolicy as CustomerApplicationPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Customer::class => CustomerApplicationPolicy::class,
        User::class => CreateUserPolicy::class,
        Application::class => AgentApplicationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
       // Gate::define('user.create', 'OnTheMove\Policies\User\CreateUserPolicy@create');
        
        Gate::define('administer', function ($user) {
            return $user->role->name === 'adminstrator';
        });


        // Passport::routes(); // implemented in routes/oauth.php instead
        // Passport::enableImplicitGrant(); // not needed
        
        Auth::provider('eloquentFlowbiz', function ($app, array $config) {
            return new FlowbizUserProvider($app['hash'], $config['model']);
        });
    }
}
