<?php

namespace OnTheMove\Providers;

use Hash;
use OnTheMove\MD5Hasher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Hashing\HashServiceProvider;

class MD5ServiceProvider extends HashServiceProvider
{
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    // public function register()
    // {
    //     $this->app['hash'] = $this->app->share(function () {
    //         return new MD5Hasher();
    //     });

    //     $cacheManager->extend('sha1', function ($app) use ($cacheManager) {
    //         return $cacheManager->repository(new MD5Hasher);
    //     });


    //     // $this->app->singleton('hash', function ($app) {
    //     //     return new HashManager($app);
    //     // });
    //     parent::register();
    // }


    // /**
    //  * Get the services provided by the provider.
    //  *
    //  * @return array
    //  */
    // public function provides()
    // {
    //     return array('hash');
    // }

    public function boot()
    {
        Hash::extend('md5', function ($app) {
            return new MD5Hasher();
        });
    }
}
