<?php

namespace OnTheMove\Providers;

use Hash;
use OnTheMove\MD5Hasher;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class FlowbizUserProvider extends EloquentUserProvider
{

    /**
    * Validate a user against the given credentials.
    *
    * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
    * @param  array  $credentials
    * @return bool
    */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];
        $hashedValue = $user->getAuthPassword();
        $md5HashedValue = $user->getFlowbizPassword();

        // If the user hasn't done this password upgrade
        if (! $hashedValue) {
            // Check if MD5 Hashed password matches in DB.
            $md5Hasher = new MD5Hasher();
            $flowbizPasswordCheck = $md5Hasher->check($plain, $md5HashedValue, ['userid' => $user->user_id]);

            // If password is correct, bring it into the 21st Century
            if ($flowbizPasswordCheck) {
                $user->password = Hash::make($plain);
                $user->save();
            }

            return $flowbizPasswordCheck;
        } else {
            return $this->hasher->check($plain, $hashedValue);
        }

        // if ($this->hasher->needsRehash($hashedValue) && $hashedValue === md5($plain)) {
        //     $user->password = Hash($plain);
        //     $user->save();
        // }

        // return $this->hasher->check($plain, $user->getAuthPassword());
    }
}
