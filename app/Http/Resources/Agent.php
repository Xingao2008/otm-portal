<?php

namespace OnTheMove\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Agent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
            "agent_id" => $this->agent_id,
            "company_id" => $this->company_id,
            "username" => $this->username,
            "title" => $this->title,
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "email" => $this->email,
            "agency" => $this->agency,
            "is_default_property_manager" => $this->is_default_agent,
            "active" => $this->active,
        ];
    }
}
