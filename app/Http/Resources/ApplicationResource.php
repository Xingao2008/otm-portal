<?php

namespace OnTheMove\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            "id" => $this->id,
            "reference" => $this->flowbiz_application_id,
            "status" => $this->flowbiz_application_status,
            "connection_date" => optional($this->connection_date)->format('Y-m-d'),
             $this->mergeWhen($this->whenLoaded('realEstateAgent'), [
                "real_estate_agency" => [
                    "company" => optional($this->realEstateAgent)->agency,
                    "property_manager" => [
                        "firstname" => optional($this->realEstateAgent)->firstname,
                        "lastname" => optional($this->realEstateAgent)->lastname,
                    ],
                ],
            ]),
            "address" => [
                "unit_number" => $this->unit_number,
                "street_number" => $this->street_number,
                "street_name" => $this->street_name,
                "street_type" => $this->street_type,
                "suburb" => $this->suburb,
                "postcode" => $this->postcode,
                "state" => $this->state,
            ],
            $this->mergeWhen($this->whenLoaded('customers'), [
                "tenants" => CustomerResource::collection($this->whenLoaded('customers')),
            ]),
            "recieved_at" => $this->created_at->format('Y-m-d H:i:s'),
            "updated_at" => $this->updated_at->format('Y-m-d H:i:s'),
        ];
    }
}
