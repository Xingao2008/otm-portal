<?php

namespace OnTheMove\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            "title"     => $this->title,
            "firstname" => $this->firstname,
            "lastname"  => $this->lastname,
            "dob"       => optional($this->dob)->format('Y-m-d'),
            "email"     => $this->email,
            "mobile"    => $this->moble,
            "phone"     => $this->phone,
        ];
    }
}
