<?php

namespace OnTheMove\Http\Requests\Connector;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Log;
class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * We trust '1st party' integraters to conform the the api unless
         * there's specific business operational reasons not to
         */
        if(isset($this->user()->role->name)) {
            if($this->user()->role->name === 'api') {
                return [];
            }
        }
         
        
        /**
         * But 3rd party implementors ('api-external') must conform
         * the data requirements regardless of agreed process
         */
        return [

            'renter'                           => 'required|boolean',
            'connection_date'                  => 'required|date_format:Y-m-d',
            'notes'                            => 'nullable',
            'application_type_id'              => 'nullable',
            'contact_consent_given'            => 'required|boolean',

            'real_estate_agency.company_name'  => 'required',
            'real_estate_agency.abn'           => 'nullable|digits:11',
            'real_estate_agency.phone'         => 'nullable|numeric|min:10',
            'real_estate_agency.postcode'      => 'nullable|digits:4',
            'real_estate_agency.otm_id'        => 'nullable|numeric',

            'property_manager.firstname'       => 'required',
            'property_manager.lastname'        => 'required',
            'property_manager.email'           => 'nullable|email',
            'property_manager.mobile_phone'    => 'nullable|numeric|min:10',
            'property_manager.landline_phone'  => 'nullable|numeric|min:10',
            'property_manager.otm_id'          => 'nullable|numeric',

            'address.unit_number'              => 'nullable',
            'address.street_number'            => 'required',
            'address.street_name'              => 'required',
            'address.street_type'              => 'required',
            'address.suburb'                   => 'required',
            'address.postcode'                 => 'required|digits:4',
            'address.state'                    => [
                'required', 
                Rule::in([
                    'VIC', 'NSW', 'QLD', 'ACT', 'TAS', 'SA', 'WA', 'NT'
                ])
            ],
            'address.dpid'                     => 'nullable',

            'tenants.*.title'                  => [
                'nullable', 
                Rule::in([
                    'Mr', 'Mrs', 'Miss', 'Ms', 'Dr'
                ])
            ],
            'tenants.*.firstname'              => 'required',
            'tenants.*.lastname'               => 'required',
            'tenants.*.email'                  => 'required|email',
            'tenants.*.mobile'                 => 'required|numeric|min:10',
            'tenants.*.phone'                  => 'nullable|numeric|min:10',
            'tenants.*.dob'                    => 'nullable|date_format:Y-m-d',
            'tenants.*.identifications.type'   => 'nullable',
            'tenants.*.identifications.number' => 'nullable',
            'tenants.*.identifications.expiry' => 'nullable|date_format:Y-m-d',
            'tenants.*.identifications.issuer' => 'nullable',
        ];
    }

    public function response(array $error)
    {
        Log::error('Connection API 422', ['error' => $error]);
        return response()->json(['error' => $error], 422);
    }


    public function messages() {
        return [
            "address.state.in" => "Address State must be one of: 'VIC', 'NSW', 'QLD', 'ACT', 'TAS', 'SA', 'WA' or 'NT'",
            "tenants.*.title.in" => "Tenant Title must be one of: 'Mr', 'Mrs', 'Miss', 'Ms' or 'Dr'",
        ];
    }
}
