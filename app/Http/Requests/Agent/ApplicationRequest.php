<?php

namespace OnTheMove\Http\Requests\Agent;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        /**
         * But 3rd party implementors ('api-external') must conform
         * the data requirements regardless of agreed process
         */
        return [

            'details.renter'              => 'required|boolean',
            'details.connection_date'     => 'required|date_format:Y-m-d',
            'details.notes'               => 'nullable',
            'details.application_type_id' => 'required',
            //'contact_consent_given'       => 'required',

            'details.address.split.unit_number'   => 'nullable',
            'details.address.split.street_number' => 'required',
            'details.address.split.street_name'   => 'required',
            'details.address.split.street_type'   => 'nullable',
            'details.address.split.suburb'        => 'required',
            'details.address.split.postcode'      => 'required|digits:4',
            'details.address.split.state'         => [
                'required',
                Rule::in([
                    'VIC', 'NSW', 'QLD', 'ACT', 'TAS', 'SA', 'WA', 'NT',
                ]),
            ],
            'details.address.dpid'                => 'nullable',

            'applicants.*.title'                  => [
                'required',
                Rule::in([
                    'Mr', 'Mrs', 'Miss', 'Ms', 'Dr',
                ]),
            ],
            'applicants.*.firstname'              => 'required',
            'applicants.*.lastname'               => 'required',
            'applicants.*.email'                  => 'required|email',
            'applicants.*.mobile'                 => 'required|numeric|min:10',
            'applicants.*.phone'                  => 'nullable|numeric|min:10',
            'applicants.*.dob'                    => 'required|date_format:Y-m-d',
            'applicants.*.identifications.type'   => 'nullable',
            'applicants.*.identifications.number' => 'nullable',
            'applicants.*.identifications.expiry' => 'nullable|date_format:Y-m-d',
            'applicants.*.identifications.issuer' => 'nullable',

        ];
    }

    public function response(array $error)
    {
        return response()->json(['error' => $error], 422);
    }

    public function attributes()
    {
        return [
            'details.application_type_id' => 'Application Type',
            'details.renter' => 'Applicant Type',
            'details.connection_date' => 'Connection Date',
           // 'contact_consent_given' => 'consent to contact applicant(s)',
            
            'details.address.split.street_number' => 'Connection Address',
            'details.address.split.street_name' => 'Connection Address',
            'details.address.split.street_type' => 'Connection Address',
            'details.address.split.suburb' => 'Connection Address',
            'details.address.split.postcode' => 'Connection Address',
            'details.address.split.state' => 'Connection Address',

            'applicants.*.title' => 'Applicant Title',
            'applicants.*.firstname' => 'Applicant First Name',
            'applicants.*.lastname' => 'Applicant Last Name',
            'applicants.*.email' => 'Applicant Email Address',
            'applicants.*.mobile' => 'Applicant Mobile',
            'applicants.*.phone' => 'Applicant Phone Number',
            'applicants.*.dob' => 'Applicant Date of Birth',

            'applicants.*.identifications.type'   => 'Applicant ID Type',
            'applicants.*.identifications.number' => 'Applicant ID Number',
            'applicants.*.identifications.expiry' => 'Applicant ID Expiry',
            'applicants.*.identifications.issuer' => 'Applicant ID Issuer',
        ];
    }
}

