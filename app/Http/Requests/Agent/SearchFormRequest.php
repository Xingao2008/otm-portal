<?php

namespace OnTheMove\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class SearchFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // agent can only search for their own apps
        return $this->user('agent')->agent_id === (int) request()->input('AgentId');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ApplicationType" => "in:All,Vendor,Renter,Landlord,Purchaser",
            "Firstname" => "nullable|alpha",
            "Lastname" => "nullable|alpha",
            "ToDate" => "nullable|required_without:Reference|date_format:Y-m-d|after_or_equal:FromDate",
            "FromDate" => "nullable|required_without:Reference|date_format:Y-m-d|before_or_equal:ToDate",
            "AgentId" => "required|numeric|exists:agents,agent_id|",
            "View" => "required|in:PM,AllPMs,REA",
            "Reference" => [
                "regex:/A\d{7}/",
                "nullable",
            ],
        ];
    }

    public function messages()
    {
        return [
            'ToDate.required_without' => 'To Date is required without an application reference',
            'FromDate.required_without'  => 'From Date is required without an application reference',
            'ToDate.after_or_equal' => 'The To Date must be a date after the From Date',
            'FromDate.before_or_equal'  => 'The From Date must be a date before the To Date',
            'Reference.regex'  => 'Ensure your application reference starts with an "A" followed by 7 digits',
        ];
    }
}
