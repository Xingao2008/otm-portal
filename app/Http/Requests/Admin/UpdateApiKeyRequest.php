<?php

namespace OnTheMove\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use OnTheMove\Rules\IpAddressArray;
class UpdateApiKeyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('administer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'allowed_ips' => ['nullable', 'string', new IpAddressArray]
        ];
    }
}
