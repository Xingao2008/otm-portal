<?php

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) {
        return $output;
    }
}

/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function areActiveRoutes(array $routes, $output = "active")
{
    foreach ($routes as $route) {
        if (Route::currentRouteName() == $route) {
            return $output;
        }
    }
}

function getMapUrl($address)
{
    $map = new \mastani\GoogleStaticMap\GoogleStaticMap(config('services.google.gmaps.key'));
    $url = $map->setSecret(config('services.google.gmaps.signature'))
                ->setCenter(urlencode($address))
                ->setZoom(19)
                ->setScale(2)
                ->setSize(600, 300)
                ->setFormat('jpg')
                ->addMarker(urlencode($address), '', 'red', 'large')
                ->make(); // Return url contain map address.
    return $url;
}


function getTagManagerId()
{
    $env = (env('APP_ENV') === 'production') ? 'production' : 'nonprod';

    return config("services.google.tagmanager.$env");
}


function validateCidr($cidr)
{
    $parts = explode('/', $cidr);
    if (count($parts) != 2) {
        return false;
    }
    $ip = $parts[0];
    $netmask = intval($parts[1]);
    if ($netmask < 0) {
        return false;
    }
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        return $netmask <= 32;
    }
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
        return $netmask <= 128;
    }

    return false;
}

