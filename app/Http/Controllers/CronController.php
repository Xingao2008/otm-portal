<?php

namespace OnTheMove\Http\Controllers;

use Log;
use Artisan;
use Illuminate\Http\Request;

class CronController extends Controller
{
    public function schedule()
    {
        Artisan::call('schedule:run');
        Artisan::call('queue:work');
        Log::info('schedule:run called');
    }
}
