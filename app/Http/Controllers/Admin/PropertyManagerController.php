<?php

namespace OnTheMove\Http\Controllers\Admin;

use Auth;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Str;
use OnTheMove\Models\Agent;
use Illuminate\Http\Request;
use OnTheMove\Models\Application;
use Illuminate\Support\Facades\Hash;
use OnTheMove\TwoOneTwoEff\RewardsApi;
use Illuminate\Auth\Events\PasswordReset;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Http\Requests\Agent\ResetAgentPasswordRequest;

class PropertyManagerController extends Controller
{
    public function show(Request $request, Agent $agent)
    {
        $app = new Application;
        $agent->load(['applications']);

        if ($request->has('filter')) {
            $filter = $request->query('filter');
            $apps = Application::whereAgentId($agent->id);

            if ($filter === 'status-submitted') {
                $apps->whereIn('flowbiz_exported', [$app::EXPORTED_TO_FLOWBIZ, $app::IMPORTED_FROM_FLOWBIZ]);
            }
        
            if ($filter === 'status-unsubmitted') {
                $apps->whereIn('flowbiz_exported', [$app::READY_FOR_EXPORT, $app::NOT_READY_FOR_EXPORT]);
            }

            if ($filter === 'status-past') {
                $apps->where('connection_date', '<', Carbon::today())->whereNotIn('flowbiz_exported', [$app::EXPORTED_TO_FLOWBIZ, $app::IMPORTED_FROM_FLOWBIZ]);
            }
        
            if ($filter === 'order-as') {
                $apps->orderBy('created_at', 'asc');
            }

            // Get the results and return them.
            $applications = $apps->paginate(10);
        } else {
            $applications = Application::whereAgentId($agent->id)->paginate(10);
        }
        
        
        if (env('APP_ENV') === 'local') {
            $api = new RewardsApi('participant-test');
        } else {
            $api = new RewardsApi($agent->agent_id);
        }
            
        $points = $api->getPointsBalance();
        $history = $api->getPointsHistory(30);
       
        return view('admin.agent.show', compact('agent', 'applications', 'points', 'history'));
    }

    public function impersonate(Request $request, Agent $agent)
    {
        $user = Auth::guard('web')->user();
        Auth::guard('agent')->login($agent);

        session()->put(['impersonation' => ['user' => $user, 'agent' => $agent]]);
        
        return redirect()->route('agent.dashboard');
    }

    public function exitImpersonation(Request $request)
    {
        Auth::guard('agent')->logout();

        session()->forget('impersonation');
        
        return redirect()->route('admin.dashboard');
    }



    public function forgotPassword(Request $request, Agent $agent)
    {
        return view('admin.agent.password', compact('agent'));
    }

    public function resetPassword(ResetAgentPasswordRequest $request, Agent $agent)
    {
        $agent->password = Hash::make($request->input('password'));
        $agent->setRememberToken(NULL);
        $agent->save();

        event(new PasswordReset($agent));

        flash()->success("Password reset for $agent->displayName");

        return redirect()->route('admin.pm.show', $agent);
    }
}
