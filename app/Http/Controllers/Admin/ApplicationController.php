<?php

namespace OnTheMove\Http\Controllers\Admin;

use Carbon\Carbon;
use OnTheMove\Models\Agent;
use Illuminate\Http\Request;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;
use OnTheMove\Models\ExceptionMapping;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Models\ApplicationException;

class ApplicationController extends Controller
{
    public function queue(Request $request)
    {
        //$apps = Application::whereNull('agent_id')->get();
        $apps = ApplicationException::all();

        return view('admin.applications.queue', compact('apps'));
    }

    public function match(ApplicationException $exception, Request $request)
    {
        $app = $exception->application;
  
        $agents = Agent::all(['id', 'company_id', 'first_name', 'last_name'])->groupBy('company_id');
        $rea = RealEstateAgency::all(['id', 'company_id', 'company_name', 'group'])->sortBy('company_name')->groupBy('group');
    
        return view("admin.applications.exception_match", compact('exception', 'app', 'rea', 'agents'));
    }

    public function fix(ApplicationException $exception, Request $request)
    {
        $app = $exception->application;
        $method = "fix" . studly_case($exception->type) . "Exception";

        $outcome = $this->{$method}($exception, $request);

        if (! $outcome) {
            flash()->error("Exception mapping couldn't be saved");

            return redirect()->route('admin.application.queue');
        }
        
        if (env('APP_ENV') !== 'local') {
            $exception->delete();
        }
    
        return redirect()->route('admin.application.queue');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = Application::orderBy('created_at', 'desc')->paginate(50);

        return view('admin.applications.index', compact('apps'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OnTheMove\Models\Application  $app
     * @return \Illuminate\Http\Response
     */
    public function show(Application $app = null, Request $request)
    {
        if (! $app) {
            return $this->showFromFlowbiz($request, $request->value('reference'));
            //$app = $this->appRepo->retrieveFromFlowbiz($request->value('reference'));
        }

        return view('admin.applications.show', compact('app'));
    }

 

    protected function saveExceptionMapping(ApplicationException $exception, $key)
    {
        $mapping = new ExceptionMapping;
        $mapping->payload = $exception->payload;
        $mapping->key = $key;

        // an expection mapping resolved and application exception
        $mapping->resolved()->associate($exception);

        $mapping->save();

        return $mapping;
    }

    protected function fixMissingConnectionDateException(ApplicationException $exception, Request $request)
    {
        $app = $exception->application;

        $app->connection_date = Carbon::createFromFormat('Y-m-d', $request->input('connection_date'))
                                        ->hour(0)
                                        ->minute(0)
                                        ->second(0);
    
        $app->save();
        
        flash()->success('Application Connection Date set to ' . $app->connection_date->format('d/m/Y'));

        return true;
    }

    protected function fixMissingTenantMobileException(ApplicationException $exception, Request $request)
    {
        $pl = json_decode($exception->payload);
        $customer = Customer::find($pl->id);
        $customer->mobile = $request->input('mobile');
        $customer->save();

        flash()->success('Tenant Mobile set to ' . $customer->mobile);

        return true;
    }
    protected function fixMissingTenantEmailException(ApplicationException $exception, Request $request)
    {
        $pl = json_decode($exception->payload);
        $customer = Customer::find($pl->id);

        $customer->email = $request->input('email');
        $customer->save();

        flash()->success('Tenant Email set to ' . $customer->email);

        return true;
    }

    protected function fixMissingPropertyManagerException(ApplicationException $exception, Request $request)
    {
        $agent = Agent::find($request->input('property_manager'));
        $app = $exception->application;

        $app->agent_id = $agent->id;
        $app->save();

        $mapping = $this->saveExceptionMapping($exception, $agent->id);

        flash()->success('Mapped Application Exception to ' . $agent->name);

        return true;
    }


    protected function fixManualApprovalException(ApplicationException $exception, Request $request)
    {
        
        flash()->success('Released app to Flowbiz');

        return true;
    }
}
