<?php

namespace OnTheMove\Http\Controllers\Admin;

use Illuminate\Http\Request;
use OnTheMove\Models\Product;

use OnTheMove\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::withTrashed()->orderBy('type');
        if ($request->query('type')) {
            $products->where('type', $request->query('type'));
        }
            
        $products = $products->get();
        $types = Product::distinct('type')->pluck('type');

        return view('admin.products.index', compact('products', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \OnTheMove\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OnTheMove\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OnTheMove\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $bool = $product->update($request->except('id', '_token', '_method'));
        if ($bool) {
            flash('Updated')->success();
        } else {
            flash('Rah Roh')->error();
        }

        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OnTheMove\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        flash("Deleted!")->success();

        return redirect()->route('admin.product.index');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  \OnTheMove\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $productId)
    {
        $p = Product::withTrashed()->findOrFail($productId);
        if ($p->trashed()) {
            $p->restore();
            flash()->success("Product '$p->name' successfully restored");

            return redirect()->route('admin.product.index');
        } else {
            flash()->error("Product '$p->name' wasn't deleted, can't restore!");

            return redirect()->route('admin.product.index');
        }
    }
}
