<?php

namespace OnTheMove\Http\Controllers\Admin;

use Exception;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\Models\Resource;
use OnTheMove\Http\Controllers\Controller;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::all(); //Media::where('collection_name', $collection_name)->get()

        return view('admin.rea.resource.index')->with([
            'resources' => $resources,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rea.resource.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resource = new Resource();

        $resource->name = $request->input('name');
        $resource->type = "agent-resource";
        
        $resource->save();

        $resource->addMediaFromRequest('file')->toMediaCollection($resource->type);

        try {

            $agencies = RealEstateAgency::whereState($request->input('state'))->get();
            $count = $agencies->count();
            $resource->agencies()->attach($agencies->pluck('id')->toArray());
           

            flash()->success("Added " . $request->input('name') . " to $count REA's in " . $request->input('state'));
        } catch (Exception $e) {
            flash()->error("Rah Roh: " . $e->getMessage());
        }

        return redirect()->route('admin.rea.resource.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
    
        return view('admin.rea.resource.show')->with(['resource' => $resource]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media = Media::findOrFail($id);

        return view('admin.rea.resource.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $media = Media::findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        try {
            
            $resource->delete();
            flash()->success("Deleted!");
            
        } catch (Exception $e) {
            flash()->error("Rah Roh: " . $e->getMessage());
        }

        return redirect()->route('admin.rea.resource.index');
    }
}
