<?php

namespace OnTheMove\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use OnTheMove\Models\Application;
use OnTheMove\Models\ExceptionMapping;
use OnTheMove\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [];
        $dateRange = [Carbon::now()->subDays(30), Carbon::now()];
        $apps = Application::setEagerLoads([])
                ->whereBetween('connection_date', $dateRange)
                ->orderBy('connection_date', 'asc')
                ->get(['id', 'connection_date'])
                ->groupBy(function ($date) {
                    return $date->connection_date->format('Y-m-d');
                });
        
        $apps = $apps->map(function ($item) {
            return $item->count();
        });

        $exceptions = ExceptionMapping::setEagerLoads([])
                ->whereBetween('created_at', $dateRange)
                ->orderBy('created_at', 'asc')
                // ->withTrashed()
                ->get(['id', 'created_at'])
                ->groupBy(function ($date) {
                    return $date->created_at->format('Y-m-d');
                });
        
        $exceptions = $exceptions->map(function ($item) {
            return $item->count();
        });
        
        return view('admin.dashboard', compact('apps', 'exceptions'));
    }
}
