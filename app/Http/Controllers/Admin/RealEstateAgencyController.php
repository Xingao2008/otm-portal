<?php

namespace OnTheMove\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\TwoOneTwoEff\RewardsApi;
use Illuminate\Support\Facades\Artisan;

use OnTheMove\Http\Controllers\Controller;

use Symfony\Component\Console\Output\BufferedOutput;

class RealEstateAgencyController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->query('q');
        if ($search) {
            $rea = RealEstateAgency::where("company_name", "LIKE", "%$search%")->paginate(50);
        } else {
            $rea = RealEstateAgency::orderBy('active', 'desc')->paginate(50);
        }
        
        return view('admin.rea.index', compact('rea'));
    }


    public function show(RealEstateAgency $rea)
    {
        $defaultPm = $rea->agents()->where('is_default_agent', 1)->first();
        if ($defaultPm) {
            if (env('APP_ENV') === 'local') {
                $api = new RewardsApi('participant-test');
            } else {
                $api = new RewardsApi($defaultPm->agent_id);
            }
            $points = $api->getPointsBalance();
        } else {
            $points = null;
        }
        $startOfMonth = Carbon::now()->startOfMonth();
        if ($startOfMonth->day === 1) {
            $startOfMonth->subMonth();
        }

        //$apps = $rea->applications()->with('services')->where('applications.created_at', '<', Carbon::now()->subDays(30))->get();
        $apps = $rea->applications()->with('services')->where('applications.created_at', '>', $startOfMonth)->get();
        $products = $apps->sum(function ($item) {
            return $item->services()->whereNotNull('connection_date')->count();
        });
  
        return view('admin.rea.show', compact('rea', 'points', 'apps', 'products'));
    }


    public function sync()
    {
        return view('admin.rea.sync');
    }

    public function syncPost()
    {
        $output = new BufferedOutput;
        $exitCode = Artisan::call('otm:flowbiz:import:agents', [], $output);

        session()->put('reaSync', [
            'exit' => $exitCode,
            'output' => $output->fetch(),
        ]);
        
        if ($exitCode === 0) {
            flash()->error('Something went wrong...');
        } else {
            flash()->success('Synced!');
        }
        
        return redirect('admin.rea.sync');
    }

    public function rewardsTypeEdit(Request $request, RealEstateAgency $rea)
    {
        return view('admin.rea.rewards.edit', compact('rea'));
    }

    public function rewardsTypeUpdate(Request $request, RealEstateAgency $rea)
    {
        $rea->rewards_type = $request->input('rewards_type') ?? null;
        if ($rea->save()) {
            flash()->success("$rea->company_name's rewards type is now: $rea->rewards_type");

            return redirect()->route('admin.rea.show', $rea);
        } else {
            flash()->error("Rah Roh. Couldn't update rewards type");
            
            return redirect()->back();
        }
    }
}
