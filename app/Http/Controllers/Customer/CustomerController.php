<?php

namespace OnTheMove\Http\Controllers\Customer;

use Illuminate\Http\Request;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;

use Illuminate\Support\Facades\URL;
use OnTheMove\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $app = Application::create($data);

        return $app;
    }

    /**
     * Display the specified resource.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer, Application $app)
    {
        dd($customer, $app);
    }

    
    public function toComplete(Customer $customer, Application $app)
    {
        //$this->authorize('update', $post);

        //dd($customer, $app);

        return view('customers.complete', compact('customer', 'app'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function email(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }


    /**
     * Trigger an email to the show/edit method.
     *
     * @param  \OnTheMove\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function sendEmail(Customer $customer)
    {
        return URL::signedRoute('customers.email', ['customer' => $customer->id]);
    }
}
