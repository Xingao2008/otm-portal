<?php

namespace OnTheMove\Http\Controllers\Agent;

use Auth;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use OnTheMove\TwoOneTwoEff\RewardsApi;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Repositories\ApplicationRepository;

use OnTheMove\Models\Alert;

class DashboardController extends Controller
{
    protected $appRepo;

    public function __construct()
    {
        $this->appRepo = new ApplicationRepository();
    }

    public function index()
    {
        $user = Auth::guard('agent')->user();

        $recentApps = $user->applications()
                           ->whereNotNull('flowbiz_application_id')
                           ->where('flowbiz_application_status', '!=', 'Void')
                           ->orderBy('flowbiz_application_id', 'desc')
                           ->take(10)
                           ->get();

        $startOfMonth = Carbon::now()->startOfMonth();
        $startOfLastMonth = $startOfMonth->copy()->subMonths(1);
        $monthProgress = Carbon::now()->subMonth();

        $appsThisMonth = $user->applications()
                    ->withCount('services')
                    ->where('connection_date', '>', $startOfMonth)
                    ->where('flowbiz_application_status', '!=', 'Void')
                    ->orderBy('connection_date', 'asc')
                    ->get(['id']); // only counting here...
       
        $appsLastMonth = $user->applications()
                    ->withCount('services')
                    ->where('connection_date', '>', $startOfLastMonth)
                    ->where('flowbiz_application_status', '!=', 'Void')
                    ->where('connection_date', '<', $monthProgress)
                    ->orderBy('connection_date', 'asc')
                    ->get(['id']); // only counting here...
                       
        $productsThisMonth = $appsThisMonth->sum(function ($app) {
            return $app->services_count;
        });

        $productsLastMonth = $appsLastMonth->sum(function ($app) {
            return $app->services_count;
        });

        try {
            if (env('APP_ENV') === 'local') {
                $api = new RewardsApi('participant-test');
            } else {
                $api = new RewardsApi($user->agent_id);
            }

            $points = $api->getPointsBalance();
        } catch (Exception $e) {
            $points = [
                'property_manager_balance' => null,
                'real_estate_agency_balance' => null,
            ];
        }


        $alert = Alert::whereActive(1)->first();

        return view('agent.dashboard', compact('recentApps', 'appsThisMonth', 'appsLastMonth', 'productsThisMonth', 'productsLastMonth', 'points', 'alert'));
    }
}
