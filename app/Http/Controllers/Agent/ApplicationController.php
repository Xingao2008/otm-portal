<?php

namespace OnTheMove\Http\Controllers\Agent;

use Log;
use Exception;

use Carbon\Carbon;
use Illuminate\Http\Request;
use OnTheMove\Models\Product;

use OnTheMove\Models\Agent;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;
use OnTheMove\Models\ApplicationType;
use OnTheMove\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

use OnTheMove\Repositories\ApplicationRepository;
use OnTheMove\Repositories\FlowbizApplicationRepository;

use OnTheMove\Http\Requests\Agent\SearchFormRequest;

class ApplicationController extends Controller
{
    protected $appRepo;
    protected $flowbizAppRepo;

    public function __construct()
    {
        $this->appRepo = new ApplicationRepository();
        $this->flowbizAppRepo = new FlowbizApplicationRepository();
        $this->authorizeResource(Application::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $apps = $this->appRepo->indexAgent();
        $apps = $request->user()->applications()->where('flowbiz_application_status', '!=', 'Void')->paginate(25);
        return view('application.index', compact('apps'));
    }

    public function search(Request $request)
    {
        //$apps = session()->pull('applications.search.results') ?? null;
        $apps = session()->get('applications.search.results') ?? null;
        
        if ($apps) {
            $apps = $this->paginateSerach($request, $apps);
        }

        return view('agent.application.search', compact('apps'));
    }

    /**
     * POST Search
     *
     * @param SearchFormRequest $request    Validated search parameters
     * @return void
     */
    public function searchPost(SearchFormRequest $request)
    {
        try {
            $results = $this->appRepo->search($request->validated());
            session()->put('applications.search.results', $results);
            $apps = $this->paginateSerach($request, $results);

            return view('agent.application.search', compact('apps'));

        } catch (Exception $e) {
            Log::error('Flowbiz API Error: Unable to search', $request->validated());
            flash()->error('Unable to retrieve search results. This has been reported, please try again later.');
            return back()->withInput();
        }
        
    }


    public function showFromFlowbiz(Request $request, $reference)
    {
        try {
            $app = $this->flowbizAppRepo->retrieveFromFlowbiz($reference);
            return redirect()->route('agent.application.show', $app);
            //return view('agent.application.show', compact('app'));

         } catch (Exception $e) {
            Log::error('Flowbiz API Error: Unable to search', ['application' => $reference]);
            flash()->error('Unable to retrieve application. This has been reported, please try again later.');
            return back()->withInput();
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $app = session()->get('app') ?? null;
        if ($app) {
            $app = $this->appRepo->get($app);
        }
        //  dd($app, $app->toArray(), $app->renter);
        $types = ApplicationType::all()->pluck('name', 'id');
        $pms = Agent::where('company_id', $request->user()->company_id)->get(['id', 'first_name', 'last_name', 'agent_id']);
        // $pms = $request->user()->realEstateAgency()->agents()->get();
        return view('agent.application.create', compact('types', 'app', 'pms'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \OnTheMove\Models\Application  $app
     * @return \Illuminate\Http\Response
     */
    public function show(Application $app = null, Request $request)
    {
        if (! $app) {
            return $this->showFromFlowbiz($request, $request->value('reference'));
        }

        return view('application.show', compact('app'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OnTheMove\Models\Application  $app
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $app)
    {
        if ($app->flowbiz_application_id) {
            flash('Application has already been provisioned.')->warning();

            return back()->withInput();
        }

        return view('application.edit', compact('app'));
    }


    protected function paginateSerach(Request $request, $data)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 15;

        // $currentItems = array_slice($data, $perPage * ($currentPage - 1), $perPage);


        // $items = array_reverse(array_sort($data, function ($value) {
        //     return $value['created_at'];
        // }));
        //$currentItems = array_slice($data, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($data->forPage($currentPage, $perPage), $data->count(), $perPage, $currentPage, ['path' => route('agent.application.search')]);
        //$results = $paginator->appends('filter', request('filter'));

        // dump($paginator, $currentPage);
        // $collection = collect($collection);

        // $page = 1;
        // $perPage = 10;

        // $paginate = new LengthAwarePaginator($collection->forPage($page, $perPage), $collection->count(), $perPage, $page, ['path'=>url('api/products')]);

        //$apps = new LengthAwarePaginator($results, $results->count(), 25);

        return $paginator;
    }
}
