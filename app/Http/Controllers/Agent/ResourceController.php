<?php

namespace OnTheMove\Http\Controllers\Agent;

use Illuminate\Http\Request;
use OnTheMove\Http\Controllers\Controller;

class ResourceController extends Controller
{
    public function index(Request $request) {

        $rea = $request->user()->realEstateAgency;
        
        $resources = $rea->resources()->get();
        

        return view('agent.resource.index', compact('resources'));
    }
}
