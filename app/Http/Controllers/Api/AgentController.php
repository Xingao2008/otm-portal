<?php

namespace OnTheMove\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Http\Resources\Agent as AgentResource;
use OnTheMove\TwoOneTwoEff\RewardsApi;
class AgentController extends Controller
{

    /**
     * @apiGroup           Agents
     * @apiName            Agent Details
     * @api                {post} /api/v1/agent User Login
     * @apiDescription     Get details of currently logged in agent (property maanger)
     * @apiVersion         1.0.0
     * @apiPermission      OAuth
     *
     * @apiHeader          Accept application/json
     * @apiHeader          Authorization Bearer: <VALID_ACCESS_TOKEN>
     *
     * @apiSuccessExample  {json}       Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "agent_id": 145056,
     *           "company_id": 139505,
     *           "username": "test",
     *           "title": "Mr",
     *           "first_name": "Test",
     *           "last_name": "Tester",
     *           "email": "test@onthemove.com.au",
     *           "agency": "OTM",
     *           "is_default_property_manager": true,
     *           "active": 1
     *       }
     *   }
     *
     * @apiErrorExample  {json}       Error-Response:
     *   HTTP/1.1 401 Unauthenticated
     *   {
     *      "message": "Unauthenticated."
     *   }
     *
     */
    public function show(Request $request)
    {
        $agent = Auth::guard('agent-oauth')->user();

        return new AgentResource($agent);
    }

    public function pointsBalance(Request $request)
    {
        $agent = $request->user('agent-api');
      
        if(env('APP_ENV') === 'local') {
            $api = new RewardsApi('participant-test');
        } else {
            $api = new RewardsApi($agent->agent_id);
        }
        return $api->getPointsBalance();
    
        // return [
        //     "property_manager_balance" => 123,
        //     "real_estate_agency_balance" => 1234,
        // ];
    }

    public function pointsHistory(Request $request)
    {
        $agent = $request->user('agent-api');

        if(env('APP_ENV') === 'local') {
            $api = new RewardsApi('participant-test');
        } else {
            $api = new RewardsApi($agent->agent_id);
        }
        
        return $api->getPointsHistory($request->input('days'));

        // return [
        //    "property_manager" =>[
        //         "total_points_accrued" => 12345, // sum of 'history[].points'
        //         "history" => [
        //             [
        //                 "reference" => "A1234567",
        //                 "connection_date" => "2018-06-20",
        //                 "sale_date" => "2018-06-30",
        //                 "state" => "VIC",
        //                 "category" => "Power",
        //                 "retailer" => "AGL",
        //                 "points" => 123
        //             ],
        //             [
        //                 "reference" => "A1234567",
        //                 "connection_date" => "2018-06-20",
        //                 "sale_date" => "2018-06-30",
        //                 "state" => "VIC",
        //                 "category" => "Gas",
        //                 "retailer" => "Origin",
        //                 "points" => 321
        //             ]
        //         ]
        //     ],
        //    "real_estate_agency" =>[
        //         "total_points_accrued" => 12345, // sum of 'history[].points'
        //         "history" => [
        //             [
        //                 "reference" => "A1234567",
        //                 "connection_date" => "2018-06-20",
        //                 "sale_date" => "2018-06-30",
        //                 "state" => "VIC",
        //                 "category" => "Power",
        //                 "retailer" => "AGL",
        //                 "points" => 123
        //             ],
        //             [
        //                 "reference" => "A1234567",
        //                 "connection_date" => "2018-06-20",
        //                 "sale_date" => "2018-06-30",
        //                 "state" => "VIC",
        //                 "category" => "Gas",
        //                 "retailer" => "Origin",
        //                 "points" => 321
        //             ]
        //         ]
        //     ]
        // ];
    }


}
