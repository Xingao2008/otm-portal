<?php

namespace OnTheMove\Http\Controllers\Api;

use Log;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use OnTheMove\Models\Customer;
use OnTheMove\Models\Application;

use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Jobs\ExportApplicationToFlowbiz;
use OnTheMove\Repositories\ApplicationRepository;
use OnTheMove\Http\Requests\Agent\ApplicationRequest;

class ApplicationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationRequest $request)
    {
        $appRepo = new ApplicationRepository();
        
        $app = $appRepo->store($request->all());
        
        try {
            $app->flowbiz_exported = $app::EXPORT_IN_PROGRESS;
            $app->save();

            $job = new ExportApplicationToFlowbiz($app);
            $app = $this->dispatchNow($job);

            if ($app instanceof Application) {
                $msg = "Application successfully submitted. Your reference number is: " . $app->flowbiz_application_id;
                $sync = true;
            } else {
                $this->dispatch($job);
                $msg = "Application successfully submitted";
                $sync = false;
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            Log::error("Couldn't send application syncronously to flowbiz", $e->getMessage());
            
            $app->flowbiz_exported = $app::READY_FOR_EXPORT;
            $app->save();

            $msg = "Application successfully submitted";
            $sync = false;
        }

        flash('Application submitted')->success();

        return response()->json([
            'id' => $app->id,
            'success' => true,
            'app' => $app,
            'message' => $msg,
            'sync' => $sync,
        ]);
    }


    public function search(Request $request)
    {
        $req = $request->all();
        
        $validatedData = $request->validate([
                'title' => 'required|unique:posts|max:255',
                'body' => 'required',
        ]);

        $searchParams = [
            'ToDate' => $now->format('Y-m-d'),
            'FromDate' => $ago->format('Y-m-d'),
        ];

        $appRepo = new ApplicationRepository();
        $app = $appRepo;
        $results = $this->flowbiz->searchApplications($searchParams);
    }
}
