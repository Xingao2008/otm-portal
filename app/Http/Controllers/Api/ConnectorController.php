<?php

namespace OnTheMove\Http\Controllers\Api;

use Log;
use Notification;
use OnTheMove\Models\User;
use OnTheMove\Models\Application;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Models\ApplicationException;
use OnTheMove\Repositories\ConnectorRepository;
use OnTheMove\Http\Requests\Connector\StoreRequest;
use OnTheMove\Notifications\Admin\NewApplicationException;
use OnTheMove\Http\Resources\ApplicationResource;

class ConnectorController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  OnTheMove\Http\Requests\Api\ConnectorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $appRepo = new ConnectorRepository();
        $app = $appRepo->store($request->all());
        
        if (! $app instanceof Application) {
            return response()->json([
                'message' => $app,
                'error' => []
            ], 400); 
        }

        if ($request->user()->role->name === 'api-external') {
            Log::info("New External API App from " . $request->user()->name, ['data' => $request->all()]);
            $this->storeAndSendException($app, 'manual approval', $app->toJson());
        }
        
        return (new ApplicationResource($app))->response(201);
        return response()->json($app, 201);
    }

    private function storeAndSendException(Application $app, string $type, $payload)
    {
        $exception = new ApplicationException();

        $exception->type = $type;
        $exception->payload = $payload;

        $exception->application()->associate($app);
       
        $exception->save();
        
        // A dozen better ways to do this, but this'll do
        $users = User::whereHas('role', function ($query) {
            $query->where('name', 'staff');
        })->get();

        if (env('APP_ENV') === 'production') {
            // only send if in prod
            Notification::send($users, new NewApplicationException($exception));
        }

        return $exception;
    }
}
