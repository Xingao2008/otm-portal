<?php

namespace OnTheMove\Http\Controllers\Api;

use Illuminate\Http\Request;

use OnTheMove\Experian\QasValidate;
use OnTheMove\Http\Controllers\Controller;
use Log;
use Exception;

class ValidateController extends Controller
{
    protected $validate;
    protected $qas;

    public function __construct(Validate $validator, QasValidate $qas)
    {
        $this->validate = $validator;
        $this->qas = $qas;
    }

    /**
     * mobile function.
     *
     * @access public
     * @param Request $request
     * @return void
     */
    public function mobile(Request $request)
    {
        $mobileNumber = $request->input('value');

        try {
            $resp = $this->validate->mobile($mobileNumber);
            Log::debug("QAS Mobile Response", $resp);
        } catch (Exception $e) {
            $resp = ['isValid' => true, 'value' => $mobileNumber];
        }

        return response()->json($resp);
    }


    /**
     * email function.
     *
     * @access public
     * @param Request $request
     * @return void
     */
    public function email(Request $request)
    {
        $emailAddress = $request->input('value');

        try {
            $resp = $this->validate->emailQas($emailAddress);
            Log::debug("QAS Email Response", $resp);
        } catch (Exception $e) {
            $resp = ['isValid' => true, 'value' => $emailAddress];
        }
        
        $resp = ['isValid' => true, 'value' => $emailAddress];

        return response()->json($resp);
    }


    /**
     * address function.
     *
     * @access public
     * @param Request $request
     * @return void
     */
    public function address(Request $request)
    {
        $searchParams = [
            "method" => "Search",
            "countryId" => "AUS",
            "engine" => "Intuitive",
            "flatten" => 1,
            "promptset" => "Default",
            "intensity" => "Extensive",
            "threshold" => 15,
            "timeout" => 10000,
            "requestTag" => "",
            "localisation" => "",
            "layout" => "QADefault",
            "search" => $request->input('value'),
            "formattedAddressInPicklist" => 1,
        ];

        $resp = $this->qas->address($searchParams);

        $r = $resp->result;
        $picklist = $r->Picklist;

        $resultArr = [];

        foreach ($picklist->Items as $key => $val) {
            $resultArr[] = [
                "address" => $val->Text,
                "addressMoniker" => $val->Moniker,
            ];
        }

        return response()->json(["results" => $resultArr]);
    }


    public function addressSplit(Request $request)
    {
        $moniker = $request->input('value');
        if(!$moniker) {
            return response()->json(["results" => false]);

        }
        // Split out Address
        $addressMonkier = [
            "method" => "GetAddress",
            "requestTag" => "",
            "localisation" => "",
            "layout" => "AllElements",
            "moniker" => $moniker,
        ];
        $response = $this->qas->address($addressMonkier);


        $formattedResponse = $this->parseQasFormattedResponse($response);

        return response()->json(["results" => $formattedResponse]);
    }
   


    public function nmi(Request $request)
    {
        $req = $request->all();
        $msats = new Msats(5);
        try {
            return (array) $msats->moniker($req);
        } catch (Exception $e) {
            return ['errror' => true, 'message' => $e->getMessage()];
        }
    }

    public function mirn(Request $request)
    {
        $req = $request->all();
        $mirn = new Mirn(5);
        try {
            return (array) $mirn->moniker($req);
        } catch (Exception $e) {
            return ['errror' => true, 'message' => $e->getMessage()];
        }
    }

    public function siteIdentifer(Request $request, Validate $validator)
    {
        $req = $request->all();
        try {
            return [
                "isValid" => $validator->siteIdentifer($req['value']),
                "value"   => $req['value'],
            ];
        } catch (Exception $e) {
            return [
                "isValid" => true,
                "value"   => $req['value'],
            ];
        }
    }

    /**
     * Parse a QAS address layout, to the format Flowbiz expects
     *
     * @param Object $response  The raw QAS response object
     * @return Array $fmtAddr   A formmated address array
     */
    protected function parseQasFormattedResponse($response)
    {
        $address = [];
        foreach ($response->result->AddressLines as $part) {
            $key = camel_case(str_slug($part->Label));
            $address[$key] = $part->Line;
        }

        if ($address['streetTypeSuffix'] !== "") {
            $streetType = $address['streetType'] . " " . $address['streetTypeSuffix'];
        } else {
            $streetType = $address['streetType'];
        }

        $streetNumber = $address["allotmentLot"] . " " . $address["allotmentNumber"] . " " . $address["buildingLevelType"] . $address["buildingLevelNumber"] . " " . $address["buildingNumber"] . $address["subBuildingNumber"];

        $streetNumber = trim(preg_replace('/\s\s/', '', $streetNumber));

        $fmtAddr = [
            "property_type" => ($address['flatunitType']) ?"Unit" : "House",
            "unit_number" => $address['flatunitNumber'],
            "street_number" => $streetNumber,
            "street_name" => $address['streetName'],
            "street_type" => $streetType,
            "suburb" => $address['locality'],
            "postcode" => $address['postcode'],
            "state" => $address['stateCode'],
            "dpid" => $address['dpiddid'],
        ];
    
        return $fmtAddr;
    }
}
