<?php

namespace OnTheMove\Http\Controllers\Auth\Agent;

use Log;
use Auth;
use Carbon\Carbon;
use OnTheMove\Models\Agent;
use Illuminate\Http\Request;
use OnTheMove\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('agent.login');
    }

    public function username()
    {
        return 'username';
    }

    public function showLoginForm()
    {
        return view('agent.login');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        Log::info("Agent attempting login: " . $request->input('username'));

        $username = Agent::where("username", $request->input('username'))->first();
    
        $success = $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );

        if (! $username) {
            Log::info("Agent attempted login with incorrect username: " . $request->input('username'));
        } elseif ($username && ! $success) {
            $fbPw = ($username->password_flowbiz) ? "true" : "false";
            $nePw = ($username->password) ? "true" : "false";
            Log::info("Agent attempted login with incorrect password: " . $request->input('username') . " - FlowbizPW: $fbPw  - New Password: $nePw");
        }

        return $success;
    }

    protected function authenticated(Request $request, $user)
    {
        Log::info("Agent authenticated: " . $user->username);
        $user->update([
            'last_login_at' => Carbon::now(),
            'last_login_ip' => $request->getClientIp(),
        ]);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        Log::info("Agent failed to login in: " . $request->input('username'));
        $username = Agent::where("username", $request->input('username'))->first();
    

        $messages = [];

        if(!$username) {
            if(strpos($request->input('username'), '@') !== false) {
                $messages[$this->username()] = ["Sorry, that username looks like an email address."];
            } else {
                $messages[$this->username()] = ["Sorry, we couldn't find an account with that username."];

            }
            
        }
        if($username) {
            $messages['password'] = ["Sorry, that password isn't right. Try again or click Forgot password to reset it."];
        }
        if(empty($messages)) {
            $messages[$this->username()] = [trans('auth.failed')];
        }

        throw ValidationException::withMessages($messages);
    }

    protected function credentials(Request $request)
    {
        return $request->only(strtolower($this->username()), 'password');
    }

    protected function guard()
    {
        return Auth::guard('agent');
    }
}
