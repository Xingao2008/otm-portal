<?php

namespace OnTheMove\Http\Controllers\Auth\OAuth;

use Illuminate\Http\Request;

use Laravel\Passport\Bridge\User;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\ClientRepository;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response as Psr7Response;

use Laravel\Passport\Http\Controllers\AuthorizationController as PassportAuthorizationController;

class AuthorizationController extends PassportAuthorizationController
{
    /**
     * Authorize a client to access the user's account.
     *
     * @param  \Psr\Http\Message\ServerRequestInterface  $psrRequest
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laravel\Passport\ClientRepository  $clients
     * @param  \Laravel\Passport\TokenRepository  $tokens
     * @return \Illuminate\Http\Response
     */
    public function authorize(
        ServerRequestInterface $psrRequest,
        Request $request,
        ClientRepository $clients,
        TokenRepository $tokens
    ) {
        return $this->withErrorHandling(function () use ($psrRequest, $request, $clients, $tokens) {
            $authRequest = $this->server->validateAuthorizationRequest($psrRequest);

            $scopes = $this->parseScopes($authRequest);

            $token = $tokens->findValidToken(
                $user = $request->user(),
                $client = $clients->find($authRequest->getClient()->getIdentifier())
            );

            if ($token && $token->scopes === collect($scopes)->pluck('id')->all()) {
                return $this->approveRequest($authRequest, $user);
            }

            $request->session()->put('authRequest', $authRequest);


            // from PassportAuthorizationController::approveRequest()
            // all OAuth requests are auotmatically approved

            $authRequest->setUser(new User($user->getKey()));

            $authRequest->setAuthorizationApproved(true);

            return $this->convertResponse(
                $this->server->completeAuthorizationRequest($authRequest, new Psr7Response)
            );
            
            // end from PassportAuthorizationController::approveRequest()


            // return $this->response->view('passport::authorize', [
            //     'client' => $client,
            //     'user' => $user,
            //     'scopes' => $scopes,
            //     'request' => $request,
            // ]);
        });
    }
}
