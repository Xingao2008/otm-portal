<?php

namespace OnTheMove\Http\Controllers\Auth\User;

use OnTheMove\Models\Role;
use OnTheMove\Models\User;
use Illuminate\Http\Request;
use OnTheMove\Http\Controllers\Controller;
use OnTheMove\Http\Requests\Admin\UpdateApiKeyRequest;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.user.index', compact('users'));
    }


    public function edit(Request $request, User $user)
    {
        $roles = Role::all();
        $u = $user; //use in
        return view('admin.user.edit', compact('u', 'roles'));
    }

    public function editApiKey(Request $request, User $user)
    {
        if(!in_array($user->role->name, ['api', 'api-external'])) {
            flash()->error($user->name . " isn't an API type user");

            return back();
        }

        $u = $user; //use in
        return view('admin.user.edit-key', compact('u', 'roles'));
    }

    public function updateApiKey(UpdateApiKeyRequest $request, User $user)
    {
        try {
            
            $ips = array_map('trim', explode(',', $request->input('allowed_ips')));
            $user->allowed_ips = $ips;
            $user->save();
    
            flash()->success($user->name . "'s Allowed IP ranges have been updated");
        } catch (Exception $e) {
            flash()->error("Error updating API key: " . $e->getMessage());

            return back();
        }
        $users = User::all();

        return view('admin.user.index', compact('users'));

    }
}
