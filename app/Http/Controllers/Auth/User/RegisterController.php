<?php

namespace OnTheMove\Http\Controllers\Auth\User;

use OnTheMove\Models\User;
use OnTheMove\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use OnTheMove\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/';

    // protected $redirectPath = '/admin/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showRegistrationForm()
    {
        $roles = Role::all();
        return view('admin.user.register', compact('roles'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // very very ugly
        $apiRole1Id = Role::whereName('api')->first()->id;
        $apiRole2Id = Role::whereName('api-external')->first()->id;

        return Validator::make($data, [
            "name" => "required|string|max:255",
            "email" => "required_unless:role,$apiRole1Id,$apiRole2Id|nullable|string|email|max:255|unique:users",
            "password" => "required_unless:role,$apiRole1Id,$apiRole2Id|nullable|string|min:6|confirmed",
            "role" => "required",
        ], [
            'email.required_unless' => 'Email is required unless user is of a Api or Api-External type',  
            'password.required_unless' => 'Email is required unless user is of a Api or Api-External type'  
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \OnTheMove\Models\User
     */
    protected function create(array $data)
    {
        $role = Role::findOrFail($data['role']);

        // API users cannot log into the interface.
        if(in_array($role->name, ['api', 'api-external'])) {
            unset($data['email'], $data['password'], $data['password_confirmation']);
        }
   
        $password = (isset($data['password'])) ? Hash::make($data['password']) : null;
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'] ?? null,
            'password' => $password ?? null,
        ]);
        
        $user->role()->associate($role);
    
        if(in_array($user->role->name, ['api', 'api-external'])) {
          
            $user->api_token = str_random(60);
            flash()->info('API token is: <pre>' . $user->api_token . '</pre>');
        }
        $user->save();

        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        
        if ($user->role->name !== 'api') {
            flash()->success('User Created: ' . $user->name);
        }

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    protected function guard()
    {
        return Auth::guard('web');
    }
}
