<?php

namespace OnTheMove\Http\Controllers\Auth\User;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use OnTheMove\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm()
    {
        return view('admin.login');
    }

    
    protected function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now(),
            'last_login_ip' => $request->getClientIp(),
        ]);
    }


    protected function guard()
    {
        return Auth::guard('web');
    }
}
