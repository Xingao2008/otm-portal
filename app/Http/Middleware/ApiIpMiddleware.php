<?php

namespace OnTheMove\Http\Middleware;

use Auth;
use Log;
use Closure;
use OnTheMove\Models\User;
use Symfony\Component\HttpFoundation\IpUtils;

class ApiIpMiddleware
{
    /**
     * List of valid IPs.
     *
     * @var array
     */
    protected $ips = [];
 
    /**
     * List of valid IP-ranges.
     *
     * @var array
     */
    protected $ipRanges = [];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::guard('user-api')->check()) {
            Log::info("User-API guard check failed", [
                'request-ips' => $request->getClientIps(),
                'data' => $request->all(),
                'headers' => $request->header()
            ]);
            return abort(401, 'Unauthorized');
        }
        
        $user = Auth::guard('user-api')->user();
        if (! $user->allowed_ips) {
            return $next($request);
        }

        $this->setIpAddress($user);

        foreach ($request->getClientIps() as $ip) {
            if (! $this->isValidIp($ip) && ! $this->isValidIpRange($ip)) {
                Log::warning("$user->name request blocked for failing IP whitelist", [
                    'request-ips' => $request->getClientIps(),
                    'whitelisted-ips' => $user->allowed_ips,
                    'data' => $request->all(),
                    'headers' => $request->header()
                ]);
                return abort(403, 'Invalid IP address');
            }
        }

        return $next($request);
    }

    protected function setIpAddress(User $user)
    {
        foreach ($user->allowed_ips as $ip) {
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $this->ips[] = $ip;
            } elseif (validateCidr($ip)) {
                $this->ipRanges[] = $ip;
            }
        }
    }
    /**
     * Check if the given IP is valid.
     *
     * @param $ip
     * @return bool
     */
    protected function isValidIp($ip)
    {
        return in_array($ip, $this->ips);
    }
 
    /**
     * Check if the ip is in the given IP-range.
     *
     * @param $ip
     * @return bool
     */
    protected function isValidIpRange($ip)
    {
        return IpUtils::checkIp($ip, $this->ipRanges);
    }
}
