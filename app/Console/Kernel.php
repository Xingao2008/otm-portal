<?php

namespace OnTheMove\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use OnTheMove\Console\Commands\CronCheckCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \OnTheMove\Console\Commands\ImportFlowBizAgents::class,
        \OnTheMove\Console\Commands\ImportFlowBizProducts::class,
        \Bugsnag\BugsnagLaravel\Commands\DeployCommand::class,
        \OnTheMove\Console\Commands\CronCheckCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('cron:check')->hourly()->onOneServer();

        /**************************************************************
         *  PM and REA Sync & Import
         */

        $schedule->command('otm:flowbiz:import:agents')
                            ->dailyAt('3:00')
                            ->onOneServer();

        /**************************************************************
         *  App Sync & Import
         */

        /* --- Sync ------------------------------------------------ */

        $schedule->command('otm:flowbiz:import:applications --update 250')
                            ->hourly()
                            ->onOneServer();

        // /* --- Import ---------------------------------------------- */
        
        // More frequently during business hours
        $schedule->command('otm:flowbiz:import:applications --recent 100')
                            ->everyFifteenMinutes()
                            ->weekdays()
                            ->between('7:00', '19:00')
                            ->onOneServer();
                      
        // Less frequently overnight and weekends
        $schedule->command('otm:flowbiz:import:applications --recent 200')
                            ->hourly()
                            ->weekdays()
                            ->unlessBetween('7:00', '19:00')
                            ->onOneServer();

        $schedule->command('otm:flowbiz:import:applications --recent 100')
                            ->hourly()
                            ->weekends()
                            ->onOneServer();

        // Daily deep sync
        $schedule->command('otm:flowbiz:import:applications --update 5000')
                            ->dailyAt('4:00')
                            ->onOneServer();

        $schedule->command('otm:flowbiz:import:applications --recent 5000')
                            ->dailyAt('4:15')
                            ->onOneServer();


        /**************************************************************
         *  App Export
         */

        $schedule->command('otm:flowbiz:export:applications')
                           ->everyMinute()
                           ->onOneServer();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
