<?php

namespace OnTheMove\Console\Commands;

use Hash;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;
use OnTheMove\Models\RealEstateAgency;
use OnTheMove\Jobs\Tasks\ImportOrUpdateRealEstateAgency;

class ImportFlowBizAgents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'otm:flowbiz:import:agents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Flowbiz Agents and Agencies';

    protected $api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->api = new Api(60);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Running ImportFlowBizAgents');

        $agencies = $this->api->getAgencyList();
        
        $i = count($agencies["Results"]);
        $this->info("Starting import of " . $i . " agencies");
        $bar = $this->output->createProgressBar($i);
        $bar->setFormat('debug');

        foreach ($agencies["Results"] as $agency) {

            $job = new ImportOrUpdateRealEstateAgency($agency);
            dispatch($job);
            //$this->updateOrCreateRealEstateAgency($agency);
    
            $bar->advance();
        }

        $bar->finish();
        $this->info("\n\rCompleted import of agents & agencies");
    }

    
}
