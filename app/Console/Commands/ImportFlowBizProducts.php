<?php

namespace OnTheMove\Console\Commands;

use Illuminate\Support\Facades\Log;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Product;

use Illuminate\Console\Command;

class ImportFlowBizProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'otm:flowbiz:import:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Flowbiz Product Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Running ImportFlowBizProducts');

        $api = new Api(60);

        $retailers = $api->getRetailers();

        $i = count($retailers["Results"]);
        $this->info("Starting import of " . $i . " retailers products");
        $bar = $this->output->createProgressBar($i);

        //dd($retailers["Results"][292]);

        foreach ($retailers["Results"] as $retailer) {
            $products = $api->getProduct($retailer["ID"]);

            foreach ($products["Results"] as $product) {
                $state = [];
                preg_match("/(VIC|NSW|QLD|SA)/", $product["Name"], $state);
                
                $p = Product::whereFlowbizId($product["ID"])->first();
                if (! $p) {
                    $p = Product::create([
                        "flowbiz_id" => $product["ID"],
                        "type" => $product["ProductType"],
                        "provider" => $retailer["Name"],
                        "name" => $product["Name"],
                        "state" => $state[1] ?? null,
                    ]);
                } else {
                    $p->type = $product["ProductType"];
                    $p->provider = $retailer["Name"];
                    $p->name = $product["Name"];
                    $p->state = $state[1] ?? null;
               
                    $p->save();
                }
            }
            $bar->advance();
        }

        $bar->finish();
        $this->info("Complete");
    }
}
