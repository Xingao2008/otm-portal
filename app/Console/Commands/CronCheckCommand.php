<?php

namespace OnTheMove\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CronCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Logs the status of the cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(env('APP_ENV') === 'production') {
            Log::info('Scheduler Called');
        }
    }
}
