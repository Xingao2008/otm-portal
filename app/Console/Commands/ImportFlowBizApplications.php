<?php

namespace OnTheMove\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use OnTheMove\Models\Application;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;

use OnTheMove\Jobs\ImportOrUpdateFlowbizApplication;

class ImportFlowBizApplications extends Command
{
    use DispatchesJobs;

    const FLOWBIZ_APP_FORMAT = "A%'.07d";
    const FLOWBIZ_MAXIMUM_ID = 120000;

    public $appRepo;

    protected $signature = "otm:flowbiz:import:applications 
                                { --queue=default : Which queue the job should be placed in }
                                { --recent : Only import recent application }
                                { --update : Import apps that are more recent that the last app in the database }
                                { number? : The number of apps to seek ahead or behind for. Defaults to 500 }
                                { --app= : The app to import }
                            ";

    protected $description = 'Import Flowbiz App Data';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        Log::info('Running ImportFlowBizApplications');

        $start = 0;
        $end = self::FLOWBIZ_MAXIMUM_ID;

        $queue = $this->option('queue');
        $recent = $this->option('recent');
        $update = $this->option('update');
        $specificApp = $this->option('app');
        $number = $this->argument('number');
       
        $defaultNumber = (env('APP_ENV') === 'production') ? 500 : 25 ;
        $number = (! $number) ? $defaultNumber : (int) $number;

        if ($recent || $update) {
            $lastApp = Application::setEagerLoads([])->orderBy('flowbiz_application_id', 'desc')->take(1)->first();
            $lastId = (int) substr($lastApp->flowbiz_application_id, 1, 7);
        }
        
        if ($recent) {
            $start = $lastId;
            $end = $start + $number;
        }

        if ($update) {
            $start = $lastId - round($number / 4);
            $end = $lastId + $number;
        }

        if ($specificApp) {
           $start = (int) substr($specificApp, 1, 7);
           $end = $start + 1;
       }
    
        $length = $end - $start;
        $first = sprintf(self::FLOWBIZ_APP_FORMAT, $start);

        $this->info("Starting import of " . $length . " applications, starting at: $first");
        $bar = $this->output->createProgressBar($length);

        do {
            try {
                $appId = sprintf(self::FLOWBIZ_APP_FORMAT, $start);
                ImportOrUpdateFlowbizApplication::dispatch($appId); //->onQueue($queue);
                $start++;

            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
            
            $bar->advance();
        } while ($start < $end);

        
        $bar->finish();
        $this->info("");
        $this->info("Complete. Last app imported: $appId");
    }
}
