<?php

namespace OnTheMove\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use OnTheMove\Models\Application;
use Illuminate\Support\Facades\Log;
use OnTheMove\Jobs\ExportApplicationToFlowbiz;

class ExportFlowbizApplications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "otm:flowbiz:export:applications { number=5 : The number of apps export at a time. Defaults to 5 for safety }";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export apps to Flowbiz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Running ExportFlowbizApplications');

        // only export 5 apps at a time.
        // this limits the impact of any screwup,
        // without slowing down the export frequency
        $apps = Application::readyToExport()->take($this->argument('number'))->get();

        $length = $apps->count();

        if ($length === 0) {
            $this->info("No applications to export");

            return true;
        }
        Log::info("Starting export of " . $length . " applications");
        $this->info("Starting export of " . $length . " applications");
        $bar = $this->output->createProgressBar($length);

        foreach ($apps as $app) {
            try {
                $app->flowbiz_exported = $app::EXPORT_IN_PROGRESS;
                ExportApplicationToFlowbiz::dispatch($app);//->onQueue('flowbiz');
                $app->save();
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }

            $bar->advance();
        }
        
        $bar->finish();
        $this->info("");
        $this->info("Complete");
    }
}
