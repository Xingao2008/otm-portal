<?php

namespace OnTheMove\Flowbiz;

use Log;
use Bugsnag;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;

class Api
{
    protected $client;
    protected $timeout;
    
    public function __construct($timeout = 10)
    {
        $this->client = new Client();
        $this->timeout = $timeout;
    }

    public function getAgencyList()
    {
        return $this->call('GET', 'agencies', null);
    }

    public function getPropertyManagers($agencyId)
    {
        return $this->call('GET', 'propertymanagers', $agencyId);
    }

    public function submitApplication($app)
    {
        return $this->call('POST', 'application', $app);
    }

    public function getApplication($appId)
    {
        return $this->call('GET', 'application', $appId);
    }

    public function getRetailers()
    {
        return $this->call('GET', 'retailer', null);
    }

    public function getRetailersByType($type)
    {
        return $this->call('GET', 'retailer', $type);
    }

    public function getProduct(string $productId)
    {
        return $this->call('GET', 'product', $productId);
    }

    public function getApplicationDetails(string $applicationId)
    {
        return $this->call('GET', 'details', $applicationId);
    }

    public function searchApplications(array $searchParams)
    {
        return $this->call('GET', 'search', $searchParams);
    }

    protected function call($method = 'GET', $type = 'application', $data = null)
    {
        $url = config("services.flowbiz.endpoints.base");
        $url .= config("services.flowbiz.endpoints.$type");

        if ($type === 'application') {
            Log::debug("Flowbiz URL", [$url, $data]);
        }


        $headers = [
            'Authorization' => 'Bearer ' . config('services.flowbiz.token'),
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
        ];

        $request = [
            'headers' => $headers,
            'timeout' => $this->timeout,
            'verify'  => false, // DNS isn't working back to Flowbiz :(
        ];

        if ($data && $method === 'POST') {
            $request['body'] = json_encode($data);
        }

        // build the right kind of GET
        if ($data && $method === 'GET') {
            switch (gettype($data)) {
                case "array":
                    $request['query'] = $data;
                break;
                case "string":
                    $url .= "/".$data;
                break;
                case "integer":
                    $url .= "/".$data;
                break;
                default:
                    throw new Exception('Data is of an unexpected type');
                break;
            }
        }

        //var_dump($request['body']);

        try {
            $response = $this->client->request($method, $url, $request);

            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                throw new Exception('FlowBiz JSON response error: ' .  json_last_error_msg());
            }

            return $resp;
        } catch (ClientException $e) {
            // Flowbiz Returns a 400 if an application couldn't be found
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            
            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                Bugsnag::notifyException($e);
                Log::error('FlowBiz JSON response error', ['error' => json_last_error_msg()]);
            } else {
                // if it wasn't an application get.. report it
                if ($type !== 'application' && $method !== 'GET') {
                    Bugsnag::notifyException($e);
                    Log::info('Flowbiz JSON response', ['info' => $resp]);
                }
            }

            return false;
        } catch (ServerException $e) {
            Bugsnag::notifyException($e);

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            
            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                Log::error('FlowBiz JSON response error', ['error' => json_last_error_msg()]);
            } else {
                Log::info('Flowbiz JSON response', ['info' => $resp]);
            }

            return false;
        } catch (RequestException $e) {
            Bugsnag::notifyException($e);

            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $exception = json_decode($exception);

                return new JsonResponse($exception, $e->getCode());
            } else {
                return new JsonResponse($e->getMessage(), 503);
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            Log::error('Could not decode FlowBiz JSON response', ['error' => $e->getMessage()]);
            throw new Exception('Could not decode FlowBiz JSON response' . $e->getMessage());

            return false;
        }
    }
}
