<?php

namespace OnTheMove;

use RuntimeException;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class MD5Hasher implements HasherContract
{
    const FLOWBIZ_ALGO_VERSION_PAD = "1";

    /**
     * Create a new hasher instance.
     *
     * @param  array  $options
     * @return void
     */
    //public function __construct(array $options = [])
    public function __construct()
    {
        if (! env('FLOWBIZ_SALT', null)) {
            throw new RuntimeException("FLOWBIZ_SALT Enviroment variable is required");
        }
        // $this->userid = $options['userid'];
    }


    /**
     * Get information about the given hashed value.
     *
     * @param  string  $hashedValue
     * @return array
     */
    public function info($hashedValue)
    {
        return password_get_info($hashedValue);
    }

    /**
     * Hash the given value.
     *
     * @param  string  $value
     * @return array   $options
     * @return string
     */
    public function make($value, array $options = [])
    {
        if (! isset($options['userid'])) {
            throw new RuntimeException("UserID is required");
        }
    
        $valueString = $options['userid'] . "-" . $value . "-" . env('FLOWBIZ_SALT');
        $md5 = md5($valueString, true);

        return self::FLOWBIZ_ALGO_VERSION_PAD . base64_encode($md5);
    }


    /**
     * Check the given plain value against a hash.
     *
     * @param  string  $value
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function check($value, $hashedValue, array $options = [])
    {
        return $this->make($value, $options) === $hashedValue;
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = [])
    {
        return false;
    }

    // /**
    //  * Set the default password threads factor.
    //  *
    //  * @param  int  $threads
    //  * @return $this
    //  */
    // public function setUserID(int $userid)
    // {
    //     $this->userid = $userid;

    //     return $this;
    // }

    // /**
    //  * Extract the memory cost value from the options array.
    //  *
    //  * @param  array  $options
    //  * @return int
    //  */
    // protected function userid(array $options)
    // {
    //     if (! isset($options['userid']) && ! isset($this->userid)) {
    //         throw new RuntimeException("UserID is required");
    //     }

    //     return $options['userid'] ?? $this->userid;
    // }
}
