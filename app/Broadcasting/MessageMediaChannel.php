<?php

namespace OnTheMove\Broadcasting;

use OnTheMove\Models\User;

use MessageMediaMessagesLib\APIHelper;
use MessageMediaMessagesLib\MessageMediaMessagesClient;

class MessageMediaChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \OnTheMove\Models\User  $user
     * @return array|bool
     */
    public function join(User $user)
    {
        //
    }

    protected function send($content, $phone)
    {
        $authUserName = 'YOUR_API_KEY'; // The API key to use with basic/HMAC authentication
        $authPassword = 'YOUR_API_SECRET'; // The API secret to use with basic/HMAC authentication
        $useHmacAuthentication = false; // Change to true if you are using HMAC keys

        $client = new MessageMediaMessagesClient($authUserName, $authPassword, $useHmacAuthentication);

        $messages = $client->getMessages();

        $bodyValue = "{
            'messages':[
                {
                    'content': '$content',
                    'destination_number':'$phone'
                }
            ]
        }";

        $body = APIHelper::deserialize($bodyValue);

        $result = $messages->createSendMessages($body);
    }
}
