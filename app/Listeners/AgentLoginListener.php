<?php

namespace OnTheMove\Listeners;

use Auth;
use Log;
use Exception;
use Carbon\Carbon;
use OnTheMove\Flowbiz\Api;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use OnTheMove\Jobs\ImportOrUpdateFlowbizApplication;

class AgentLoginListener
{
    use DispatchesJobs;

    protected $flowbiz;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->flowbiz = new Api(5);
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(!Auth::guard('agent')->check()) {
            return false;
        }
        // if($event->user->guard !== 'agent') {
        //     return false;
        // }

        $now = Carbon::today();
        $ago = Carbon::today()->subYear(5);

        if(!isset($event->user->agent_id)) {
            return true;
        }

        $searchParams = [
            'ToDate' => $now->format('Y-m-d'),
            'FromDate' => $ago->format('Y-m-d'),
            'AgentID' => $event->user->agent_id, //"151430",
            'View' => 'PM',
        ];
 
        try {
            $results = $this->flowbiz->searchApplications($searchParams);
            // dump($results, $searchParams);

            if(gettype($results) !== 'array') {
                return true;
            }
            if(!isset($results['Results'])) {
                return true;
            }
            if (count($results['Results']) === 0) {
                return true;
            }
            
            $results = collect($results['Results']);
            $number = 5;
            $recent = $results->reverse()->take($number);
            $rest = $results->slice($number);


            try {
                // import the most recent x apps in to the DB
                $recent->each(function ($app) {
                    $job = new ImportOrUpdateFlowbizApplication($app['Reference']);
                    $this->dispatchNow($job);
                });
            } catch (Exception $e) {
                $recent->each(function ($app) {
                    $job = new ImportOrUpdateFlowbizApplication($app['Reference']);
                    $this->dispatch($job);
                });
            }
            // queue the rest for later
            $rest->each(function ($app) {
                $job = new ImportOrUpdateFlowbizApplication($app['Reference']);
                $this->dispatch($job);
            });
        } catch (Exception $e) {
            Log::warning("Unable to import new apps on log in", ['error' => $e->getMessage()]);
            return true;
        }
        
        return true;

    }
}
