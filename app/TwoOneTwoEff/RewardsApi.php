<?php

namespace OnTheMove\TwoOneTwoEff;

use Log;
use Bugsnag;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;

class RewardsApi
{
    protected $client;
    protected $timeout;
    
    public function __construct($agent_id, $timeout = 10)
    {
        $this->user = $agent_id;
        $this->client = new Client();
        $this->timeout = $timeout;
    }

    public function getPointsBalance()
    {
        return $this->call('GET', 'balance');
    }

    public function getPointsHistory($days = 30)
    {
        return $this->call('GET', 'history', ['period' => $days]);
    }
    protected function call($method = 'GET', $type = 'balance', $data = null)
    {
        $url = $this->getURL($type);
        
        $headers = [
            'Accept' => 'application/json',
        ];

        $request = [
            'headers'  => $headers,
            'timeout'  => $this->timeout,
            'auth'     => config('services.212f.auth'),
        ];

        // build the right kind of GET
        if ($data && $method === 'GET') {
            switch (gettype($data)) {
                case "array":
                    $request['query'] = $data;
                break;
                default:
                    throw new Exception('Data is of an unexpected type');
                break;
            }
        }
       // dump($method, $url, $request);
        try {
            $response = $this->client->request($method, $url, $request);

            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                Log::error('JSON response error', ['error' => json_last_error_msg()]);

                return false;
            }

            return $resp;
        } catch (ClientException $e) {
            Bugsnag::notifyException($e);

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            
            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                Log::error('JSON response error', ['error' => json_last_error_msg()]);
            } else {
                Log::info('JSON response', ['info' => $resp]);
            }

            return false;
        } catch (ServerException $e) {
            Bugsnag::notifyException($e);

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            
            $resp = json_decode($response->getBody(), true);

            if ($resp === null) {
                Log::error('JSON response error', ['error' => json_last_error_msg()]);
            } else {
                Log::info('JSON response', ['info' => $resp]);
            }

            return false;
        } catch (RequestException $e) {
            Bugsnag::notifyException($e);

            if ($e->hasResponse()) {
                $exception = (string) $e->getResponse()->getBody();
                $exception = json_decode($exception);
                Log::error('JSON response error', ['error' => $exception]);

                //return new JsonResponse($exception, $e->getCode());
                return false;
            } else {
                //return new JsonResponse($e->getMessage(), 503);
                return false;
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            Log::error('Could not decode JSON response', ['error' => $e->getMessage()]);
            throw new Exception('Could not decode JSON response' . $e->getMessage());

            return false;
        }
    }
    
    private function getURL($type)
    {
        return config("services.212f.endpoints.base") . '/' . $this->user . config("services.212f.endpoints.$type");
    }
}
