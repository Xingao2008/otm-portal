<?php

namespace OnTheMove\Models;

use Spatie\MediaLibrary\File;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Resource extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public function agencies()
    {
        return $this->belongsToMany(RealEstateAgency::class, 'real_estate_agency_resource', 'resource_id', 'rea_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->nonQueued();

    
    }

    // public function registerMediaCollections()
    // {
    //     $this
    //         ->addMediaCollection('resources')
    //         ->acceptsFile(function (File $file) {
    //             return $file->mimeType === 'application/pdf';
    //         });
    //     $this
    //         ->addMediaCollection('brand')
    //         ->acceptsFile(function (File $file) {
    //             return in_array($file->mimeType, ['image/jpeg', 'image/png']);
    //         });

    // }
}
