<?php

namespace OnTheMove\Models;

use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class ExceptionMapping extends Model implements AuditableInterface
{
    use Auditable;

    public function resolved()
    {
        return $this->belongsTo(ApplicationException::class, 'application_exception_id');
    }
}
