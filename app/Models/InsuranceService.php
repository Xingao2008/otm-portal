<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceService extends Model
{
    protected $table = 'services_insurance';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getInsuranceRequiredAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }
}
