<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
  * The products that are sold to customers.
  * Specifically features or constructs of a particular product
  */
class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    // YOLO
    protected $guarded = [];
    // protected $fillable = [
    //     'flowbiz_id',
    //     'name',
    //     'type',
    //     'provider'
    // ];

    public function getNameAttribute($value)
    {
        $value = preg_replace('/^(\d+\s+)?(.*)$/', '$2', $value);

        return trim(preg_replace("/\([^)]+\)|\-\s/", "", $value));
    }

    public function getColorAttribute()
    {
        switch ($this->type) {
            case 'Power':
                return "yellow";
            break;
            case 'Gas':
                return "purple";
            break;
            case 'Water':
                return "azure";
            break;
            case 'Phone':
                return "lime";
            break;
            case 'Internet':
                return "teal";
            break;
            case 'Mobile':
                return "green";
            break;
            case 'Bundle':
                return "gray-dark";
            break;
            default:
                return "blue";
            break;
        }
    }


    public function terms()
    {
        return $this->belongsToMany(ProductTerms::class);
    }
}
