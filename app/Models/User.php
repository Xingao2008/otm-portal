<?php

namespace OnTheMove\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OnTheMove\Notifications\Admin\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login_at', 'last_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', '',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'last_login_at',
    ];

    protected $with = ['role'];

    /* ------ Notifications -------------------------------------------------- */


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

     /* ------ Relationships -------------------------------------------------- */


    public function realEstateAgencies()
    {
        return $this->hasMany(RealEstateAgency::class)
                    ->withPivot('relationship')
                    ->withTimestamps();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

     /* ------ Attributes ---------------------------------------------------- */


    public function setAllowedIpsAttribute($value)
    {
        $this->attributes['allowed_ips'] = json_encode($value);
    }

    public function getAllowedIpsAttribute($value)
    {
        return json_decode($value);
    }

    public function getDiplayAllowedIpsAttribute()
    {
        $str = "";
        
        if (! $this->allowed_ips || count($this->allowed_ips) === 0) {
            return $str;
        }

        foreach ($this->allowed_ips as $ip) {
            $str .= $ip . ", ";
        }
        
        return substr($str, 0, -2);
    }
}
