<?php

namespace OnTheMove\Models;

use ReflectionClass;
use OwenIt\Auditing\Auditable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class ApplicationException extends Model implements AuditableInterface
{
    use SoftDeletes, Auditable;

    /**
     * Used to pick correct view (and maybe method) to
     * use on the frontend
     */
    const EXCEPTION_TYPES = [
        'missing property manager',
        'missing tenant mobile',
        'missing tenant email',
        'missing connection date',
    ];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function mapping()
    {
        return $this->hasOne(ExceptionMapping::class);
    }

    public static function getConstants()
    {
        $oClass = new ReflectionClass(__CLASS__);

        return $oClass->getConstants();
    }
}
