<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RealEstateAgency extends Model
{
    use SoftDeletes;

    const REWARDS_TYPES = [
        'PM', 'Split', 'REA'
    ];

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function setAbnAttribute($value)
    {
        $this->attributes['abn'] = preg_replace('/\s+/', ' ', $value);
    }

    public function setGroupAttribute($value)
    {
        $this->attributes['group'] = trim($value);
    }

    public function getGroupAttribute()
    {
        if (preg_match("/.*(Ray White|Century 21|Coronis|Starr Partners|Barry Plant|Laing (\&|\+) Simmons|Raine \& Horne|BIGGIN & SCOTT|Dingle Partners).*/i", $this->company_name, $matches, PREG_OFFSET_CAPTURE) === 1) {
            return title_case($matches[1][0]);
        } else {
            return null;
        }
    }

    public function getFullAddressAttribute()
    {
        if (! $this->street_name) {
            return null;
        }
      
        return "$this->street_name, $this->suburb, $this->state $this->postcode";
    }


    public function agents()
    {
        return $this->hasMany(Agent::class, 'company_id', 'company_id');
    }

    public function applications()
    {
        return $this->hasManyThrough(Application::class, Agent::class, 'company_id', 'agent_id', 'company_id', 'id');
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class, 'real_estate_agency_resource', 'rea_id', 'resource_id');
    }

    public function managers()
    {
        return $this->belongsToMany(User::class)->withPivot('relationship')->withTimestamps();
    }
 
    public function businessManager()
    {
        return $this->managers()->wherePivot('relationship', 'bdm');
    }

    public function accountManager()
    {
        return $this->managers()->wherePivot('relationship', 'am');
    }

    /**
     * An active agency must have an active flag,
     * and have an ABN. Those without an ABN
     * are likely property managers in FB
     */
    public function scopeIsActive($query)
    {
        return $query->where('active', 1)
                     ->where('abn', '!=', '')
                     ->where('abn', '!=', null);
    }

    public function getIsActiveAttribute()
    {
        if ($this->active !== 1  || ! $this->abn) {
            return false;
        }

        return true;
    }
}
