<?php

namespace OnTheMove\Models;

use OnTheMove\Models\Application;
use Illuminate\Database\Eloquent\Model;

class ApplicationType extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function applications()
    {
        return $this->belongsToMany(Application::class, 'application_type_id', 'id');
    }
}
