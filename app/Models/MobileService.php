<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class MobileService extends Model
{
    protected $table = 'services_mobile';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }


}
