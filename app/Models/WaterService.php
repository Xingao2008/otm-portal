<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class WaterService extends Model
{
    protected $table = 'services_water';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getWaterRequiredAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }
}
