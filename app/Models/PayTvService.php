<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class PayTvService extends Model
{
    protected $table = 'services_paytv';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getExistingCustomerAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getResidentialAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getCurrentEmploymentDurationAttribute($value)
    {
        return $this->getYearsAndMonthsArray($value);
    }

    public function getPreviousEmploymentDurationAttribute($value)
    {
        return $this->getYearsAndMonthsArray($value);
    }

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }

    /**
     * Parse a months duration into Years and Months array, as required
     * by the FlowBiz API. Decison to store these values as number of
     * months will invariably be a silly one...
     *
     * @param  String $duration number of months
     * @return Array            format required by the FB Api
     */
    protected function getYearsAndMonthsArray($duration)
    {
        return [
            "Year"  => (string) floor($duration / 12),
            "Month" => (string) ($duration % 12),
        ];
    }
}
