<?php

namespace OnTheMove\Models;

use Gravatar;
use Exception;
use OwenIt\Auditing\Auditable;
use OnTheMove\Models\CustomerId;
use OnTheMove\Models\Application;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Foundation\Auth\User as Authenticatable;


use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use Propaganistas\LaravelPhone\Exceptions\NumberParseException;


use OnTheMove\Notifications\Customer\AppToBeCompletedNotification;

class Customer extends Authenticatable implements AuditableInterface
{
    use SoftDeletes, Notifiable, Auditable;

    protected $guard = 'customer';
    
    protected $with = ['identifications'];

    protected $dates = [
        'dob',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* ------ Relationships ---------------------------------------------------- */


    public function applications()
    {
        return $this->belongsToMany(Application::class);
    }

    public function identifications()
    {
        return $this->hasMany(CustomerId::class);
    }

    /* ------ Attributes ------------------------------------------------------- */


    public function getNameAttribute()
    {
        return $this->firstname . " " . $this->lastname;
    }

    public function getFullNameAttribute()
    {
        return $this->title . " " . $this->firstname . " " . $this->lastname;
    }

    public function getE164MobileAttribute()
    {
        return PhoneNumber::make($this->mobile, 'AU')->formatE164();
    }

    public function getGravatarImage()
    {
        if ($this->email) {
            try {
                return Gravatar::get($this->email);
            } catch (Exception $e) {
                return Gravatar::get("test@test.com");
            }
        } else {
            return Gravatar::get("test@test.com");
        }
    }


    /* ------ Notifications -------------------------------------------------- */

    public function sendAppReadyForCompletion($app)
    {
        $this->notify(new AppToBeCompletedNotification($app));
    }
}
