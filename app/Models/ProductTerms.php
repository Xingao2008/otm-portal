<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTerms extends Model
{
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
