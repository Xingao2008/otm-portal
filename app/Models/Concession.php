<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Concession extends Model
{
    use SoftDeletes;

    protected $dates = [
        'expiry',
    ];

    protected $casts = [
        'expiry' => 'date:Y-m-d',
    ];

    public function customerId()
    {
        return $this->morphMany(CustomerId::class, 'type');
    }

    public function getFlowbizIssuerAttribute()
    {
        return $this::TYPES[$this->type];
    }

    const TYPES = [
        "CDHCC"    => "CDHCC (Health Care Card Carer (Child Under 16))",
        "DVA_DIS"  => "DVA_DIS (DVA Gold Card (Disability Pension))",
        "DVAG_TPI" => "DVAG_TPI (DVA Gold Card TPI Only)",
        "DVAGC"    => "DVAGC (Dept of Veteran Affairs Gold Card)",
        "DVAGC_RHC"=> "DVAGC (DVA Gold Card Repatriation Health Card)",
        "DVAGC_WW" => "DVAGC_WW (DVA Gold Card War Widow)",
        "DVPC"     => "DVPC (DVA Pension Concession Card)",
        "HCC"      => "HCC (Health Care Card)",
        "PCC"      => "PCC (Centrelink Pensioner Concession Card)",
        "QGSC"     => "QGSC (Queensland Government Seniors Card)",
        "SAHCC"    => "SAHCC (Health Care Card Sickness Allowance[SA])",
        "SPHCC"    => "SPHCC (Health Care Card Special Benefit [SP])",
    ];
}
