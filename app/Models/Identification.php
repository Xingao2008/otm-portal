<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Identification extends Model
{
    use SoftDeletes;

    // What a identification's type is worth in points
    const ID_POINTS = [
        "Australian Drivers Licence"                 => 60,
        "Australian Government Issued Benefits Card" => 40,
        "Australian Passport"                        => 70,
        "Bank Statement"                             => 25,
        "Birth Certificate"                          => 40,
        "Blind Citizens Australia Identity Card"     => 60,
        "Credit Card"                                => 40,
        "Debit Card"                                 => 40,
        "International Passport"                     => 30,
        "Medicare Card"                              => 40,
        "Private Health Insurance Card"              => 25,
        "Tertiary ID Card"                           => 25,
        "Utility Bill or Rates Notice"               => 25,
        "Valid Police/Defence Force ID"              => 60,
        "Valid Shooters/Firearms Licence"            => 60,
        "Working with Children Card"                 => 50,
    ];

    protected $dates = [
        'expiry',
    ];

    protected $casts = [
        'expiry' => 'date:Y-m-d',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($id) { // get's called BEFORE model is saved
            $id->categorizeType();

            return true; // true = continue saving. false would cancel saving
        });
    }

    public function categorizeType()
    {
        if ($this->type === "Australian Drivers Licence") {
            $this->category = "Drivers License";
        } elseif (in_array($this->type, ["Australian Passport", "International Passport"])) {
            $this->category = "Passport";
        } else {
            $this->category = "Other";
        }
    }


    public function getAllIdPoints()
    {
        return self::ID_POINTS;
    }
    
    public function getPointsForIdType($type)
    {
        return self::ID_POINTS[$type];
    }


    public function customerId()
    {
        return $this->morphMany(CustomerId::class, 'type');
    }
}
