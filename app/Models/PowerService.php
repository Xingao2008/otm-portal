<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class PowerService extends Model
{
    protected $table = 'services_power';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function getMeterOutsideAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getHazardsAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getDogOnPremisesAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getMarketingOptOutAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getPowerOnAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getRemoteSafetyConfirmationAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function setNMIAttribute($value)
    {
        $this->attributes['nmi'] = substr($value, 0, 11);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }
}
