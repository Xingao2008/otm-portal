<?php

namespace OnTheMove\Models;

use Illuminate\Database\Eloquent\Model;

class GasService extends Model
{
    protected $table = 'services_gas';

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];


    public function getMeterOutsideAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getHazardsAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getDogOnPremisesAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
    public function getMarketingOptOutAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    public function getAllAttributes()
    {
        return \Schema::getColumnListing($this->table);
    }

    public function setMIRNAttribute($value)
    {
        $this->attributes['mirn'] = substr($value, 0, 11);
    }

    public function service()
    {
        return $this->morphMany(Service::class, 'details');
    }
}
