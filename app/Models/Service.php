<?php

namespace OnTheMove\Models;

use OnTheMove\Models\Product;
use OnTheMove\Models\Application;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * The details of a service (product) attached to an application.
 * Specifically, details relating to the a products sale,
 * not the features or constructs of a particular product
 */
class Service extends Model
{
    use SoftDeletes;

    protected $dates = [
        'connection_date',
    ];

    public function application()
    {
        return $this->belongsToMany(Application::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function details()
    {
        return $this->morphTo();
    }
}
