<?php

namespace OnTheMove\Models;

use OwenIt\Auditing\Auditable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


use OnTheMove\Notifications\Agent\ResetPasswordNotification;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Agent extends Authenticatable implements AuditableInterface
{
    use SoftDeletes, Notifiable, HasApiTokens, Auditable;

    protected $guard = 'agent';

    protected $guarded = [];

    protected $hidden = [
        'password', 'password_flowbiz', 'remember_token', 'last_login_at', 'last_login_ip',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at', 'last_login_at',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function realEstateAgency()
    {
        return $this->belongsTo(RealEstateAgency::class, 'company_id', 'company_id');
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }
    
    public function completeApplications()
    {
        return $this->hasMany(Application::class)->complete();
    }

    public function incompleteApplications()
    {
        return $this->hasMany(Application::class)->incomplete();
    }

    // too clever
    // public function managers() {
    //     return $this->hasManyThrough(RealEstateAgency::class, User::class, 'agent_id', 'company_id', 'agent_id', 'id');
    // }


    public function businessManager()
    {
        return $this->realEstateAgency()->businessManager;
    }

    public function accountManager()
    {
        return $this->realEstateAgency()->accountManager;
    }

    /**
     * An active agency must have an active flag,
     * and have an ABN. Those without an ABN
     * are likely property managers in FB
     */
    public function activeRealEstateAgency()
    {
        return $this->realEstateAgency()->isActive();
    }


    public function getNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getDisplayFirstNameAttribute()
    {
        return preg_replace("/\([^)]+\)/", "", $this->first_name);
    }
    public function getDisplayNameAttribute()
    {
        return preg_replace("/\([^)]+\)/", "", $this->first_name . " " . $this->last_name);
    }
    public function getDisplayAgencyAttribute()
    {
        return trim(preg_replace("/\([^)]+\)/", "", $this->agency));
    }

    public function getGroupAttribute()
    {
        return $this->realEstateAgency->group ?? null;
    }

    public function getHasRewardsAttribute()
    {
        return [
            'property_manager' => $this->hasIndividualRewards,
            'real_estate_agency' => $this->hasBusinessRewards,
        ];
    }
    public function getHasIndividualRewardsAttribute()
    {
        return $this->realEstateAgency->rewards_type !== 'REA';
    }

    public function getHasBusinessRewardsAttribute()
    {
        if (! $this->is_default_agent) {
            return false;
        }

        return $this->realEstateAgency->rewards_type !== 'PM';
    }

    public function getFlowbizPassword()
    {
        return $this->password_flowbiz;
    }

    public function setAgencyAttribute($value)
    {
        $this->attributes['agency'] = trim($value);
    }



    // -------------------------------------------------------------- //

    public function scopeFindAgent($query, $data)
    {
        if (isset($data['otm_id'])) {
            return $query->where('agent_id', $data['otm_id']);
        }
        if (isset($data['firstname'])) {
            $query->where('first_name', $data['firstname']);
        }
        if (isset($data['lastname'])) {
            $query->where('last_name', $data['lastname']);
        }
        if (isset($data['email'])) {
            $query->where('email', $data['email']);
        }
        if (isset($data['mobile_phone'])) {
            $query->where('mobile', $data['mobile_phone']);
        }
                
        return $query;
    }
}
