<?php

namespace OnTheMove\Models;

use Auth;
use Carbon\Carbon;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Application extends Model implements AuditableInterface
{
    use SoftDeletes, Auditable;

    const NOT_READY_FOR_EXPORT  = null; // not ready for export
    const READY_FOR_EXPORT      = 0;    // ready for export
    const EXPORT_IN_PROGRESS    = 5;    // being exported via a Job (sync or queued)
    const EXPORTED_TO_FLOWBIZ   = 10;   // exported
    const IMPORTED_FROM_FLOWBIZ = 20;   // imported from flowbiz, wasn't submitted via portal / integration
    const BUGGY_EXPORT_IMPORT   = 99;   // bad juju.


    protected $guarded = [];
    
    protected $dates = [
        'connection_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'connection_date' => 'date:Y-m-d',
        'renter'          => 'boolean',
    ];

    protected $with = ['customers', 'services', 'realEstateAgent'];

    /* ------ Attributes ---------------------------------------------------- */

    // public function getRenterAttribute($value)
    // {
    //     dd($value);
    //     return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    // }

    public function getFullAddressAttribute()
    {
        if (! $this->street_name) {
            return false;
        }

        $address = "";
        if ($this->property_type && $this->unit_number) {
            $address .= "{$this->property_type} {$this->unit_number}, ";
        }
        $address .= "$this->street_number $this->street_name $this->street_type,  $this->suburb,  $this->state $this->postcode";

        return $address;
    }

    public function getIsPastConnectionDateAttribute()
    {
        $today = Carbon::today();

        return $today->gt($this->connection_date);
    }


    public function getStatusColorAttribute()
    {
        switch ($this->flowbiz_application_status) {
            case "Partial Sale":
                return "gray";
            break;
            case "Work In Progress":
                return "azure";
            break;
            case "Completed":
                return "green";
            break;
            case "Accepted":
                return "green";
            break;
            case "Not Submitted":
                return "yellow";
            break;
            case "Cancelled":
                return "red";
            break;
            case "Awaiting Call":
                return "yellow";
            break;
            case "Void":
                return "black";
            break;
            case null:
                return "yellow";
            break;
            default:
                return "yellow";
            break;
        }
    }

    /* ------ Scopes -------------------------------------------------------- */

    public function scopeComplete($query)
    {
        return $query->whereIn('flowbiz_exported', [self::EXPORTED_TO_FLOWBIZ, self::IMPORTED_FROM_FLOWBIZ]);
    }

    public function scopeIncomplete($query)
    {
        return $query->where('flowbiz_exported', self::NOT_READY_FOR_EXPORT);
    }

    public function scopeReadyToExport($query)
    {
        return $query->doesntHave('exceptions')
                     ->whereHas('customers')
                     ->whereHas('realEstateAgent')
                     ->where('flowbiz_exported', self::READY_FOR_EXPORT)
                     ->whereIn('customer_eic', [1, null]);
    }


    /* ------ Relationships ------------------------------------------------- */

    public function appType()
    {
        return $this->hasOne(ApplicationType::class, 'id', 'application_type_id');
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class);
    }

    public function realEstateAgent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function exceptions()
    {
        return $this->hasMany(ApplicationException::class);
    }


    public function setStateAttribute($value)
    {
        switch (strtolower($value)) {
            case "victoria":
                $this->attributes['state'] = "VIC";
            break;
            case "new south wales":
                $this->attributes['state'] = "NSW";
            break;
            case "queensland":
                $this->attributes['state'] = "QLD";
            break;
            case "south australia":
                $this->attributes['state'] = "SA";
            break;
            case "northern territory":
                $this->attributes['state'] = "NT";
            break;
            case "western australia":
                $this->attributes['state'] = "WA";
            break;
            case "australian capital territory":
                $this->attributes['state'] = "ACT";
            break;
            case "tasmaina":
                $this->attributes['state'] = "TAS";
            break;
            default:
                $this->attributes['state'] = strtoupper($value);
        }
    }


    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
}
