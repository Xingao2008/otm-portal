<?php

namespace OnTheMove\Models;

use OnTheMove\Models\Customer;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerId extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $types = [
        'OnTheMove\Models\Concession'     => 'Concession',
        'OnTheMove\Models\Identification' => 'Identification',
    ];

    protected $with = ['type'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function type()
    {
        return $this->morphTo();
    }


    public function imageable()
    {
        return $this->morphTo();
    }

    public function IdType($type)
    {
        return array_get($this->types, get_class($type));
    }

    public function getType()
    {
        return array_get($this->types, get_class($this->type));
    }
}
