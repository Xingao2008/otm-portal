<?php

namespace OnTheMove\Jobs;

use Bugsnag;
use Exception;
use OnTheMove\Flowbiz\Api;
use Illuminate\Bus\Queueable;
use OnTheMove\Models\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OnTheMove\Repositories\FlowbizApplicationRepository;

class ImportOrUpdateFlowbizApplication implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $flowbizApi;
    protected $appRepo;
    protected $appId;
    private $response;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $appId)
    {
        $this->appId = $appId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Log::info('JOB : Processing FB Application. ID - ' . $this->appId);

        $this->flowbizApi = new Api(60);
        $this->appRepo = new FlowbizApplicationRepository();

        try {
            $details = $this->flowbizApi->getApplicationDetails($this->appId);

            if (! $details) {
                // usually the app doesn't exist yet.
                // Log::error('JOB : Processing FB Application', ['details' => $details]);

                return false;
            }
            if (gettype($details) !== 'array') {
                Log::warning('JOB : Processing FB Application', ['details' => json_encode($details)]);

                return false;
            }

            $this->response = $this->importApplication($this->appId, $details);
            //Log::info('JOB : Processing FB Application (response)', [ $this->response]);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            Log::error($e->getMessage());
        }
    }

    public function tags()
    {
        return ['import', 'app:'.$this->appId];
    }
    
    public function getResponse()
    {
        return $this->response;
    }


    protected function importApplication(String $appId, array $details)
    {
        try {
            $app = Application::where('flowbiz_application_id', $appId)->first();

            if (! isset($details['Results'])) {
                return false;
            }
            if ($app) {
                return $this->appRepo->updateFromFlowbiz($app, $details['Results']);
            }

            return $this->appRepo->createFromFlowbiz($appId, $details['Results']);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            Log::error('ImportOrUpdateFlowbizApplication issue', ['error' => $e->getMessage(), 'app' => $appId, 'line' => $e->getLine(),'file' => $e->getFile()]);
            return false;
        }
    }
}
