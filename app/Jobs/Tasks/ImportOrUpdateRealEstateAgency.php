<?php

namespace OnTheMove\Jobs\Tasks;

use Hash;
use OnTheMove\Flowbiz\Api;
use OnTheMove\Models\Agent;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use OnTheMove\Models\RealEstateAgency;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportOrUpdateRealEstateAgency implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $agency;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $agency)
    {
        $this->agency = $agency;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->sanitizeRealEstateAgencyData($this->agency);
       
        // Optionally update the REA. Details do change, from time to time
        $rea = RealEstateAgency::updateOrCreate(["company_id" => $this->agency["CompanyID"]], $data->toArray());
        $api = new Api(60);
        $agents = $api->getPropertyManagers($rea->company_id);
        $allAgents = $this->importNewAgents($agents["Results"], $rea);

        $this->removeDeletedAgents($allAgents, $rea->company_id);

        return true;
    }

    protected function importNewAgents($agents, $rea)
    {
        $reAgents = collect();
        foreach ($agents as $agent) {
            if (isset($agent["UserAccount"])) {
                if ($agent["UserAccount"] === 'gransom') {
                    continue;
                }
            }
            
            $reAgent = Agent::whereAgentId($agent["ID"])->withTrashed()->first();
            $agent = $this->sanitizeAgentData($agent);

            $data = [
                "agent_id"         => $agent["ID"] ?? null,
                "user_id"          => $agent["UserID"] ?? null,
                "company_id"       => $rea->company_id,
                "title"            => $agent["Title"] ?? null,
                "first_name"       => $agent["Firstname"] ?? null,
                "last_name"        => $agent["Lastname"] ?? null,
                "email"            => $agent["Email"] ?? null,
                "mobile"           => $agent["Mobile"] ?? null,
                "business_phone"   => $agent["BusinessPhone"] ?? null,
                "agency"           => $agent["Agency"] ?? null,
                "active"           => $agent["Active"] ?? null,
                "username"         => $agent["UserAccount"] ?? null,
                "is_default_agent" => $agent["DefaultPropertyManager"] ?? null,
            ];
            
            if ($reAgent) {
                if ($reAgent->username && ! $reAgent->api_token) {
                    $data["api_token"] = str_random(60);
                }

                if ($reAgent->username && ! $reAgent->password && $reAgent->password_flowbiz) {
                    $data["password"] = $this->generateDefaultPassword($rea);
                }

                $reAgent->update($data);
            } else {
                if (isset($agent["UserAccount"])) {
                    if (Agent::where("username", $agent["UserAccount"])->first()) {
                        Log::warning("Agent username is not unique");
                        continue;
                    }
                }

                $data["password"] = $this->generateDefaultPassword($rea);
                $data["api_token"] = str_random(60); // for Vue authentication

                $reAgent = Agent::create($data);
            }

            $reAgents->push($reAgent);
        }

        return $reAgents;
    }

    protected function removeDeletedAgents($agents, $company_id)
    {
        $deletedAgents = Agent::whereNotIn('id', $agents->pluck('id'))
                              ->whereCompanyId($company_id)
                              ->get();
        
        $deletedAgents->each(function ($dAgent) {
            $dAgent->delete(); // Soft-deleted, that is..
        });
    }

    private function sanitizeAbn($abn)
    {
        $abn = preg_replace('/\s+|\D+/', '', $abn);

        // can't fix stupid
        if (strlen($abn) > 11) {
            $abn = substr($abn, 0, 11);
        }

        if ($abn === '') {
            $abn = null;
        }

        return $abn;
    }
    private function sanitizeState($state)
    {
        $state = strtoupper(preg_replace('/\s+/', '', $state));

        switch ($state) {
                    case "VICTORIA":
                        $state = 'VIC';
                    break;
                    case "NEW SOUTH WALES":
                        $state = 'NSW';
                    break;
                    case "QUUENSLAND":
                        $state = 'QLD';
                    break;
                    case "QUEENSLAND":
                        $state = 'QLD';
                    break;
                    case "TASMANIA":
                        $state = 'TAS';
                    break;
                }

        return $state;
    }

    private function sanitizeRealEstateAgencyData(array $agency)
    {
        if (isset($agency["ABN"])) {
            $agency["ABN"] = $this->sanitizeAbn($agency["ABN"]);
        }
        if (isset($agency["Address"]["State"])) {
            $agency["Address"]["State"] = $this->sanitizeState($agency["Address"]["State"]);
        }
        $agency["BusinessPhone"] = preg_replace('/\s+|\-|\(|\)/', '', $agency["BusinessPhone"]);
            
        if (strlen($agency["BusinessPhone"]) > 10) {
            $agency["BusinessPhone"] = substr($agency["BusinessPhone"], 0, 10);
        }

        if (isset($agency["Address"]["Postcode"])) {
            $agency["Address"]["Postcode"] = preg_replace('/\s+/', '', $agency["Address"]["Postcode"]);
        }

        $agency["CompanyName"] = (isset($agency["CompanyName"])) ? trim($agency["CompanyName"]) : null;
            
        $agency = array_filter($agency);
        
        $data = collect([
                "company_id"   => $agency["CompanyID"] ?? null,
                "company_name" => $agency["CompanyName"] ?? null,
                "company_type" => $agency["CompanyType"] ?? null,
                "group"        => $agency["Group"] ?? null,
                "ranking"      => $agency["Ranking"] ?? null,
                "abn"          => $agency["ABN"] ?? null,
                "is_gst_registered" => $agency["GSTRegistered"] ?? null,
                "business_phone"    => $agency["BusinessPhone"] ?? null,
                "address1"     => $agency["Address"]["Address1"] ?? null,
                "address2"     => $agency["Address"]["Address2"] ?? null,
                "suburb"       => $agency["Address"]["Suburb"] ?? null,
                "postcode"     => $agency["Address"]["Postcode"] ?? null,
                "state"        => $agency["Address"]["State"] ?? null,
                "dpid"         => $agency["Address"]["DPID"] ?? null,
            ]);

        $data->each(function ($item, $key) {
            trim($item);
        });
            
        $data['active'] = $this->isReaActive($data);

        return $data;
    }

    private function sanitizeAgentData(array $agent)
    {
        $agent["UserID"] = ($agent["UserID"] !== 0) ? $agent["UserID"] : null;
        $agent["UserAccount"] = ($agent["UserAccount"] !== "") ? $agent["UserAccount"] : null;
        $agent["Mobile"] = preg_replace('/\s+|\-|\(|\)/', '', $agent["Mobile"]) ?? null;
        $agent["BusinessPhone"] = preg_replace('/\s+|\-|\(|\)/', '', $agent["BusinessPhone"]) ?? null;
        $agent = array_filter($agent);


        return $agent;
    }
    private function generateDefaultPassword(RealEstateAgency $rea)
    {
        try {
            $key = $rea->suburb ?? "otm";
            $password = bcrypt(preg_replace('/\s+/', '', strtolower($key)));
        } catch (\Exception $e) {
            $password = bcrypt("otm");
        }

        return $password;
    }

    private function isReaActive($agency)
    {
        $active = true;

        if ($agency["ranking"] === "E - VOID") {
            return false;
        }

        // phrases used the data to indicate an inactive agency
        $inactiveFlags = [
            "Duplicated",
            "duplicate",
            "sold RR",
            "RR sold",
            "RR bought out",
            "sold rent roll",
            "no longer exists",
            "DO NOT CALL",
            "do not use",
            "DO NOT",
            "DONT USE",
            "CLOSED DOWN",
            "VOID",
            "DUPE",
            "old account",
            "CLOSED",
            "TERMINATED",
            "DROPPED OFF",
            "\(Old\)",
            "\(Sold\)",
            "Rupert",
        ];

        // Does the company name have an indicatior that
        // they're inactive?
        foreach ($inactiveFlags as $flag) {
            if (preg_match("/$flag/i", $agency["company_name"])) {
                return false;
            }
        }

        return true;
    }
}
