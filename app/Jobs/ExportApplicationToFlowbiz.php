<?php

namespace OnTheMove\Jobs;

use Log;
use Bugsnag;
use Illuminate\Bus\Queueable;
use OnTheMove\Models\Application;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

use OnTheMove\Repositories\BaseRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExportApplicationToFlowbiz implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $app;
    protected $appRepo;
    private $response = false;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->appRepo = new BaseRepository();
        
        try {
            $this->setResponse($this->appRepo->export($this->app));

            if (! $this->getResponse()) {
                return false;
            }

            return $this->getResponse();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            Log::error($e->getMessage());

            return false;
        }
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($resp)
    {
        $this->response = $resp;
    }
}
