<img src="/public/img/otmportal.png?raw=true" width="350" alt="OTM Portal" title="OTM Portal">

---

# Table of Contents
<details>
<summary>Expand Table of Contents</summary>

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
- [Getting Started](#getting-started)
    - [Pre-requisites](#pre-requisites)
    - [Configure .env for Running Locally](#configure-env-for-running-locally)
    - [Installing Dependencies](#installing-dependencies)
    - [Adding New PHP Libraries](#adding-new-php-libraries)
- [Running the application](#running-the-application)
    - [Logging & Error Tracking](#logging--error-tracking)
    - [Auth](#auth)
    - [Tests](#tests)
        - [Performance Test](#performance-test)
- [Technical Details](#technical-details)
    - [API](#api)
    - [Architecture](#architecture)
    - [Connector](#connector)
        - [Flow](#flow)
    - [OAuth2 API](#oauth2-api)
    - [Application Model ERD](#application-model-erd)
- [Journeys](#journeys)
    - [As a renter](#as-a-renter)
    - [As a landlord](#as-a-landlord)
    - [As a real estate agent](#as-a-real-estate-agent)
    - [As a On The Move manager](#as-a-on-the-move-manager)
    - [As a retailer offering services to OTM](#as-a-retailer-offering-services-to-otm)

</details>

# Overview

The OTM Portal is a 1st-party developed replacement of the previous Flowbiz-developed "Kiosk" and "Rewards Portal".


# Getting Started

## Pre-requisites

The following software is required to build the OTM Portal:

* [Composer Package Manager](https://getcomposer.org/) - getcomposer.org

* Node.js Package Manager - Either of the following can be used:
  * [npm](https://www.npmjs.com/) - npmjs.com
  * [yarn](https://yarnpkg.com/en/) - yarnpkg.com

* Other Dependencies (OSX):
  * Autoconf - `brew install autoconf`
  * Automake - `brew install automake`


## Configure .env for Running Locally

From the root directory of the checked out source:

```
cp .env.example .env
```

The following settings in the new `.env` file need to be updated for the funnel to run properly:

```
APP_ENV=local

APP_KEY=(Generated from `php artisan key:generate`)

DB_HOST=database
DB_PORT=3306
DB_DATABASE=otmportal
DB_USERNAME=root
DB_PASSWORD=secret

```

## Installing Dependencies

```
composer install
npm install
```

## Adding New PHP Libraries

We are using a `composer.lock` to make sure that component versions and install order are consistent on all environments. Requiring adding a new library updates this file.

If you need to add a new library, please install via `composer require` to ensure the lock file is updated.


---

# Running the application

This OTM portal app is dockerized - so getting started _should_ be as easy as running `make up` to build the containers and get you going. No database has been specified in the current config, so you're free to roll you're own (connect to existing local, add a container, etc).

To scaffold the database, run `php artisan migrate` set up the DB tables (if there's any missing columns, please add to the migrations).

We're also using Laravel's Mix to build the FE dependencies. To get started, run a `npm install` - and then running `npm run dev`, or `npm run watch` to watch assets for WebPack build server. For production, run `npm run prod`.

There's also a few custom commands to import Flowbiz data. `php artisan otm:flowbiz:import:agents` to import all REAs and PMs. `php artisan otm:flowbiz:import:products` imports all Flowbiz product (as the product ID's are required for sales)

## Logging & Error Tracking

We use a combination of New Relic, Bugsnag & Papertrail for monitoring, exception tracking & logging.


## Auth

We have 3 auth users, Customers (customer guard), Agents (agent guard), OTM Agents (user guard) - each can log in with an email and password - although it's intended that Customers are only authenticated via 1-time 'signed' URLs - that are sent to them via email and SMS.

## Tests

The tests are currently mostly working. Front end development has broken some of the tests - but until the FE is more resolved, we won't update the repository tests.

To run the test suite, simple run `vendor/bin/phpunit` or `make unit` from this projects root directory.

When adding to the test suite, please use Laravel's model factories and states where relevant. 

We've got 4 test suites that can be run independently:

    - Unit Tests (run by CI/CD) `vendor/bin/phpunit --testsuite Unit`
    - Feature Tests `vendor/bin/phpunit --testsuite Feature`
    - Integration Tests `vendor/bin/phpunit --testsuite Intergration`
    - Flowbiz Tests `vendor/bin/phpunit --testsuite Flowbiz`

### Performance Test

To ensure Flowbiz can handle the level of load we intend to send, there's performance tests under `tests/performance`. Still very small in coverage, by mainly targeting retrieving applications from Flowbiz.  

We've used jMeter for now.

@TODO, load-test our own API


# Technical Details

## API

Our API for 3rd parties to integrate against is available as a:
- [API Blueprint document](docs/connector.apib)
- [Apiary.io private API doc](https://onthemove.docs.apiary.io/)
- [A static single HTML artifact](docs/OTM-api-documentation.html)
    - To recompile this, simply run `npm run generate-doc` 


## Architecture 

<img src="https://confluence.amaysim.net/rest/gliffy/1.0/embeddedDiagrams/fbbe0886-eb8a-4360-9c8f-0718f17fa881.png">

## Connector

### Flow

<img src="https://confluence.amaysim.net/rest/gliffy/1.0/embeddedDiagrams/f8bfa72d-3139-4e5d-902d-f844c611d9d4.png">

## OAuth2 API

<img src="https://confluence.amaysim.net/rest/gliffy/1.0/embeddedDiagrams/f74353c9-f8ab-471d-a795-be27a2363768.png">

```
GET https://rewards.onthemove.com.au/api/v1/agent
  -H 'Accept: application/json'
  -H 'Authorization: Bearer <YOUR_VALID_ACCESS_TOKEN>'
 
// Successful Response
HTTP / 1.1 200 OK
{
    'data': {
        'agent_id': 145056,
        'company_id': 139505,
        'username': 'test',
        'title': 'Mr',
        'first_name': 'Test',
        'last_name': 'Tester',
        'email': 'test@onthemove.com.au',
        'agency': 'OTM',
        'active': 1
    }
}
 
 
// Error Response
HTTP / 1.1 401 Unauthenticated
{
    'message': 'Unauthenticated.'
}
```


## Application Model ERD

![ERD](/graph.png?raw=true "Model ERD")


---

# Journeys

## As a renter

- [ ] I can sign up online, choosing my products and get connected
- [ ] I can also resume my application if I can’t complete it straight away
- [ ] I can get reminders via sms and email to complete my app
- [ ] I also could have received my app via email or sms, in some level of completeness (pre-filled)

## As a landlord

- [ ] I can set up temporary services for my property

## As a real estate agent

- [x] I can log in via the existing FlowBiz portal, that I'm familiar with
- [ ] I can create new app, filling in all my tenants data, with them to fulfill.
- [ ] I can create new apps with my tenant filling in the majority of data, self-fulfilling
- [x] I can create temporary connections on behalf of a landlord, in the Agencies name
- [x] My portal has my branding in it, so my tenants have a consistent experience

## As a On The Move manager

- [x] I can see statistics of applications, by real estate agent.

Whitelabel

## As a retailer offering services to OTM

