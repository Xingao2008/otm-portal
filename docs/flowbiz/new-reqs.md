# Flowbiz API Updates

Several updates to the 2018 Flowbiz API are required, primarily to enable the submission of ‘complete’ temporary connections for a real estate agency, as well as other information about property managers and agencies to assist with user interface and operational requirements.

Where possible, the field type, optionality, and any further context are provided.

**The changes below are to enhance or update the existing API endpoints**

___

<br>

_Update_
## New Application <small>`POST /api/otm/Application`</small>

To enable 'Agent' temporary connections, a company name, ABN, and billing address (for convenience) is required to properly enable theses connections.

Also, for same-day & next-day connections, the "High Priority" flag is required to be expose, allowing for appropriate prioritization.

### New Fields

- `HighPriority` → Boolean, Required
- `CompanyName` → String, Optional
- `CompanyABN` → String, Optional, 11 characters (ABN length)
- `BillingAddress` → Object, Optional _(follows internal presentational format of billing addresses)_


###### Example Request Body _(abbrevaited)_
```
{
    "Application": {
        ...
        "HighPriority": true,
        "CompanyName": "Century 21 Collingwood",
        "CompanyABN": "01234567890",
        ...
    },
    "BillingAddress": {
        "Address 1": "Level 1",
        "Address 2": "90 Collins St",
        "Suburb": "Melbourne",
        "State": "VIC",
        "Postcode": "3000",
        "DPID": "",
    },
    Applicants: {
        ...
    }
    "Address": {
        ...
    },
    "Products": {
        ...
    }
}
```

<br>

_Update_
## Agency List <small>`GET /api/otm/AgencyList/`</small>

For each agency, several new fields are required to enable business temps applications, and better identification of brands and their internal rankings.

### New Fields

- `GSTRegistered` → Boolean
- `Group` → String
- `CompanyType` → String
- `Ranking` → String
- `BusinessPhone` → String _(if not set, fallback to Agency `HomePhone`, or `Mobile` if provided)_
- `Address` → Object, _(split to same level as required for an shown within the new Application UI in Flowbiz i.e. StreetName split to Address 1/2)_

###### Example Response

```
{
    "Result": [{
        "CompanyID": 12345,
        "CompanyName": "Century 21 Collingwood",
        "CompanyType": "Real Estates",
        "Group": "Century 21",
        "ABN": "1234567890",
        "GSTRegistered": Boolean,
        "Ranking": "B - NEW BUSINESS",
        "BusinessPhone": "0312345678",
        "Address": {
            "Address 1": "63 Cambridge St",
            "Address 2": "",
            "Suburb": "Collingwood",
            "Postcode": "3066",
            "State": "VIC",
            "DPID": ""
        }
    },
    ...
    ]
}
```

<br>

_Update_
## Property Managers List <small>`GET /api/otm/PropertyManagers/{AgencyID}`</small>

We also need more information about each property manager within an agency. The `DefaultPropertyManager` boolean must represent if this PM (or user), is the user who can see all rewards for the agency. It's my understanding that this is the 'Default Property Manager', and *not* the 'Rewards Admin Person' field within FlowBiz UI.

### New Fields

- `Mobile` → String
- `BusinessPhone` → String
- `Active` → Boolean
- `UserAccount` → String _(This is the username an agent would log into Flowbiz with)_
- `DefaultPropertyManager` → Boolean

###### Example Response
```
{
    "Result": [{
        "ID": 123,
        "Title": "Dr",
        "Firstname": "Firstname",
        "Lastname": "Lastname",
        "Mobile": "0400301235",
        "BusinessPhone": "0312345678",
        "Email": "a@b.co",
        "Active" true
        "UserAccount" "flastname"
        "Agency": "OTM",
        "DefaultPropertyManager": true
    },
    ...
    ]
}
```





<br><br><br><br><br><br>







# New Flowbiz APIs

The further enable UI and operational requirements, the 2018 Flowbiz API to view and redeem rewards, on a Agency and Property Manager level. In addition, a list of redeemable rewards - and their related data - are also required to enable to redemption of rewards.

Furthermore, a specific rewards 'history' API endpoint is require to provide REAs & PM better visibility of the value they are generating for themselves. 

Show total points, plus a graph of last 30 days - per PM and REA
Redeem rewards for PM or REA


_Note: the URIs, request/response body formats below are examples only._



GET Reward List

rea / pm level

POST redeem rewards
GET redemption history ?days, 

Transaction Type (Earned, Redeemed, Adjustments)
Type (PM, REA)
From
To



GET current balance - today (every time)
GET points history - once daily, cached


___

<br>



## Rewards <small>`GET /api/otm/RewardsList`</small>

Get a list of all redeemable rewards, with their name, and points 'cost', plus any fees OTM charges.


###### Example Response
```
{
    "Results": [{
        "ID": 123,
        "Name": "Bunnings $200 Gift Card",
        "Points": 200,
        "Fee": 2,
        ... _(plus any other data available)_
    },
    ...
    ]
}
```

## Agency Rewards <small>`GET /api/otm/Rewards/{AgencyID}/Points`</small>

Get rewards details for an agency. This API call should include a live 'balance' of point.


###### Example Response
```
{
    "Result": {
        "Balance": {
            "Points": 150,
            "Applications": 20
        }
    }
}
```

## Agency Rewards History <small>`GET /api/otm/Rewards/{AgencyID}/Points/History[?Days=30]`</small>

Get the rewards history for an agency. This API call should include the points balance at the end of each day, plus a flag to indicate if the REA made a redemption on that day.

It should also include the number of Applications submitted each day - which _should_ correlate to the number of points gained from the products sold for those Applications.

This API call should also accept a query, that relates to the number of days the rewards history will return, from today. By default, or if not provided, this value should be 30 days.

###### Example Request Query
```
    Days: 22  // the number of days the history should return, from today. Defaults to 30. 
```
Example URI: `GET /api/otm/Rewards/987654321/Points/History?Days=22` 


###### Example Response
```
{
    "Result": [{
            "Date": "2018-05-03",
            "Applications": 3,
            "Points": 150, // REA Redeemed 50 points yesterday
            "Redeemed": false
        },{
            "Date": "2018-05-02",
            "Applications": 8,
            "Points": 200,
            "Redeemed": true  // REA Redeemed this day
        },{
            "Date": "2018-05-01",
            "Applications": 10,
            "Points": 190,
            "Redeemed": false
        }
        ...
    ]

}
```

## Agency Redemption History <small>`GET /api/otm/Rewards/{AgencyID}/Redeem/History[?Days=30]`</small>

Get the rewards history for an agency. This API call should include the points balance at the end of each day, plus a flag to indicate if the REA made a redemption on that day.

It should also include the number of Applications submitted each day - which _should_ correlate to the number of points gained from the products sold for those Applications.

This API call should also accept a query, that relates to the number of days the rewards history will return, from today. By default, or if not provided, this value should be 30 days.

###### Example Request Query
```
    Days: 22  // the number of days the history should return, from today. Defaults to 30. 
```
Example URI: `GET /api/otm/Rewards/987654321/Points/History?Days=22` 


###### Example Response
```
{
    "Result": [{
            "Date": "2018-05-03 23:52:33",
            "PointsRedeemed": 50, // REA Redeemed 50 points
            "PointsRemaining": 100, 
            "Rewards: [
                {
                    "ID": 123, // The ID of the reward redeemed
                    "Quantity": 2,
                    "TotalPoints": 25,
                    "TotalFees": 2,
                },
                ...
            ]         
        },{
            "Date": "2018-05-02 09:03:01",
            "Applications": 8,
            "Points": 200,
            "Redeemed": true  // REA Redeemed this day
        }
        ...
    ]

}
```

## Property Manager Rewards <small>`GET /api/otm/Rewards/{PropertyManagerID}/Points`</small>

Get rewards details for an Property Manager. This API call should include a live 'balance' of point, as well as the number of applications submitted today.


###### Example Response
```
{
    "Result": {
        "Balance": {
            "Points": 150,
            "Applications": 20
        }
    }
}
```

## Property Manager Rewards <small>`GET /api/otm/Rewards/{PropertyManagerID}/Points/History[?Days=30]`</small>

Get rewards history for an Property Manager. This API call should include the points balance at the end of each day, plus a flag to indicate if the PM made a redemption on that day.

It should also include the number of Applications submitted each day - which _should_ correlate to the number of points gained from the products sold for those Applications.

This API call should also accept a query, that relates to the number of days the rewards history will return, from today. By default, or if not provided, this value should be 30 days.

###### Example Request Query
```
    Days: 22  // the number of days the history should return, from today. Defaults to 30. 
```
Example URI: `GET /api/otm/Rewards/123456789/Points/History?Days=22` 


###### Example Response
```
{
    "Result": [{
            "Date": "2018-05-03",
            "Applications": 3,
            "Points": 150, // PM Redeemed 50 points yesterday
            "Redeemed": false
        },{
            "Date": "2018-05-02",
            "Applications": 8,
            "Points": 200,
            "Redeemed": true  // PM Redeemed this day
        },{
            "Date": "2018-05-01",
            "Applications": 10,
            "Points": 190,
            "Redeemed": false
        }
        ...
    ]

}
```






## Agency Redeem Rewards <small>`POST /api/otm/Rewards/{AgencyID}/Redeem`</small>

Redeem one or many rewards of a certain quantity. RewardID should relate to the IDs in provided in the `RewardList` call. A reward can be redeeming in multiples - i.e. 2x gift cards - thus each reward should have a quantity provided. Optionally, the expected points to be deducted can be provide to in the request.  

###### Example Request Body
```
{
    "Rewards": [
        {
            "RewardID": 123,
            "Quantity": 2,
            "Points": 104
        },
        {
            "RewardID": 456,
            "Quantity": 1,
            "Points": 75
        },
        ...
    ]
}
```

###### Example Successfull Response
```
{
    "Result": [
        {
            "RewardID": 123,
            "Success": true,
            "Message": ""
        },
        {
            "RewardID": 123,
            "Success": true,
            "Message": ""
        },
        ...
    ]
}
```


## Property Managers Redeem Rewards <small>`POST /api/otm/Rewards/{PropertyManagersID}/Redeem`</small>

Redeem one or many rewards of a certain quantity. RewardID should relate to the IDs in provided in the `RewardList` call. A reward can be redeeming in multiples - i.e. 2x gift cards - thus each reward should have a quantity provided. Optionally, the expected points to be deducted can be provide to in the request.  

###### Example Request Body
```
{
    "Rewards": [
        {
            "RewardID": 123,
            "Quantity": 2,
            "Points": 104
        },
        {
            "RewardID": 456,
            "Quantity": 1,
            "Points": 75
        },
        ...
    ]
}
```












<br>
<br>
<br>
<br>
<br>

# OTM Agent Rewards Portal
## Google Tag Manager integration

On _all pages_ within the agent rewards portal, we require the following snippets of code to be placed on the page, in the specificed area of the page.


Place this code as high in the `<head>` of the page as possible:

```
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-ML8788L');</script>
<!-- End Google Tag Manager -->
```


Additionally, place this code immediately after the opening `<body>` tag:
```
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-ML8788L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
```











# Passwords

```
public string EncodePassword(string pass, string salt)
{
    byte[] bytes = Encoding.Unicode.GetBytes(pass);
    byte[] src = Encoding.Unicode.GetBytes(salt);
    byte[] dst = new byte[src.Length + bytes.Length];
    Buffer.BlockCopy(src, 0, dst, 0, src.Length);
    Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
    HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
    byte[] inArray = algorithm.ComputeHash(dst);
    return Convert.ToBase64String(inArray);
}
```