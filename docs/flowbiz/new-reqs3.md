# Flowbiz APIs - Spring '18 Changes

Below are a new API required to help provide agents with their correct business contact, and an update to an existing API to enable more automation of the prep workflow. 

_Update_
## New Application <small>`POST /api/otm/Application`</small>

To enable applications to sent to FlowBIz that are able to queued for a immediate call (awaiting call), bypassing the prepping stage. In the `Application` object we optionally allow a prepping NMI or MIRN to be provided in the request body, as well as any notes relevant to the application.

Furthermore, a new field `CustomerGivenEIC` - mapped to `AutoEIC` field in the flowbiz interface - is to be provided, which will indicate whether a customer has agreed to the EIC without specific products submitted.

### New Fields

- `PrepNMI` → String, Optional, 11 characters
- `PrepMIRN` → String, Optional, 11 characters
- `PrepNotes` → String, Optional
- `CustomerGivenEIC` → Boolean, Optional


##### Example Request Body _(abbreviated)_
```
{
    "Application": {
        ...
        "PrepNMI": "6100123123",
        "PrepNMI": "5400123123",
        "PrepNotes": "NMI is A, clear meter access",
        "CustomerGivenEIC": false, 
        ...
    },
    ...
}
```

_New API_
## Get Real Estate Agency BDMs & AMs <small>`GET /api/otm/BusinessManagers/{AgencyID}`</small>

A new API to retrieve the business contacts for a given agency. These contacts are the 'Assigned To' (Business Manager), and 'Account Manager' fields within the flowbiz interface currently.


##### Example Request Parameters

- `AgencyID` → Integer, Required, eg. 1234567

<br>

The data returned should include the contacts basic details, including their username, user type, and id. The 'assigned to' field should be mapped to 'business manager'. If either field (AM & BM) isn't assigned, and empty object should be returned instead.

##### 200 Success
```
{
    "Results": {
        "AccountManager": {
            "ID": 12345,
            "Username": "acollins",
            "Name": "Alex Collins",
            "Type": "General",
            "Email": "acollins@onthemove.com.au"
        },
        "BusinessManager": {
            "ID": 98765,
            "Username": "tfarrelly",
            "Name": "Tim Farrelly",
            "Type": "General",
            "Email": "tfarrelly@onthemove.com.au"
        }
    }
}
```

###### No BM assigned to Agency

```
{
    "Results": {
        "AccountManager": {
            "ID": 12345,
            "Username": "acollins",
            "Name": "Alex Collins",
            "Type": "General",
            "Email": "acollins@onthemove.com.au"
        },
        "BusinessManager": {}
    }
}
```

###### No AM assigned to Agency

```
{
    "Results": {
        "AccountManager": {},  
        "BusinessManager": {
            "ID": 98765,
            "Username": "tfarrelly",
            "Name": "Tim Farrelly",
            "Type": "General",
            "Email": "tfarrelly@onthemove.com.au"
        }
    }
}
```

###### No AM or BM assigned to Agency

```
{
    "Results": {
        "AccountManager": {},  
        "BusinessManager": {}
    }
}
```

