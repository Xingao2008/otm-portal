# New Flowbiz APIs

Below are 2 new API requirements - with these, all applications should be able to be searched for and found, and specific details about the application requested.


_New API_
## Search for Applications <small>`GET /api/otm/Application/Search?`</small>

A new API endpoint that enables applications to be search for a given criteria. We need to be able to search on any of these fields. If multiple fields are provided, this should be interpreted as an 'AND' statement - where all criteria is required.


###### Example Request Parameters

- `ApplicationType` → String, One of `All`, `Vendor`, `Renter`, `Landlord` or `Purchaser`
- `FromDate` → String, YYYY-MM-DD format, Required. E.g. `2018-01-13` // required
- `ToDate` → String, YYYY-MM-DD format, Required. E.g. `2018-02-28` // required
- `Firstname` → String. E.g. `John`
- `Lastname` → String. E.g. `Smith`
- `Reference` → String. E.g. `A1234567`
- `View` → String. One of `PM`, `AllPMs` or `REA`. E.g. `PM` _"PM" returns applications for that property manager specified in the `AgentID` field. "AllPMs" return applications from all property managers in the real estate agency that the `AgentID` belongs to. "REA" returns all applications for that agency the `AgentID` field belongs to._
- `AgentId` → String, Required. E.g. `1234567` 

The below query string is an valid example of a request that could be made: 
`GET /api/otm/Application/Search?ApplicationType=All&FromDate=2016-01-01&ToDate=2017-01-0&Firstname=John&Lastname=Smith&Reference=A1234567&View=PM&AgentId=1234567`

<br>

The response should contain an array of all Applications that meet the criteria, with their basic details.

###### Example Response
```
{
    "Results": {
        [
            "Reference": "A1234567",
            "Customer": "John Smith",
            "Status": "Completed"
            "Address": "Unit 987, 123 Somewhere St, Someplace, VIC 3000",
            "ConnectionDate": "2016-06-31",
            "AgentId": 1234567
        ],
        ...
    }
}
```

_New API_
## Get Application Details <small>`GET /api/otm/Application/Details/{ApplicationReference}`</small>

A new API to retrieve details about an application - more than it's status. The data returned should be a similar structure to what's provided to the _"New Application"_ API, where applications are submitted. 


###### Example Request Parameters

- `ApplicationReference` → String, Required, eg. "A1234567"


###### 200 Success
```
{
    "Results": {
        "Application": {
            "ConnectionDate": "2018-05-18",
            "ApplicationType": "Agent" | "Normal" | "Landlord Temp",
            "Renter": true,
            "Notes": "",
            "RealEstateID": "139554",
            "PropertyManagerID": "148495"
            "Reference": "A1234567"
            "Status": "Complete"
        },
        "Applicants": [
            {
                "Title": "Mr",
                "Firstname": "John",
                "Lastname": "Smith",
                "DOB": "2018-05-18",
                "Email": "jsmith@email.com",
                "Mobile": "0444111222",
                "Phone": "0999991111",
                "Primary": true,
                "Occupation": "",
                "Notes": "",
                "Identifications": [
                    {
                        "IdType": "Drivers License" | "Concession" | "Passport" | "Other",
                        "Number": "234232",
                        "Expiry": "2018-05-18",
                        "Issuer": "VIC"
                    },
                    ...
                ]
            },
            ...
        ],
        "Address": {
            "PropertyType": "House" | "Unit",
            "StreetNumber": "",
            "StreetName": "",
            "UnitNumber": "",
            "Suburb": "",
            "Postcode": "",
            "State": "",
            "DPID": ""
        },
        "Products": [
            {
                "ID": 99898909,
                "ProductType": "",
                "Provider": "Origin",
                "ConnectionDate": "2018-05-18",
                "ConnectionDetails": {
                    "Fieldname": "Value",
                    ...
                    ...
                }
            },
            ...
        ]
    }
}
```

