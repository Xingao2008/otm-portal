let mix = require('laravel-mix');
const webpack = require('webpack');
const { BugsnagSourceMapUploaderPlugin } = require('webpack-bugsnag-plugins')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
if (mix.inProduction()) {
  mix.options({
    uglify: {
      uglifyOptions: {
        compress: {
          drop_console: true,
        }
      }
    }
  });
}
// Additonal webpack config for Mix to use
mix.webpackConfig({
  plugins: [
    // reduce bundle size by ignoring moment.js locale files
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    // jquery.. amiright
    new webpack.ProvidePlugin({
      //   $: 'jquery',
      //   jQuery: 'jquery',
      //   'window.jQuery': 'jquery',
      //   'window.$': 'jquery',
      //   svg4everybody: 'svg4everybody',
      'window.moment': 'moment',
      moment: 'moment',
      //   // angular: 'angular'
    }),

    // new BugsnagSourceMapUploaderPlugin({
    //   apiKey: '9b73c773336c8d53bd22394dd2613d07'
    // })
  ],
  
  // module: {
  //   loaders: [{
  //       test: /[\/]angular\.js$/,
  //       loader: "exports?angular"
  //     },
  //     // Angular HMR - wip
  //     // {
  //     // 		test: /(signup|quote)\.js$/,
  //     // 		use: [{
  //     // 			loader: 'angular-hot-loader',
  //     // 			options: {
  //     // 				log: true,
  //     // 				rootElement: 'div#ng'
  //     // 			}
  //     // 		}
  //   ]
  // }
});

mix.js('resources/assets/js/app.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .sourceMaps()
  .version();

//mix.js('resources/assets/js/app.js', 'public/js');
//.sass('resources/assets/sass/app.scss', 'public/css');


// https://docs.bugsnag.com/build-integrations/webpack/ for prod.