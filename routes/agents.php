<?php



// --------- Authentication -------------------------------------------------------------------------------- //


Route::namespace("Auth\Agent")->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('agent.login');
    Route::post('login', 'LoginController@login')->name('agent.login');
    Route::any('logout', 'LoginController@logout')->name('agent.logout');

    // Registration Routes...
    // Route::get('register', 'RegisterController@showRegistrationForm')->name('agent.register');
    // Route::post('register', 'RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('agent.password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('agent.password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('agent.password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('agent.password.reset');
});




// --------- Application -------------------------------------------------------------------------------- //

Route::namespace("Agent")->name('agent.')->middleware('auth:agent')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');

   


    Route::get('application/search', 'ApplicationController@search')->name('application.search');
    Route::post('application/search', 'ApplicationController@searchPost')->name('application.search');

    Route::get('application/{reference}', 'ApplicationController@showFromFlowbiz')->name('application.show.fromFlowbiz')->where('reference', 'A\d{7}');;


    Route::resource('application', 'ApplicationController');
    Route::resource('service', 'ServiceController');


    Route::get('resources', 'ResourceController@index')->name('resource.index');


});
