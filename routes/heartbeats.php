<?php

use Illuminate\Http\Request;
use OnTheMove\TwoOneTwoEff\RewardsApi;
use OnTheMove\Flowbiz\Api as FlowbizApi;

Route::get('/', function () {
    return response()->json([
        'health' => true,
    ]);
})->middleware('web')->name('application');

Route::get('/flowbiz', function () {
    try {
        $fbApi = new FlowbizApi(10);
        $appNo = 'A0022034';
        $resp = $fbApi->getApplicationDetails($appNo);
        if (gettype($resp) !== 'array') {
            return response()->json([
                'health' => false,
            ]);
        }
        if (isset($resp['Results']['Application']['ApplicationReference'])) {
            $appRef = $resp['Results']['Application']['ApplicationReference'];

            return response()->json([
                'health' => ($appRef === $appNo),
            ]);
        }

        return response()->json([
            'health' => false,
            'message' => 'Reference mismatch',
        ]);
    } catch (\Exception $e) {
        return response()->json([
            'health' => false,
            'message' => $e->getMessage(),
        ]);
    }
})->middleware('web')->name('flowbiz');

Route::get('/rewards', function () {
    try {
        $rewards = new RewardsApi(172771);
        $points = $rewards->getPointsBalance();

        return response()->json([
            'health' => (isset($points['property_manager_balance'])),
        ]);
    } catch (\Exception $e) {
        return response()->json([
            'health' => false,
            'message' => $e->getMessage(),
        ]);
    }
})->middleware('web')->name('rewards');

Route::get('/auth/user-api', function () {
    return response()->json([
        'health' => true,
    ]);
})->middleware('auth:user-api')->name('auth.user-api');

Route::get('/auth/user-api/ip-whitelist', function () {
    return response()->json([
        'health' => true,
    ]);
})->middleware(['auth:user-api', 'apiipaddress'])->name('auth.user-api.whitelist');


Route::get('/auth/oauth-api', function () {
    return response()->json([
        'health' => true,
    ]);
})->middleware('auth:agent-oauth')->name('auth.oauth-api');
