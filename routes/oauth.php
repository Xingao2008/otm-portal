<?php


/*
|--------------------------------------------------------------------------
| OAuth Routes
|--------------------------------------------------------------------------
|
| Registered manually instead of 'Passport::routes()' in the Auth Service
| Provider to allow for a different middleware group (agents) and the
| removal of un-used routes
|
*/


Route::post('oauth/token', ['uses' => 'AccessTokenController@issueToken', 'middleware' => 'throttle']);

Route::prefix('oauth')
        ->middleware(['web', 'auth:agent'])
        ->group(function () {

            /**
             * Register the routes needed for authorization.
             */
            Route::get('/authorize', ['uses' => '\OnTheMove\Http\Controllers\Auth\OAuth\AuthorizationController@authorize']);
            Route::post('/authorize', ['uses' => 'ApproveAuthorizationController@approve']);
            Route::delete('/authorize', ['uses' => 'DenyAuthorizationController@deny']);

            /**
             * Register the routes for retrieving and issuing access tokens.
             */
            Route::get('/tokens', ['uses' => 'AuthorizedAccessTokenController@forUser']);
            Route::delete('/tokens/{token_id}', ['uses' => 'AuthorizedAccessTokenController@destroy']);

            /**
             * Register the routes needed for refreshing transient tokens.
             */
            Route::post('/token/refresh', ['uses' => 'TransientTokenController@refresh']);

            /**
             * Register the routes needed for managing clients.
             */
            Route::get('/clients', ['uses' => 'ClientController@forUser']);
            Route::post('/clients', ['uses' => 'ClientController@store']);
            Route::put('/clients/{client_id}', ['uses' => 'ClientController@update']);
            Route::delete('/clients/{client_id}', ['uses' => 'ClientController@destroy']);

            /**
             * Register the routes needed for managing personal access tokens.
             */
            Route::get('/scopes', ['uses' => 'ScopeController@all']);
            Route::get('/personal-access-tokens', ['uses' => 'PersonalAccessTokenController@forUser']);
            Route::post('/personal-access-tokens', ['uses' => 'PersonalAccessTokenController@store']);
            Route::delete('/personal-access-tokens/{token_id}', ['uses' => 'PersonalAccessTokenController@destroy']);
        });
