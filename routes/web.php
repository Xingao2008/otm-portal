<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::prefix('auth')->namespace("Auth")->group(function () {
//     Route::get('/agent/{agent}', 'AuthController@loginAgent')->name('auth.agent.login')->middleware('signed');
//     Route::get('/customer/{customer}/{application}', 'AuthController@loginCustomer')->name('auth.customer.login')->middleware('signed');

// });


// --------- AGENT -------------------------------------------------------------------------------- //




// --------- ADMIN -------------------------------------------------------------------------------- //





// --------- Customer -------------------------------------------------------------------------------- //


Route::prefix('customer')->namespace("Customer")->name('customer.')->group(function () {
    Route::get('/retrieveFromEmail/{customer}', 'CustomerController@email')->name('email')->middleware('signed');
    Route::get('/{customer}/{application}', 'CustomerController@toComplete')->name('application.complete');//->middleware('can:view,application');
});



// --------- GENERIC ROUTES -------------------------------------------------------------------------------- //


Route::get('admin/login', 'LoginController@showLoginForm')->name('login');



// --------- API ROUTES ------------------------------------------------------------------------------------ //

Route::prefix('api/v1')->namespace("Api")->name('api.v1.')->group(function () {
    Route::get('/validate/email', 'ValidateController@email')->name('validate.email');
    Route::get('/validate/mobile', 'ValidateController@mobile')->name('validate.mobile');
    Route::get('/validate/address', 'ValidateController@address')->name('validate.address');
    Route::post('/validate/address/split', 'ValidateController@addressSplit')->name('validate.address.split');
    
    Route::get('/healthcheck', function () {
        return response()->json([
                'health' => true,
                'version' => 'has',
            ]);
    })->name('health.check');
});


// Route::prefix('health')->group(function () {
//     Route::get('/check', function () {
//         return response()->json([
//             'health' => true,
//         ]);
//     })->name('health.check');
//     Route::get('/version', function () {
//         return response()->json([
//             'verion' => true,
//         ]);
//     })->name('health.version');
    
//     // Route::get('/customer/{customer}/{application}', 'AuthController@loginCustomer')->name('auth.customer.login')->middleware('signed');

//     // Route::get('/agent/{agent}/impersonate', 'AuthController@impersonateAgent')->name('auth.admin.impersonate.agent')->middleware('auth:web');
// });



Route::get('rewards', function () {
    //if (env('APP_ENV') === 'production') {
        return redirect("https://redeem.onthemove.com.au/dashboard");
    // } else {
    //     $url = "https://" . env('APP_ENV') . ".redeem.onthemove.com.au";

    //     return redirect($url);
    // }
})->name('rewardsportal');
