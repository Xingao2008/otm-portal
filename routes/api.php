<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::namespace("Api")->prefix('v1')->name('api.v1.')->group(function () {

    // frontend
    Route::middleware('auth:agent-api')->group(function () {
        Route::post('/application', 'ApplicationController@store')->name('application.store');
        Route::post('/search', 'ApplicationController@search')->name('application.search');

        Route::get('/agent/points', 'AgentController@pointsBalance')->name('agent.points.balance');
        Route::get('/agent/points/history', 'AgentController@pointsHistory')->name('agent.points.history');

    });

    // oauth
    Route::middleware('auth:agent-oauth')->group(function () {
        Route::get('/agent', 'AgentController@show')->name('agent.get');
    });



    // api
    Route::middleware(['auth:user-api', 'apiipaddress'])->group(function () {
        Route::post('/connector', 'ConnectorController@store')->name('connector.store');
    });

});
