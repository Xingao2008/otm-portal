<?php



// Authentication Routes for Admins (Staff)
Route::prefix('admin')->name('admin.')->group(function () {

    Route::namespace("Admin")->middleware('auth:web')->group(function () {

        Route::get('/', 'DashboardController@index')->name('dashboard');
        

        Route::get('product/{product_id}/restore', 'ProductController@restore')->name('product.restore');


        Route::get('pm/{agent}/reset-password', 'PropertyManagerController@forgotPassword')->name('pm.password.reset');
        Route::post('pm/{agent}/reset-password', 'PropertyManagerController@resetPassword')->name('pm.password.reset');

        Route::get('pm/{agent}/impersonate', 'PropertyManagerController@impersonate')->name('pm.impersonate');
        Route::get('pm/impersonate/exit', 'PropertyManagerController@exitImpersonation')->name('pm.impersonate.exit');
        Route::get('pm/{pm_id}/restore', 'PropertyManagerController@restore')->name('pm.restore');
        

        Route::get('rea/{rea_id}/restore', 'RealEstateAgencyController@restore')->name('rea.restore');
        Route::get('rea/sync', 'RealEstateAgencyController@sync')->name('rea.sync');
        Route::post('rea/sync', 'RealEstateAgencyController@syncPost')->name('rea.sync.post');

        Route::get('rea/{rea}/rewards-type', 'RealEstateAgencyController@rewardsTypeEdit')->name('rea.rewards.edit');
        Route::patch('rea/{rea}/rewards-type', 'RealEstateAgencyController@rewardsTypeUpdate')->name('rea.rewards.update');


        Route::get('application/queue', 'ApplicationController@queue')->name('application.queue');
        Route::get('application/queue/{application_exception}', 'ApplicationController@match')->name('application.match');
        Route::patch('application/queue/{application_exception}', 'ApplicationController@fix')->name('application.match');

        Route::name('rea.')->group(function () {
            Route::resource('rea/resource', 'ResourceController');
        });

        Route::resource('product', 'ProductController');
        Route::resource('pm', 'PropertyManagerController');
        Route::resource('rea', 'RealEstateAgencyController');
        Route::resource('application', 'ApplicationController');
        
        
        

    });

    Route::namespace("Auth\User")->group(function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('login');
        Route::get('logout', 'LoginController@logout')->name('logout');
        Route::post('logout', 'LoginController@logout')->name('logout');


        // Registration Routes...
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register')->middleware('can:administer');
        Route::post('register', 'RegisterController@register')->name('register')->middleware('can:administer');

        Route::get('users', 'AdminController@index')->name('user.index')->middleware('can:administer');
       // Route::get('users/{user}', 'AdminController@edit')->name('user.edit')->middleware('can:administer');
        Route::get('users/{user}/key', 'AdminController@editApiKey')->name('user.edit-key')->middleware('can:administer');
        Route::patch('users/{user}/key', 'AdminController@updateApiKey')->name('user.update-key')->middleware('can:administer');
       // Route::patch('users/{user}', 'AdminController@update')->name('user.update')->middleware('can:administer');


        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
    });
});
