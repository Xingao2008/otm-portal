<header class="header py-4">

  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="header-brand" href="/">
        <img src="/img/otmportal.svg" class="header-brand-img" alt="On The Move Portal">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        @include('components.admin.menu') 
        
        @if($user)
        <ul class="navbar-nav">

          <li class="nav-item dropdown">

            <a href="#" class="nav-link dropdown-toggle pr-0 leading-none" data-toggle="dropdown">
              <span class="ml-2 d-none d-lg-block text-right">

                <span class="text-default">{{ $user->name }}</span>
                <small class="text-muted d-block mt-1">{{ $user->agency }}</small>

              </span>
              @if($user->group)
              <img class="rounded border ml-2" height="38" width="38" src="/img/groups/{{ strtolower(camel_case($user->group)) }}.png"> {{--
              <span class="avatar avatar-lg" style="background-image: url('/img/groups/{{ strtolower(camel_case($user->group)) }}.png')"></span> --}} @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">

              @can('administer')
              <a class="dropdown-item" href="{{ route('admin.user.index', [], false) }}">
                <i class="dropdown-icon fe mr-1 fe-user"></i>All Users
              </a>
              <a class="dropdown-item" href="{{ route('admin.register', [], false) }}">
                <i class="dropdown-icon fe mr-1 fe-user-plus"></i>Create User
              </a>
              <div class="dropdown-divider"></div>
              @endcan

              <a class="dropdown-item" href="{{ route('admin.logout', [], false) }}">
                <i class="dropdown-icon fe mr-1 fe-log-out"></i>Sign out
              </a>
            </div>
          </li>
      </ul>
      @endif
    </div>
    </div>
  </nav>

</header>