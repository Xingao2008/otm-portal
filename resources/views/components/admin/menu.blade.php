<ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle {{ areActiveRoutes(['admin.application.index', 'admin.application.create']) }}" data-toggle="dropdown"><i class="fe fe-check-square mr-1"></i> Applications</a>
      <div class="dropdown-menu dropdown-menu-arrow">
        <a href="{{ route('admin.application.index', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.application.index') }}">All Applications</a>
        <a href="{{ route('admin.application.queue', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.application.queue') }}">Application Queue</a>
      </div>
    </li>
    {{-- <li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fe fe-package mr-1"></i> Products</a>
      <div class="dropdown-menu dropdown-menu-arrow">
          <a href="{{ route('admin.product.index', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.product.index') }}">All Products</a>
        <div class="dropdown-divider"></div>
        <a href="{{ route('admin.product.index', [], false) }}" class="dropdown-item">Provider Logos</a>
      </div>
    </li> --}}
    <li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle {{ areActiveRoutes(['admin.rea.*', 'admin.pm.*']) }}" data-toggle="dropdown"><i class="fe fe-users mr-1"></i> REAs & PMs</a>
      <div class="dropdown-menu dropdown-menu-arrow">
        <a href="{{ route('admin.rea.index', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.rea.index') }}">All Real Estate Agencies</a>
        {{-- <a href="{{ route('admin.pm.index', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.pm.index') }}">All Property Managers</a> --}}
        <h6 class="dropdown-header">ADMIN</h6>
        <a href="{{ route('admin.rea.sync', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.rea.sync') }}">Sync from Flowbiz</a>
        <h6 class="dropdown-header text-black">RESOURCES</h6>
        <a href="{{ route('admin.rea.resource.index', [], false) }}" class="dropdown-item {{ isActiveRoute('admin.rea.resource') }}">REA Resources</a>
      </div>
    </li>

    <li class="nav-item">
      <a id="barelog-trigger"  class="nav-link"><i class="fe fe-bell mr-1"></i> Updates</a>
    </li>


</ul>