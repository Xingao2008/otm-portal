@if($user->agent_id)
<footer class="footer mt-5">
  <div class="container">

    <div class="row align-items-end flex-row">
      <div class="col-4">
        <h4>On The Move Portal</h4>
        <nav class="list-unstyled">
          <li><a href="{{ route('agent.application.create', [], false) }}">New application</a></li>
          <li><a href="{{ route('rewardsportal') }}" target="_blank">Redeem rewards</a></li>
        </nav>

      </div>
      <div class="col-4">
        <h4>Contact Us</h4>
        <nav class="list-unstyled">
          <li>Call: <a href="tel:1300850360">1300 850 360</a></li>
          <li>Email: <a href="mailto:service@onthemove.com.au">service@onthemove.com.au</a></li>
        </nav>
      </div>
      <div class="col text-right">
        <p class="mb-0 text-muted">Copyright &copy; {{ date('Y') }} On The Move Pty Ltd</p>
      </div>
    </div>
  </div>
</footer>
@else
<footer class="footer py-4 mt-5">
  <p class="mb-0 text-center text-muted">Copyright &copy; {{ date('Y') }} On The Move Pty Ltd</p>
</footer>
@endif