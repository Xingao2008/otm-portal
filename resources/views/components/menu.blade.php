<ul class="navbar-nav align-items-center mr-auto">
  <li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle {{ areActiveRoutes(['agent.application.index', 'agent.application.create']) }}" data-toggle="dropdown"><i class="fe fe-check-square mr-1"></i>Applications</a>
    <div class="dropdown-menu dropdown-menu-arrow">
      <a href="{{ route('agent.application.create', [], false) }}" class="dropdown-item {{ isActiveRoute('agent.application.create') }}">Submit new application</a>
<div class="dropdown-divider"></div>
      <a href="{{ route('agent.application.index', [], false) }}" class="dropdown-item {{ isActiveRoute('agent.application.index') }}">My applications</a>
      
      
      <a href="{{ route('agent.application.search', [], false) }}" class="dropdown-item {{ isActiveRoute('agent.application.search') }}">Search applications</a>
    </div>
  </li>
  @if($user->hasIndividualRewards || $user->hasBusinessRewards)
  <li class="nav-item">
    <a href="{{ route('rewardsportal') }}" target="_blank" class="nav-link "><i class="fe fe-dollar-sign  mr-1"></i>Rewards</a>
  </li>
  @endif
</ul>