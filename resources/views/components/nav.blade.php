




<header class="header py-4">

<nav class="navbar navbar-expand-lg navbar-light">
  <div class="container">
  <a class="header-brand" href="/">
    <img src="/img/otmportal.svg" class="header-brand-img" alt="On The Move Portal">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    @include('components.menu')

    @if($user)
    <ul class="navbar-nav">
      <li class="nav-item cursor-normal">
      <otm-agent-nav-points 
        v-bind:data-rewards-visibility="{{ json_encode($user->hasRewards) }}
      "></otm-agent-nav-points>
        </li>
      <li class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle pr-0 leading-none" data-toggle="dropdown">
            <span class="ml-2 d-none d-lg-block text-right">
             
              <span class="text-default">{{ $user->displayName }}</span>
              <small class="text-muted d-block mt-1">{{ $user->displayAgency }}</small>
               
            </span>
            @if($user->group)
            <img class="rounded border ml-2" height="38" width="38" src="/img/groups/{{ strtolower(camel_case($user->group)) }}.png">
                {{-- <span class="avatar avatar-lg" style="background-image: url('/img/groups/{{ strtolower(camel_case($user->group)) }}.png')"></span> --}}
              @endif
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            {{-- <a class="dropdown-item" href="#">
              <i class="dropdown-icon fe fe-user"></i> Profile
            </a>
            <div class="dropdown-divider"></div>
             --}}
             <a class="dropdown-item" href="{{ route('agent.resource.index', [], false) }}">
              <i class="dropdown-icon fe fe-paperclip"></i> Resources
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('agent.logout', [], false) }}">
              <i class="dropdown-icon fe fe-log-out"></i> Sign out
            </a>
          </div>
      </li>
      

      </ul>
      @endif
  </div>
  </div>
</nav>

</header>