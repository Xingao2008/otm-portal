<div class="card">
    <div class="card-body p-3 text-center @if(!isset($previous)) pt-6 @endif">
        @if(isset($previous))
        <div class="d-flex">
            <p class="display-inline-block ml-auto mb-0 @if($current < $previous)       
                text-red
            @elseif($current > $previous)
                text-green
            @elseif($current === $previous)
                text-gray
            @endif"
                data-toggle="tooltip"
                data-placement="right" 
                title="Comparatively to this time, last month"
            >
            @if($previous === 0 && $current === 0)
                0%
            @elseif($current === 0)
                -&infin;
            @elseif($previous === 0)
                &infin;
            @else
                {{ round(($current / $previous - 1) * 100) }}%
            @endif

            @if($current < $previous)       
                <i class="fe fe-trending-down ml-2"></i>
            @elseif($current > $previous)
                <i class="fe fe-trending-up ml-2"></i>
            @elseif($current === $previous)
                <i class="fe fe-minus ml-2"></i>
            @endif
            </p>
        </div>
        @endif
        
        <div class="h1 m-0">
            @if(is_numeric($current))
            {{ $current }}
            @else
                <span class="text-muted">N/A</span>
            @endif
        </div>
        <div class="text-muted mb-4">{{ $slot }}</div>
    </div>
</div>