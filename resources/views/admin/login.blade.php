@extends('layouts.plain')

@section('content')


<div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6 mt-6">
                <img src="/img/otmportal.svg" class="h-6" alt="">
              </div>
              <form class="card" method="POST" action="{{ route('admin.login', [], false) }}">
                @if($errors->any())
                <div class="card-alert alert alert-danger mb-0">
                  <ul class="list-unstyled mb-0">
                    @foreach ($errors->all() as $error)
                    <li><i class="fe fe-arrow-right mr-2"></i>{{ $error }}</li>
                    @endforeach
                  </ul>
                  </div>
                @endif
                @csrf
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>
                  <div class="form-group">
                    <label class="form-label">Email Address</label>
                    <input type="text" name="email" tabindex="1" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('email') }}"  placeholder="Enter email" required autofocus>
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                      <a href="{{ route('admin.password.request', [], false) }}" class="float-right small">Forgot password?</a>
                    </label>
                    <input type="password" name="password" tabindex="2" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                      <span class="custom-control-label">Remember me</span>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" tabindex="3" class="btn btn-primary btn-block">Sign in</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>



@endsection
