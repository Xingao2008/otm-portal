@extends('layouts.admin') 

@section('content')
<div class="container mt-6">

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Users</h4>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email / API Key</th>
                    <th>Last Login</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    @if(Auth::guard('web')->user()->role->name !== 'adminstrator')
                        @if(in_array($user->role->name, ['api', 'api-external']))
                            @continue
                        @endif
                    @endif
                <tr>
                    <td>
                        {{ $user->name ?? "No Name" }}
                        <div class="small text-muted">
                           {{ studly_case(optional($user->role)->name ?? "N/A") }}
                        </div>
                    </td>
                    
                    <td>
                        @if(!optional($user->role)->name)
                            N/A
                        @else 
                            @if(in_array($user->role->name, ['api', 'api-external']))
                                <div class="d-flex flex-row align-items-center">
                                    <code class="code-overflow">{{ $user->api_token ?? "No Token" }}</code>
                                    @if($user->allowed_ips && $user->role->name === "api-external")
                                        <span class="ml-2 tag tag-green">IP Restricted</span>
                                    @elseif(!$user->allowed_ips && $user->role->name === "api-external")
                                        <span class="ml-2 tag tag-danger">IP Unrestricted</span>
                                    @endif
                                </div>
                            @else
                                {{ $user->email }}
                            @endif
                        @endif
                    </td>
                    <td>
                        @if(!$user->last_login_at)
                            N/A
                        @else
                            <code>{{$user->last_login_ip}}</code>
                            <div class="small text-muted">
                                {{ optional($user->last_login_at)->diffForHumans() }}
                            </div>
                        @endif
                    </td>
                    <td>
                        @if(optional($user->role)->name)
                            @if(in_array($user->role->name, ['api', 'api-external']))
                            <a href="{{ route('admin.user.edit-key', $user, false) }}" class="btn btn-secondary btn-sm">Edit API Details</a>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>
</div>
@endsection