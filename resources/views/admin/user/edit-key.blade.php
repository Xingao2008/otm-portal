@extends('layouts.admin') 

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="POST" action="{{ route('admin.user.update-key', ['user' => $u->id], false) }}">
                <div class="card">
                    <div class="card-header">Update API Details</div>
                    
                    <div class="card-body">
                        @method('PATCH')
                        @csrf

                         <div class="form-group">
                            <label for="allowed_ips" class="form-label">API Key</label>
                            <pre>{{$u->api_token }}</pre>
                        </div>
                        
                        <div class="form-group">
                            <label for="allowed_ips" class="form-label">Allowed IP Addresses
                                 <span class="form-label-small">Comma seperated list</span>
                            </label>
                            
                            <textarea class="form-control{{ $errors->has('allowed_ips') ? ' is-invalid' : '' }}"  name="allowed_ips" id="allowed_ips" rows="3">{!! $u->diplayAllowedIps !!}</textarea>
                          
                          
                            @if ($errors->has('allowed_ips'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('allowed_ips') }}</strong>
                                </span>
                            @endif
                        </div>
          

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            Update User
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection