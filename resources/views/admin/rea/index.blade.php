@extends('layouts.admin') 

@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Real Estate Agencies</h4>
            <div class="card-options">
                <form method="GET" action="{{ route('admin.rea.index', [], false) }}">
                <div class="input-group">
                    <input type="text" class="form-control form-control-sm" placeholder="Search..." name="q">
                    <span class="input-group-btn ml-2">
                    <button class="btn btn-sm btn-default" type="submit">
                        <span class="fe fe-search"></span>
                    </button>
                    </span>
                </div>
                </form>
            </div>
        </div>
      
        <div class="table-resposive">
            <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Group</th>
                        <th colspan="2">State</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rea as $r)
                    <tr class="small {{ ($r->trashed() || $r->isActive) ?: "text-muted text-strikethrough" }}">
                        <td>{{ $r->company_name }}</td>
                        <td>{{ $r->group }}</td>
                        <td class="{{ (!$r->state) ? 'text-muted' : '' }}">{{ $r->state ?? "N/A" }}</td>
                        <td>
                            {{-- @if(!$r->trashed() &&  $r->isActive) --}}
                                <a href="{{ route('admin.rea.show', $r, false) }}" class="btn btn-info btn-xs">View</a>
                            {{-- @else --}}
                                {{-- <a href="{{ route('admin.rea.restore', $r, false) }}" class="btn btn-secondary btn-sm">Restore</a> --}}
                            {{-- @endif --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{ $rea->appends(request()->only('q'))->links() }}
    

</div>
@endsection