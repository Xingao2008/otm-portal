@extends('layouts.admin') 

@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Sync from Flowbiz</h4>
        </div>
        <div class="card-body">
            
            <h4>Sync REA & PM records from Flowbiz</h4>
            <p>Uses an create-or-update methodology - so records will be overwritten with new data.</p>

            <form method="POST" action="{{ route('admin.rea.sync.post', [], false) }}">
                @csrf

                <button class="btn btn-azure">Sync Records</button>
            </form>
            <p class="mt-2 text-muted">Will take a long time. One day it'll stream the output here...</p>
            @if(session()->has('reaSync'))

            <hr>

            <p>Exit Code: {!! session()->get('reaSync.exit') !!}</p>
            <pre style="height: 50%; overflow: scroll;">{!! session()->get('reaSync.output') !!}</pre>
            {{ session()->forget('reaSync') }}
            @endif
        </div>
    </div>
    
    

</div>
@endsection