@extends('layouts.admin') 

@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Real Estate Agencies Resources</h4>
            <div class="card-options">
            <a class="btn btn-sm btn-success" href="{{ route('admin.rea.resource.create', [], false) }}">New Resource</a>
            </div>
        </div>
      
        <div class="table-resposive">
            <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th colspan="2">Filename</th>
                    </tr>
                </thead>
                <tbody>
                    @if($resources->count() !== 0)
                        @foreach($resources as $r)
                        <tr>
                            <td>{{ $r->name }}</td>
                            <td>{{ $r->type }}</td>
                            <td>
                                {{-- {{ $r->getMedia($r->type)->first() }} --}}
                            </td>
                            <td>
                                <a href="{{ route('admin.rea.resource.show', $r, false) }}" class="btn btn-info btn-sm">View Resource</a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                    <tr>
                        <td colspan="4">Nothing yet... </td>
                    </tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>

    

</div>
@endsection