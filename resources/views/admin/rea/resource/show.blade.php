@extends('layouts.admin') 

@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Real Estate Agencies Resource</h4>
            <div class="card-options">
            <a class="btn btn-sm btn-primary" href="{{ route('admin.rea.resource.index', [], false) }}">All  Resources</a>
            </div>
        </div>
      
        <div class="card-body">
              
            @foreach($resource->getMedia($resource->type) as $media)
                <h5>{{ $media->name }}</h5>
                <a class="btn btn-sm btn-success" target="_blank" href="{{ $media->getFullUrl() }}"><i class="fe fe-download mr-2"></i>Download</a>
                
            @endforeach
        
        </div>
        <div class="card-footer bg-danger">
            <form action="{{ route('admin.rea.resource.destroy', $resource, false) }}" method="POST">
                @method("DELETE")
                @csrf
                <button class="btn btn-white" type="submit">DELETE</button>
            </form>
        </div>
    </div>

    

</div>
@endsection