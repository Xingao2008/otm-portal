@extends('layouts.admin') 

@section('content')
<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">New Real Estate Agencies Resources</h4>
        </div>      

        <div class="card-body">
            <form class="form" method="POST" action="{{ route('admin.rea.resource.store', [], false) }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="state">REA State</label>
                  <select class="form-control" name="state" id="state" required>
                    <option value="">--- SELECT STATE ---</option>
                    <option value="VIC">VIC</option>
                    <option value="NSW">NSW</option>
                    <option value="QLD">QLD</option>
                    <option value="SA">SA</option>
                  </select>
                </div>
                <hr>
                <div class="form-group">
                  <label for="name">Resource Name</label>
                  <input type="text"
                    class="form-control" name="name" id="name" aria-describedby="helpName" placeholder="The name of the resource">
                  <small id="helpName" class="form-text text-muted">Customer Facing</small>
                </div>

                <div class="form-group">
                  <label for="file">Upload File</label>
                  <input type="file" class="form-control-file" name="file" id="file" placeholder="" aria-describedby="fileHelpId" required>
                  <small id="fileHelpId" class="form-text text-muted">PDF Only (currently)</small>
                </div>

                <button type="submit" class="btn btn-primary">Upload</button>
            </form>
        </div>
        </div>
    </div>

    

</div>
@endsection