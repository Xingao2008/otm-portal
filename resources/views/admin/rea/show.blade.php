@extends('layouts.admin') @section('content')
<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        @if($rea->group)
                        <img class="rounded border mr-5" height="75" width="75" src="/img/groups/{{ strtolower(camel_case($rea->group)) }}.png">
                        @endif
                        <div class="media-body">
                            <h4 class="m-0 pt-2">{{ $rea->company_name }}</h4>
                            <p class="text-muted mb-0">{{ $rea->group }}</p>
                        </div>
                    </div>
                </div>
                @if($rea->fullAddress)
                <div class="card-map card-map-placeholder" style="background-size: cover; background-image: url('{{ getMapUrl($rea->fullAddress) }}')"></div>
                @endif
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="col-12">
                            <div class="h6 mb-0">Address</div>
                            <p>{{ $rea->fullAddress ?? "N/A" }}</p>
                        </div>
                        <div class="col-6">
                            <div class="h6 mb-0">ABN</div>
                            <p class="text-monospace">{{ $rea->abn ?? "Not Provided" }}</p>
                        </div>
                        <div class="col-6">
                            <div class="h6 mb-0">Active</div>
                            <p>{{ $rea->isActive ? "Yes" : "No" }}</p>
                        </div>
                        <div class="col-12">
                            <div class="h6 mb-0">Rewards Type</div>
                            <p>{{ $rea->rewards_type ?? "N/A" }} <a class="ml-2" href="{{ route('admin.rea.rewards.edit', $rea) }}"><span class="badge badge-warning">Edit</span></a></p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="row">
                <div class="col"> 
                    <div class="card">
                        <div class="card-body p-3 text-center">
                            <div class="h1 m-0 pt-4">{{ $points['real_estate_agency_balance'] ?? "N/A" }}</div>
                            <div class="text-muted mb-4">Points Balance</div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body p-3 text-center">
                            <div class="h1 m-0 pt-4">{{ $apps->count() }}</div>
                            <div class="text-muted mb-4">Apps (MTD)</div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body p-3 text-center">
                            <div class="h1 m-0 pt-4">{{ $products }}</div>
                            <div class="text-muted mb-4">Products (MTD)</div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Property Managers</h4>
                </div>
                <div class="table-resposive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Details</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rea->agents as $agent)
                            <tr class="{{ (!$agent->trashed() && $agent->active) ?: " text-muted text-strikethrough " }}">
                                <td>
                                    {{ $agent->name }}
                                    @if($agent->is_default_agent)
                                        <br>
                                        <span class="tag tag-grey">Default PM</span>
                                    @endif
                                </td>
                                <td class="small">
                                    <strong>Email:</strong> {{ $agent->email ?? "No Email Address" }}
                                    @if($agent->username)
                                        <br><strong>Username:</strong> {{ $agent->username }}
                                    @endif
                                </td>
                                <td>
                                    @if(!$agent->trashed())
                                    <a href="{{ route('admin.pm.show', $agent, false) }}" class="btn btn-info btn-sm"><i class="fe fe-eye mr-1" aria-hidden="true"></i>View</a>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">
                                            <i class="fe fe-settings"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="{{ route('admin.pm.impersonate', $agent, false) }}" data-turbolinks="false" class="dropdown-item"><i class="fe fe-user-check mr-1" aria-hidden="true"></i>Impersonate</a>
                                            <a href="{{ route('admin.pm.password.reset', $agent, false) }}"  class="dropdown-item"><i class="fe fe-shield mr-1" aria-hidden="true"></i>Reset Password</a>
                                        </div>
                                    </div>

                                    @endif
                                    
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>



</div>
@endsection