@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <div class="card">
                 
                <div class="card-header">
                    <h4 class="card-title">Change Rewards Type</h4>
                </div>
                @if ($errors->any())
                    <div class="card-alert alert alert-danger mb-0">
                        <ul class="list-unstyled mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">
                    <p>Change the rewards type for an Agency. This affects which agents can be shown rewards within the Portal</p>
                    <form action="{{ route('admin.rea.rewards.update', $rea, false) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">

                            <label class="form-label" for="rewards_type">Rewards Type</label>
                            <select class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="rewards_type" id="rewards_type">
                                <option value="">N/A</option>
                                @foreach($rea::REWARDS_TYPES as $type)
                                    <option value="{{ $type }}" @if($rea->rewards_type === $type) {{ "selected" }} @endif>{{$type}}</option>
                                @endforeach
                       </select>
                 

                        </div>
                     
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection