@extends('layouts.plain')

@section('content')


<div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6 mt-6">
                <img src="/img/otmportal.svg" class="h-6" alt="">
              </div>
              
                <form class="card" method="POST" action="{{ route('admin.password.email', [], false) }}">
                    @csrf

                    @if (session('status'))
                        <div class="card-alert alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                <div class="card-body p-6">
                  <div class="card-title">Forgot password</div>
                  <p class="text-muted">Enter your email address and you will be emailed a link to reset your password.</p>
                  <div class="form-group">

                    <label class="form-label" for="email">Email address</label>
                    <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"   placeholder="Enter email" value="{{ old('email') }}" required autofocus>

            
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                  </div>
                </div>
              </form>
              <div class="text-center text-muted">
                Forget it, <a href="{{ route('admin.login', [], false) }}">send me back</a> to the sign in screen.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
