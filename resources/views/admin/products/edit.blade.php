@extends('layouts.admin') @section('content')
<div class="container">

  <div class="row">
    <div class="col-8">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Edit {{ $product->name }}
            <span class="tag tag-{{ $product->color }}">{{ $product->type }}</span>
          </h3>
        </div>
        <div class="card-body">

          {{ Form::model($product, ['route' => ['admin.product.update', $product->id]]) }} @csrf @method('PATCH')

          <div class="form-group">
            <label class="form-label">Description</label>
            <textarea name="description" class="form-control" rows="2">{{ $product->description }}</textarea>
          </div>
          <div class="form-group">
            <label class="form-label">Features</label>
            <textarea name="features" class="form-control" rows="5">{{ $product->features }}</textarea>
          </div>

          <div class="form-group">
            <label class="form-label">State</label>
            {{ Form::select('state', ['' => 'N/A', 'VIC' => 'VIC', 'NSW' => 'NSW', 'QLD' => 'QLD', 'SA' => 'SA', 'ACT' => 'ACT'], null, ['class' => 'form-control'])
            }}



          </div>

          <div class="form-footer">
            <button type="submit" class="btn btn-primary btn-block">Save</button>
          </div>

          {{Form::close() }}
        </div>
      </div>
    </div>
    <div class="col">
      
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Terms & Conditions EIC</h3>
          </div>
          <div class="card-body">
            blah
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Concession EIC</h3>
          </div>
          <div class="card-body">
            blah
          </div>
        </div>
     

          <div class="card text-white bg-danger mb-3">
      <div class="card-body p-1 d-flex justify-content-between">
        <h3 class="h3ull-left m-0">
          <a class="text-white" href="https://www.youtube.com/watch?v=d3D7Y_ycSms" target="_blank">DANGER ZONE</a>
        </h3>
        <form role="form" method="POST" id="deleteForm" action="{{ route('admin.product.destroy', $product, false) }}">
          @method('DELETE') @csrf
          <p class="card-text">
            <button type="submit" class="btn btn-sm btn-white" id="deleteBtn">Delete Product</button>
          </p>
        </form>
      </div>
    </div>
    </div>
    </div>









    

    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Delete Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>
              <img src="https://media.giphy.com/media/3oEjHLzm4BCF8zfPy0/giphy.gif" class="img-fluid" alt="Are you sure?">
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" id="confirm-yes" class="btn btn-danger">Delete</button>
            <button type="button" id="confirm-no" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>


  </div>
  @endsection @section('foot')
  <script type="text/javascript" defer>
    (function () {

      $('#deleteBtn').on('click', function (e) {
        e.preventDefault();

        $('#deleteModal').modal({
            backdrop: 'static',
            keyboard: false
          })
          .on('click', '#confirm-yes', function () {
            $('#deleteForm').submit();
          })
          .on('click', '#confirm-yes', function () {
            $('#deleteModal').modal('hide');
          });
        return false;
      });

    }());
  </script>
  @endsection