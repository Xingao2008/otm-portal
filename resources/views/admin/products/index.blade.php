@extends('layouts.admin') 

@section('content')
<div class="container">

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Products</h4>
        <div class="card-options">
            <form method="GET" action="{{ route('admin.product.index', [], false) }}">
                <div class="input-group">
                    <select class="form-control form-control-sm" name="type">
                        <option value="" {{ (request()->query('type') === '') ? "selected" : "" }}>All Products</option>
                        @foreach($types as $type)
                        <option value="{{ $type }}" {{ (request()->query('type') === $type) ? "selected" : "" }}>{{ $type }}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn ml-2">
                        <button class="btn btn-sm btn-default" type="submit">
                            <span class="fe fe-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>

    </div>
    <div class="table-resposive">
        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>State</th>
                    <th>Provider</th>
                    <th colspan="2">Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $p)
                <tr class="{{ !$p->trashed() ?: "text-muted text-strikethrough" }}">
                    <td>{{ $p->type }}</td>
                    <td class="{{ (!$p->state) ? 'text-muted' : '' }}">{{ $p->state ?? "N/A" }}</td>
                    <td>{{ $p->provider }}</td>
                    <td>{{ $p->name }}<br><small class="text-muted">{{$p->getOriginal('name')}}</small></td>
                    <td>
                        @if(!$p->trashed())
                        <a href="{{ route('admin.product.edit', $p, false) }}" class="btn btn-info btn-sm">Manage</a>
                        @else
                        <a href="{{ route('admin.product.restore', $p, false) }}" class="btn btn-secondary btn-sm">Restore</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection