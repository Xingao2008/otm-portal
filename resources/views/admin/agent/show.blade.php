@extends('layouts.admin') 
@section('content')
<div class="container">
    <div class="row">
        <div class="col-5">
            <div class="card">
                <div class="card-body">
                    <div class="media">
                        <img class="rounded border mr-5" height="75" width="75" src="/img/groups/{{ strtolower(camel_case($agent->group)) }}.png">
                        <div class="media-body">
                            <h4 class="m-0 pt-2">{{ $agent->name }}</h4>
                            <p class="text-muted mb-1">{{ $agent->realEstateAgency->company_name }}</p>
                            
                            @if($agent->is_default_agent)
                            <p class="m-0 p-0"><span class="tag tag-grey">Default PM</span></p>
                            @endif
                        </div>
                    </div>
                </div>
                @if($agent->fullAddress)
                <div class="card-map card-map-placeholder" style="background-size: cover; background-image: url('{{ getMapUrl($agent->fullAddress) }}')"></div>
                @endif
                <div class="card-body pb-1">
                    <div class="row">
                        <div class="col-12">
                            <div class="h6">Username</div>
                            <p>{{ $agent->username ?? "No Username" }}</p>
                        </div>          
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="h6">Email Address</div>
                            <p><a href="mailto:{{ $agent->email }}">{{ $agent->email }}</a></p>
                        </div>      
                    </div>
                </div>
                <div class="card-footer">
                    <div class="btn-list">
                        <a href="{{ route('admin.pm.impersonate', $agent, false) }}" data-turbolinks="false" class="btn btn-sm btn-primary"><i class="fe fe-user-check mr-1" aria-hidden="true"></i>Impersonate</a>
                        <a href="{{ route('admin.pm.password.reset', $agent, false) }}" class="btn btn-sm btn-danger"><i class="fe fe-shield mr-1" aria-hidden="true"></i>Reset Password</a>
                        
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Rewards</h4>
                        </div>
                        <div class="card-body p-3 text-center">
                            <div class="row">
                            @if($points)
                            <div class="col">
                                <div class="h1 m-0 pt-4">{{ $points['property_manager_balance'] ?? "N/A" }}</div>
                                <div class="text-muted mb-4">PM Points Balance</div>
                            </div>
                            @endif
                            @if($agent->is_default_agent && $points)
                            <div class="col">
                                <div class="h1 m-0 pt-4">{{ $points['real_estate_agency_balance'] ?? "N/A" }}</div>
                                <div class="text-muted mb-4">REA Points Balance</div>
                            </div>
                            @endif
                            </div>
                        </div>
                    </div>
                    {{-- <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">History</h4>
                        </div>
                        <div class="table-resposive">
                            <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                                <thead>
                                    <tr>
                                        <th>App ID</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Retailer</th>
                                        <th>Points</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($history['real_estate_agency']['history'] as $line)
                                    <tr class="small">
                                        <td>{{ $line['reference'] }}</td>
                                        <td>{{ $line['connection_date'] }}</td>
                                        <td>{{ $line['category'] }}</td>
                                        <td>{{ $line['retailer'] }}</td>
                                        <td>{{ $line['points'] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>

{{-- "reference" => "A1234567",
                        "connection_date" => "2018-06-20",
                        "sale_date" => "2018-06-30",
                        "state" => "VIC",
                        "category" => "Power",
                        "retailer" => "AGL",
                        "points" => 123, --}}



        <div class="col-7">


            @if($agent->applications->count() !== 0)
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Applications</h4>
                    
                    <div class="card-options">
                        <form method="GET" action="{{ route('admin.pm.show', [$agent], false) }}">
                            <div class="input-group">
                                <select class="form-control form-control-sm" name="filter">
                                    <option value="order-desc" {{ (request()->query('filter') === "order-desc") ? "selected" : "" }}>Most Recent</option>
                                    <option value="order-asc" {{ (request()->query('filter') === "order-asc") ? "selected" : "" }}>Oldest</option>
                                    <option value="status-submitted" {{ (request()->query('filter') === "status-submitted") ? "selected" : "" }}>Submitted</option>
                                    <option value="status-past" {{ (request()->query('filter') === "status-past") ? "selected" : "" }}>Past Connection Date</option>
                                    <option value="status-unsubmitted" {{ (request()->query('filter') === "status-unsubmitted") ? "selected" : "" }}>Not Submitted</option>
                                </select>
                                <span class="input-group-btn ml-2">
                                    <button class="btn btn-sm btn-default" type="submit">
                                        <span class="fe fe-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table-resposive">
                    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Reference</th>
                                <th width="130">Status</th>  
                                <th>Address</th>
                                <th colspan="2">Products</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($applications as $app)
                                @if(!$app->flowbiz_application_id)
                                    @continue
                                @endif
                            <tr>
                                <td>
                                    @if($app->flowbiz_application_id)
                                         <a href="{{ route('agent.application.show', ['application' => $app], false) }}" class="text-underline">{{ $app->flowbiz_application_id }}</a>
                                    @else
                                         <span class='text-muted'>N/A</span>
                                    @endif
                                </td>
                                <td>
                                    <span class="status-icon bg-{{ $app->statusColor }}"></span>
                                    <small>{{ $app->flowbiz_application_status ?? "Not Submitted" }}</small>
                                </td>
                                <td><small>{{ $app->fullAddress }}</small></td>
                                <td>
                                @if($app->services->count() === 0) 
                                    <span class='text-muted'>N/A</span>
                                @else
                                    @foreach($app->services as $service)
                                        <span class="tag tag-{{ $service->product->color }}">{{ $service->product->type }}</span>
                                    @endforeach
                                @endif
                                </td>
                                <td class="text-right">
                                    <a href="{{ route('agent.application.show', ['application' => $app], false) }}" class="btn btn-xs btn-primary">View<i class="fe fe-arrow-right ml-1"></i></a>
                                    </td>

{{--                                 
                                <td>
                                   <i class="fe fe-info"></i>


                                {{-- <div class="item-action dropdown">
                                  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="false"><i class="fe fe-more-vertical"></i></a>
                                  <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> Action </a>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-edit-2"></i> Another action </a>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-message-square"></i> Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-link"></i> Separated link</a>
                                  </div>
                                </div> 
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $applications->appends(request()->only('filter'))->links() }} @else
            <h5 class="text-center text-muted text-300 p-3">
                <i class="fe fe-alert-circle mr-2"></i>No Applications Submitted</h5>
            @endif
        </div>
    </div>


</div>



</div>
@endsection