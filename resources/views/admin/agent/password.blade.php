@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <div class="card">
                 
                <div class="card-header">
                    <h4 class="card-title">Manually Reset Password</h4>
                </div>
                @if ($errors->any())
                    <div class="card-alert alert alert-danger mb-0">
                        <ul class="list-unstyled mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">

                    <p>Resetting the password for <strong>{{ $agent->displayName }}</strong> from <strong>{{ $agent->agency }}</strong></p>
                    <form action="{{ route('admin.pm.password.reset', $agent, false) }}" method="POST">
                        @csrf
                        <div class="form-group">

                            <label class="form-label" for="password">Password</label>
                            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  name="password" required id="password" placeholder="New Password">
 
                        </div>
                        <div class="form-group">

                            <label class="form-label" for="password">Confirm Password</label>
                            <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required id="password_confirmation" placeholder="Confirm New Password">
                
                        </div>

                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection