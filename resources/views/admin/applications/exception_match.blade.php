@extends('layouts.admin') 

@section('content')
<div class="container mt-6">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Failed Application</h4>
            <div class="card-options">
                
            </div>
        </div>
    
        <div class="card-body">
            <h2>{{ title_case($exception->type) }}</h2><br>
            <div class="row">
                <div class="col-6">
                    <form action="{{ route('admin.application.match', $exception, false) }}" method="POST">
                        @method('PATCH')
                        @csrf

                        @include('admin.applications.exceptions.' . snake_case($exception->type))
                      
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
                <div class="col-6">
                    <h4>Raw data provided:</h4>
                    <pre><code class="language-json">{{ json_encode(json_decode($exception->payload), JSON_PRETTY_PRINT) }}</code></pre>
                    <h4 class="mt-5">All data available:</h4>
                    <pre><code class="language-json">{{ json_encode(json_decode($app->toJson()), JSON_PRETTY_PRINT) }}</code></pre>
                </div>
            </div>
        </div>

        
    
</div>
</div>

@endsection

@section('foot')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism-coy.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/components/prism-json.min.js"></script>
@endsection