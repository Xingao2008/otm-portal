@extends('layouts.admin') 

@section('content')
<div class="container mt-6">

<div class="card">
    <div class="card-header">
        <h4 class="card-title">Failed Application Queue</h4>
    </div>
    @if($apps->count())
    <div class="table-resposive">
        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
            <thead>
                <tr>
                    <th>App</th>
                    <th>State</th>
                    <th>Issue</th>
                    <th colspan="2">Age</th>
                </tr>
            </thead>
            <tbody>
                @foreach($apps as $app)
                <tr>
                    <td>{{ $app->id }}</td>
                    <td class="{{ (!$app->state) ? 'text-muted' : '' }}">{{ $app->state ?? "N/A" }}</td>
                    <td>{{ title_case($app->type) }}</td>
                    <td>{{$app->created_at->diffForHumans() }}</td>
                    <td>
                       <a href="{{ route('admin.application.match', $app, false) }}" class="btn btn-secondary btn-sm">Fix Application</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    <div class="card-body">
        <p class="p-6 text-center text-muted">No application waiting in queue</p>
    </div>
    @endif
</div>
</div>
@endsection