<otm-date-picker 
    :name="'connection_date'" 
    :label="'Connection Date'" 
    :column="false" 
    :required="true" 
    :placeholder="'DD / MM / YYYY'"
    :config="{
        wrap: true,
        altFormat: 'd / m / Y',
        altInput: true,
        dateFormat: 'Y-m-d',
        minDate: 'today'
    }"
></otm-date-picker>