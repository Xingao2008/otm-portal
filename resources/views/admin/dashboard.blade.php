@extends('layouts.admin')

@section('content')

<div class="container mt-6">

    <div class="row">
        <div class="col-12">
            <h1 class="ml-5">Dashboard</h1>
        </div>
   
    </div>
    <div class="row row-cards row-deck">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Applications Received</h3>
                </div>
    
                <div class="card-body pb-0">
                    @if($apps->count() !== 0)
                        <otm-admin-chart
                            :series-y-data='{!! $apps->keys() !!}'
                            :series-x-data='{!! $apps->values() !!}'
                            :series-x-data-name="'Applications'"
                        ></otm-admin-chart>
                    @else
                        <p class="text-muted">No Applications in last 30 days</p>
                    @endif
                </div>

            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Exceptions Mapped</h3>
                </div>
    
                <div class="card-body pb-0">
                    @if($exceptions->count() !== 0)
                    <otm-admin-chart
                        :series-y-data='{!! $exceptions->keys() !!}'
                        :series-x-data='{!! $exceptions->values() !!}'
                        :series-x-data-name="'Exceptions'"
                    ></otm-admin-chart>
                    @else
                        <p class="text-muted">No Exceptions in last 30 days</p>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Useful Links</h3>
                </div>
                <div class="card-body">
                    <p>
                        <a href="https://redeem.onthemove.com.au/?miltonLogout=true" target="_blank" class="btn btn-primary">Log out of rewards<i class="ml-2 fe fe-external-link"></i></a><br>
                        <small class="text-muted">Note: this will show an 'error' with in their, but it works.</small>
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
