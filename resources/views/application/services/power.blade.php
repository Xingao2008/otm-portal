<div class="row p-3">
    <div class="col-3 mb-1">
        <h5 class="mb-0">NMI</h5>
        <p>{{ $service->details->nmi ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Connection options</h5>
        <p>{{ $service->details->connection_options ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Appointment time</h5>
        <p>{{ $service->details->appointment_time ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Fee acceptance</h5>
        <p>{{ $service->details->fee_acceptance ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Additional access notes</h5>
        <p>{{ $service->details->additional_access_notes ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Meter outside</h5>
        <p>{{ $service->details->meter_outside ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Hazards</h5>
        <p>{{ $service->details->hazards ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Dog on premises</h5>
        <p>{{ $service->details->dog_on_premises ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Access issues</h5>
        <p>{{ $service->details->access_issues ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Marketing opt out</h5>
        <p>{{ $service->details->marketing_opt_out ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Life support</h5>
        <p>{{ $service->details->life_support ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Power on</h5>
        <p>{{ $service->details->power_on ?? "N/A" }}</p>
    </div>
    <div class="col-3 mb-1">
        <h5 class="mb-0">Remote safety confirmation</h5>
        <p>{{ $service->details->remote_safety_confirmation ?? "N/A" }}</p>
    </div>
</div>
