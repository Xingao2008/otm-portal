@extends('layouts.default')

@section('content')


<div class="container">
    <div class="row">

        <div class="col-12 col-lg-10 offset-lg-1">
            <h1 class="ml-5 mb-4  mt-6">All applications</h1>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Applications</h3>
                    <div class="card-options">
                            <a href="{{ route('agent.application.search') }}" class="btn btn-primary btn-sm"><i class="fe fe-search mr-1"></i>Search for an application</a>
                        </div>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th>Reference</th>
                                <th width="130">Status</th>  
                                <th>Address</th>
                                <th colspan="2">Products</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($apps as $app)
                                @if(!$app->flowbiz_application_id)
                                    @continue
                                @endif
                            <tr>
                                <td>
                                    @if($app->flowbiz_application_id)
                                         <a href="{{ route('agent.application.show', ['application' => $app], false) }}" class="text-underline">{{ $app->flowbiz_application_id }}</a>
                                    @else
                                         <span class='text-muted'>N/A</span>
                                    @endif
                                </td>
                                <td>
                                    <span class="status-icon bg-{{ $app->statusColor }}"></span>
                                    <small>{{ $app->flowbiz_application_status ?? "Not Submitted" }}</small>
                                </td>
                                <td><small>{{ $app->fullAddress }}</small></td>
                                <td>
                                @if($app->services->count() === 0) 
                                    <span class='text-muted'>N/A</span>
                                @else
                                    @foreach($app->services as $service)
                                        <span class="tag tag-{{ $service->product->color }}">{{ $service->product->type }}</span>
                                    @endforeach
                                @endif
                                </td>
                                <td class="text-right">
                                    <a href="{{ route('agent.application.show', ['application' => $app], false) }}" class="btn btn-xs btn-secondary">View application<i class="fe fe-arrow-right ml-1"></i></a>
                                    </td>

{{--                                 
                                <td>
                                   <i class="fe fe-info"></i>


                                {{-- <div class="item-action dropdown">
                                  <a href="javascript:void(0)" data-toggle="dropdown" class="icon" aria-expanded="false"><i class="fe fe-more-vertical"></i></a>
                                  <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> Action </a>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-edit-2"></i> Another action </a>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-message-square"></i> Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-link"></i> Separated link</a>
                                  </div>
                                </div> 
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if( $apps->links())
                <div class="card-footer">
                    {{ $apps->appends(request()->query())->links() }}
                </div>
                @endif
            </div>
        </div>

    </div>
</div>

@endsection

