@extends('layouts.default')

@section('content')

<div class="container mt-6">
    <div class="row">
        <div class="col">
            <h1>Application</h1>
        </div>
        {{-- <div class="col text-right">
            @if($app->flowbiz_exported === null)
            <a href="{{ route('agent.application.edit', ['app' => $app]) }}" class="btn btn-primary mr-1">Edit Application</a>
            @endif
            <a href="{{ route('agent.application.index') }}" class="btn btn-info">Back</a>
        </div> --}}
    </div>
    <form>


    <div class="row row-cards">
      <div class="col-lg-5 col-md-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Details</h3>

          </div>
          @if($app->fullAddress)
          <div class="card-map card-map-placeholder" style="background-size: cover; background-image: url('{{ getMapUrl($app->fullAddress) }}')"></div>
          @endif
          <div class="card-body">
          
          @if($app->fullAddress)
          <h6 class="mb-2">Application Address</h6>
          <address class="h4 font-weight-normal">{{ $app->fullAddress ?? "N/A" }}</address>
          @else
          <label for="address">Application Address</label>
          <input type="text" id="address" class="form-control" name="address">
          @endif
            <div class="row mt-5">
              <div class="col-6">
                <div class="h6 mb-0">For</div>
                <p>{{ ($app->renter) ? "Renter" : "Landlord" }}</p>
              </div>
              <div class="col-6">
                <div class="h6 mb-0">Type</div>
                <p>{{ $app->appType->type }}</p>
              </div>
              <div class="col-6">
                <div class="h6 mb-0">Connection Date</div>
                <p>{{ optional($app->connection_date)->format('d / m / Y') ?? "N/A" }}</p>
              </div>
              <div class="col-6">
                <div class="h6 mb-0">Reference</div>
                <p>{{ $app->flowbiz_application_id }}</p>
              </div>

            </div>
            <div class="media mt-5">
              <img class="d-flex mr-5 rounded" src="https://placehold.it/50" alt="Generic placeholder image">
              <div class="media-body">
                <h5 class="mb-0 mt-2">{{ $app->realEstateAgent->name }}</h5>
                <p class="text-muted">{{ $app->realEstateAgent->realEstateAgency->company_name }}</p>
              </div>
            </div>
          </div>
        </div>


    </div>
      <div class="col-lg-7 col-md-6">
        @foreach($app->customers as $customer)
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Applicant</h3>
          </div>
          <div class="card-body pb-2">

              <div class="row">
                  <div class="col">
                      <div class="media">
                          <span class="avatar avatar-xxl mr-5 img-thumbnail" style="background-image: url('{{ $customer->getGravatarImage() }}')"></span>
                          <div class="media-body">
                            <h3 class="m-0 mt-3">
                                <label>First Name</label>
                                <input type="text" class="form-control" value="{{$customer->firstname}}">

                                <label>Last Name</label>
                                <input type="text" class="form-control" value="{{$customer->lastname}}">


                                @if($customer->id === $app->primary_customer_id)
                                    <span class="ml-2 tag tag-blue position-relative" style="top:-0.2rem;">Primary</span>
                                @endif
                            </h3>
                            <p class="text-muted mb-0">{{$customer->occupation}}</p>

                            <div class="row mt-5">
                              <div class="col-12">
                                <h6 class="mb-1">Email</h6>
                                <h4 class="font-weight-normal">{{ $customer->email }}</h4>
                              </div>

                              <div class="col-6">
                                <h6 class="mb-1">Phone</h6>
                                <h4 class="font-weight-normal">{{ $customer->mobile }}</h4>
                              </div>
                              <div class="col-6">
                                <h6 class="mb-1">Land Line</h6>
                                <h4 class="font-weight-normal">{{ $customer->phone ?? "N/A" }}</h4>
                              </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

              @if($customer->identifications)
              <div class="row row-cards row-deck mt-5">
                @foreach($customer->identifications as $id)
                <div class="col">
                    <div class="card">
                        <div class="card-body d-flex align-content-between flex-wrap pb-2">
                            <div class="align-items-start">
                            <h5><i class="fa fa-address-card-o"></i>&ensp;{{$id->IdType($id->type)}}</h5>
                            <h6 class="mb-1">
                                {{$id->type->type}}
                                <small>{{$id->type->issuer}}</small>
                            </h6>
                        </div>
                            <div class="row">
                                <div class="col"><p class="font-monospace">{{$id->type->number}}</p></div>
                                <div class="col"><p>{{ optional($id->type->expiry)->format('m / Y') ?? ""}}</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
              </div>
              @else
                No ID
              @endif
          </div>
        </div>
        @endforeach
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Services</h3>
          </div>
          <div class="table-responsive">
              <table class="table card-table table-striped table-vcenter">
                  <thead>
                      <tr>
                          <th>Type</th>
                          <th>Product</th>
                          <th>Details</th>
                          <th colspan="2">Conncetion Date</th>
                      </tr>
                  </thead>
                  <tbody>
                     @if($app->services->count() === 0)
                         <tr>
                             <td class="text-muted" colspan="4">
                                 No Services
                             </td>
                        </tr>
                     @else
                         @foreach($app->services as $service)
                          <tr>
                              <td>{{ $service->product->type }}</td>
                              <td>{{ $service->product->name }}</td>
                              <td>
                                  @switch($service->product->type)
                                      @case('Power')
                                          <span class="text-muted">NMI:</span> <small class="font-monospace">{{ $service->details->nmi }}</small>
                                          @break

                                      @case('Gas')
                                          <span class="text-muted">MIRN:</span> <small class="font-monospace">{{ $service->details->mirn }}</small>
                                          @break

                                      @default
                                          <span class="text-muted">N/A</span>
                                  @endswitch
                              </td>
                              <td class="text-right">{{ $service->connection_date->format('d/m/Y') }}</td>
                              <td>
                                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#productModal{{ $loop->index }}">
                                    Details
                                  </button>
                              </td>
                          </tr>

                          <!-- Button trigger modal -->


                          <!-- Modal -->
                          <div class="modal fade" id="productModal{{ $loop->index }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">{{ $service->product->type }} Details</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    @includeIf('application.services.'. strtolower($service->product->type))
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>

                          @endforeach
                      @endif
                  </tbody>
              </table>
          </div>
          @if(!$app->flowbiz_application_id)
          {{-- <div class="card-footer">
              <a href="#" class="btn btn-block btn-primary"><i class="fe fe-plus mr-2"></i>Add another product</a>
           </div> --}}
          @endif



        </div>


      </div>
    </div>

</div>

@endsection
