@extends('layouts.default') @section('content')

<otm-application>
    <form>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>New Application</h1>
                </div>
            </div>


            <div class="row">
                <div class="col">
                    <otm-applicant v-bind:is-primary="true"></otm-applicant>
                    <otm-applicant v-bind:is-primary="false"></otm-applicant>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Address</h3>
                        </div>
                        <div class="card-map card-map-placeholder" style="background-image: url(https://tabler.github.io/tabler/demo/staticmap.png)"></div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Find you address</label>
                                <div class="input-icon mb-3">
                                    <input type="text" class="form-control" placeholder="123 Home St...">
                                    <span class="input-icon-addon">
                                        <i class="fe fe-search"></i>
                                    </span>
                                </div>

                            </div>

                            <hr>
                            <example-component></example-component>
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="dob">Date of Birth</label>
                                <input type="date" class="form-control" id="dob" name="dob" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile</label>
                                <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="phone">Land Line</label>
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="occupation">Occupation</label>
                                <input type="text" class="form-control" id="occupation" name="occupation" placeholder="">
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</otm-application>
@endsection