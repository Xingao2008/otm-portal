@extends('layouts.plain')

@section('content')


        <div class="container pt-6">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">

                        <h3 class="ml-5 mb-4 mt-6">Your Application</h3>
                    <div class="card">
                        @if($app->fullAddress)
                        <div class="card-map card-map-placeholder" style="background-size: cover; background-image: url('{{ getMapUrl($app->fullAddress) }}')"></div>
                            @endif
                        <div class="card-body p-6">
                            <h5>Let's get your utilities connected {{ $app->customers->first()->firstname }}</h5>

                            

                            <pre>{{ $app->toJson() }}</pre>
                        </div>
                    </div>
                    


                </div>
            </div>
        </div>

