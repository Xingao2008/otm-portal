@extends('layouts.default')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1>New Application</h1>
                <form action="{{ route('customers.store', [], false) }}" method="POST">
                    @csrf
                    <p><button class="btn btn-primary" type="submit">Submit App</button></p>
                </form>


            </div>
        </div>


@endsection
