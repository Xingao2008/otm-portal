@extends('layouts.plain')

@section('content')


<div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6 mt-6">
                <img src="/img/otmportal.svg" class="h-6" alt="">
              </div>
                
              <form class="card" method="POST" action="{{ route('agent.login', [], false) }}">
                @if ($errors->any())
                    <div class="card-alert alert alert-danger mb-0">
                        <ul class="list-unstyled mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @csrf
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>
                  <div class="form-group">
                    <label class="form-label">Username
                      <span class="form-label-small">
                          <span class="form-help form-help-primary" v-tooltip.auto="{ html:'tooltipContent'}">?</span>
                      </span>
                    </label>
                    <input type="text" name="username" tabindex="1" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" value="{{ old('username') }}"  placeholder="e.g. jsmith" required autofocus tabindex="1">
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                      <a href="{{ route('agent.password.request', [], false) }}" data-turbolinks="false" class="float-right small">Forgot password?</a>
                    </label>
                    <input type="password" name="password" tabindex="2" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  required tabindex="2">
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                      <span class="custom-control-label">Remember me</span>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" tabindex="3" class="btn btn-primary btn-block">Log in</button>
                  </div>
                </div>
              </form>
              
               <p class="text-center">
                 <small>
                   <a class="text-muted text-uppercase" href="{{ route('admin.login', [], false) }}">
                        Admin login
                    </a>
                    </small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

<span style="display:none;">
  <aside id="tooltipContent" >
      <p class="small mb-0">Your username is generally the first letter of your first name, and your surname - all lowercase</p>
  </aside>
</span>
@endsection
