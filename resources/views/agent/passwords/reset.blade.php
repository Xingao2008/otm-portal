@extends('layouts.plain')

@section('content')


<div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="/img/otmportal.svg" class="h-6" alt="">
              </div>

                  @if ($errors->has('token'))
                        <div class="card-alert alert alert-danger mb-0">
                            <strong>{{ $errors->first('token') }}</strong>
                        </div>
                    @endif

              <form class="card" method="POST" action="{{ route('agent.password.request', [], false) }}">
               @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                <div class="card-body p-6">
                  <div class="card-title">Reset password</div>
                  <p class="text-muted">Enter your email address and new password.</p>

                    <div class="form-group">
                        
                    <label class="form-label" for="email">Email address</label>
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="Enter email">
                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                    </div>
                    <div class="form-group">

                    <label class="form-label" for="password">Password</label>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required id="password" placeholder="New Password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                <div class="form-group">

                    <label class="form-label" for="password">Confirm Password</label>
                    <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required id="password_confirmation" placeholder="Confirm New Password">
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>

                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>



@endsection
