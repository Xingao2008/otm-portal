@extends('layouts.default')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <h3 class="ml-5 mb-4 mt-6">Resources</h1>
        </div>

        <div class="col-6">

            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">Property manager resources</h2>
                </div>
                <table class="table card-table">
                    <tbody>
                        @foreach($resources as $resource)
                            @foreach($resource->getMedia($resource->type) as $media)
                                <tr>
                                    <td>{{ $resource->name }}</td>
                                    <td class="text-right">
                                        <a class="btn btn-sm btn-success" target="_blank" href="{{ $media->getFullUrl() }}"><i class="fe fe-download mr-2"></i>Download</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>


        </div>
        <div class="col-6">
            <div class="card">

                <div class="card-body">
                    <h3>Welcome to the On The Move partner resource area!</h3>
                    <p class="lead">Here you will find links to key information on processing applications, how-to guides and useful information for your tenants and owners.</p>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection