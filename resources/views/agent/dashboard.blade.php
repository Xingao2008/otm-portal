@extends('layouts.default')

@section('content')

    
    <div class="container mt-5">
        @if($alert)
        <div class="alert alert-warning mb-6">
            <h4 class="mb-1"><i class="fe fe-alert-triangle pr-2"></i>{{ $alert->header }}</h4>
            <p class="mb-0">{{ $alert->body }}</p>
        </div>
        @endif

        <div class="row mb-4">
            @php
                $time = date("H");
    
                if ($time < "12") {
                    $timeOfDay = "Good morning";
                } else if ($time >= "12" && $time < "17") {
                    $timeOfDay = "Good afternoon";
                } else if ($time >= "17") {
                    $timeOfDay = "Good evening";
                } else {
                    $timeOfDay = "Welcome";
                }
    
            @endphp
            <div class="col-12">
                <h1 class="ml-5 mb-4 text-400">{{ $timeOfDay }} {{ $user->displayFirstName }}</h1>
            </div>
            <div class="col">
                @component('components.agent.tile', ['current' => $appsThisMonth->count(), 'previous' => $appsLastMonth->count()])
                    Applications this month
                @endcomponent
            </div>
            <div class="col">
                @component('components.agent.tile', ['current' => $productsThisMonth, 'previous' => $productsLastMonth])
                    Products this month
                @endcomponent
            </div>
            @if($user->hasIndividualRewards)
            <div class="col">
                @component('components.agent.tile', ['current' => $points['property_manager_balance']])
                    Your points balance
                @endcomponent
            </div>
            @endif
            @if($user->hasBusinessRewards)
            <div class="col">
                @component('components.agent.tile', ['current' => $points['real_estate_agency_balance']])
                    Agency points balance
                @endcomponent
            </div>
            @endif
        </div>



        <div class="row">
            <div class="col-4">
                <h3 class="ml-5 mb-4">Quick links</h1>

                <div class="card card-profile">
				   	 <div class="card-header" style="background-image: url('/img/renters.jpg');"></div>
                     <div class="card-body">
						<a href="{{ route('agent.application.create', [], false) }}" class="btn btn-primary btn-lg btn-block"><i class="fe fe-check-square mr-1"></i>Submit new application</a>
                     </div>
               </div>
               @if($user->hasIndividualRewards || $user->hasBusinessRewards)
               <div class="card card-profile">
				   	 <div class="card-header" style="background-image: url('/img/rewards.jpg');"></div>
                     <div class="card-body">
						<a href="{{ route('rewardsportal') }}" target="_blank" class="btn btn-primary btn-lg btn-block"><i class="fe fe-dollar-sign mr-1"></i>Redeem rewards</a>
                     </div>
               </div> 
                @endif

            </div>
            <div class="col-8">
                <h3 class="ml-5 mb-4">Latest applications</h3>
              
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Applications</h3>
                        @if($recentApps->count() !== 0)
                        <div class="card-options">
                            <a href="{{ route('agent.application.index') }}" class="btn btn-primary btn-sm">All applications<i class="fe fe-arrow-right ml-1"></i></a>
                        </div>
                        @endif
                    </div>
                    <div class="table-responsive">
                        <table class="table card-table table-striped table-vcenter">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th width="130">Status</th>  
                                    <th>Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($recentApps->count() === 0)
                                    <tr>
                                        <td colspan="3">
                                            <p class="mb-0">No applications yet. <a href="mailto:sales@onthemove.com.au" class="text-underline">Get help</a> submitting your first app.</p>
                                        </td>
                                    </tr>
                                @else 
                                    @foreach ($recentApps as $app)
                                    <tr>
                                        <td>
                                            @if($app->flowbiz_application_id)
                                                <a href="{{ route('agent.application.show', ['application' => $app], false) }}" class="text-underline">{{ $app->flowbiz_application_id }}</a>
                                            @else
                                                <span class='text-muted'>N/A</span>
                                            @endif
                                        </td>
                                        <td>
                                            <span class="status-icon bg-{{ $app->statusColor }}"></span>
                                            <small>{{ $app->flowbiz_application_status ?? "Not submitted" }}</small>
                                        </td>
                                        <td><small>{{ $app->fullAddress }}</small></td>

                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
              
                </div>
        
            </div>
        </div>
    </div>

</div>

@endsection


@section('foot')


@endsection
