<div class="card">
  <div class="card-header">
    <h3 class="card-title">Select Products</h3>
  </div>
  <div class="card-body">
    @foreach($services as $category => $provider)
        <h4>{{ $category }}</h4>
        <div class="form-group">
            @foreach($provider as $name => $products)
                <div class="form-label">{{ $name }}</div>
                <div class="custom-controls-stacked">
                @foreach($products as $product)
                    <label class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name={{ strtolower($category)}} value="{{ $product->id }}"> 
                        <div class="custom-control-label">{{ $product->getOriginal('name') }}&ensp;<small class="text-muted">{{ $product->name }}</small></div>
                    </label>
                @endforeach
            </div>
             @endforeach
        </div>
    @endforeach
  </div>
</div>

