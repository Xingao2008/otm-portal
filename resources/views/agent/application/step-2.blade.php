
<h3 class="ml-5 mb-4 mt-6">Applicants</h3>
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Applicant </h3>
    <div class="card-options">
    <span class="tag tag-blue">Primary</span>
    <a href="#" class="ml-2 btn btn-outline-secondary btn-sm">Add another applicant</a>
  </div>
  </div>
  <div class="card-body">
   
    <div class="form-row">
            <div class="form-group col-3">
                <label class="form-label" for="title">Title</label>
                <select class="form-control" id="title" name="title">
                    <option value="">--Select--</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Ms">Ms</option>
                    <option value="Miss">Miss</option>
                    <option value="Dr">Dr</option>
                </select>
            </div>
            <div class="form-group col">

                <label class="form-label" for="firstname">First Name*</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="John">

            </div>
            <div class="form-group col">

                <label class="form-label" for="lastname">Last Name*</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Smith">

            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-7">
                <label class="form-label" for="email">Email Address*</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="john@example.com">
            </div>



            <div class="form-group col-5">
                <label class="form-label" for="mobile">Mobile*</label>
                <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="0412345678">
            </div>


        </div>

            <div class="form-row">
                <div class="form-group col-4">
                    <label for="dob">Date of Birth</label>
                    <input type="date" class="form-control" id="dob" name="dob" placeholder="DD/MM/YYY">
                </div>
                <div class="form-group col">
                    <label for="phone">Land Line</label>
                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="0312345678">
                </div>
                
            </div>

             <div class="form-group">
                    <label for="occupation">Occupation</label>
                    <input type="text" class="form-control" id="occupation" name="occupation" placeholder="Job">
            </div>



         <div class="btn-list text-center mt-4">
            <a href="#" class="btn btn-info"><i class="fa fa-address-card-o mr-2"></i>Add Identification</a>
            <a href="#" class="btn btn-info"><i class="fa fa-address-card-o mr-2"></i>Add Concession</a>
        </div>
  
            <div class="row row-cards row-deck mt-5">
       
                <div class="col">
                    <div class="card">
                        <div class="card-body d-flex align-content-between flex-wrap pb-2">
                            <div class="align-items-start">
                            <h5><i class="fa fa-address-card-o"></i>&ensp;Identification</h5>
                            <h6 class="mb-1">
                                Type
                                <small>Issuer</small>
                            </h6>
                        </div>
                            <div class="row">
                                <div class="col"><p class="font-monospace">Number</p></div>
                                <div class="col"><p>Expiry</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body d-flex align-content-between flex-wrap pb-2">
                            <div class="align-items-start">
                            <h5><i class="fa fa-address-card-o"></i>&ensp;Concession</h5>
                            <h6 class="mb-1">
                                Type
                                <small>Issuer</small>
                            </h6>
                        </div>
                            <div class="row">
                                <div class="col"><p class="font-monospace">Number</p></div>
                                <div class="col"><p>Expiry</p></div>
                            </div>
                        </div>
                    </div>
                </div>
               
              </div>

    
  </div>
</div>
