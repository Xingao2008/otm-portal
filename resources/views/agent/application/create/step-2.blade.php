@extends('layouts.default') 

@section('content')
<div class="container mt-4">

    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">

            <h3 class="ml-5 mb-4 mt-6">Application</h3>
            
            <otm-agent-app-two 
                data-route-complete="{{route('agent.application.step2.store.complete', $app, false) }}"
                data-route-continue="{{route('agent.application.step2.store', $app, false) }}"
                v-bind:data-app="{{ $app->toJson() }}"
            ></otm-agent-app-two>
                

    </div>
</div>

</div>
@endsection