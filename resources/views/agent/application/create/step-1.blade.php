@extends('layouts.default') 

@section('content')
<div class="container mt-4">

    <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">

            <h3 class="ml-5 mb-4 mt-6">New Application</h3>
            
            <otm-agent-app-one 
                v-bind:data-app-types="{{ $types->toJson() }}"
                v-bind:data-app="{{ ($app) ? $app->toJson() : '{}' }}"
            ></otm-agent-app-one>
                

    </div>
</div>

</div>
@endsection