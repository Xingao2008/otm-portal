@extends('layouts.default')

@section('content')

    <div class="container">
              
        <h1 class="mb-4 mt-6">New Application</h1>

        <otm-application
            v-bind:data-app-types="{{ $types->toJson() }}"
            v-bind:data-property-managers="{{ $pms->toJson() }}"
            v-bind:data-business-abn="'{{ $user->realEstateAgency->abn }}'"
            v-bind:data-business-name="'{!! $user->displayAgency !!}'"

        ></otm-application>

                    {{-- @include('agent.application.step-1')
                    @include('agent.application.step-2')
                    @include('agent.application.step-3') --}}
    </div>

@endsection