@extends('layouts.default')

@section('content')


<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="ml-5 mb-4 mt-6">Search applications</h1>
        </div>
    </div>

    <div class="row">
        
        <div class="col-md-6 col-lg-5 col-xl-4">
            <form action="{{ route('agent.application.search', [], false) }}" method="POST">
                @csrf
                <div class="card">

                @if ($errors->any())
                    <div class="card-alert alert alert-danger mb-0">
                        <ul class="list-unstyled mb-0">
                            @foreach ($errors->all() as $error)
                                <li><i class="fe fe-arrow-right mr-2"></i>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    

                    {{-- <div class="card-header">
                        <h3 class="card-title">Applications</h3>
                    </div> --}}
                    <div class="card-body">
                
                        <div class="form-group">
                            <label class="form-label" for="reference">Application reference</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="reference" name="Reference" placeholder="e.g. A0123456" value="{{ old('Reference') }}" data-hj-whitelist>
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><i class="fe fe-search mr-1"></i>Show application</button>
                                </span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="form-label" for="app_type">Application type</label>
                            <select class="form-control" id="app_type" name="ApplicationType">
                                <option value="All">All types</option>
                                <option value="Vendor">Vendor</option>
                                <option value="Renter">Renter</option>
                                <option value="Landlord">Landlord</option>
                                <option value="Purchaser">Purchaser</option>
                            </select>
                        </div>
                         <div class="form-row">
                             <otm-date-picker :name="'FromDate'" :label="'From date'" :column="true" :required="false" :placeholder="'YYYY / MM / DD'"></otm-date-picker>
                             
                             <otm-date-picker :name="'ToDate'" :label="'To date'" :column="true" :required="false" :placeholder="'YYYY / MM / DD'"></otm-date-picker>

                             
                            {{-- <div class="form-group col">
                                <label class="form-label" for="from_date">From Date</label>
                                <input type="text" class="form-control" id="from_date" name="FromDate" placeholder="2018-01-01">
                            </div> --}}
                            {{-- <div class="form-group col">
                                <label class="form-label" for="to_date">To Date</label>
                                <input type="text" class="form-control" id="to_date" name="ToDate" placeholder="2018-01-31"> --}}
                            {{-- </div> --}}
                        </div>
                        <div class="form-row">
                            <div class="form-group col @if(!$user->is_default_agent) mb-0 @endif">
                                <label class="form-label" for="firstname">Applicant first name</label>
                                <input type="text" class="form-control" id="firstname" name="Firstname" placeholder="John" value="{{ old('Firstname') }}">
                            </div>
                            <div class="form-group col  @if(!$user->is_default_agent) mb-0 @endif">
                                <label class="form-label" for="lastname">Applicant last name</label>
                                <input type="text" class="form-control" id="lastname" name="Lastname" placeholder="Smith" value="{{ old('Lastname') }}">
                            </div>
                        </div>
                        

                        @if($user->is_default_agent)
                        <div class="form-group mb-0">
                            <label class="form-label" for="view">Find applications from</label>
                            <select class="form-control" id="view" name="View">
                                <option value="PM">Just me</option>
                                <option value="AllPMs">All property managers</option>
                                <option value="REA">The real estate agency</option>
                            </select>
                        </div>
                        @else 
                        <input type="hidden" name="View" value="PM">
                        @endif
                        <input type="hidden" name="AgentId" value="{{ $user->agent_id }}">
                
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-block btn-primary"><i class="fe fe-search mr-1"></i>Search for applications</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6 col-lg-6 col-xl-8 d-md-flex">

            <hr class="d-md-none pb-4">
        
        @if($apps)
           
            @if($apps->count() !== 0)
            
            {{-- <h3 class="ml-5 mb-4 mt-6">Results</h1> --}}
               
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Search results</h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-outline table-vcenter card-table">
                        <thead>
                            <th>Reference</th>
                            <th>Address</th>
                            <th>Connection date</th>
                            @if($user->is_default_agent)
                            <th>Agent</th>
                            @endif
                        </thead>
                        <tbody>
                            @foreach($apps as $app)
                            <tr>
                                <td><a href="{{ route('agent.application.show', ['reference' => $app->Reference], false) }}" class="text-underline">{{ $app->Reference }}</a></td>
                                <td><small>{{ $app->Address }}</small></td>
                                <td>{{ $app->ConnectionDate->format('Y/m/d') }}</td>
                                @if($user->is_default_agent)
                                <td>{{ $app->agent->displayName }}</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if($apps->links())
                <div class="card-footer">
                    {{ $apps->links() }}
                </div>
                @endif
            </div>
            
            @else
            <div class="text-center d-flex flex-fill align-items-center justify-content-center">
                <div class="mb-6">
                    <h3 class="mb-1">No results</h3>
                    <p>Try expanding your search date range</p>
                </div>
            </div>
            @endif
        @else 
        <div class="text-center d-flex flex-fill align-items-center justify-content-center">
                <div class="mb-6">
                    <h3 class="text-muted">No results</h3>
                </div>
            </div>
        @endif
    
        </div><!-- /.col -->
    </div><!-- /.row -->

   
</div><!-- /.container -->

@endsection
