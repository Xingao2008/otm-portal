
<h3 class="ml-5 mb-4 mt-6">New Application</h3>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Details</h3>
    </div>
    <div class="card-body">

        <div class="form-row">
            <div class="form-group col">
                <label class="form-label">Application Type*</label>
                {{ Form::select('app_type', $types->toArray(), null, ['class' => 'form-control']) }}
            </div>


            <div class="form-group col">
                <label class="form-label">Applicant Type*</label>
                {{ Form::select('renter', ['renter' => 'Renter', 'owner' => 'Owner'], null, ['class' => 'form-control']) }}
            </div>
        </div>


        <div class="form-row">
            <div class="form-group col-8">
                <label class="form-label" for="mobile">Connection Address*</label>
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fe fe-search"></i>
                    </span>
                    <input type="text" class="form-control" id="address" name="address" placeholder="123 Some Street, Somewhere">
                </div>
            </div>

            <div class="form-group col-4">
                <label class="form-label">Connection Date*</label>
                <input type="date" class="form-control" name="connection_date">
            </div>

        </div>

        <hr class="mt-3 mb-4">

        <div class="form-row">
            <div class="form-group col-3">
                <label class="form-label" for="title">Title</label>
                <select class="form-control" id="title" name="title">
                    <option value="">--Select--</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Ms">Ms</option>
                    <option value="Miss">Miss</option>
                    <option value="Dr">Dr</option>
                </select>
            </div>
            <div class="form-group col">

                <label class="form-label" for="firstname">First Name*</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="John">

            </div>
            <div class="form-group col">

                <label class="form-label" for="lastname">Last Name*</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Smith">

            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-7">
                <label class="form-label" for="email">Email Address*</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="john@example.com">
            </div>



            <div class="form-group col-5">
                <label class="form-label" for="mobile">Mobile*</label>
                <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="0412345678">
            </div>


        </div>
      
        <div class="btn-list text-center mt-4">
            <a href="#" class="btn btn-secondary">Save, with Customer to Complete<i class="fe fe-corner-right-up ml-1"></i></a>
            <a href="#" class="btn btn-success">Save & Continue Application<i class="fe fe-arrow-right ml-1"></i></a>
        </div>

    </div>
</div>