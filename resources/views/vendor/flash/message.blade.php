@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <div class="alert mt-4
                    alert-{{ $message['level'] }}
                    {{ $message['important'] ? 'alert-important' : '' }}"
                    role="alert"
        >
            @if ($message['important'])
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                ></button>
            @endif
            @switch($message['level'])
                @case('primary')
                     <i class="fe fe-check mr-2" aria-hidden="true"></i>
                @break
                @case('success')
                     <i class="fe fe-thumbs-up mr-2" aria-hidden="true"></i>
                @break
                @case('danger')
                     <i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i>
                @break
                @case('warning')
                     <i class="fe fe-help-circle mr-2" aria-hidden="true"></i>
                @break
                @case('info')
                     <i class="fe fe-info mr-2" aria-hidden="true"></i>
                @break
            @endswitch
            {!! $message['message'] !!}
        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
