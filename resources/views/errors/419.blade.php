@extends('layouts.plain')

@section('content')
    <div class="page">
      <div class="page-content">
        <div class="container text-center">
          <div class="display-1 text-muted mb-5"><i class="si si-exclamation"></i> 419</div>
          <h1 class="h2 mb-3">Oops.. The page has expired due to inactivity.</h1>
          <p class="h4 text-muted font-weight-normal mb-7">Please refresh and try again.</p>
          <a class="btn btn-primary" href="javascript:location.reload()">
            <i class="fe fe-refresh-ccw mr-2"></i>Refresh
          </a>
        </div>
      </div>
    </div>

@endsection