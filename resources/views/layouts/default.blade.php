<!DOCTYPE html>
<html lang="en_AU" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('components.seo')

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="bearer" content="{!! $user->api_token !!}">
        <script>
            window.App = @json([
                'csrfToken' => csrf_token(),
                'user' => $user ?? null
            ]);
        </script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Roboto:300,400,400i,500,700,700i" rel="stylesheet" defer>
        @routes
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">    

        @yield('head')
        @include('layouts.partials.analytics_head')
        
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('foot')
       
    </head>
    <body>
        @include('layouts.partials.analytics_body')
        <div id="app" class="app-container">
            @include('layouts.partials.env')
            @include('layouts.partials.impersonation')
            @include('components.nav')
            <main class="app-content">
                <div class="container">
                    @include('flash::message')
                </div>
                
                <section class="mt-6">
                    @yield('content')
                </section>
            </main>
            @include('components.footer')

        </div>
    </body>
</html>
