<!DOCTYPE html>
<html lang="en_AU" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('components.seo')

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="bearer" content="{!! $user->api_token !!}">
        <script>
            window.App = @json([
                'csrfToken' => csrf_token(),
                'user' => $user ?? null
            ]);
        </script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Roboto:300,400,400i,500,700,700i" rel="stylesheet" defer>
        {{-- <link href="{{ mix('css/app.css') }}" rel="stylesheet"> --}}
        <link href="/css/app.css" rel="stylesheet">

        @routes

        @yield('head')
        @include('layouts.partials.analytics_head')
        <script src="{{ mix('js/app.js') }}"></script>
        <script async>
                (function(b,a,r,e,l,o,g){
                o=a.createElement(r),
                g=a.getElementsByTagName(r)[0];
                o.async=1;o.src=e;g.parentNode.insertBefore(o,g);
                o.onload=a.onreadystatechange=function(){
                a.readyState==="complete"&&Barelog(options);}
                })(window,document,'script','https://cdn.barelog.com/widget.js');

                var options = {
                id: "l5vnn8oab8yt",
                trigger: "#barelog-trigger",
                type: "modal",
                width: "640px",
                height: "450px",
                countStyle: "small"
                }
        </script>
        @yield('foot')
        
    </head>
    <body>
        @include('layouts.partials.analytics_body')
        <div id="app" class="app-container">
            @include('layouts.partials.env')
            @include('components.admin.nav')
            {{-- @include('components.admin.menu') --}}
            
            <main class="app-content">
                <div class="container">
                    @include('flash::message')
                </div>
                <section class="mt-6">
                @yield('content')
                </section>
            </main>
        
            @include('components.footer')

        </div>
    </body>
</html>
