@if (session('impersonation'))
<div class="d-flex justify-content-between bg-primary p-2 ">
<div><h6 class="text-white m-0"><span class="text-400">Impersonating: </span><span class="text-700">{{ session('impersonation.agent.name') }}</span><span class="text-400"> from {{ session('impersonation.agent.agency') }}</span></h6></div>

<div><h6 class="m-0 p-0"><a class="text-underline text-white m-0 p-0" href="{{ route('admin.pm.impersonate.exit', [], false) }}" data-turbolinks="false">Exit and return</a></h6></div>
</div>
@endif