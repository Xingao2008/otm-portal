@if(env('APP_ENV') !== 'production')
<div class="d-flex justify-content-between bg-danger p-2">
    <div><h6 class="text-white m-0"><span class="text-400">Application Environment: </span><span class="text-700">{{ title_case(env('APP_ENV')) }}</span></h6></div>
    <div><h6 class="text-white m-0"><span class="text-400">Flowbiz Environment: </span><span class="text-700">{{ title_case(env('FLOWBIZ_ENV')) }}</span></h6></div>
</div>
@endif