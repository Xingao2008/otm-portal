<!DOCTYPE html>
<html lang="en_AU" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('components.seo')

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Roboto:300,400,400i,500,700,700i" rel="stylesheet" defer>
        
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
        @yield('head')
        @include('layouts.partials.analytics_head')
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('foot')
        
    </head>
    <body>
        @include('layouts.partials.analytics_body')
        <div id="app" class="page">
            @include('layouts.partials.env')
            <div class="container">
                @include('flash::message')
            </div>

            @yield('content')
        </div>
    </body>
</html>
