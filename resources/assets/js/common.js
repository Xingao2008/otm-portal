const UiField = {

    props: {
        value: [String, Number],
        label: String,
    },

    data() {
        // keep track of user input
        return {
            input: this.value
        }
    },

    computed: {
        // output allows input to be processed before sending out
        output() {
            return this.input
        }
    },

    watch: {
        // watch allows prop value to be checked before updating
        value(value) {
            if (value !== this.input) {
                this.input = value
            }
        }
    },

    methods: {
        onInput() {
            this.$emit('input', this.output)
        }
    }
}

const UiOptions = {
    props: {
        options: [Array, String]
    },

    computed: {
        // computed _options allows you to sanitise props input
        _options() {
            // parse strings into an array of label:value pairs
            return typeof this.options === 'string' ?
                this.options
                .split('|')
                .map(pair => pair.split(':'))
                .map(([label, value]) => ({
                    label,
                    value
                })) :
                this.options
        }
    }
}

export {
    UiField,
    UiOptions
};