    var Ziggy = {
        namedRoutes: {"api.v1.application.store":{"uri":"api\/v1\/application","methods":["POST"],"domain":null},"api.v1.agent.points.balance":{"uri":"api\/v1\/agent\/points","methods":["GET","HEAD"],"domain":null},"api.v1.agent.points.history":{"uri":"api\/v1\/agent\/points\/history","methods":["GET","HEAD"],"domain":null},"api.v1.agent.get":{"uri":"api\/v1\/agent","methods":["GET","HEAD"],"domain":null},"rewardsportal":{"uri":"rewards","methods":["GET","HEAD"],"domain":null},"agent.application.create":{"uri":"application\/create","methods":["GET","HEAD"],"domain":null},"agent.application.show":{"uri":"application\/{application}","methods":["GET","HEAD"],"domain":null}},
        baseUrl: 'https://rewards.onthemove.com.au/',
        baseProtocol: 'https',
        baseDomain: 'rewards.onthemove.com.au',
        basePort: false,
        defaultParameters: []
    };

    export {
        Ziggy
    }
