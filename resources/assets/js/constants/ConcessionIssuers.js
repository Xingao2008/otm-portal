export const CONCESSION_ISSERS = [
    {abbreviation: "CTLK", name: "Centrelink"},
    {abbreviation: "DVA", name: "Department of Veterans' Affairs"},
    {abbreviation: "QDC", name: "Queensland Department of Communities, Child Safety and Disability Services"},
    {abbreviation: "DIBP", name: "Department of Immigration and Border Protection"},
];