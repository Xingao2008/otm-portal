export const STATES = [
        {abbreviation:"VIC", name:"Victoria"},
        {abbreviation:"NSW", name:"New South Wales"},
        {abbreviation:"QLD", name:"Queensland"},
        {abbreviation:"ACT", name:"Australian Capital Territory"},
        {abbreviation:"NT", name:"Northern Territory"},
        {abbreviation:"WA", name:"Western Australia"},
        {abbreviation:"SA", name:"South Australia"},
        {abbreviation:"TAS", name:"Tasmania"},
];