export const CONCESSION_CARDS = [
    {abbreviation: "HCC", name: "Health Care Card"},
    {abbreviation: "PCC", name: "Centrelink Pensioner Concession Card"},
    {abbreviation: "CDHCC", name: "Health Care Card Carer - Child Under 16"},
    {abbreviation: "DVA_DIS", name: "DVA Gold Card - Disability Pension"},
    {abbreviation: "DVAG_TPI", name: "DVA Gold Card TPI Only"},
    {abbreviation: "DVAGC", name: "Dept of Veteran Affairs Gold Card"},
    {abbreviation: "DVAGC_RHC", name: "DVA Gold Card Repatriation Health Card"},
    {abbreviation: "DVAGC_WW", name: "DVA Gold Card War Widow"},
    {abbreviation: "DVPC", name: "DVA Pension Concession Card"},
    {abbreviation: "QGSC", name: "Queensland Government Seniors Card"},
    {abbreviation: "SAHCC", name: "Health Care Card Sickness Allowance (SA)"},
    {abbreviation: "SPHCC", name: "Health Care Card Special Benefit (SP)"},
];