// import {
//     identification,
//     concession
// } from './identificantions.js';



// initial state
const state = {
    application_type_id: "",
    renter: "",
    connection_date: "",
    address: {
        full: "",
        moniker: "",
        split: {
            property_type: "",
            unit_number: "",
            street_number: "",
            street_name: "",
            street_type: "",
            suburb: "",
            postcode: "",
            state: "",
            dpid: ""
        },
    },
};

// getters
const getters = {
    //allApplicants: state => state.applicants
};

// actions
const actions = {
    // getAllProducts({ commit }) {
    //     shop.getProducts(applicants => {
    //         commit('setProducts', applicants)
    //     })
    // }
};

// mutations
const mutations = {
    // addSecondaryAppliant(state) {
    //     let secondary = state.applicant;
    //     secondary.primary = false;
    //     state.applicants.push(secondary);
    // }

    // setProducts(state, applicants) {
    //     state.all = applicants
    // },

    // decrementProductInventory(state, { id }) {
    //     const product = state.all.find(product => product.id === id)
    //     product.inventory--
    // }
};

export default {
    state,
    getters,
    actions,
    mutations
};