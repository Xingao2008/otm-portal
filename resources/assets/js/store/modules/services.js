class Power {

    constructor() {
        this.originalData = {
            product_id: 0,
            connection_date: "",
            details: {
                nmi: "",
                connection_options: "",
                appointment_time: "",
                fee_acceptance: "",
                additional_access_notes: "",
                meter_outside: false,
                hazards: false,
                dog_on_premises: true,
                access_issues: "",
                marketing_opt_out: false,
                life_support: "",
                power_on: true,
                remote_safety_confirmation: false
            }
        };

        for (let field in data) {
            this[field] = data[field];
        }

    }

    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }
}

export default {
    Power
};