// import {
//     identification,
//     concession
// } from './identificantions.js';



// initial state
const state = {
    applicants: [],
    applicant: {
        primary: true,
        title: "",
        firstname: "",
        lastname: "",
        dob: "",
        email: "",
        mobile: "",
        phone: "",
        occupation: "",
        notes: "",
        id: [{
            type: "",
            number: "",
            expiry: "",
            issuer: ""
        }],
        concession: {
            type: "",
            number: "",
            expiry: "",
            issuer: ""
        }
    }
};

// getters
const getters = {
    allApplicants: state => state.applicants
};

// actions
const actions = {
    // getAllProducts({ commit }) {
    //     shop.getProducts(applicants => {
    //         commit('setProducts', applicants)
    //     })
    // }
};

// mutations
const mutations = {
    addSecondaryAppliant(state) {
        let secondary = state.applicant;
        secondary.primary = false;
        state.applicants.push(secondary);
    }

    // setProducts(state, applicants) {
    //     state.all = applicants
    // },

    // decrementProductInventory(state, { id }) {
    //     const product = state.all.find(product => product.id === id)
    //     product.inventory--
    // }
};

export default {
    state,
    getters,
    actions,
    mutations
};