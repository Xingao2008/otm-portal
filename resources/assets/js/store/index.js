import Vue from 'vue';
import Vuex from 'vuex';
import {
    getField,
    updateField
} from 'vuex-map-fields';
//import applicants from './modules/applicants';
// import { state, mutations } from './mutations'
// import plugins from './plugins'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    strict: debug,

    // modules: {
    //     applicants
    // },

    state: {
        application: {
            details: {
                app_type: '',
                renter: '',
                connection_date: '',
            }
        }
    },

    getters: {
    //     googleMapsImage() {
    //         let addr = this.$store.state;
    //         let address = `${addr.street_number} ${addr.street_name} ${addr.street_type}, ${addr.suburb} ${addr.postcode} ${addr.state}`;
    //         if (addr.unit_number) {
    //             address = `${addr.property_type} ${addr.unit_number},  ${address}`;
    //         }
    //         return encodeURIComponent(address);
    //     }
        getField
    },
    
    mutations: {
        // increment(state) {
        //     state.count++;
        // }
        updateField
    }
});




// {
//     "app_type": 2,
//     "renter": true,
//     "connection_date": "04/11/2019",
//     "notes": "Test",
//     "property_type": "Unit",
//     "unit_number": "1",
//     "street_number": "63",
//     "street_name": "Cambridge",
//     "street_type": "St",
//     "suburb": "Collingwood",
//     "postcode": "3066",
//     "state": "VIC",
//     "applicants": [{
//             "primary": true,
//             "title": "Mr",
//             "firstname": "John",
//             "lastname": "McClane",
//             "dob": "01/01/1979",
//             "email": "xin.gao.au@gmail.com",
//             "mobile": "0400301235",
//             "phone": "",
//             "occupation": "NYPD Officer",
//             "notes": "",
//             "id": [{
//                 "type": "Australian Drivers Licence",
//                 "number": "123123123",
//                 "expiry": "05/11/2020",
//                 "issuer": "Victoria"
//             }, {
//                 "type": "Medicare Card",
//                 "number": "123123123",
//                 "expiry": "11/02/2024",
//                 "issuer": "Australia"
//             }],
//             "concession": {
//                 "type": "HCC",
//                 "number": "1231231",
//                 "expiry": "22/05/2018",
//                 "issuer": "DHS"
//             }
//         },
//         {
//             "primary": false,
//             "title": "Mrs",
//             "firstname": "Mia",
//             "lastname": "Wallance",
//             "dob": "03/04/1977",
//             "email": "mia@wallace.com",
//             "mobile": "",
//             "phone": "0312345678",
//             "occupation": "Actor",
//             "notes": "",
//             "id": []
//         }
//     ],
//     "products": [{
//             "product_id": 113445,
//             "connection_date": "05/05/2018",
//             "details": {
//                 "nmi": "64901660264",
//                 "connection_options": "Standard",
//                 "appointment_time": "",
//                 "fee_acceptance": "",
//                 "additional_access_notes": "will you, won't you.",
//                 "meter_outside": false,
//                 "hazards": false,
//                 "dog_on_premises": true,
//                 "access_issues": "",
//                 "marketing_opt_out": false,
//                 "life_support": "",
//                 "power_on": true,
//                 "remote_safety_confirmation": false
//             }
//         },
//         {
//             "product_id": 170458,
//             "connection_date": "05/05/2018",
//             "details": {
//                 "mirn": "50889179907",
//                 "additional_access_notes": "however, the.",
//                 "meter_outside": false,
//                 "hazards": true,
//                 "dog_on_premises": false,
//                 "access_issues": "off--' 'nonsense!' said.",
//                 "marketing_opt_out": true
//             }
//         },
//         {
//             "product_id": 2408,
//             "connection_date": "05/05/2018",
//             "details": {
//                 "requirements": "Alice; 'all I know who.",
//                 "water_required": true,
//                 "water_only_reason": "Not Serviceable"
//             }
//         },
//         {
//             "product_id": 2409,
//             "connection_date": "05/05/2018",
//             "details": {
//                 "insurance_required": true
//             }
//         },
//         {
//             "product_id": 114484,
//             "connection_date": "05/05/2018",
//             "details": {
//                 "existing_customer": false,
//                 "notes": "The Mouse gave a sudden.",
//                 "employer": "Johnson, Steuber and Kertzmann",
//                 "employer_phone": "8705 4988",
//                 "current_employment_duration": 11,
//                 "previous_employment_duration": 45,
//                 "residential": true,
//                 "property_type": "",
//                 "street_number": "3",
//                 "street_name": "Chauncey Colonnade",
//                 "unit_number": "0",
//                 "suburb": "Jerdeland",
//                 "postcode": "2994",
//                 "state": "NSW",
//                 "dpid": ""
//             }
//         }
//     ]
// }