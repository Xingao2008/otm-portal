/****************
 * IMPORTS
 ****************/

// Initialize Vue before we start the BugsnagVue plugin

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


/**
 * Local Imports
 */
require('./bootstrap');
require('./common');
import bugsnagClient from './bugsnag';
import { Ziggy } from "./ziggy";
import route from "../../../vendor/tightenco/ziggy/src/js/route.js";

/**
 * External Imports
 */
import Turbolinks from "turbolinks";
import TurbolinksAdapter from "vue-turbolinks";
Turbolinks.start();

import Vue from "vue";
// import Vuex from "vuex";
import VeeValidate from "vee-validate";
import bugsnagVue from "bugsnag-vue";
import Tooltip from "vue-directive-tooltip";
import Vue2Filters from "vue2-filters";
import Vue2Storage from "vue2-storage";
import Transitions from 'vue2-transitions';
//import Rollbar from 'vue-rollbar';


/****************
 * USE
 ****************/

// Vue.use(Vuex);
Vue.use(VeeValidate, {
  aria: true,
  classNames: {},
  classes: false,
  delay: 300,
  events: 'blur|on-close',
});
Vue.use(TurbolinksAdapter);
Vue.use(Tooltip);
Vue.use(Vue2Filters);
Vue.use(Vue2Storage, {
  prefix: 'otm_',
  driver: 'local',
  ttl: 60 * 60 * 24 * 1000 // 24 hours
});
Vue.use(Transitions);
// Vue.use(Rollbar, {
//   accessToken: 'b04994f3e3814d7d944548ebd41e1c3c',
//   captureUncaught: true,
//   captureUnhandledRejections: true,
//   enabled: true,
//   source_map_enabled: true,
//   environment: 'production',
//   payload: {
//     client: {
//       javascript: {
//         code_version: '1.0'
//       }
//     }
//   }
// });





/****************
 * MIXINS
 ****************/

Vue.mixin({
  methods: {
    route: (name, params, absolute) => route(name, params, absolute, Ziggy) // ziggy routes
  }
});







/****************
 * COMPONENTS
 ****************/

//import store from './store/index.js'

/**
 * App Submission Components
 */
Vue.component('otm-application', require('./components/ApplicationComponent.vue'));
  Vue.component('otm-application-details', require('./components/Application/DetailsComponent.vue'));
  Vue.component('otm-application-applicant', require('./components/Application/ApplicantComponent.vue'));
    Vue.component('otm-applicant-id', require('./components/Application/ApplicantIdComponent.vue'));


/**
 * Generic Components
 */
Vue.component('otm-input-email', require('./components/partials/EmailAddressComponent.vue'));
Vue.component('otm-input-mobile', require('./components/partials/MobileComponent.vue'));
Vue.component('otm-date-picker', require('./components/partials/DatePickerComponent.vue'));

// Vue.component('otm-chart', require('./components/Heatmap.vue'));


/**
 * Agent Components
 */
Vue.component('otm-agent-nav-points', require('./components/agent/Points.vue'));
Vue.component('otm-agent-points-history', require('./components/agent/PointsHistory.vue'));


/**
 * Admin Components
 */
Vue.component('otm-pm-matcher', require('./components/admin/PropertyManagerMatcherComponent.vue'));
Vue.component('otm-admin-chart', require('./components/admin/DashboardChart.vue'));




/****************
 * ROOT EL
 ****************/

document.addEventListener('turbolinks:load', (event) => {
  let app = new Vue({
    //store,
    el: "#app",
  });
  bugsnagClient.use(bugsnagVue(Vue));
  /**
   * Google Analytics
   */
  let url = event.data.url;
  dataLayer.push({
    'event': 'pageView',
    'virtualUrl': url
  });
  
  /**
   * Hotjar
   */
  window.hj=window.hj||function(){(hj.q=hj.q||[]).push(arguments)};
  try {
    hj("stateChange", url);
  } catch (error) {
    //console.error(error);
  }

  /**
   * Bugsnag
   */
  try {
    bugsnagClient.user = window.App.user
  } catch (error) {
    //console.error(error);
  }
});
